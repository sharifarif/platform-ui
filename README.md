# Install

## package.json
Add the `platform-ui` repository to your
[devDependencies](https://docs.npmjs.com/files/package.json#devdependencies).
```json
{
    "devDependencies": {
        "platform-ui": "https://platform-ui-read:oX6z4p3vnqu@bitbucket.org/scagroup/platform-ui.git#master"
    }
}
```