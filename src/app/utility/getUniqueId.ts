var uniqueId = 0;

/**
 * Create a unique ID.
 *
 * If you provide an `element` that already has an `id`, that `id` will be returned.
 *
 * If you provide an `element` without an `id`,
 * a unique `id` will be given to the `element` and that `id` will be returned.
 */
export default function getUniqueId(element?: HTMLElement, prefix = 'UniqueId') {
    var id: string | number;

    if (element) {
        id = element.id;
    }

    if (!id) {
        id = ++uniqueId;
        if (prefix) {
            id = prefix + id;
        }

        if (element) {
            element.id = '' + id;
        }
    }

    return id;
}
