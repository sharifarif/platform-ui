"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var uniqueId = 0;
/**
 * Create a unique ID.
 *
 * If you provide an `element` that already has an `id`, that `id` will be returned.
 *
 * If you provide an `element` without an `id`,
 * a unique `id` will be given to the `element` and that `id` will be returned.
 */
function getUniqueId(element, prefix = 'UniqueId') {
    var id;
    if (element) {
        id = element.id;
    }
    if (!id) {
        id = ++uniqueId;
        if (prefix) {
            id = prefix + id;
        }
        if (element) {
            element.id = '' + id;
        }
    }
    return id;
}
exports.default = getUniqueId;
