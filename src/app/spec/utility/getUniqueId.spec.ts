import getUniqueId from '../../utility/getUniqueId';

describe('getUniqueId()', () => {
    it('noElement', () => {
        var uniqueId1 = getUniqueId(),
            uniqueId2 = getUniqueId();

        expect((uniqueId1 as string).startsWith('UniqueId')).toBe(true);
        expect((uniqueId2 as string).startsWith('UniqueId')).toBe(true);

        expect(uniqueId1).not.toBe('UniqueId');
        expect(uniqueId2).not.toBe('UniqueId');
        expect(uniqueId1).not.toBe(uniqueId2);
    });

    it('noElement prefix', () => {
        var uniqueId1 = getUniqueId(undefined, 'awesome'),
            uniqueId2 = getUniqueId(null, 'awesome');

        expect((uniqueId1 as string).startsWith('awesome')).toBe(true);
        expect((uniqueId2 as string).startsWith('awesome')).toBe(true);

        expect(uniqueId1).not.toBe('awesome');
        expect(uniqueId2).not.toBe('awesome');
        expect(uniqueId1).not.toBe(uniqueId2);
    });

    it('noElement blankPrefix', () => {
        var uniqueId1 = getUniqueId(undefined, null),
            uniqueId2 = getUniqueId(null, '');

        expect(typeof uniqueId1).toBe('number');
        expect(typeof uniqueId2).toBe('number');

        expect(uniqueId1).not.toBe(uniqueId2);
    });

    it('element', () => {
        var element1 = document.createElement('div'),
            element2 = document.createElement('div'),
            uniqueId1 = getUniqueId(element1),
            uniqueId2 = getUniqueId(element2);

        expect(element1.id).toBe(uniqueId1 as string);
        expect(element2.id).toBe(uniqueId2 as string);

        expect((uniqueId1 as string).startsWith('UniqueId')).toBe(true);
        expect((uniqueId2 as string).startsWith('UniqueId')).toBe(true);

        expect(uniqueId1).not.toBe('UniqueId');
        expect(uniqueId2).not.toBe('UniqueId');
        expect(uniqueId1).not.toBe(uniqueId2);
    });

    it('element withId', () => {
        var element = document.createElement('div');
        element.id = 'awesome';

        var uniqueId = getUniqueId(element);

        expect(element.id).toBe('awesome');
        expect(uniqueId).toBe('awesome');
    });

    it('element prefix', () => {
        var element1 = document.createElement('div'),
            element2 = document.createElement('div'),
            uniqueId1 = getUniqueId(element1, 'awesome'),
            uniqueId2 = getUniqueId(element2, 'awesome');

        expect(element1.id).toBe(uniqueId1 as string);
        expect(element2.id).toBe(uniqueId2 as string);

        expect((uniqueId1 as string).startsWith('awesome')).toBe(true);
        expect((uniqueId2 as string).startsWith('awesome')).toBe(true);

        expect(uniqueId1).not.toBe('awesome');
        expect(uniqueId2).not.toBe('awesome');
        expect(uniqueId1).not.toBe(uniqueId2);
    });
});
