import BuyNowLink from '../../../buy-now/provider/BuyNowLink';

describe('BuyNowLink', () => {
    it('bindHtml select', () => {
        var element = $(`
            <div>
                <select class="BuyNow-select">
                    <option value="https://www.amazon.com/">Amazon</option>
                    <option value="https://www.target.com/" selected>Target</option>
                    <option value="https://www.walmart.com/">Walmart</option>
                </select>

                <a class="BuyNow-button"></a>
            </div>
        `);

        new BuyNowLink().bindHtml(element);

        expect(element.find('.BuyNow-button').attr('href')).toBe(
            'https://www.target.com/'
        );
    });

    it('bindHtml select changeInput', () => {
        var element = $(`
            <div>
                <select class="BuyNow-select">
                    <option value="https://www.amazon.com/">Amazon</option>
                    <option value="https://www.target.com/" selected>Target</option>
                    <option value="https://www.walmart.com/">Walmart</option>
                </select>

                <a class="BuyNow-button"></a>
            </div>
        `);

        new BuyNowLink().bindHtml(element);

        element
            .find('.BuyNow-select')
            .val('https://www.amazon.com/')
            .trigger('change');

        expect(element.find('.BuyNow-button').attr('href')).toBe(
            'https://www.amazon.com/'
        );

        element
            .find('.BuyNow-select')
            .val('https://www.target.com/')
            .trigger('input');

        expect(element.find('.BuyNow-button').attr('href')).toBe(
            'https://www.target.com/'
        );
    });

    it('bindHtml select noOptions', () => {
        var element = $(`
            <div>
                <select class="BuyNow-select">
                </select>

                <a href="https://www.amazon.com/" class="BuyNow-button"></a>
            </div>
        `);

        new BuyNowLink().bindHtml(element);

        expect(element.find('.BuyNow-button').attr('href')).toBe(
            'https://www.amazon.com/'
        );
    });

    it('bindHtml valueFromElement', () => {
        var element = $(`
            <a class="BuyNow-button" data-buy-now-value="https://www.amazon.com/"></a>
        `);

        new BuyNowLink().bindHtml(element);

        expect(element.attr('href')).toBe('https://www.amazon.com/');
    });

    it('bindHtml valueFromDescendant', () => {
        var element = $(`
            <div>
                <div>
                    <span data-buy-now-value="https://www.amazon.com/"></span>
                    <a class="BuyNow-button"></a>
                </div>
            </div>
        `);

        new BuyNowLink().bindHtml(element);

        expect(element.find('.BuyNow-button').attr('href')).toBe(
            'https://www.amazon.com/'
        );
    });

    it('bindHtml noValue', () => {
        var element = $(`
            <a href="https://www.amazon.com/" class="BuyNow-button"></a>
        `);

        new BuyNowLink().bindHtml(element);

        expect(element.attr('href')).toBe('https://www.amazon.com/');
    });
});
