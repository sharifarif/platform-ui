import Clic2Buy from '../../../buy-now/provider/Clic2Buy';
import BuyNowLink from '../../../buy-now/provider/BuyNowLink';
import BuyNowProviderFactory from '../../../buy-now/provider/BuyNowProviderFactory';

describe('BuyNowProviderFactory', () => {
    it('forElement Clic2Buy', () => {
        var element = $('<div data-buy-now-provider="clic2buy"></div>'),
            provider = BuyNowProviderFactory.forElement(element);

        expect(provider instanceof Clic2Buy).toBe(true);
    });

    it('forElement Link', () => {
        var element = $('<div data-buy-now-provider="link"></div>'),
            provider = BuyNowProviderFactory.forElement(element);

        expect(provider instanceof BuyNowLink).toBe(true);
    });

    it('forElement caseAgnostic', () => {
        var element = $('<div data-buy-now-provider="Clic2Buy"></div>'),
            provider = BuyNowProviderFactory.forElement(element);

        expect(provider instanceof Clic2Buy).toBe(true);
    });

    it('forElement otherName', () => {
        var element = $('<div data-buy-now-provider="other"></div>'),
            provider = BuyNowProviderFactory.forElement(element);

        expect(provider).toBeUndefined();
    });

    it('forElement noName', () => {
        var element = $('<div></div>'),
            provider = BuyNowProviderFactory.forElement(element);

        expect(provider).toBeUndefined();
    });

    it('getProviderName', () => {
        var element = $('<div data-buy-now-provider="clic2buy"></div>');

        expect(BuyNowProviderFactory.getProviderName(element)).toBe('clic2buy');
    });

    it('getProviderName fromChild', () => {
        var element = $('<div><button data-buy-now-provider="clic2buy"></button></div>');

        expect(BuyNowProviderFactory.getProviderName(element)).toBe('clic2buy');
    });

    it('getProviderName fromDescendant', () => {
        var element = $(
            '<div><span><button data-buy-now-provider="clic2buy">' +
                '</button></span></div>'
        );

        expect(BuyNowProviderFactory.getProviderName(element)).toBe('clic2buy');
    });

    it('getProviderName noName', () => {
        var element = $('<div></div>');

        expect(BuyNowProviderFactory.getProviderName(element)).toBeUndefined();
    });
});
