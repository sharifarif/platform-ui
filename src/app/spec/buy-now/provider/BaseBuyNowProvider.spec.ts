import { BaseBuyNowProvider } from '../../../buy-now/provider/BaseBuyNowProvider';
import MockProvider from './MockProvider';

describe('BaseBuyNowProvider', () => {
    it('getButton same', () => {
        var element = $('<a class="BuyNow-button"></a>');

        expect(BaseBuyNowProvider.getButton(element).length).toBe(1);
        expect(BaseBuyNowProvider.getButton(element)[0]).toBe(element[0]);
    });

    it('getButton descendant', () => {
        var button = $('<a class="BuyNow-button"></a>'),
            inbetween = $('<div></div>').append(button),
            element = $('<div></div>').append(inbetween);

        expect(BaseBuyNowProvider.getButton(element).length).toBe(1);
        expect(BaseBuyNowProvider.getButton(element)[0]).toBe(button[0]);
    });

    it('getSelect', () => {
        var select = $('<select class="BuyNow-select"></select>'),
            inbetween = $('<div></div>').append(select),
            element = $('<div></div>').append(inbetween);

        expect(BaseBuyNowProvider.getSelect(element).length).toBe(1);
        expect(BaseBuyNowProvider.getSelect(element)[0]).toBe(select[0]);
    });

    it('getValue select', () => {
        var element = $(`
            <div>
                <select class="BuyNow-select">
                    <option>awesome</option>
                    <option>sauce</option>
                    <option>not awesome</option>
                </select>
            </div>
        `);

        expect(BaseBuyNowProvider.getValue(element)).toBe('awesome');
    });

    it('getValue select selected', () => {
        var element = $(`
            <div>
                <select class="BuyNow-select">
                    <option>awesome</option>
                    <option selected>sauce</option>
                    <option>not awesome</option>
                </select>
            </div>
        `);

        expect(BaseBuyNowProvider.getValue(element)).toBe('sauce');
    });

    it('getValue select noOptions', () => {
        var element = $(`
            <div>
                <select class="BuyNow-select">
                </select>
            </div>
        `);

        expect(BaseBuyNowProvider.getValue(element)).toBeNull();
    });

    it('getValue fromElement', () => {
        var element = $(`
            <div data-buy-now-value="awesome"></div>
        `);

        expect(BaseBuyNowProvider.getValue(element)).toBe('awesome');
    });

    it('getValue fromDescendant', () => {
        var element = $(`
            <div>
                <div>
                    <div data-buy-now-value="awesome"></div>
                </div>
            </div>
        `);

        expect(BaseBuyNowProvider.getValue(element)).toBe('awesome');
    });

    it('getValue noValue', () => {
        var element = $(`
            <div></div>
        `);

        expect(BaseBuyNowProvider.getValue(element)).toBeUndefined();
    });

    it('bindHtml select', () => {
        var element = $(`
            <div>
                <select class="BuyNow-select">
                    <option>awesome</option>
                    <option selected>sauce</option>
                    <option>not awesome</option>
                </select>
            </div>
        `);

        var provider = new MockProvider();

        spyOn(provider, 'bindValue');

        provider.bindHtml(element);

        expect(provider.bindValue).toHaveBeenCalledWith(element, 'sauce');
    });

    it('bindHtml select changeInput', () => {
        var element = $(`
            <div>
                <select class="BuyNow-select">
                    <option>awesome</option>
                    <option selected>sauce</option>
                    <option>not awesome</option>
                </select>
            </div>
        `);

        var provider = new MockProvider();

        provider.bindHtml(element);

        spyOn(provider, 'bindValue');

        element
            .find('.BuyNow-select')
            .val('awesome')
            .trigger('change');

        expect(provider.bindValue).toHaveBeenCalledWith(element, 'awesome');

        element
            .find('.BuyNow-select')
            .val('sauce')
            .trigger('input');

        expect(provider.bindValue).toHaveBeenCalledWith(element, 'sauce');
    });

    it('bindHtml select noOptions', () => {
        var element = $(`
            <div>
                <select class="BuyNow-select">
                </select>
            </div>
        `);

        var provider = new MockProvider();

        spyOn(provider, 'bindValue');

        provider.bindHtml(element);

        expect(provider.bindValue).not.toHaveBeenCalled();

        element
            .find('.BuyNow-select')
            .trigger('change')
            .trigger('input');

        expect(provider.bindValue).not.toHaveBeenCalled();
    });

    it('bindHtml valueFromElement', () => {
        var element = $(`
            <div data-buy-now-value="awesome"></div>
        `);

        var provider = new MockProvider();

        spyOn(provider, 'bindValue');

        provider.bindHtml(element);

        expect(provider.bindValue).toHaveBeenCalledWith(element, 'awesome');
    });

    it('bindHtml valueFromDescendant', () => {
        var element = $(`
            <div>
                <div>
                    <div data-buy-now-value="awesome"></div>
                </div>
            </div>
        `);

        var provider = new MockProvider();

        spyOn(provider, 'bindValue');

        provider.bindHtml(element);

        expect(provider.bindValue).toHaveBeenCalledWith(element, 'awesome');
    });

    it('bindHtml noValue', () => {
        var element = $(`
            <div></div>
        `);

        var provider = new MockProvider();

        spyOn(provider, 'bindValue');

        provider.bindHtml(element);

        expect(provider.bindValue).not.toHaveBeenCalled();
    });
});
