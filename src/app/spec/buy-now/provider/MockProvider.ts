import { BaseBuyNowProvider } from '../../../buy-now/provider/BaseBuyNowProvider';

/**
 * Mock {@link BaseBuyNowProvider} for unit testing.
 */
export default class MockProvider extends BaseBuyNowProvider {
    bindValue(element: JQuery, value: string) {
        // Do nothing.
    }

    getScriptUrls(): string[] {
        return [];
    }
}
