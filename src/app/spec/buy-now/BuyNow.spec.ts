import BuyNow from '../../buy-now/BuyNow';
import Clic2Buy from '../../buy-now/provider/Clic2Buy';
import BuyNowLink from '../../buy-now/provider/BuyNowLink';

describe('BuyNow', () => {
    it('constructor Clic2Buy', () => {
        spyOn(Clic2Buy.prototype, 'loadScripts');

        var element = $('<div data-buy-now-provider="clic2buy"></div>'),
            buyNow = new BuyNow(element);

        expect(buyNow.provider instanceof Clic2Buy).toBe(true);
        expect(Clic2Buy.prototype.loadScripts).toHaveBeenCalled();
    });

    it('constructor Link', () => {
        var element = $('<div data-buy-now-provider="link"></div>'),
            buyNow = new BuyNow(element);

        expect(buyNow.provider instanceof BuyNowLink).toBe(true);
    });
});
