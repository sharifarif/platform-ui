import dataLayerPush from '../../analytics/dataLayerPush';

describe('dataLayerPush()', () => {
    afterEach(() => {
        delete (window as any).dataLayer;
    });

    it('window.dataLayer', () => {
        (window as any).dataLayer = [
            {
                event: 'Awesome'
            }
        ];

        dataLayerPush({
            event: 'Sauce'
        });

        expect((window as any).dataLayer).toEqual([
            {
                event: 'Awesome'
            },
            {
                event: 'Sauce'
            }
        ]);
    });

    it('no window.dataLayer', () => {
        delete (window as any).dataLayer;

        dataLayerPush({
            event: 'Awesome'
        });

        expect((window as any).dataLayer).toEqual([
            {
                event: 'Awesome'
            }
        ]);

        dataLayerPush({
            event: 'Sauce'
        });

        expect((window as any).dataLayer).toEqual([
            {
                event: 'Awesome'
            },
            {
                event: 'Sauce'
            }
        ]);
    });
});
