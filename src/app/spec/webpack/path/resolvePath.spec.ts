import resolvePath from '../../../webpack/path/resolvePath';

describe('resolvePath()', () => {
    it('child=string', () => {
        expect(resolvePath('/parent', 'child')).toBe('/parent/child');
    });

    it('child=string,absolute', () => {
        expect(resolvePath('/parent', '/absolute/child')).toBe('/absolute/child');
    });

    it('child=string parent=blank', () => {
        expect(resolvePath('', 'child')).toBe('child');
        expect(resolvePath(null, 'child')).toBe('child');
        expect(resolvePath(undefined, 'child')).toBe('child');
    });

    it('child=string[]', () => {
        expect(resolvePath('/parent', ['child1', 'child2'])).toEqual([
            '/parent/child1',
            '/parent/child2'
        ]);
    });

    it('child=string[],absolute', () => {
        expect(resolvePath('/parent', ['/absolute/child1', '/absolute/child2'])).toEqual([
            '/absolute/child1',
            '/absolute/child2'
        ]);
    });

    it('child=string[] parent=blank', () => {
        expect(resolvePath('', ['child1', 'child2'])).toEqual(['child1', 'child2']);
        expect(resolvePath(null, ['child1', 'child2'])).toEqual(['child1', 'child2']);
        expect(resolvePath(undefined, ['child1', 'child2'])).toEqual([
            'child1',
            'child2'
        ]);
    });

    it('child={string:string}', () => {
        expect(
            resolvePath('/parent', {
                key1: 'child1',
                key2: 'child2'
            })
        ).toEqual({
            key1: '/parent/child1',
            key2: '/parent/child2'
        });
    });

    it('child={string:string},absolute', () => {
        expect(
            resolvePath('/parent', {
                key1: '/absolute/child1',
                key2: '/absolute/child2'
            })
        ).toEqual({
            key1: '/absolute/child1',
            key2: '/absolute/child2'
        });
    });

    it('child={string:string} parent=blank', () => {
        expect(
            resolvePath('', {
                key1: 'child1',
                key2: 'child2'
            })
        ).toEqual({
            key1: 'child1',
            key2: 'child2'
        });

        expect(
            resolvePath(null, {
                key1: 'child1',
                key2: 'child2'
            })
        ).toEqual({
            key1: 'child1',
            key2: 'child2'
        });

        expect(
            resolvePath(undefined, {
                key1: 'child1',
                key2: 'child2'
            })
        ).toEqual({
            key1: 'child1',
            key2: 'child2'
        });
    });

    it('child={string:string[]}', () => {
        expect(
            resolvePath('/parent', {
                key1: ['child1', 'child2'],
                key2: ['child3', 'child4']
            })
        ).toEqual({
            key1: ['/parent/child1', '/parent/child2'],
            key2: ['/parent/child3', '/parent/child4']
        });
    });

    it('child={string:string[]},absolute', () => {
        expect(
            resolvePath('/parent', {
                key1: ['/absolute/child1', '/absolute/child2'],
                key2: ['/absolute/child3', '/absolute/child4']
            })
        ).toEqual({
            key1: ['/absolute/child1', '/absolute/child2'],
            key2: ['/absolute/child3', '/absolute/child4']
        });
    });

    it('child={string:string[]} parent=blank', () => {
        expect(
            resolvePath('', {
                key1: ['child1', 'child2'],
                key2: ['child3', 'child4']
            })
        ).toEqual({
            key1: ['child1', 'child2'],
            key2: ['child3', 'child4']
        });

        expect(
            resolvePath(null, {
                key1: ['child1', 'child2'],
                key2: ['child3', 'child4']
            })
        ).toEqual({
            key1: ['child1', 'child2'],
            key2: ['child3', 'child4']
        });

        expect(
            resolvePath(undefined, {
                key1: ['child1', 'child2'],
                key2: ['child3', 'child4']
            })
        ).toEqual({
            key1: ['child1', 'child2'],
            key2: ['child3', 'child4']
        });
    });

    it('child=blank', () => {
        expect(resolvePath('/parent', '')).toBeUndefined();
        expect(resolvePath('/parent', null)).toBeUndefined();
        expect(resolvePath('/parent', undefined)).toBeUndefined();
    });

    it('child=blank parent=blank', () => {
        expect(resolvePath('', '')).toBeUndefined();
        expect(resolvePath(null, null)).toBeUndefined();
        expect(resolvePath(undefined, undefined)).toBeUndefined();
    });
});
