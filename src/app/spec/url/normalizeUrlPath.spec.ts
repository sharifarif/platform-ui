import normalizeUrlPath from '../../url/normalizeUrlPath';

describe('normalizeUrlPath()', () => {
    /**
     * @see https://scadev.atlassian.net/wiki/spaces/TP/pages/195199431/Normalized+URL#NormalizedURL-Examples
     */
    it('examples', () => {
        expect(normalizeUrlPath('/')).toBe('/');

        expect(normalizeUrlPath('/Awesome_Page/')).toBe('/awesome-page');
        expect(normalizeUrlPath('/favicon.ico')).toBe('/favicon.ico');

        expect(normalizeUrlPath('/Awesome_Section/Awesome_Page/')).toBe(
            '/awesome-section/awesome-page'
        );

        expect(normalizeUrlPath('//I--Like__to-_-Write///-_WeiRd_-////URLs/////--')).toBe(
            '/i-like-to-write/weird/urls'
        );
    });

    it('alreadyNormalized', () => {
        expect(normalizeUrlPath('/awesome-page')).toBe('/awesome-page');
        expect(normalizeUrlPath('/favicon.ico')).toBe('/favicon.ico');
        expect(normalizeUrlPath('/sitemap.xml')).toBe('/sitemap.xml');

        expect(normalizeUrlPath('/awesome-section/awesome-page')).toBe(
            '/awesome-section/awesome-page'
        );
    });

    it('uppercase -> lowerCase', () => {
        expect(normalizeUrlPath('/AwEsOmE-PaGe')).toBe('/awesome-page');

        expect(normalizeUrlPath('/AwEsOmE-SeCtIoN/AwEsOmE-PaGe')).toBe(
            '/awesome-section/awesome-page'
        );
    });

    it('unknown -> dash', () => {
        expect(normalizeUrlPath('/awesome_page')).toBe('/awesome-page');
        expect(normalizeUrlPath('/awesome page')).toBe('/awesome-page');

        expect(normalizeUrlPath('/awesome section/awesome_page')).toBe(
            '/awesome-section/awesome-page'
        );

        expect(normalizeUrlPath('/awesome_section/awesome page')).toBe(
            '/awesome-section/awesome-page'
        );
    });

    it('collapse dash', () => {
        expect(normalizeUrlPath('/awesome---page')).toBe('/awesome-page');

        expect(normalizeUrlPath('/awesome_ _page')).toBe('/awesome-page');
    });

    it('collapse dot', () => {
        expect(normalizeUrlPath('/favicon...ico')).toBe('/favicon.ico');
    });

    it('collapse slash', () => {
        expect(normalizeUrlPath('///awesome-page')).toBe('/awesome-page');

        expect(normalizeUrlPath('///awesome-section////awesome-page')).toBe(
            '/awesome-section/awesome-page'
        );
    });

    it('dash stripEnd', () => {
        expect(normalizeUrlPath('/awesome-page-')).toBe('/awesome-page');

        expect(normalizeUrlPath('/awesome-page ')).toBe('/awesome-page');
    });

    it('dot remove adjacent dash', () => {
        expect(normalizeUrlPath('/favicon-.-ico')).toBe('/favicon.ico');

        expect(normalizeUrlPath('/favicon . ico')).toBe('/favicon.ico');
    });

    it('dot stripEnd', () => {
        expect(normalizeUrlPath('/awesome-page.')).toBe('/awesome-page');
    });

    it('slash remove adjacent dash,dot', () => {
        expect(normalizeUrlPath('/-.-awesome-page')).toBe('/awesome-page');

        expect(normalizeUrlPath('/.-.awesome-section/-.-awesome-page')).toBe(
            '/awesome-section/awesome-page'
        );
    });

    it('slash stripEnd', () => {
        expect(normalizeUrlPath('/awesome-page/')).toBe('/awesome-page');

        expect(normalizeUrlPath('/awesome-section/awesome-page/')).toBe(
            '/awesome-section/awesome-page'
        );
    });

    it('slash missingStart', () => {
        expect(normalizeUrlPath('Awesome_Page')).toBe('/awesome-page');
    });

    it('empty', () => {
        expect(normalizeUrlPath('')).toBe('/');
    });

    it('null', () => {
        expect(normalizeUrlPath(null)).toBe('/');
        expect(normalizeUrlPath(undefined)).toBe('/');
    });
});
