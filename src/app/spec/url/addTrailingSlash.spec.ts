import addTrailingSlash from '../../url/addTrailingSlash';

describe('addTrailingSlash()', () => {
    it('add slash', () => {
        expect(addTrailingSlash('https://www.tena.co.uk')).toBe(
            'https://www.tena.co.uk/'
        );
    });

    it('already has slash', () => {
        expect(addTrailingSlash('https://www.tena.co.uk/')).toBe(
            'https://www.tena.co.uk/'
        );
    });

    it('empty', () => {
        expect(addTrailingSlash('')).toBeUndefined();
    });

    it('null', () => {
        expect(addTrailingSlash(null)).toBeUndefined();
        expect(addTrailingSlash(undefined)).toBeUndefined();
    });
});
