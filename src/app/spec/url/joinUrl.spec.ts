import joinUrl from '../../url/joinUrl';

describe('joinUrl()', () => {
    it('domain', () => {
        expect(joinUrl('https://www.tork.co.uk')).toBe('https://www.tork.co.uk');
    });

    it('domain/', () => {
        expect(joinUrl('https://www.tork.co.uk/')).toBe('https://www.tork.co.uk/');
    });

    it('domain page', () => {
        expect(joinUrl('https://www.tork.co.uk', 'men')).toBe(
            'https://www.tork.co.uk/men'
        );
    });

    it('domain/ /page/', () => {
        expect(joinUrl('https://www.tork.co.uk/', '/men/')).toBe(
            'https://www.tork.co.uk/men/'
        );
    });

    it('domain page page', () => {
        expect(joinUrl('https://www.tork.co.uk', 'men', 'products')).toBe(
            'https://www.tork.co.uk/men/products'
        );
    });

    it('domain/ /page/ page/', () => {
        expect(joinUrl('https://www.tork.co.uk/', '/men/', 'products/')).toBe(
            'https://www.tork.co.uk/men/products/'
        );
    });

    it('blank', () => {
        expect(joinUrl('')).toBeUndefined();
        expect(joinUrl(null)).toBeUndefined();
        expect(joinUrl(undefined)).toBeUndefined();
    });

    it('blank blank', () => {
        expect(joinUrl('', '')).toBeUndefined();
        expect(joinUrl(null, null)).toBeUndefined();
        expect(joinUrl(undefined, undefined)).toBeUndefined();
    });

    it('blank page', () => {
        expect(joinUrl('', 'men')).toBe('men');
        expect(joinUrl(null, 'men')).toBe('men');
        expect(joinUrl(undefined, 'men')).toBe('men');
    });

    it('domain/ blank', () => {
        expect(joinUrl('https://www.tork.co.uk/', '')).toBe('https://www.tork.co.uk/');
        expect(joinUrl('https://www.tork.co.uk/', null)).toBe('https://www.tork.co.uk/');
        expect(joinUrl('https://www.tork.co.uk/', undefined)).toBe(
            'https://www.tork.co.uk/'
        );
    });
});
