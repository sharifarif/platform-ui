import forEach from 'lodash/forEach';

import SuitCssExceptions from '../../lint/SuitCssExceptions';

describe('SuitCssExceptions', () => {
    const ignoreSelectors = SuitCssExceptions.ignoreSelectors.map(selector => {
        return new RegExp(selector);
    });

    function isIgnored(libraryClass: string) {
        var matches = 0;

        forEach(ignoreSelectors, selector => {
            if (selector.test(libraryClass)) {
                matches++;
            }
        });

        return matches > 0;
    }

    it('classesDefinedInLibrary', () => {
        forEach(SuitCssExceptions.classesDefinedInLibrary, library => {
            forEach(library, libraryClass => {
                var ignored = isIgnored(libraryClass);

                if (!ignored) {
                    fail(libraryClass + ' is not handled by ignoreSelectors.');
                }
                expect(ignored).toBe(true);
            });
        });
    });

    it('exact', () => {
        expect(isIgnored('.active')).toBe(true);
    });

    it('exact extra', () => {
        expect(isIgnored('.active-extended')).toBe(false);
    });

    it('exact caseSensitive', () => {
        expect(isIgnored('.Active')).toBe(false);
    });

    it('prefix', () => {
        expect(isIgnored('.alert')).toBe(true);
    });

    it('prefix extra', () => {
        expect(isIgnored('.alert-extended')).toBe(true);
    });

    it('prefix caseSensitive', () => {
        expect(isIgnored('.Alert')).toBe(false);
        expect(isIgnored('.Alert-extended')).toBe(false);
    });

    it('prefix camelCase', () => {
        expect(isIgnored('.alert-camelCase')).toBe(true);

        expect(isIgnored('.alert-PascalCase')).toBe(false);
    });

    it('prefix 2 extra', () => {
        expect(isIgnored('.alert-camelCase-camelCase')).toBe(true);

        expect(isIgnored('.alert-PascalCase-camelCase')).toBe(false);
        expect(isIgnored('.alert-camelCase-PascalCase')).toBe(false);
    });

    it('prefix number', () => {
        expect(isIgnored('.alert-75')).toBe(true);
    });
});
