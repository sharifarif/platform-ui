/**
 * Combine the `path`s in a URL.
 *
 * Slashes (`/`) are added as required between each `path`.
 *
 * @param path - Paths to combine.
 */
export default function joinUrl(...path: string[]) {
    var url: string;

    var parts = path.filter(part => {
            return part;
        }),
        partCount = parts.length;

    if (partCount) {
        url = '';

        parts.forEach((part, index) => {
            if (index > 0) {
                part = part.replace(/^\//, '');
            }
            if (index < partCount - 1) {
                part = part.replace(/\/$/, '') + '/';
            }

            url += part;
        });
    }

    return url;
}
