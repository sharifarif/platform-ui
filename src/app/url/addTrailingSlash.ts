/**
 * Add a slash (`/`) to the end of a URL, if it needs one.
 *
 * @param url - URL to add a slash (`/`) to.
 */
export default function addTrailingSlash(url: string) {
    if (url) {
        return url.replace(/\/$/, '') + '/';
    }
}
