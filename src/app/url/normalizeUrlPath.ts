/**
 * Normalize a URL path.
 *
 * @param url - URL path to normalize.
 * @see https://scadev.atlassian.net/wiki/spaces/TP/pages/195199431/Normalized+URL
 */
export default function normalizeUrlPath(url: string) {
    if (url) {
        url = url.toLowerCase();
        url = url.replace(/[^a-z0-9\/\.]+/g, '-');
        url = url.replace(/(\-*\.\-*)+/g, '.');
        url = url.replace(/([\-\.]*\/[\-\.]*)+/g, '/');
        url = url.replace(/[\-\.\/]+$/, '');

        if (!url.startsWith('/')) {
            url = '/' + url;
        }
    } else {
        url = '/';
    }

    return url;
}
