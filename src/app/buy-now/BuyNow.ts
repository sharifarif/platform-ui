import { IBuyNowProvider } from './provider/IBuyNowProvider';
import BuyNowProviderFactory from './provider/BuyNowProviderFactory';

/**
 * Activate a link where a customer can buy a product.
 *
 * Simply import this file into your code:
 *
 * ```typescript
 * import 'platform-ui/src/app/buy-now/BuyNow';
 * ```
 */
export default class BuyNow {
    /**
     * Where the customer can buy the product.
     */
    provider: IBuyNowProvider;

    /**
     * Activate the provided `.BuyNow` element.
     *
     * @param element - `.BuyNow` element to activate.
     */
    constructor(element: JQuery) {
        var provider = BuyNowProviderFactory.forElement(element);

        provider.bindHtml(element);
        provider.loadScripts();

        this.provider = provider;
    }
}

$('.BuyNow').each((index, node) => {
    new BuyNow($(node));
});
