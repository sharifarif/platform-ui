import { IBuyNowProvider } from './IBuyNowProvider';

/**
 * Base class that all {@link IBuyNowProvider}s extend.
 */
export abstract class BaseBuyNowProvider implements IBuyNowProvider {
    private static AddedScriptUrls: string[] = [];

    /**
     * Get the `.BuyNow-button` element.
     */
    static getButton(element: JQuery): JQuery {
        return element.is('.BuyNow-button') ? element : element.find('.BuyNow-button');
    }

    /**
     * Get the `.BuyNow-select` element.
     */
    static getSelect(element: JQuery): JQuery {
        return element.find('.BuyNow-select');
    }

    /**
     * Get the current {@link IBuyNowVariant.value}.
     */
    static getValue(element: JQuery): string {
        var value: string;

        var select = BaseBuyNowProvider.getSelect(element);
        if (select.length) {
            value = select.val() as string;
        } else if (element.is('[data-buy-now-value]')) {
            value = element.data('buy-now-value');
        } else {
            let valueElement = element.find('[data-buy-now-value]');
            if (valueElement) {
                value = valueElement.data('buy-now-value');
            }
        }

        return value;
    }

    /**
     * Bind JavaScript actions with the HTML.
     */
    bindHtml(element: JQuery) {
        var value = BaseBuyNowProvider.getValue(element);
        if (value !== undefined && value !== null) {
            this.bindValue(element, value);
        }

        var select = BaseBuyNowProvider.getSelect(element);
        if (select.length) {
            select.on('input change', () => {
                value = select.val() as string;
                if (value !== undefined && value !== null) {
                    this.bindValue(element, value);
                }
            });
        }
    }

    /**
     * Load JavaScript dependencies.
     */
    loadScripts() {
        var urls = this.getScriptUrls();
        if (urls && urls.length) {
            var addedUrls = BaseBuyNowProvider.AddedScriptUrls;

            $.each(urls, (index, url) => {
                if (addedUrls.indexOf(url) === -1) {
                    addedUrls.push(url);

                    var script = document.createElement('script');

                    script.async = true;
                    script.defer = true;
                    script.src = url;

                    document.body.appendChild(script);
                }
            });
        }
    }

    /**
     * Bind an {@link IBuyNowVariant.value} with an HTML element.
     */
    protected abstract bindValue(element: JQuery, value: string): void;

    /**
     * Get the URLs of JavaScript dependencies.
     */
    protected abstract getScriptUrls(): string[];
}
