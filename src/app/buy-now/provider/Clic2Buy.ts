import { BaseBuyNowProvider } from './BaseBuyNowProvider';

/**
 * Use Clic2Buy for buying products.
 */
export default class Clic2Buy extends BaseBuyNowProvider {
    private static ScriptUrls = ['https://widget.clic2drive.com/assets/c2d.js?ver=1.0'];

    bindHtml(element: JQuery) {
        super.bindHtml(element);

        BaseBuyNowProvider.getButton(element).attr(
            'data-widget-element',
            'data-widget-element'
        );
    }

    protected bindValue(element: JQuery, value: string) {
        BaseBuyNowProvider.getButton(element).attr('data-widget-id', value);
    }

    protected getScriptUrls() {
        return Clic2Buy.ScriptUrls;
    }
}
