import { BaseBuyNowProvider } from './BaseBuyNowProvider';

/**
 * Use a link for buying products.
 */
export default class BuyNowLink extends BaseBuyNowProvider {
    protected bindValue(element: JQuery, value: string) {
        BaseBuyNowProvider.getButton(element).attr('href', value);
    }

    protected getScriptUrls(): string[] {
        return [];
    }
}
