import Clic2Buy from './Clic2Buy';
import BuyNowLink from './BuyNowLink';
import { IBuyNowProvider } from './IBuyNowProvider';

/**
 * Create an {@link IBuyNowProvider} for an HTML element.
 */
export default class BuyNowProviderFactory {
    /**
     * Create an {@link IBuyNowProvider} for an HTML element.
     */
    static forElement(element: JQuery): IBuyNowProvider {
        var providerName = BuyNowProviderFactory.getProviderName(element);
        if (providerName) {
            providerName = providerName.toLowerCase();

            if (providerName === 'link') {
                return new BuyNowLink();
            } else if (providerName === 'clic2buy') {
                return new Clic2Buy();
            }
        }
    }

    /**
     * Get the {@link IBuyNowProduct.provider} name of an HTML element.
     */
    static getProviderName(element: JQuery): string {
        return (
            element.data('buy-now-provider') ||
            element.find('[data-buy-now-provider]').data('buy-now-provider')
        );
    }
}
