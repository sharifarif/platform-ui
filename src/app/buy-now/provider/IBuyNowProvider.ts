/**
 * Where the customer can buy the product.
 */
export interface IBuyNowProvider {
    /**
     * Bind JavaScript actions with the HTML.
     */
    bindHtml(element: JQuery): void;

    /**
     * Load JavaScript dependencies.
     */
    loadScripts(): void;
}
