/**
 * A variant of an {@link IBuyNowProduct}.
 *
 * If an {@link IBuyNowProduct} only has one {@link IBuyNowVariant},
 * the {@link IBuyNowVariant} may describe the {@link IBuyNowProduct} itself.
 */
export interface IBuyNowVariant {
    /**
     * Name of the {@link IBuyNowVariant}
     * (or {@link IBuyNowProduct} if only 1 {@link IBuyNowVariant} is supplied).
     */
    name?: string;

    /**
     * A value passed to the {@link IBuyNowProvider},
     * such as a {@link BuyNowLink} URL or {@link Clic2Buy} ID.
     */
    value: string;

    /**
     * `true` if this is the default variant when buying an {@link IBuyNowProduct}.
     */
    default?: true;
}
