import { IBuyNowVariant } from './IBuyNowVariant';

/**
 * A product the customer can {@link BuyNow}.
 */
export interface IBuyNowProduct {
    /**
     * Where the customer can buy the product.
     */
    provider: 'Link' | 'Clic2Buy';

    /**
     * Variants of the product.
     *
     * There should always be at least 1 variant.
     */
    variants: IBuyNowVariant[];
}
