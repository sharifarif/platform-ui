import { DoCheck } from '@angular/core';

import Search from './Search';

/**
 * Base class for list components.
 *
 * @param I - Type of {@link items} in the list.
 */
export abstract class BaseList<I> implements DoCheck {
    /**
     * Items in the list.
     */
    items: I[];

    /**
     * Search query.
     */
    search?: string;

    /**
     * Items in the list that contain the search query.
     */
    searchResults?: I[];

    /**
     * Update {@link searchResults} when something changes.
     */
    ngDoCheck() {
        this.searchResults = Search.search(this.search, this.items);
    }

    /**
     * Determine whether any {@link items} in the list
     * have a value for the provided `field`.
     *
     * @param field - Field to look for.
     */
    hasField(field: string) {
        var hasField = false;

        var items = this.searchResults || this.items;
        if (items) {
            items.forEach(item => {
                if (item) {
                    var value = (item as any)[field];
                    if (value !== null && value !== undefined && value !== '') {
                        hasField = true;
                    }
                }
            });
        }

        return hasField;
    }
}
