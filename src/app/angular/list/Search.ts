import pull from 'lodash/pull';

/**
 * Search for items that contain the greatest number
 * of distinct tokens in a search query.
 */
export default class Search {
    /**
     * Ignore:
     * - Whitespace characters:                       `\s`
     * - Reserved characters in HTML:                 `"'&<>`
     * - Characters used to separate items in a list: `,;`
     */
    static readonly IgnoreCharacters = /[\s"'&<>,;]+/;

    /**
     * Get the tokens in a search `query`, and convert them to lower case.
     *
     * @param query - Search query.
     */
    static getTokensLowerCase(query: string) {
        var tokens: string[];

        if (query) {
            tokens = query.toLocaleLowerCase().split(Search.IgnoreCharacters);
            pull(tokens, '');
        }

        return tokens;
    }

    /**
     * Get the number of distinct tokens in `lowerCaseTokens` an `item` contains.
     *
     * @param lowerCaseTokens - Tokens converted to lower case.
     * @param item - Item to check.
     */
    static getTokenCount(lowerCaseTokens: string[], item: any) {
        var tokenCount = 0;

        lowerCaseTokens.forEach(token => {
            if (Search.hasToken(token, item)) {
                tokenCount++;
            }
        });

        return tokenCount;
    }

    /**
     * Determine whether an `item` contains a token.
     *
     * @param lowerCaseToken - Token converted to lower case.
     * @param item - Item to check.
     */
    static hasToken(lowerCaseToken: string, item: any) {
        var hasToken = false;

        if (typeof item === 'boolean' || typeof item === 'number') {
            item = '' + item;
        }

        if (typeof item === 'string') {
            return item.toLocaleLowerCase().includes(lowerCaseToken);
        } else if (item) {
            if (Array.isArray(item)) {
                let length = item.length;

                for (let index = 0; index < length; index++) {
                    if (Search.hasToken(lowerCaseToken, item[index])) {
                        return true;
                    }
                }
            } else {
                for (let key in item) {
                    if (Search.hasToken(lowerCaseToken, item[key])) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * Search for `items` that contain the greatest number
     * of distinct tokens in a search `query`.
     *
     * @param query - Search query.
     * @param items - Items to search.
     */
    static search<T>(query: string, items: T[]) {
        if (items) {
            if (query) {
                let tokens = Search.getTokensLowerCase(query);
                if (tokens.length) {
                    let byTokenCount: T[][] = [];

                    items.forEach(item => {
                        var tokenCount = Search.getTokenCount(tokens, item);
                        if (tokenCount) {
                            let withTokenCount = byTokenCount[tokenCount];
                            if (!withTokenCount) {
                                withTokenCount = [];
                                byTokenCount[tokenCount] = withTokenCount;
                            }

                            withTokenCount.push(item);
                        }
                    });

                    for (let tokenCount = tokens.length; tokenCount > 0; tokenCount--) {
                        let withTokenCount = byTokenCount[tokenCount];
                        if (withTokenCount) {
                            return withTokenCount;
                        }
                    }

                    return [];
                }
            }

            return items;
        }
    }
}
