import { InjectionToken } from '@angular/core';

/**
 * Get the environment variables stored in `window.environment`:
 *
 * ```typescript
 * constructor(
 *     @Inject(ENVIRONMENT) environment: IEnvironment
 * )
 * ```
 *
 * Dependency injection is used because server-side rendering does not have a `window`.
 */
export const ENVIRONMENT = new InjectionToken('environment');
