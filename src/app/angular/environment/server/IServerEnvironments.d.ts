import { IServerEnvironment } from './IServerEnvironment';

/**
 * Environment variables for each site.
 */
export interface IServerEnvironments<E extends IServerEnvironment> {
    /**
     * Default environment variables.
     */
    default?: E;

    /**
     * Environment variables for each site.
     */
    sites: E[];
}
