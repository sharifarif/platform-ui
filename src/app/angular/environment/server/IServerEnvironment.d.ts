import { IApiEnvironment } from '../../api/IApiEnvironment';

/**
 * Environment variables for a site.
 */
export interface IServerEnvironment extends IApiEnvironment {
    /**
     * Environment variables for a site.
     */
    site: {
        /**
         * Normalized URL every path on the site starts with.
         *
         * @see https://scadev.atlassian.net/wiki/spaces/TP/pages/249036881/Configuration
         */
        basePath: string;

        /**
         * Site’s domain, without “www.”.
         *
         * @see https://scadev.atlassian.net/wiki/spaces/TP/pages/249036881/Configuration
         */
        domain: string;
    };

    /**
     * Location of the `/assets/` folder.
     *
     * @see https://scadev.atlassian.net/wiki/spaces/TP/pages/101758905/Static+Asset+CDN
     */
    StaticDirectory: string;
}
