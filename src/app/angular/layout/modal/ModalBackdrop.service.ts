import { BehaviorSubject, Observable } from 'rxjs';
import { DOCUMENT } from '@angular/common';
import {
    Inject,
    Injectable,
    OnDestroy,
    Renderer2,
    RendererFactory2
} from '@angular/core';

/**
 * Control modal backdrops on the page.
 */
@Injectable()
export class ModalBackdropService implements OnDestroy {
    readonly modals: Observable<Object[]>;
    private readonly modalsSubject: BehaviorSubject<Object[]>;
    private readonly renderer2: Renderer2;
    private backdrop: HTMLDivElement;
    private htmlWidth: number;
    private htmlScrollTop: number;
    private bodyScrollTop: number;

    constructor(
        @Inject(DOCUMENT) private document: any,
        rendererFactory2: RendererFactory2
    ) {
        var modalsSubject = new BehaviorSubject<Object[]>([]),
            modalsObservable = modalsSubject.asObservable();

        this.modals = modalsObservable;
        this.modalsSubject = modalsSubject;
        this.renderer2 = rendererFactory2.createRenderer(document.body, null);

        modalsObservable.subscribe(modals => {
            if (modals.length) {
                this.addBackdrop();
            } else {
                this.removeBackdrop();
            }
        });
    }

    /**
     * Add a modal with a backdrop.
     *
     * @param caller - An instance of a class controlling the modal.
     */
    addModal(caller: Object) {
        var modalsSubject = this.modalsSubject,
            modals = modalsSubject.getValue().slice();

        modals.push(caller);

        modalsSubject.next(modals);
    }

    /**
     * Remove a modal.
     *
     * @param caller - An instance of a class controlling the modal.
     */
    removeModal(caller: Object) {
        var modalsSubject = this.modalsSubject,
            modals = modalsSubject.getValue(),
            modalIndex = modals.indexOf(caller);

        if (modalIndex >= 0) {
            modals = modals.slice();
            modals.splice(modalIndex, 1);

            modalsSubject.next(modals);
        }
    }

    ngOnDestroy() {
        this.removeBackdrop();
    }

    /**
     * Add the backdrop to the DOM.
     */
    protected addBackdrop() {
        var backdrop = this.backdrop;
        if (!backdrop) {
            let document: HTMLDocument = this.document,
                htmlElement = document.documentElement,
                body = document.body,
                renderer2 = this.renderer2;

            backdrop = renderer2.createElement('div');
            renderer2.addClass(backdrop, 'modal-backdrop');
            renderer2.addClass(backdrop, 'show');

            renderer2.appendChild(body, backdrop);
            renderer2.addClass(body, 'modal-open');

            this.backdrop = backdrop;
            this.htmlWidth = htmlElement.clientWidth;
            this.htmlScrollTop = htmlElement.scrollTop;
            this.bodyScrollTop = body.scrollTop;
        }
    }

    /**
     * Remove the backdrop from the DOM.
     */
    protected removeBackdrop() {
        var backdrop = this.backdrop;
        if (backdrop) {
            let document: HTMLDocument = this.document,
                htmlElement = document.documentElement,
                body = document.body,
                renderer2 = this.renderer2;

            renderer2.removeChild(body, backdrop);
            renderer2.removeClass(body, 'modal-open');

            // Reset the scroll position if the window has not changed size.
            if (htmlElement.clientWidth === this.htmlWidth) {
                htmlElement.scrollTop = this.htmlScrollTop;
                body.scrollTop = this.bodyScrollTop;
            }

            this.backdrop = undefined;
        }
    }
}
