import { Directive, Input, OnChanges, OnDestroy } from '@angular/core';

import { ModalBackdropService } from './ModalBackdrop.service';

/**
 * Add a modal backdrop to the page.
 */
@Directive({
    selector: '[platformModalBackdrop]'
})
export class ModalBackdropDirective implements OnChanges, OnDestroy {
    /**
     * `false` to remove the modal backdrop.  (Defaults to `true`.)
     */
    @Input() platformModalBackdrop?: boolean;

    constructor(private modalBackdropService: ModalBackdropService) {}

    ngOnChanges() {
        var modalBackdropService = this.modalBackdropService;

        if (this.platformModalBackdrop === false) {
            modalBackdropService.removeModal(this);
        } else {
            modalBackdropService.addModal(this);
        }
    }

    ngOnDestroy() {
        this.modalBackdropService.removeModal(this);
    }
}
