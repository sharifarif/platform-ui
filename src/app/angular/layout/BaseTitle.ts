import { Title } from '@angular/platform-browser';

/**
 * Base class for updating the `<title>`.
 *
 * In your project, extend {@link BaseTitle} as shown below:
 *
 * ```typescript
 * @ Injectable()
 * export class MySiteTitle extends BaseTitle {
 *
 *     constructor(title: Title) {
 *         super(title, 'My Site Name');
 *     }
 * }
 * ```
 *
 * In the `Component` for each page, set the title in `ngOnInit()`:
 *
 * ```typescript
 * constructor(private mySiteTitle: MySiteTitle) {}
 *
 * ngOnInit() {
 *     this.mySiteTitle.setTitle(['Page', 'Parent Page']);
 * }
 * ```
 *
 * Change `MySite`, `mySite`, and `'My Site Name'`
 * in the above examples to match your project.
 */
export class BaseTitle {
    /**
     * @param title - Angular’s `Title` service.
     * @param siteName - Text placed at the end of the `<title>`.
     * @param separator - Separator placed between each segment in the `<title>`.
     */
    constructor(
        private title: Title,
        private siteName?: string,
        private separator = ' - '
    ) {}

    /**
     * Set the `<title>` for the page.
     *
     * Examples:
     * * `setTitle('Page')` ⟶ `'Page - My Site Name'`
     * * `setTitle(['Page', 'Parent Page'])` ⟶ `'Page - Parent Page - My Site Name'`
     *
     * @param title - `<title>` for the page.
     */
    setTitle(title?: string | string[]) {
        var segments = [this.siteName];

        if (title) {
            if (Array.isArray(title)) {
                Array.prototype.unshift.apply(segments, title);
            } else {
                segments.unshift(title);
            }
        }

        segments = segments.filter(segment => {
            return !!segment;
        });

        this.title.setTitle(segments.join(this.separator));
    }
}
