import { BehaviorSubject, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import isEqual from 'lodash/isEqual';
import uniqWith from 'lodash/uniqWith';
import without from 'lodash/without';
import { map } from 'rxjs/operators';

import { IAlert } from './IAlert';

/**
 * Collect alerts at an application level.
 *
 * Any component that subscribes to {@link alerts} will re-render
 * when an alert is added or removed.
 */
@Injectable()
export class AlertService {
    /**
     * Alerts shown to the user.
     */
    alerts: {
        /**
         * All alerts.
         */
        all: Observable<IAlert[]>;

        /**
         * All alerts, with duplicates removed.
         */
        unique: Observable<IAlert[]>;
    };
    private alertsSubject: BehaviorSubject<IAlert[]>;

    constructor() {
        var alertsSubject = new BehaviorSubject<IAlert[]>([]),
            alertsAll = alertsSubject.asObservable();

        this.alerts = {
            all: alertsAll,
            unique: alertsAll.pipe(map(alerts => uniqWith(alerts, isEqual)))
        };
        this.alertsSubject = alertsSubject;
    }

    /**
     * Add an alert.
     *
     * @param alert - Alert to add.
     * @param timeout - How long to show the alert (in milliseconds).
     */
    addAlert(alert: IAlert, timeout?: number) {
        var alertsSubject = this.alertsSubject,
            alerts = alertsSubject.getValue();

        alerts = alerts.slice();
        alerts.push(alert);
        alertsSubject.next(alerts);

        if (timeout) {
            setTimeout(() => {
                this.remove(alert);
            });
        }
    }

    /**
     * Show an alert, and remove any old alerts.
     *
     * @param alert - Alert to show.
     * @param timeout - How long to show the alert (in milliseconds).
     */
    setAlert(alert: IAlert, timeout?: number) {
        this.alertsSubject.next([alert]);

        if (timeout) {
            setTimeout(() => {
                this.remove(alert);
            });
        }
    }

    /**
     * Remove an alert.
     *
     * @param alert - Alert to remove.
     */
    remove(alert: IAlert) {
        var alertsSubject = this.alertsSubject,
            alerts = alertsSubject.getValue();

        alerts = without(alerts, alert);
        alertsSubject.next(alerts);
    }

    /**
     * Remove all alerts.
     */
    removeAll() {
        this.alertsSubject.next([]);
    }
}
