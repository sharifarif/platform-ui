import { AlertType } from './AlertType';

/**
 * Alert shown to the user.
 */
export interface IAlert {
    /**
     * Text shown to the user.
     */
    message: string;

    /**
     * Severity of the alert.
     */
    type: AlertType;

    /**
     * `false` if the alert cannot be closed by the user.
     */
    close?: boolean;

    /**
     * Icon shown to the user.
     */
    iconClass?: string;
}
