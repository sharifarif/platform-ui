export type AlertType = 'success' | 'warning' | 'error' | 'info';

/**
 * Severity of an alert.
 */
export const AlertType = {
    /**
     * Successful action.
     */
    success: 'success' as AlertType,

    /**
     * Issue that prevents data from being shown.
     */
    warning: 'warning' as AlertType,

    /**
     * Issue that prevents the user from executing an action.
     */
    error: 'error' as AlertType,

    /**
     * Information that is neither good nor bad.
     */
    info: 'info' as AlertType
};
