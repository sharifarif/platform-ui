import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AlertService } from './Alert.service';
import { AlertType } from './AlertType';
import { IAlert } from './IAlert';

/**
 * Show application level alerts to the user.
 */
@Component({
    selector: 'platform-alerts',
    templateUrl: 'Alerts.component.html'
})
export class AlertsComponent implements OnInit {
    /**
     * Icon shown in an alert with `type` {@link AlertType.success}.
     */
    @Input() iconClassSuccess?: string;

    /**
     * Icon shown in an alert with `type` {@link AlertType.warning}.
     */
    @Input() iconClassWarning = 'fa fa-warning';

    /**
     * Icon shown in an alert with `type` {@link AlertType.error}.
     */
    @Input() iconClassError = 'fa fa-warning';

    /**
     * Icon shown in an alert with `type` {@link AlertType.info}.
     */
    @Input() iconClassInfo = 'fa fa-info-circle';

    /**
     * Alerts shown to the user.
     */
    alerts: IAlert[];

    constructor(private alertService: AlertService, private router: Router) {}

    ngOnInit() {
        var alertService = this.alertService;

        alertService.alerts.unique.subscribe(alerts => (this.alerts = alerts));
        this.router.events.subscribe(() => alertService.removeAll());
    }

    /**
     * Determine whether an alert can be closed by the user.
     *
     * @param alert - Alert to check.
     */
    canClose(alert: IAlert) {
        return alert.close !== false;
    }

    /**
     * Close an alert.
     *
     * @param alert - Alert to close.
     */
    close(alert: IAlert) {
        this.alertService.remove(alert);
    }

    /**
     * Get the class of an alert’s `<div>`.
     *
     * @param alert - Alert to get the class for.
     */
    getClass(alert: IAlert) {
        var alertClass = 'alert alert-';

        alertClass += alert.type === AlertType.error ? 'danger' : alert.type;

        if (this.canClose(alert)) {
            alertClass += ' alert-dismissible';
        }

        alertClass += ' mb-0 rounded-0';

        return alertClass;
    }

    /**
     * Get the class of the icon in an alert.
     *
     * @param alert - Alert to get the icon class for.
     */
    getIconClass(alert: IAlert) {
        var iconClass = alert.iconClass;

        if (!iconClass) {
            switch (alert.type) {
                case AlertType.success:
                    iconClass = this.iconClassSuccess;
                    break;
                case AlertType.warning:
                    iconClass = this.iconClassWarning;
                    break;
                case AlertType.error:
                    iconClass = this.iconClassError;
                    break;
                case AlertType.info:
                    iconClass = this.iconClassInfo;
            }
        }

        return iconClass;
    }
}
