import { bindCallback } from 'rxjs';
import { Inject, Injectable } from '@angular/core';

import { ENVIRONMENT } from '../../environment/ENVIRONMENT';
import { IAuthEnvironment } from './IAuthEnvironment';

/**
 * Login and Logout with Azure Active Directory.
 */
@Injectable()
export class AuthService {
    /**
     * Information about the user.
     */
    readonly user: adal.User;
    private authentication: adal.AuthenticationContext;

    constructor(@Inject(ENVIRONMENT) private environment: IAuthEnvironment) {
        const AuthenticationContext: adal.AuthenticationContextStatic = require('adal-angular');

        var applicationId = this.getAzureAdApplicationId(),
            authentication = new AuthenticationContext({
                clientId: applicationId,
                extraQueryParameter: 'nux=1',
                // FIXME: See if /logout and /adal-iframe.html can be customized.
                // Otherwise, add documentation to describe their use.
                postLogoutRedirectUri: this.getBaseUrl() + '/logout',
                redirectUri: this.getBaseUrl() + '/adal-iframe.html',
                tenant: 'sca.onmicrosoft.com'
            });

        this.authentication = authentication;

        this.processLoginLogout();

        this.user = authentication.getCachedUser();
    }

    /**
     * `true` if the user is logged in.
     */
    get loggedIn(): boolean {
        return !!this.authentication.getCachedToken(this.getAzureAdApplicationId());
    }

    /**
     * Get a token that can prove the user’s identity to a server.
     */
    getToken() {
        var error: string;

        return bindCallback(
            callback => {
                var applicationId = this.getAzureAdApplicationId();

                this.authentication.acquireToken(
                    applicationId,
                    (message: string, token: any) => {
                        if (message) {
                            error = message;
                        }

                        callback(token);
                    }
                );
            },
            (token: string) => {
                if (!token) {
                    throw error;
                }

                return token;
            }
        )();
    }

    /**
     * Get the user’s first name.
     */
    getUserFirstName(): string {
        var user = this.user,
            profile = user && user.profile,
            firstName = profile && profile.given_name,
            fullName = profile && profile.name,
            userName = user && user.userName;

        return firstName || fullName || userName || undefined;
    }

    getUserId(): string {
        var user = this.user,
            profile = user && user.profile;

        return profile && profile.oid;
    }

    getUserName(): string {
        return this.user.userName;
    }

    /**
     * Get the user’s full name.
     */
    getUserFullName(): string {
        var user = this.user,
            profile = user && user.profile,
            firstName = profile && profile.given_name,
            fullName = profile && profile.name,
            userName = user && user.userName;

        return fullName || firstName || userName || undefined;
    }

    /**
     * Show the login page.
     *
     * @param redirectUri - Page to send the user to after logging in.
     */
    login(redirectUri?: string) {
        if (!redirectUri || redirectUri === '/logout') {
            redirectUri = '/';
        }
        redirectUri = this.getBaseUrl() + redirectUri;

        var authentication = this.authentication;

        authentication.config.redirectUri = redirectUri;
        authentication.login();
    }

    /**
     * Log the user out.
     */
    logout() {
        this.authentication.logOut();
    }

    /**
     * Get the Application ID used by Azure Active Directory.
     */
    protected getAzureAdApplicationId() {
        return this.environment.azureAd.applicationId;
    }

    /**
     * Process any pending login and logout commands.
     */
    protected processLoginLogout() {
        var location = window.location;

        if (location.hash) {
            // Prevent the page from redirecting twice after a login or logout.
            let url = this.getBaseUrl() + location.pathname + '#';
            sessionStorage.setItem('adal.login.request', url);

            this.authentication.handleWindowCallback();
        }
    }

    // FIXME: See if <base href=""> can be used.
    private getBaseUrl() {
        var location = window.location,
            port = location.port;

        return location.protocol + '//' + location.hostname + (port ? ':' + port : '');
    }
}
