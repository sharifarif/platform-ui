import {
    ActivatedRouteSnapshot,
    CanActivate,
    CanActivateChild,
    RouterStateSnapshot
} from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { AuthService } from './Auth.service';

/**
 * Require the user to login before seeing a page.
 *
 * In your `NgModule`, add this to every route that requires a login:
 *
 * ```typescript
 * canActivate: [AuthGuard]
 * ```
 *
 * If all children of a route require a login, add this to the parent route,
 * instead of adding adding `canActivate: [AuthGuard]` to each child route:
 *
 * ```typescript
 * canActivateChild: [AuthGuard]
 * ```
 */
@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild {
    constructor(private authService: AuthService) {}

    /**
     * Determine whether the user can see the page.
     *
     * If not, send them to the login page.
     * After they log in, send them to the target page.
     *
     * @param route - Current route.
     * @param state - Route the user is trying to go to.
     */
    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean> | boolean {
        var authService = this.authService,
            loggedIn = authService.loggedIn;

        if (!loggedIn) {
            authService.login(state.url);
        }

        return loggedIn;
    }

    /**
     * Determine whether the user can see a child page.
     *
     * If not, send them to the login page.
     * After they log in, send them to the target page.
     *
     * @param route - Current route.
     * @param state - Route the user is trying to go to.
     */
    canActivateChild(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean> | boolean {
        return this.canActivate(route, state);
    }
}
