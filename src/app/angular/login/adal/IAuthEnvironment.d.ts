/**
 * To use {@link AuthService}, add the `applicationId` to `environment.azureAd`,
 * as shown below:
 *
 * ```typescript
 * environment = {
 *     azureAd: {
 *         applicationId: '<Application ID used by Azure Active Directory>'
 *     }
 * };
 * ```
 */
export interface IAuthEnvironment {
    azureAd: {
        /**
         * Application ID used by Azure Active Directory.
         */
        applicationId: string;
    };
}
