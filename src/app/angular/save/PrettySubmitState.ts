/**
 * The state of a saveable `<form>`.
 */
export enum PrettySubmitState {
    /**
     * The user is disconnected from the Internet.
     *
     * Unless offline saving is implemented,
     * the user won’t be able to save until they go back online.
     */
    Offline = 1,

    /**
     * The data in the `<form>` is already saved.
     */
    Saved,

    /**
     * The `<form>` is currently saving.
     */
    Saving,

    /**
     * An error occurred while saving the `<form>`,
     * and it’s not safe for the code to try saving the same `<form>` again.
     */
    UnrecoverableError,

    /**
     * The `<form>` has changes that still need to be saved.
     */
    Unsaved
}
