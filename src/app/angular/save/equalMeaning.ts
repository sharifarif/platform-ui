import isEqualWith from 'lodash/isEqualWith';
import isObject from 'lodash/isObject';

function equalsNull(value: any, falseEqualsNull: any, blankEqualsNull: any): boolean {
    return (
        value === null ||
        value === undefined ||
        value !== value ||
        (value === false && falseEqualsNull !== false) ||
        (value === '' && blankEqualsNull !== false)
    );
}
/**
 * Determine whether two values have the same meaning.
 *
 * The following are considered equal, since they usually mean the same thing:
 * * `null` == `undefined` == `NaN`
 * * `null` == `false`
 * * `null` == `''`
 * * `#` == `'#'`
 * * `{}` == `{key: null}`
 *
 * The following are considered different:
 * * `null` != `0`
 * * `''` != `0`
 *
 * @param a - Value to compare with `b`.
 * @param b - Value to compare with `a`.
 * @param falseEqualsNull - `false` if `false` and `null` mean different things.
 *     If you have a `<select>` with a Yes (`true`), No (`false`), and blank (`null`)
 *     option, then set this to `false`.
 * @param blankEqualsNull - `false` if `""` and `null` mean different things.
 */
export default function equalMeaning(
    a: any,
    b: any,
    falseEqualsNull = true,
    blankEqualsNull = true
): boolean {
    // tslint:disable-next-line:max-func-body-length
    function equals(x: any, y: any): boolean {
        if (
            equalsNull(x, falseEqualsNull, blankEqualsNull) &&
            equalsNull(y, falseEqualsNull, blankEqualsNull)
        ) {
            return true;
        }
        if (typeof x === 'number' && typeof y === 'string') {
            return '' + x === y;
        }
        if (typeof x === 'string' && typeof y === 'number') {
            return x === '' + y;
        }

        if (isObject(x) && isObject(y)) {
            for (let key in x) {
                if (x.hasOwnProperty(key)) {
                    const localX: any = x[key as keyof object];
                    const localY: any = y[key as keyof object];
                    if (!isEqualWith(localX, localY, equals)) {
                        return false;
                    }
                }
            }
            for (let key in y) {
                if (y.hasOwnProperty(key)) {
                    if (
                        !(key in x) &&
                        !equalsNull(
                            y[key as keyof object],
                            falseEqualsNull,
                            blankEqualsNull
                        )
                    ) {
                        return false;
                    }
                }
            }

            return true;
        }
    }

    return isEqualWith(a, b, equals);
}
