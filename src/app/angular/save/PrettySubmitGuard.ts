import { CanDeactivate } from '@angular/router';
import { ElementRef, Injectable } from '@angular/core';
import pull from 'lodash/pull';
import { TranslateService } from '@ngx-translate/core';

import { PrettySubmitDirective } from './PrettySubmit.directive';

/**
 * Warn the user when they’re leaving a page with unsaved changes.
 *
 * In your `NgModule`, add this to any route that uses the {@link PrettySubmitDirective}:
 *
 * `canDeactivate: [PrettySubmitGuard]`
 */
@Injectable()
export class PrettySubmitGuard implements CanDeactivate<any> {
    /**
     * A list of every active {@link PrettySubmitDirective}.
     *
     * This is handled automatically by {@link PrettySubmitDirective}.
     */
    readonly submits: PrettySubmitDirective[] = [];

    constructor(private translate: TranslateService) {}

    /**
     * Add a {@link PrettySubmitDirective} to {@link submits}.
     *
     * This is handled automatically by {@link PrettySubmitDirective}.
     *
     * @param submit - {@link PrettySubmitDirective} to add.
     */
    add(submit: PrettySubmitDirective) {
        this.submits.push(submit);
    }

    /**
     * @returns `true` if the user can leave the page.
     */
    canDeactivate() {
        return this.canRemoveComponent();
    }

    /**
     * @param elementRef - Element being removed.
     * @returns `true` if a component can be removed.
     */
    canRemoveComponent(elementRef?: ElementRef) {
        var deactivate = true;

        this.submits.forEach(submit => {
            if (
                !elementRef ||
                elementRef.nativeElement.contains(submit.elementRef.nativeElement)
            ) {
                if (!submit.canDeactivate()) {
                    deactivate = false;
                }
            }
        });

        return (
            deactivate ||
            window.confirm(
                this.translate.instant('PrettySubmit.Unsaved.DiscardConfirmation')
            )
        );
    }

    /**
     * Remove a {@link PrettySubmitDirective} from {@link submits}.
     *
     * This is handled automatically by {@link PrettySubmitDirective}.
     *
     * @param submit - {@link PrettySubmitDirective} to remove.
     */
    remove(submit: PrettySubmitDirective) {
        pull(this.submits, submit);
    }
}
