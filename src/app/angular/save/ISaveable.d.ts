import { Observable } from 'rxjs';

/**
 * The contract that should be implemented by every saveable `<form>`.
 *
 * @see {@link BaseSaveable}
 */
export interface ISaveable<D> {
    /**
     * Data in the `<form>`.
     */
    data: D;

    /**
     * `true` if the `<form>` is currently saving.
     */
    saving: boolean;

    /**
     * Returns `true` if the data in the `<form>` is already saved.
     */
    isSaved(): boolean;

    /**
     * Save the `<form>`.
     */
    save(): Observable<any>;
}
