import cloneDeep from 'lodash/cloneDeep';
import { EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';
import { share } from 'rxjs/operators';

import equalMeaning from './equalMeaning';
import { ISaveable } from './ISaveable';

/**
 * Base class for a saveable `<form>`.
 *
 * To build a saveable `<form>`, extend {@link BaseSaveable} and implement:
 * * An `ngOnInit()` function that calls either {@link initNew} or {@link initSaved}.
 * * A {@link performSave} function.
 */
export abstract class BaseSaveable<D> implements ISaveable<D> {
    /**
     * Data in the `<form>`.
     */
    data: D;

    /**
     * `true` if the user’s last attempt to save the `<form>` resulted in an error.
     *
     * This may be used to display a message to the user.
     */
    errorSaving = false;

    /**
     * Event fired when the `<form>` successfully saves.
     */
    onSave = new EventEmitter<D>();

    /**
     * Data saved on the server.
     */
    savedData: D;

    /**
     * `true` if the `<form>` is currently saving.
     */
    saving = false;
    private initialData: D;

    /**
     * Initialize the `<form>` with an item that is new (has never been saved).
     *
     * @param initialData - A blank item to start with.
     */
    initNew(initialData: D) {
        this.data = cloneDeep(initialData);
        this.initialData = initialData;
        this.savedData = undefined;
    }

    /**
     * Initialize the `<form>` with an item from the server.
     *
     * @param savedData - An item from the server.
     */
    initSaved(savedData: D) {
        this.data = cloneDeep(savedData);
        this.savedData = savedData;
    }

    /**
     * Return `true` if both:
     * * The item is new (has never been saved).
     * * The data is the same as when the `<form>` was created.
     */
    isInitialData() {
        return this.isNew() && this.equals(this.initialData);
    }

    /**
     * Return `true` if the item is new (has never been saved).
     */
    isNew() {
        return !this.savedData;
    }

    /**
     * Returns `true` if the data in the `<form>` is already saved.
     */
    isSaved() {
        return this.equals(this.savedData);
    }

    /**
     * Save the `<form>`.
     */
    save() {
        var data = this.data,
            savedData = cloneDeep(data);

        this.saving = true;

        var observable = this.performSave(data, savedData).pipe(share());

        observable.subscribe(
            () => {
                this.savedData = savedData;
                this.saving = false;

                this.onSave.emit(savedData);
                this.errorSaving = false;
            },
            () => {
                this.saving = false;
                this.errorSaving = true;
            }
        );

        return observable;
    }

    /**
     * Save the data to the server.
     *
     * Implement this function in your subclass.
     *
     * @param data - The {@link data} to save.
     * @param savedData - What {@link savedData} will become after the save completes.
     */
    protected abstract performSave(data: D, savedData: D): Observable<any>;

    /**
     * Return a copy of `data` that only contains the data that gets saved by the server.
     *
     * If {@link PrettySubmitDirective} says the `<form>` needs to be saved
     * when it does not, you can override this function in a subclass.
     *
     * @param data - Data to filter.
     */
    protected getSaveableData(data: D): any {
        return data;
    }

    private equals(savedData: D) {
        var data = this.data;
        if (data) {
            if (!savedData) {
                return false;
            }

            return equalMeaning(
                this.getSaveableData(data),
                this.getSaveableData(savedData)
            );
        }
    }
}
