import {
    Directive,
    ElementRef,
    HostListener,
    Input,
    OnChanges,
    OnDestroy,
    OnInit
} from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { PrettySubmitGuard } from './PrettySubmitGuard';
import { PrettySubmitState } from './PrettySubmitState';

/**
 * Make a submit `<button>` show the current state of a `<form>`.
 */
@Directive({
    selector: '[platformPrettySubmit]'
})
export class PrettySubmitDirective implements OnChanges, OnDestroy, OnInit {
    /**
     * Icon shown in the `<button>` when the `<form>` is already saved.
     *
     * @default `'fa fa-check'`
     */
    @Input() classIconSaved?: string;

    /**
     * Icon shown in the `<button>` when the `<form>` is currently saving.
     *
     * @default `'fa fa-spin fa-spinner'`
     */
    @Input() classIconSaving?: string;

    /**
     * Icon shown in the `<button>` when either:
     * * The user is disconnected from the Internet.
     * * An error occurred while saving the `<form>`,
     *   and it’s not safe for the code to try saving the same `<form>` again.
     *
     * @default `'fa fa-warning'`
     */
    @Input() classIconWarning?: string;

    /**
     * Class to add to the `<button>` when the user is disconnected from the Internet.
     *
     * @default `'btn-warning'`
     */
    @Input() classOffline?: string;

    /**
     * Class to add to the `<button>` when either:
     * * The `<form>` has changes that still need to be saved.
     * * The `<form>` is currently saving.
     *
     * @default `'btn-primary'`
     */
    @Input() classSave?: string;

    /**
     * Class to add to the `<button>` when the `<form>` is already saved.
     *
     * @default `'btn-success'`
     */
    @Input() classSaved?: string;

    /**
     * Class to add to the `<button>` when an error occurred while saving the `<form>`,
     * and it’s not safe for the code to try saving the same `<form>` again.
     *
     * @default `'btn-danger'`
     */
    @Input() classUnrecoverableError?: string;

    /**
     * `true` if the user inputted invalid data.
     */
    @Input() formInvalid?: boolean;

    /**
     * `true` if both:
     * * The item is new (has never been saved).
     * * The data is the same as when the `<form>` was created.
     */
    @Input() initialData?: boolean;

    /**
     * `true` to disable the `<button>`.
     */
    @Input() prettySubmitDisabled?: boolean;

    /**
     * `true` to warn users before leaving an unsaved page.
     *
     * Note: Add {@link PrettySubmitGuard} to the route in your `NgModule`.
     */
    @Input() promptUnsaved?: boolean;

    /**
     * `true` if the `<form>` is saved.
     */
    @Input() saved?: boolean;

    /**
     * `true` if the `<form>` is saving.
     */
    @Input() saving?: boolean;

    /**
     * Text in the `<button>` when the user is disconnected from the Internet.
     *
     * @default Translated `'Offline'`
     */
    @Input() textOffline?: string;

    /**
     * Text in the `<button>` when the `<form>` has changes that still need to be saved.
     *
     * @default Translated `'Save'`
     */
    @Input() textSave?: string;

    /**
     * Text in the `<button>` when the `<form>` is already saved.
     *
     * @default Translated `'Saved'`
     */
    @Input() textSaved?: string;

    /**
     * Text in the `<button>` when the `<form>` is currently saving.
     *
     * @default Translated `'Saving'`
     */
    @Input() textSaving?: string;

    /**
     * Text in the `<button>` when an error occurred while saving the `<form>`,
     * and it’s not safe for the code to try saving the same `<form>` again.
     *
     * @default Translated `'Error Saving'`
     */
    @Input() textUnrecoverableError?: string;

    /**
     * `true` if an error occurred while saving the `<form>`,
     * and it’s not safe for the code to try saving the same `<form>` again.
     */
    @Input() unrecoverableError?: boolean;

    constructor(
        public elementRef: ElementRef,
        private prettySubmitGuard: PrettySubmitGuard,
        private translate: TranslateService
    ) {}

    /**
     * Returns `false` if the user should be warned before leaving the page.
     */
    canDeactivate() {
        var canDeactivate =
            !this.promptUnsaved ||
            this.saved ||
            this.initialData ||
            this.unrecoverableError;

        return !!canDeactivate;
    }

    /**
     * Get the state of the saveable `<form>`.
     */
    getState() {
        var state: PrettySubmitState;

        if (this.unrecoverableError) {
            state = PrettySubmitState.UnrecoverableError;
        } else if (this.isOffline()) {
            state = PrettySubmitState.Offline;
        } else if (this.saving) {
            state = PrettySubmitState.Saving;
        } else if (this.saved && !this.formInvalid) {
            state = PrettySubmitState.Saved;
        } else {
            state = PrettySubmitState.Unsaved;
        }

        return state;
    }

    /**
     * Get the text in the `<button>`.
     *
     * @param state - State of the saveable `<form>`.
     */
    getText(state: PrettySubmitState) {
        var text: string;

        switch (state) {
            case PrettySubmitState.Offline:
                text =
                    this.textOffline ||
                    this.translate.instant('PrettySubmit.ButtonText.Offline');
                break;
            case PrettySubmitState.Saved:
                text =
                    this.textSaved ||
                    this.translate.instant('PrettySubmit.ButtonText.Saved');
                break;
            case PrettySubmitState.Saving:
                text =
                    this.textSaving ||
                    this.translate.instant('PrettySubmit.ButtonText.Saving');
                break;
            case PrettySubmitState.UnrecoverableError:
                text =
                    this.textUnrecoverableError ||
                    this.translate.instant('PrettySubmit.ButtonText.Error Saving');
                break;
            default:
                text =
                    this.textSave ||
                    this.translate.instant('PrettySubmit.ButtonText.Save');
        }

        return text;
    }

    /**
     * Determine whether the `<button>` should be disabled.
     *
     * @param state - State of the saveable `<form>`.
     */
    isDisabled(state: PrettySubmitState) {
        return state !== PrettySubmitState.Unsaved || !!this.prettySubmitDisabled;
    }

    /**
     * Determine whether the user is disconnected from the Internet.
     */
    isOffline() {
        return window.navigator.onLine === false;
    }

    ngOnChanges() {
        this.render();
    }

    ngOnDestroy() {
        this.prettySubmitGuard.remove(this);
    }

    ngOnInit() {
        this.prettySubmitGuard.add(this);
        this.render();
    }

    /**
     * Handle the `window`’s `beforeunload` event.
     */
    @HostListener('window:beforeunload', ['$event'])
    onBeforeUnload($event: BeforeUnloadEvent) {
        if (!this.canDeactivate()) {
            $event.returnValue = this.translate.instant('PrettySubmit.Unsaved.Warning');
        }
    }

    /**
     * Handle the `window`’s `online` and `offline` event.
     */
    @HostListener('window:online', ['$event'])
    @HostListener('window:offline', ['$event'])
    onOnlineOffline() {
        this.render();
    }

    /**
     * Update the `<button>`’s user interface.
     */
    render() {
        var state = this.getState();

        this.renderClasses(state);
        this.renderContent(state);

        var disabled = this.isDisabled(state);

        this.renderDisabled(disabled);
    }

    /**
     * Update the classes on the `<button>`.
     *
     * @param state - State of the saveable `<form>`.
     */
    renderClasses(state: PrettySubmitState) {
        var add: string,
            remove: string[] = [];

        var classOffline = this.classOffline || 'btn-warning';
        if (state === PrettySubmitState.Offline) {
            add = classOffline;
        } else {
            remove.push(classOffline);
        }

        var classSave = this.classSave || 'btn-primary';
        if (state === PrettySubmitState.Saving || state === PrettySubmitState.Unsaved) {
            add = classSave;
        } else {
            remove.push(classSave);
        }

        var classSaved = this.classSaved || 'btn-success';
        if (state === PrettySubmitState.Saved) {
            add = classSaved;
        } else {
            remove.push(classSaved);
        }

        var classUnrecoverableError = this.classUnrecoverableError || 'btn-danger';
        if (state === PrettySubmitState.UnrecoverableError) {
            add = classUnrecoverableError;
        } else {
            remove.push(classUnrecoverableError);
        }

        var element: HTMLButtonElement = this.elementRef.nativeElement;

        element.classList.remove.apply(element.classList, remove.join(' ').split(' '));
        element.classList.add.apply(element.classList, add.split(' '));
    }

    /**
     * Update the HTML in the `<button>`.
     *
     * @param state - State of the saveable `<form>`.
     */
    renderContent(state: PrettySubmitState) {
        var text = this.getText(state),
            textNode = document.createTextNode(text),
            nodes: Node[] = [textNode];

        if (state === PrettySubmitState.Saved) {
            let icon = document.createElement('i');

            icon.className = (this.classIconSaved || 'fa fa-check') + ' ml-btn';

            nodes.push(icon);
        } else if (state === PrettySubmitState.Saving) {
            let icon = document.createElement('i');

            icon.className =
                (this.classIconSaving || 'fa fa-spin fa-spinner') + ' ml-btn';

            nodes.push(icon);
        } else if (
            state === PrettySubmitState.Offline ||
            state === PrettySubmitState.UnrecoverableError
        ) {
            let icon = document.createElement('i');

            icon.className = (this.classIconWarning || 'fa fa-warning') + ' mr-btn';

            nodes.unshift(icon);
        }

        var element: HTMLButtonElement = this.elementRef.nativeElement;

        while (element.hasChildNodes()) {
            element.removeChild(element.lastChild);
        }

        nodes.forEach(node => {
            element.appendChild(node);
        });
    }

    /**
     * Update the disabled property on the `<button>`.
     *
     * @param disabled - `true` to disable the `<button>`.
     */
    renderDisabled(disabled: boolean) {
        this.elementRef.nativeElement.disabled = disabled;
    }
}
