import { AfterViewInit, Directive, ElementRef, Input } from '@angular/core';

/**
 * Focus an element automatically when it gets added to the page.
 */
@Directive({
    selector: '[autofocus]' // tslint:disable-line:directive-selector
})
export class AutofocusDirective implements AfterViewInit {
    /**
     * If `false`, the element will not be focused automatically.
     */
    @Input() autofocus?: boolean;

    constructor(private elementRef: ElementRef) {}

    ngAfterViewInit() {
        setTimeout(() => {
            if (this.autofocus !== false) {
                this.elementRef.nativeElement.focus();
            }
        }, 0);
    }
}
