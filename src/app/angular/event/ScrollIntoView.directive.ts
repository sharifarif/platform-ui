import { AfterViewInit, Directive, ElementRef, Input, OnChanges } from '@angular/core';

/**
 * Automatically scroll an element into view.
 */
@Directive({
    selector: '[platformScrollIntoView]'
})
export class ScrollIntoViewDirective implements AfterViewInit, OnChanges {
    /**
     * If `false`, `element.scrollIntoView()` will not be called.
     *
     * Whenever `platformScrollIntoView` changes from `false` to a non-`false` value,
     * `element.scrollIntoView()` will be called again.
     */
    @Input() platformScrollIntoView?: boolean;

    private lastScrollIntoView: boolean;

    constructor(private elementRef: ElementRef) {}

    ngAfterViewInit() {
        this.scrollIntoView();
    }

    ngOnChanges() {
        this.scrollIntoView();
    }

    /**
     * Scroll the element into view if either:
     * * `scrollIntoView()` was never called.
     * * {@link platformScrollIntoView} went from `false` to a non-`false` value.
     */
    scrollIntoView() {
        var scrollIntoView = this.platformScrollIntoView !== false,
            valueChanged = scrollIntoView !== this.lastScrollIntoView;

        if (scrollIntoView && valueChanged) {
            let element: HTMLElement = this.elementRef.nativeElement;

            setTimeout(() => {
                if ((element as any).scrollIntoViewIfNeeded) {
                    (element as any).scrollIntoViewIfNeeded(false);
                } else {
                    element.scrollIntoView({
                        behavior: 'auto',
                        block: 'nearest'
                    });
                }
            }, 0);
        }

        this.lastScrollIntoView = scrollIntoView;
    }
}
