import { Injectable } from '@angular/core';

import { HoverItems } from './HoverItems';
import { MouseLocationService } from './MouseLocation.service';

/**
 * Factory that creates instances of {@link HoverItems}.
 */
@Injectable()
export class HoverItemsFactory {
    constructor(private mouseLocationService: MouseLocationService) {}

    /**
     * Create an instance of {@link HoverItems}.
     *
     * @param speed - `'fast'` to respond quickly to hover events.
     *                Main menus should be set to `'slow'`.
     * @param current - Current item.
     */
    createHoverItems<I>(speed: 'slow' | 'fast' = 'slow', current?: I) {
        return new HoverItems(this.mouseLocationService, speed, current);
    }
}
