/**
 * Location of the mouse.
 */
export interface IMouseLocation {
    /**
     * `true` if the mouse pointer is inside the document’s viewport.
     */
    readonly insideViewport?: boolean;

    /**
     * X coordinate of the mouse pointer in local (DOM content) coordinates.
     */
    readonly x?: number;

    /**
     * Y coordinate of the mouse pointer in local (DOM content) coordinates.
     */
    readonly y?: number;
}
