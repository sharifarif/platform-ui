import { BehaviorSubject, Observable } from 'rxjs';

import { IMouseLocation } from './IMouseLocation';
import { MouseLocationService } from './MouseLocation.service';

/**
 * Respond to hover events only when the user stops moving his or her mouse.
 */
export class HoverItems<I> {
    /**
     * How long to wait, in milliseconds, after a use hovers over an item.
     */
    static readonly TimeoutMilliseconds = {
        slow: 100,
        fast: 33
    };

    /**
     * Minimum distance, in pixels, after which a user is still considered
     * to be moving his or her mouse.
     */
    static readonly MouseMoveThresholdPixels = 6;

    /**
     * Current item.
     */
    readonly current: Observable<I>;

    /**
     * `'fast'` if responding quickly to hover events.
     * Main menus should be set to `'slow'`.
     */
    readonly speed: 'slow' | 'fast';

    /**
     * Like {@link current}, except the item can be set or gotten immediately.
     */
    protected readonly currentSubject: BehaviorSubject<I>;

    /**
     * Item currently under the mouse.
     */
    protected actualCurrent: I;

    /**
     * Mouse location.
     */
    protected mouseLocation: IMouseLocation;

    /**
     * Timer that changes {@link current}.
     */
    protected timer: NodeJS.Timer | number;

    /**
     * Mouse location when the {@link timer} was created.
     */
    protected timerMouseLocation: IMouseLocation;

    /**
     * Speed of the {@link timer}.
     */
    protected timerSpeed: 'slow' | 'fast';

    /**
     * Value {@link current} will be set to when {@link timer} completes.
     */
    protected timerTarget: I;

    constructor(
        mouseLocationService: MouseLocationService,
        speed: 'slow' | 'fast' = 'slow',
        current?: I
    ) {
        var currentSubject = new BehaviorSubject<I>(current);

        this.current = currentSubject.asObservable();
        this.currentSubject = currentSubject;
        this.speed = speed;

        mouseLocationService.observable.subscribe(mouseLocation => {
            this.mouseLocation = mouseLocation;
            this.delayTimerIfMoved();
        });
    }

    /**
     * Immediately set the current `item`.
     *
     * @param item - New current item.
     */
    setCurrent(item: I) {
        this.actualCurrent = item;

        var currentSubject = this.currentSubject;
        if (currentSubject.getValue() !== item) {
            currentSubject.next(item);
        }

        this.clearTimer();
    }

    /**
     * Call this when the the user moves their mouse over an `item`.
     *
     * @param item - Item the user moved their mouse over.
     */
    onMouseEnter(item: I) {
        this.actualCurrent = item;

        this.updateTimer(this.speed);
    }

    /**
     * Call this when the the user’s mouse leaves an `item`.
     *
     * @param item - Item the user’s mouse left.
     */
    onMouseLeave(item: I) {
        if (this.actualCurrent === item) {
            this.actualCurrent = undefined;
        }

        this.updateTimer('fast');
    }

    /**
     * Remove any active timers.
     */
    protected clearTimer() {
        var timer = this.timer;
        if (timer) {
            clearInterval(timer as any);

            this.timer = undefined;
        }
    }

    /**
     * Create a timer that sets {@link current} to {@link actualCurrent}
     * if the user’s mouse is still in the viewport.
     *
     * @param speed - `'fast'` to respond quickly to hover events.
     *                Main menus should be set to `'slow'`.
     */
    protected createTimer(speed: 'slow' | 'fast') {
        this.timerMouseLocation = this.mouseLocation;
        this.timerSpeed = speed;
        this.timerTarget = this.actualCurrent;

        this.timer = setInterval(() => {
            if (this.mouseLocation.insideViewport !== false) {
                this.setCurrent(this.actualCurrent);
                this.clearTimer();
            }
        }, HoverItems.TimeoutMilliseconds[speed]);
    }

    /**
     * Delay any active timers such that the remaining time is
     * the amount of time they were originally set for.
     */
    protected delayTimer() {
        if (this.timer) {
            let speed = this.timerSpeed;

            this.clearTimer();
            this.createTimer(speed);
        }
    }

    /**
     * Delay any active timers where the mouse has moved more than
     * {@link MouseMoveThresholdPixels} since the timer was created.
     */
    protected delayTimerIfMoved() {
        if (this.timer) {
            let distance = MouseLocationService.distance(
                this.mouseLocation,
                this.timerMouseLocation
            );

            if (distance >= HoverItems.MouseMoveThresholdPixels) {
                this.delayTimer();
            }
        }
    }

    /**
     * Update timers such that their goal is to
     * set {@link current} to {@link actualCurrent}.
     *
     * @param speed - `'fast'` to respond quickly to hover events.
     *                Main menus should be set to `'slow'`.
     */
    protected updateTimer(speed: 'slow' | 'fast') {
        var actualCurrent = this.actualCurrent;

        if (actualCurrent !== this.timerTarget) {
            this.clearTimer();
        }
        if (!this.timer && actualCurrent !== this.currentSubject.getValue()) {
            this.createTimer(speed);
        }
    }
}
