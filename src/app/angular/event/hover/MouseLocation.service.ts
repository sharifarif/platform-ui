import { BehaviorSubject, Observable } from 'rxjs';
import { DOCUMENT } from '@angular/common';
import { Inject, Injectable, OnDestroy } from '@angular/core';
import isEqual from 'lodash/isEqual';

import { IMouseLocation } from './IMouseLocation';

/**
 * Location of the mouse.
 */
@Injectable()
export class MouseLocationService implements IMouseLocation, OnDestroy {
    /**
     * `true` if the mouse pointer is inside the document’s viewport.
     */
    insideViewport?: boolean;

    /**
     * X coordinate of the mouse pointer in local (DOM content) coordinates.
     */
    x?: number;

    /**
     * Y coordinate of the mouse pointer in local (DOM content) coordinates.
     */
    y?: number;

    /**
     * Observe changes to the mouse location.
     */
    readonly observable: Observable<IMouseLocation>;

    private readonly eventListeners: {
        mouseMove: (event: MouseEvent) => void;
        mouseOut: (event: MouseEvent) => void;
    };
    private readonly subject: BehaviorSubject<IMouseLocation>;

    /**
     * Get the distance in pixels between two {@link IMouseLocation}s.
     *
     * @param location1 - First location to compare.
     * @param location2 - Second location to compare.
     */
    static distance(location1: IMouseLocation, location2: IMouseLocation) {
        var distance: number;

        if (location1 && location2) {
            distance = Math.hypot(location1.x - location2.x, location1.y - location2.y);
        }

        return Number.isNaN(distance) ? undefined : distance;
    }

    constructor(@Inject(DOCUMENT) private readonly document: any) {
        var mouseMove = (event: MouseEvent) => {
                this.onMouseMove(event);
            },
            mouseOut = (event: MouseEvent) => {
                this.onMouseOut(event);
            };

        document.addEventListener('mousemove', mouseMove, true);
        document.addEventListener('mouseout', mouseOut, true);

        this.eventListeners = {
            mouseMove: mouseMove,
            mouseOut: mouseOut
        };

        var subject = new BehaviorSubject<IMouseLocation>(this.getImmutable());

        this.observable = subject.asObservable();
        this.subject = subject;
    }

    /**
     * Get an immutable {@link IMouseLocation}
     * that represents the current location of the mouse.
     */
    getImmutable(): IMouseLocation {
        return {
            insideViewport: this.insideViewport,
            x: this.x,
            y: this.y
        };
    }

    /**
     * Remove event listeners used by the {@link MouseLocationService}.
     */
    ngOnDestroy() {
        var document = this.document,
            eventListeners = this.eventListeners;

        document.removeEventListener('mousemove', eventListeners.mouseMove, true);
        document.removeEventListener('mouseout', eventListeners.mouseOut, true);
    }

    /**
     * Update the {@link IMouseLocation} when the mouse moves.
     *
     * @param event - Information about the `mousemove` event.
     */
    protected onMouseMove(event: MouseEvent) {
        // FIXME: insideViewport only if clientX <= body.clientWidth && clientY <= body.clientHeight.

        this.setState({
            insideViewport: true,
            x: event.clientX,
            y: event.clientY
        });
    }

    /**
     * Update the {@link IMouseLocation} when the mouse leaves an element.
     *
     * @param event - Information about the `mouseout` event.
     */
    protected onMouseOut(event: MouseEvent) {
        // FIXME: insideViewport only if clientX <= body.clientWidth && clientY <= body.clientHeight.

        this.setState({
            insideViewport: !!event.relatedTarget
        });
    }

    /**
     * Update the {@link IMouseLocation} with new information.
     *
     * @param state - New information about the {@link IMouseLocation}.
     */
    protected setState(state: IMouseLocation) {
        const oldState = this.getImmutable();

        Object.assign(this, state);

        const newState = this.getImmutable();

        if (!isEqual(oldState, newState)) {
            this.subject.next(newState);
        }
    }
}
