import { Rendition } from './Rendition';

/**
 * Image, Video, or Document.
 */
export interface IMedia {
    /**
     * CDN URLs for the image, video, or document.
     *
     * @see {@link getCdnPaths}
     */
    CDNPaths: { [rendition in Rendition]?: string } | string;

    /**
     * Height of the original image, video, or document.
     */
    Height?: number;

    /**
     * Width of the original image, video, or document.
     */
    Width?: number;

    /**
     * Description of the image, video, or document for search engines and blind people.
     */
    AltText?: string;
}
