/**
 * Rendition created by MediaBank.
 */
export enum Rendition {
    /**
     * No more than 240 pixels wide.
     *
     * No restriction on height.
     *
     * @see [Images: Output Widths](https://scadev.atlassian.net/wiki/spaces/TP/pages/106382518/Images#Images-OutputWidths)
     */
    '240w' = '240w',

    /**
     * No more than 240 pixels wide.
     *
     * No restriction on height.
     *
     * @see [Images: Output Widths](https://scadev.atlassian.net/wiki/spaces/TP/pages/106382518/Images#Images-OutputWidths)
     */
    '240w_webp' = '240w_webp',

    /**
     * No more than 480 pixels wide.
     *
     * No restriction on height.
     *
     * @see [Images: Output Widths](https://scadev.atlassian.net/wiki/spaces/TP/pages/106382518/Images#Images-OutputWidths)
     */
    '480w' = '480w',

    /**
     * No more than 480 pixels wide.
     *
     * No restriction on height.
     *
     * @see [Images: Output Widths](https://scadev.atlassian.net/wiki/spaces/TP/pages/106382518/Images#Images-OutputWidths)
     */
    '480w_webp' = '480w_webp',

    /**
     * No more than 690 pixels wide.
     *
     * No restriction on height.
     *
     * @see [Images: Output Widths](https://scadev.atlassian.net/wiki/spaces/TP/pages/106382518/Images#Images-OutputWidths)
     */
    '690w' = '690w',

    /**
     * No more than 690 pixels wide.
     *
     * No restriction on height.
     *
     * @see [Images: Output Widths](https://scadev.atlassian.net/wiki/spaces/TP/pages/106382518/Images#Images-OutputWidths)
     */
    '690w_webp' = '690w_webp',

    /**
     * No more than 720 pixels wide.
     *
     * No restriction on height.
     *
     * @see [Images: Output Widths](https://scadev.atlassian.net/wiki/spaces/TP/pages/106382518/Images#Images-OutputWidths)
     */
    '720w' = '720w',

    /**
     * No more than 720 pixels wide.
     *
     * No restriction on height.
     *
     * @see [Images: Output Widths](https://scadev.atlassian.net/wiki/spaces/TP/pages/106382518/Images#Images-OutputWidths)
     */
    '720w_webp' = '720w_webp',

    /**
     * No more than 1080 pixels wide.
     *
     * No restriction on height.
     *
     * @see [Images: Output Widths](https://scadev.atlassian.net/wiki/spaces/TP/pages/106382518/Images#Images-OutputWidths)
     */
    '1080w' = '1080w',

    /**
     * No more than 1080 pixels wide.
     *
     * No restriction on height.
     *
     * @see [Images: Output Widths](https://scadev.atlassian.net/wiki/spaces/TP/pages/106382518/Images#Images-OutputWidths)
     */
    '1080w_webp' = '1080w_webp',

    /**
     * No more than 1200 pixels wide.
     *
     * No restriction on height.
     *
     * @see [Images: Output Widths](https://scadev.atlassian.net/wiki/spaces/TP/pages/106382518/Images#Images-OutputWidths)
     */
    '1200w' = '1200w',

    /**
     * No more than 1200 pixels wide.
     *
     * No restriction on height.
     *
     * @see [Images: Output Widths](https://scadev.atlassian.net/wiki/spaces/TP/pages/106382518/Images#Images-OutputWidths)
     */
    '1200w_webp' = '1200w_webp',

    /**
     * No more than 1440 pixels wide.
     *
     * No restriction on height.
     *
     * @see [Images: Output Widths](https://scadev.atlassian.net/wiki/spaces/TP/pages/106382518/Images#Images-OutputWidths)
     */
    '1440w' = '1440w',

    /**
     * No more than 1440 pixels wide.
     *
     * No restriction on height.
     *
     * @see [Images: Output Widths](https://scadev.atlassian.net/wiki/spaces/TP/pages/106382518/Images#Images-OutputWidths)
     */
    '1440w_webp' = '1440w_webp',

    /**
     * Original file uploaded by the content editor.
     */
    Original = 'Original',

    /**
     * Video: Streaming
     *
     * @see https://scadev.atlassian.net/wiki/spaces/TP/pages/102863110/Streaming
     */
    'H264 Broadband 720p' = 'H264 Broadband 720p',

    /**
     * Video: Progressive Download
     *
     * @see https://scadev.atlassian.net/wiki/spaces/TP/pages/102863110/Streaming
     */
    'H264_4500kbps_AAC_und_ch2_128kbps' = 'H264_4500kbps_AAC_und_ch2_128kbps'
}
