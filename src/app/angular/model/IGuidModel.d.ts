/**
 * Contract for editable data models.
 */
export interface IGuidModel {
    /**
     * Unique ID of the item.
     */
    id: string;

    /**
     * Version that updates every time an item is saved.
     *
     * The server should reject an update if the user has an outdated `rowVersion`.
     */
    rowVersion: string;

    /**
     * `true` if the item is inactive.
     *
     * Inactive items should not be displayed or used in logic.
     */
    isInactive: boolean;
}
