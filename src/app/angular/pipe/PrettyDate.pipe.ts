import { DatePipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';

/**
 * Prettify a date for the user.
 *
 * Dates are formatted in the user’s locale.
 *
 * A user with the en-US locale will get:
 * * `{{ '2017-07-21' | platformPrettyDate }}` ⟶ `7/21/2017`
 * * `{{ '2017-07-21T00:00:00' | platformPrettyDate }}` ⟶ `7/21/2017`
 * * `{{ '2017-07-21T14:30:00' | platformPrettyDate }}` ⟶ `7/21/2017, 2:30 PM`
 */
@Pipe({
    name: 'platformPrettyDate'
})
export class PrettyDatePipe implements PipeTransform {
    /**
     * Length of a date in ISO 8601 format, without a time.
     */
    static readonly IsoDateLength = 10;
    private static readonly DefaultFormat = 'short';

    /**
     * Determine whether a `date` represents midnight.
     *
     * @param date - Date to check.
     */
    static isMidnight(date: Date | string) {
        if (date) {
            if (typeof date === 'string') {
                if (date.length <= PrettyDatePipe.IsoDateLength) {
                    return true;
                }

                date = new Date(date);
            }

            return !date.getHours() && !date.getMinutes();
        }
    }

    constructor(private datePipe: DatePipe) {}

    /**
     * Prettify a `date` for the user.
     *
     * @param date - Date to prettify.
     * @param pattern - Whether to use a `'short'` or `'medium'` length format.
     *                  Defaults to a `'short'` format.
     */
    transform(date: Date | string, pattern?: 'medium' | 'short'): string {
        var prettyPattern: string = pattern || PrettyDatePipe.DefaultFormat;

        if (PrettyDatePipe.isMidnight(date)) {
            prettyPattern += 'Date';
        }

        return this.datePipe.transform(date, prettyPattern);
    }
}
