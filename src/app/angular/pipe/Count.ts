/**
 * Get the number of `items` in an `Array` or `Object`.
 *
 * @param items - `Array` or `Object` to count the `items` in.
 */
export default function count(items: number | any[] | any): number {
    if (items) {
        if (Array.isArray(items)) {
            items = items.length;
        } else if (typeof items === 'object') {
            items = Object.keys(items).length;
        }
    }

    return items;
}
