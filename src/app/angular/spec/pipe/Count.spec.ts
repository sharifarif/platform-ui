import count from '../../pipe/count';

describe('count', () => {
    it('number', () => {
        expect(count(0)).toBe(0);
        expect(count(1)).toBe(1);
        expect(count(2)).toBe(2);
        expect(count(1000)).toBe(1000);
    });

    it('array', () => {
        expect(count([])).toBe(0);
        expect(count(['awesome'])).toBe(1);
        expect(count(['awesome', 'sauce'])).toBe(2);
    });

    it('object', () => {
        expect(count({})).toBe(0);

        expect(
            count({
                awesome: 'totally awesome'
            })
        ).toBe(1);

        expect(
            count({
                awesome: 'totally awesome',
                sauce: 'totally sauce'
            })
        ).toBe(2);
    });

    it('null', () => {
        expect(count(undefined)).toBeUndefined();
        expect(count(null)).toBeNull();
    });
});
