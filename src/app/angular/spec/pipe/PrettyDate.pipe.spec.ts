import { DatePipe } from '@angular/common';

import { PrettyDatePipe } from '../../pipe/PrettyDate.pipe';

describe('PrettyDatePipe', () => {
    var datePipe: DatePipe, prettyDate: PrettyDatePipe;

    beforeEach(() => {
        datePipe = new DatePipe('en-US');
        prettyDate = new PrettyDatePipe(datePipe);
    });

    it('isMidnight() string short', () => {
        expect(PrettyDatePipe.isMidnight('2017-05-19')).toBe(true);
    });

    it('isMidnight() string tooShort', () => {
        expect(PrettyDatePipe.isMidnight('2017-5-1')).toBe(true);
    });

    it('isMidnight() string long', () => {
        expect(PrettyDatePipe.isMidnight('2017-05-19T00:00:00')).toBe(true);

        expect(PrettyDatePipe.isMidnight('2017-05-19T00:15:00')).toBe(false);
        expect(PrettyDatePipe.isMidnight('2017-05-19T02:00:00')).toBe(false);
    });

    it('isMidnight() object', () => {
        expect(PrettyDatePipe.isMidnight(new Date(2017, 4, 19))).toBe(true);

        expect(PrettyDatePipe.isMidnight(new Date(2017, 4, 19, 0, 15))).toBe(false);
        expect(PrettyDatePipe.isMidnight(new Date(2017, 4, 19, 2))).toBe(false);
    });

    it('isMidnight() empty', () => {
        expect(PrettyDatePipe.isMidnight('')).toBeUndefined();
    });

    it('isMidnight() null', () => {
        expect(PrettyDatePipe.isMidnight(null)).toBeUndefined();
        expect(PrettyDatePipe.isMidnight(undefined)).toBeUndefined();
    });

    it('transform() string short', () => {
        expect(prettyDate.transform('2017-05-19')).toBe(
            datePipe.transform('2017-05-19', 'shortDate')
        );

        expect(prettyDate.transform('2017-05-19', 'medium')).toBe(
            datePipe.transform('2017-05-19', 'mediumDate')
        );
    });

    it('transform() string long midnight', () => {
        expect(prettyDate.transform('2017-05-19T00:00:00')).toBe(
            datePipe.transform('2017-05-19T00:00:00', 'shortDate')
        );

        expect(prettyDate.transform('2017-05-19T00:00:00', 'medium')).toBe(
            datePipe.transform('2017-05-19T00:00:00', 'mediumDate')
        );
    });

    it('transform() string long notMidnight', () => {
        expect(prettyDate.transform('2017-05-19T02:30:00')).toBe(
            datePipe.transform('2017-05-19T02:30:00', 'short')
        );

        expect(prettyDate.transform('2017-05-19T02:30:00', 'medium')).toBe(
            datePipe.transform('2017-05-19T02:30:00', 'medium')
        );
    });

    it('transform() object midnight', () => {
        expect(prettyDate.transform(new Date(2017, 4, 19))).toBe(
            datePipe.transform(new Date(2017, 4, 19), 'shortDate')
        );

        expect(prettyDate.transform(new Date(2017, 4, 19), 'medium')).toBe(
            datePipe.transform(new Date(2017, 4, 19), 'mediumDate')
        );
    });

    it('transform() object notMidnight', () => {
        expect(prettyDate.transform(new Date(2017, 4, 19, 2, 30))).toBe(
            datePipe.transform(new Date(2017, 4, 19, 2, 30), 'short')
        );

        expect(prettyDate.transform(new Date(2017, 4, 19, 2, 30), 'medium')).toBe(
            datePipe.transform(new Date(2017, 4, 19, 2, 30), 'medium')
        );
    });

    it('transform() empty', () => {
        expect(prettyDate.transform('')).toBe(null);
    });

    it('transform() null', () => {
        expect(prettyDate.transform(null)).toBe(null);
        expect(prettyDate.transform(undefined)).toBe(null);
    });
});
