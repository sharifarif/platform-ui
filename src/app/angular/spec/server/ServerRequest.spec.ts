import { NgSetupOptions, RenderOptions } from '@nguniversal/express-engine';

import { ExpressEngine } from '../../server/express/ExpressEngine';
import { ExpressEngineCallback } from '../../server/express/ExpressEngineCallback';
import { IServerEnvironments } from '../../environment/server/IServerEnvironments';
import MockNgExpressEngineFactory from './MockNgExpressEngineFactory';
import { ServerRequest } from '../../server/ServerRequest';
import { ServerRequestDomainType } from '../../server/ServerRequestDomainType';

describe('ServerRequest', () => {
    var environments: IServerEnvironments<any>,
        module: any,
        ngExpressEngineFactory: MockNgExpressEngineFactory,
        options: RenderOptions,
        requestHeaders: { [header: string]: string },
        serverRequest: ServerRequest<any>;

    beforeEach(() => {
        environments = {
            sites: []
        };
        module = {};
        ngExpressEngineFactory = new MockNgExpressEngineFactory();
        options = {
            req: {
                header: (header: string) => requestHeaders[header]
            }
        } as RenderOptions;
        requestHeaders = {};
        serverRequest = new ServerRequest(
            environments,
            module,
            ngExpressEngineFactory.create(),
            options
        );
    });

    it('basePath DedicatedDomain', () => {
        environments.sites = [
            {
                site: {
                    basePath: '/',
                    domain: 'tork.co.uk'
                }
            }
        ];
        requestHeaders.host = 'prdlv.tork.co.uk:80';
        options.req.originalUrl = '/';

        expect(serverRequest.basePath).toBe('/');
    });

    it('basePath SharedDomain', () => {
        environments.sites = [
            {
                site: {
                    basePath: '/',
                    domain: 'tork.co.uk'
                }
            }
        ];
        requestHeaders.host = 'server.azurewebsites.net:80';
        options.req.originalUrl = '/tork.co.uk/';

        expect(serverRequest.basePath).toBe('/');
    });

    it('basePath SharedDomainDevelopment', () => {
        environments.sites = [
            {
                site: {
                    basePath: '/',
                    domain: 'tork.co.uk'
                }
            }
        ];
        requestHeaders.host = 'localhost:8000';
        options.req.originalUrl = '/tork.co.uk/';

        expect(serverRequest.basePath).toBe('/tork.co.uk/');
    });

    it('basePath / DedicatedDomain', () => {
        environments.sites = [
            {
                site: {
                    basePath: '/en',
                    domain: 'tork.ca'
                }
            }
        ];
        requestHeaders.host = 'prdlv.tork.ca:80';
        options.req.originalUrl = '/en';

        expect(serverRequest.basePath).toBe('/en/');
    });

    it('basePath / SharedDomain', () => {
        environments.sites = [
            {
                site: {
                    basePath: '/en',
                    domain: 'tork.ca'
                }
            }
        ];
        requestHeaders.host = 'server.azurewebsites.net:80';
        options.req.originalUrl = '/tork.ca/en';

        expect(serverRequest.basePath).toBe('/en/');
    });

    it('basePath / SharedDomainDevelopment', () => {
        environments.sites = [
            {
                site: {
                    basePath: '/en',
                    domain: 'tork.ca'
                }
            }
        ];
        requestHeaders.host = 'localhost:8000';
        options.req.originalUrl = '/tork.ca/en';

        expect(serverRequest.basePath).toBe('/tork.ca/en/');
    });

    it('domainType DedicatedDomain', () => {
        environments.sites = [
            {
                site: {
                    basePath: '/',
                    domain: 'tork.co.uk'
                }
            }
        ];
        requestHeaders.host = 'prdlv.tork.co.uk:80';
        options.req.originalUrl = '/';

        expect(serverRequest.domainType).toBe(ServerRequestDomainType.DedicatedDomain);
    });

    it('domainType SharedDomain notLocalhost', () => {
        environments.sites = [
            {
                site: {
                    basePath: '/',
                    domain: 'tork.co.uk'
                }
            }
        ];
        requestHeaders.host = 'server.azurewebsites.net:80';
        options.req.originalUrl = '/tork.co.uk/';

        expect(serverRequest.domainType).toBe(ServerRequestDomainType.SharedDomain);
    });

    it('domainType SharedDomain reverseProxy', () => {
        environments.sites = [
            {
                site: {
                    basePath: '/',
                    domain: 'tork.co.uk'
                }
            }
        ];
        requestHeaders.host = 'localhost:8000';
        requestHeaders['x-forwarded-for'] =
            'https://server.azurewebsites.net:80/tork.co.uk/';
        options.req.originalUrl = '/tork.co.uk/';

        expect(serverRequest.domainType).toBe(ServerRequestDomainType.SharedDomain);
    });

    it('domainType SharedDomainDevelopment', () => {
        environments.sites = [
            {
                site: {
                    basePath: '/',
                    domain: 'tork.co.uk'
                }
            }
        ];
        requestHeaders.host = 'localhost:8000';
        options.req.originalUrl = '/tork.co.uk/';

        expect(serverRequest.domainType).toBe(
            ServerRequestDomainType.SharedDomainDevelopment
        );
    });

    it('environment localhost domain', () => {
        environments.sites = [
            {
                site: {
                    basePath: '/',
                    domain: 'tork.co.uk'
                }
            },
            {
                site: {
                    basePath: '/',
                    domain: 'torkusa.com'
                }
            }
        ];
        requestHeaders.host = 'localhost:8000';
        options.req.originalUrl = '/tork.co.uk/';

        expect(serverRequest.environment).toEqual({
            site: {
                basePath: '/',
                domain: 'tork.co.uk'
            }
        });
    });

    it('environment localhost basePath', () => {
        environments.sites = [
            {
                site: {
                    basePath: '/en',
                    domain: 'tork.ca'
                }
            },
            {
                site: {
                    basePath: '/fr',
                    domain: 'tork.ca'
                }
            }
        ];
        requestHeaders.host = 'localhost:8000';
        options.req.originalUrl = '/tork.ca/fr';

        expect(serverRequest.environment).toEqual({
            site: {
                basePath: '/fr',
                domain: 'tork.ca'
            }
        });
    });

    it('environment localhost /', () => {
        environments.sites = [
            {
                site: {
                    basePath: '/en/',
                    domain: 'tork.ca'
                }
            },
            {
                site: {
                    basePath: '/fr/',
                    domain: 'tork.ca'
                }
            }
        ];
        requestHeaders.host = 'localhost:8000';
        options.req.originalUrl = '/tork.ca/fr';

        expect(serverRequest.environment).toEqual({
            site: {
                basePath: '/fr/',
                domain: 'tork.ca'
            }
        });
    });

    it('environment localhost / missing', () => {
        environments.sites = [
            {
                site: {
                    basePath: '/',
                    domain: 'tork.co.uk'
                }
            },
            {
                site: {
                    basePath: '/',
                    domain: 'torkusa.com'
                }
            }
        ];
        requestHeaders.host = 'localhost:8000';
        options.req.originalUrl = '/torkusa.com';

        expect(serverRequest.environment).toEqual({
            site: {
                basePath: '/',
                domain: 'torkusa.com'
            }
        });
    });

    it('environment localhost case', () => {
        environments.sites = [
            {
                site: {
                    basePath: '/eN',
                    domain: 'TorK.Ca'
                }
            },
            {
                site: {
                    basePath: '/fR',
                    domain: 'TorK.Ca'
                }
            }
        ];
        requestHeaders.host = 'localhost:8000';
        options.req.originalUrl = '/tORk.cA/Fr';

        expect(serverRequest.environment).toEqual({
            site: {
                basePath: '/fR',
                domain: 'TorK.Ca'
            }
        });
    });

    it('environment prdlv domain', () => {
        environments.sites = [
            {
                site: {
                    basePath: '/',
                    domain: 'tork.co.uk'
                }
            },
            {
                site: {
                    basePath: '/',
                    domain: 'torkusa.com'
                }
            }
        ];
        requestHeaders.host = 'prdlv.tork.co.uk:80';
        options.req.originalUrl = '/';

        expect(serverRequest.environment).toEqual({
            site: {
                basePath: '/',
                domain: 'tork.co.uk'
            }
        });
    });

    it('environment prdlv basePath', () => {
        environments.sites = [
            {
                site: {
                    basePath: '/en',
                    domain: 'tork.ca'
                }
            },
            {
                site: {
                    basePath: '/fr',
                    domain: 'tork.ca'
                }
            }
        ];
        requestHeaders.host = 'prdlv.tork.ca:80';
        options.req.originalUrl = '/fr';

        expect(serverRequest.environment).toEqual({
            site: {
                basePath: '/fr',
                domain: 'tork.ca'
            }
        });
    });

    it('environment prdlv /', () => {
        environments.sites = [
            {
                site: {
                    basePath: '/en/',
                    domain: 'tork.ca'
                }
            },
            {
                site: {
                    basePath: '/fr/',
                    domain: 'tork.ca'
                }
            }
        ];
        requestHeaders.host = 'prdlv.tork.ca:80';
        options.req.originalUrl = '/en';

        expect(serverRequest.environment).toEqual({
            site: {
                basePath: '/en/',
                domain: 'tork.ca'
            }
        });
    });

    it('environment prdlv / missing', () => {
        environments.sites = [
            {
                site: {
                    basePath: '/',
                    domain: 'tork.co.uk'
                }
            },
            {
                site: {
                    basePath: '/',
                    domain: 'torkusa.com'
                }
            }
        ];
        requestHeaders.host = 'prdlv.torkusa.com:80';
        options.req.originalUrl = '';

        expect(serverRequest.environment).toEqual({
            site: {
                basePath: '/',
                domain: 'torkusa.com'
            }
        });
    });

    it('environment prdlv case', () => {
        environments.sites = [
            {
                site: {
                    basePath: '/eN',
                    domain: 'TorK.Ca'
                }
            },
            {
                site: {
                    basePath: '/fR',
                    domain: 'TorK.Ca'
                }
            }
        ];
        requestHeaders.host = 'prdlv.tORk.cA:80';
        options.req.originalUrl = '/Fr';

        expect(serverRequest.environment).toEqual({
            site: {
                basePath: '/fR',
                domain: 'TorK.Ca'
            }
        });
    });

    it('environment longestPath', () => {
        environments.sites = [
            {
                site: {
                    domain: 'tork.ca'
                }
            },
            {
                site: {
                    basePath: '/en',
                    domain: 'tork.ca'
                }
            },
            {
                site: {
                    basePath: '/en/connect',
                    domain: 'tork.ca'
                }
            },
            {
                site: {
                    basePath: '/fr',
                    domain: 'tork.ca'
                }
            }
        ];
        requestHeaders.host = 'prdlv.tork.ca:80';
        options.req.originalUrl = '/en/connect/';

        expect(serverRequest.environment).toEqual({
            site: {
                basePath: '/en/connect',
                domain: 'tork.ca'
            }
        });
    });

    it('environment notFound', () => {
        environments.sites = [
            {
                site: {
                    basePath: '/',
                    domain: 'tork.co.uk'
                }
            }
        ];
        requestHeaders.host = 'localhost:8000';
        options.req.originalUrl = '/torkusa.com/';

        expect(serverRequest.environment).toBeUndefined();
    });

    it('environment notFound noSites', () => {
        environments.sites = [];
        requestHeaders.host = 'localhost:8000';
        options.req.originalUrl = '/torkusa.com/';

        expect(serverRequest.environment).toBeUndefined();
    });

    it('normalizeRequest', () => {
        requestHeaders.host = 'prdlv.tork.ca:8000';
        options.req.originalUrl = '/en';

        expect(serverRequest.normalizeRequest).toEqual({
            host: 'prdlv.tork.ca',
            originalUrl: '/en'
        });
    });

    it('normalizeRequest lowercase', () => {
        requestHeaders.host = 'PRDLV.TORK.CA:8000';
        options.req.originalUrl = '/EN';

        expect(serverRequest.normalizeRequest).toEqual({
            host: 'prdlv.tork.ca',
            originalUrl: '/en'
        });
    });

    it('normalizeRequest null', () => {
        expect(serverRequest.normalizeRequest).toEqual({
            host: '',
            originalUrl: '/'
        });
    });

    it('normalizeRequest null request', () => {
        options.req = null;

        expect(serverRequest.normalizeRequest).toEqual({
            host: '',
            originalUrl: '/'
        });
    });

    it('normalizeRequest null options', () => {
        expect(
            new ServerRequest(environments, module, ngExpressEngineFactory.create(), null)
                .normalizeRequest
        ).toEqual({
            host: '',
            originalUrl: '/'
        });
    });

    it('normalizeSites', () => {
        environments.sites = [
            {
                site: {
                    domain: 'tork.ca'
                }
            },
            {
                site: {
                    basePath: '/en',
                    domain: 'tork.ca'
                }
            }
        ];

        expect(serverRequest.normalizeSites).toEqual([
            {
                basePath: '/en',
                domain: 'tork.ca',
                environment: {
                    site: {
                        basePath: '/en',
                        domain: 'tork.ca'
                    }
                }
            },
            {
                basePath: '',
                domain: 'tork.ca',
                environment: {
                    site: {
                        domain: 'tork.ca'
                    }
                }
            }
        ]);
    });

    it('normalizeSites sortBy longestDomain, longestPath', () => {
        environments.sites = [
            {
                site: {
                    domain: 'tork.ca'
                }
            },
            {
                site: {
                    basePath: '/en',
                    domain: 'tork.ca'
                }
            },
            {
                site: {
                    basePath: '/en/connect',
                    domain: 'tork.ca'
                }
            },
            {
                site: {
                    basePath: '/fr',
                    domain: 'tork.ca'
                }
            }
        ];

        expect(serverRequest.normalizeSites).toEqual([
            {
                basePath: '/en/connect',
                domain: 'tork.ca',
                environment: {
                    site: {
                        basePath: '/en/connect',
                        domain: 'tork.ca'
                    }
                }
            },
            {
                basePath: '/en',
                domain: 'tork.ca',
                environment: {
                    site: {
                        basePath: '/en',
                        domain: 'tork.ca'
                    }
                }
            },
            {
                basePath: '/fr',
                domain: 'tork.ca',
                environment: {
                    site: {
                        basePath: '/fr',
                        domain: 'tork.ca'
                    }
                }
            },
            {
                basePath: '',
                domain: 'tork.ca',
                environment: {
                    site: {
                        domain: 'tork.ca'
                    }
                }
            }
        ]);
    });

    it('normalizeSites lowercase', () => {
        environments.sites = [
            {
                site: {
                    basePath: '/EN',
                    domain: 'TORK.CA'
                }
            }
        ];

        expect(serverRequest.normalizeSites).toEqual([
            {
                basePath: '/en',
                domain: 'tork.ca',
                environment: {
                    site: {
                        basePath: '/EN',
                        domain: 'TORK.CA'
                    }
                }
            }
        ]);
    });

    it('normalizeSites /', () => {
        environments.sites = [
            {
                site: {
                    basePath: '/en/',
                    domain: 'tork.ca'
                }
            }
        ];

        expect(serverRequest.normalizeSites).toEqual([
            {
                basePath: '/en',
                domain: 'tork.ca',
                environment: {
                    site: {
                        basePath: '/en/',
                        domain: 'tork.ca'
                    }
                }
            }
        ]);
    });

    it('normalizeSites null', () => {
        environments.sites = [
            {
                site: {}
            }
        ];

        expect(serverRequest.normalizeSites).toEqual([
            {
                basePath: '',
                domain: '',
                environment: {
                    site: {}
                }
            }
        ]);
    });

    it('render() DedicatedDomain', () => {
        environments.sites = [
            {
                site: {
                    basePath: '/',
                    domain: 'tork.co.uk'
                }
            }
        ];
        requestHeaders.host = 'prdlv.tork.co.uk:80';
        options.req.originalUrl = '/';
        options.req.url = '/';

        serverRequest.render('index', undefined);

        expect(ngExpressEngineFactory.setupCalls.length).toBe(1);
        expect(ngExpressEngineFactory.engineCalls.length).toBe(1);
        expect(ngExpressEngineFactory.engineCalls[0].filePath).toBe('index');
        expect(options.req.originalUrl).toBe('/');
        expect(options.req.url).toBe('/');
    });

    it('render() SharedDomain', () => {
        environments.sites = [
            {
                site: {
                    basePath: '/',
                    domain: 'tork.co.uk'
                }
            }
        ];
        requestHeaders.host = 'server.azurewebsites.net:80';
        options.req.originalUrl = '/tork.co.uk/';
        options.req.url = '/tork.co.uk/';

        serverRequest.render('index', undefined);

        expect(ngExpressEngineFactory.setupCalls.length).toBe(1);
        expect(ngExpressEngineFactory.engineCalls.length).toBe(1);
        expect(ngExpressEngineFactory.engineCalls[0].filePath).toBe('index');
        expect(options.req.originalUrl).toBe('/');
        expect(options.req.url).toBe('/');
    });

    it('replaceEnvironmentVariables()', () => {
        environments.sites = [
            {
                site: {
                    basePath: '/en',
                    domain: 'tork.ca',
                    htmlLang: 'en-ca'
                },
                StaticDirectory: '/assets/'
            }
        ];
        requestHeaders.host = 'prdlv.tork.ca:80';
        options.req.originalUrl = '/en';

        expect(
            serverRequest.replaceEnvironmentVariables(
                '<!DOCTYPE html><html lang="#{site.htmlLang}"><base href="#{site.basePath}">' +
                    '<link rel="stylesheet" href="#{StaticDirectory}css/all.css">' +
                    '<link rel="shortcut icon" href="#{StaticDirectory}image/favicon.ico">'
            )
        ).toBe(
            '<!DOCTYPE html><html lang="en-ca"><base href="/en/">' +
                '<link rel="stylesheet" href="/assets/css/all.css">' +
                '<link rel="shortcut icon" href="/assets/image/favicon.ico">'
        );
    });

    it('replaceEnvironmentVariables() SharedDomainDevelopment', () => {
        environments.sites = [
            {
                site: {
                    basePath: '/',
                    domain: 'tork.co.uk'
                }
            }
        ];
        requestHeaders.host = 'localhost:8000';
        options.req.originalUrl = '/tork.co.uk/';

        expect(
            serverRequest.replaceEnvironmentVariables('<base href="#{site.basePath}">')
        ).toBe('<base href="/tork.co.uk/">');
    });

    it('replaceEnvironmentVariables() null', () => {
        expect(serverRequest.replaceEnvironmentVariables(null)).toBe(null);
        expect(serverRequest.replaceEnvironmentVariables(undefined)).toBe(undefined);
    });
});
