import { BaseServer } from '../../server/BaseServer';

/**
 * Mock {@link BaseServer} for unit testing.
 */
export default class MockServer extends BaseServer<any> {
    protected serveStatic() {
        // Do nothing.
    }
}
