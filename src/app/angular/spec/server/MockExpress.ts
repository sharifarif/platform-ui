import * as express from 'express';
import * as http from 'http';

/**
 * Mock Express.js server for unit testing.
 */
export default class MockExpress {
    request: express.Request;
    response: express.Response;

    engine(ext: string, fn: Function): express.Application {
        return this as any;
    }

    set(setting: string, val: any): express.Application {
        return this as any;
    }

    get(setting: string): any {
        return undefined;
    }

    param(): this {
        return this;
    }

    path(): string {
        return undefined;
    }

    enabled(setting: string): boolean {
        return undefined;
    }

    disabled(setting: string): boolean {
        return undefined;
    }

    enable(setting: string): express.Application {
        return this as any;
    }

    disable(setting: string): express.Application {
        return this as any;
    }

    configure(): express.Application {
        return this as any;
    }

    render(): express.Application {
        return this as any;
    }

    listen(): http.Server {
        return undefined;
    }
}
