import { NgSetupOptions, RenderOptions } from '@nguniversal/express-engine';

import { ExpressEngine } from '../../server/express/ExpressEngine';
import { ExpressEngineCallback } from '../../server/express/ExpressEngineCallback';

export default class MockNgExpressEngineFactory {
    setupCalls: NgSetupOptions[] = [];
    engineCalls: {
        filePath: string;
        options: RenderOptions;
        callback: ExpressEngineCallback;
    }[] = [];

    create() {
        return (setupOptions: NgSetupOptions) => {
            this.setupCalls.push(setupOptions);

            return (
                filePath: string,
                options: RenderOptions,
                callback: ExpressEngineCallback
            ) => {
                this.engineCalls.push({
                    filePath: filePath,
                    options: options,
                    callback: callback
                });
            };
        };
    }
}
