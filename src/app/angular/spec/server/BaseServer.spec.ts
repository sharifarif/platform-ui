import { IServerEnvironments } from '../../environment/server/IServerEnvironments';
import MockExpress from './MockExpress';
import MockNgExpressEngineFactory from './MockNgExpressEngineFactory';
import MockServer from './MockServer';

describe('BaseServer', () => {
    var express: MockExpress,
        module: any,
        ngExpressEngineFactory: MockNgExpressEngineFactory;

    beforeEach(() => {
        express = new MockExpress();
        module = {};
        ngExpressEngineFactory = new MockNgExpressEngineFactory();
    });

    function createServer(environments: IServerEnvironments<any> = { sites: [] }) {
        return new MockServer(
            environments,
            express as any,
            module,
            ngExpressEngineFactory.create()
        );
    }

    it('environments default', () => {
        expect(
            createServer({
                default: {
                    site: {
                        basePath: '/'
                    },
                    StaticDirectory: '/assets/'
                },
                sites: [
                    {
                        site: {
                            basePath: '/en',
                            domain: 'tork.ca',
                            htmlLang: 'en-ca',
                            publicationId: 'tcm:0-133-1'
                        }
                    },
                    {
                        site: {
                            domain: 'tork.co.uk',
                            htmlLang: 'en-gb',
                            publicationId: 'tcm:0-140-1'
                        }
                    }
                ]
            }).environments
        ).toEqual({
            default: {
                site: {
                    basePath: '/'
                },
                StaticDirectory: '/assets/'
            },
            sites: [
                {
                    site: {
                        basePath: '/en',
                        domain: 'tork.ca',
                        htmlLang: 'en-ca',
                        publicationId: 'tcm:0-133-1'
                    },
                    StaticDirectory: '/assets/'
                },
                {
                    site: {
                        basePath: '/',
                        domain: 'tork.co.uk',
                        htmlLang: 'en-gb',
                        publicationId: 'tcm:0-140-1'
                    },
                    StaticDirectory: '/assets/'
                }
            ]
        });
    });

    it('environments null default', () => {
        expect(
            createServer({
                sites: [
                    {
                        site: {
                            domain: 'tork.co.uk',
                            htmlLang: 'en-gb',
                            publicationId: 'tcm:0-140-1'
                        }
                    }
                ]
            }).environments
        ).toEqual({
            sites: [
                {
                    site: {
                        domain: 'tork.co.uk',
                        htmlLang: 'en-gb',
                        publicationId: 'tcm:0-140-1'
                    }
                }
            ]
        });
    });

    it('environments null', () => {
        expect(createServer({} as any).environments).toEqual({
            sites: []
        });
    });

    it('port default', () => {
        expect(createServer().port).toBe(8000);
    });

    it('port process.env.PORT', () => {
        process.env.PORT = '12345';

        expect(createServer().port).toBe(12345);

        delete process.env.PORT;
    });
});
