import { dotNotation } from '../../server/dotNotation';

describe('dotNotation()', () => {
    it('object', () => {
        expect(
            dotNotation({
                awesome: {
                    sauce: 'awesome sauce',
                    totallySauce: 'awesome totallySauce'
                },
                totally: {
                    awesome: {
                        totally: {
                            sauce: 'totally awesome totally sauce'
                        }
                    }
                }
            })
        ).toEqual({
            'awesome.sauce': 'awesome sauce',
            'awesome.totallySauce': 'awesome totallySauce',
            'totally.awesome.totally.sauce': 'totally awesome totally sauce'
        });
    });

    it('array', () => {
        expect(dotNotation(['awesome', 'sauce'])).toEqual({
            0: 'awesome',
            1: 'sauce'
        });
    });

    it('number', () => {
        expect(dotNotation(-0.25)).toBe(-0.25);

        expect(dotNotation(0)).toBe(0);
    });

    it('string', () => {
        expect(dotNotation('awesome')).toBe('awesome');

        expect(dotNotation('')).toBe('');
    });

    it('null', () => {
        expect(dotNotation(null)).toBe(null);
        expect(dotNotation(undefined)).toBe(undefined);
    });

    it('object with types', () => {
        expect(
            dotNotation({
                object: {
                    awesome: {
                        sauce: 'awesome sauce'
                    }
                },
                array: ['awesome', 'sauce'],
                number: -0.25,
                string: 'awesome',
                null: null,
                undefined: undefined
            })
        ).toEqual({
            'object.awesome.sauce': 'awesome sauce',
            'array.0': 'awesome',
            'array.1': 'sauce',
            number: -0.25,
            string: 'awesome',
            null: null,
            undefined: undefined
        });
    });
});
