import { Subject } from 'rxjs';

import { AuthService } from '../../../login/adal/Auth.service';
import { IAuthEnvironment } from '../../../login/adal/IAuthEnvironment';

/**
 * Mock {@link AuthService} for unit testing.
 */
export default class MockAuthService extends AuthService {
    tokenObservable = new Subject<string>();
    user: adal.User;
    private _loggedIn: boolean;

    constructor() {
        super({} as IAuthEnvironment);
    }

    getToken() {
        return this.tokenObservable;
    }

    get loggedIn(): boolean {
        return this._loggedIn;
    }

    set loggedIn(loggedIn: boolean) {
        this._loggedIn = loggedIn;
    }

    login() {
        // Do nothing.
    }

    logout() {
        // Do nothing.
    }

    protected getAzureAdApplicationId() {
        return 'test';
    }

    protected processLoginLogout() {
        // Do nothing.
    }
}
