import MockAuthService from './MockAuth.service';

describe('AuthService', () => {
    var authService: MockAuthService;

    beforeEach(() => {
        authService = new MockAuthService();
    });

    it('getUserFirstName() firstName', () => {
        authService.user = {
            userName: 'awesome@sauce',
            profile: {
                given_name: 'Awesome',
                name: 'Awesome Sauce'
            }
        };

        expect(authService.getUserFirstName()).toBe('Awesome');
    });

    it('getUserFirstName() fallback 1: fullName', () => {
        authService.user = {
            userName: 'awesome@sauce',
            profile: {
                name: 'Awesome Sauce'
            }
        };

        expect(authService.getUserFirstName()).toBe('Awesome Sauce');
    });

    it('getUserFirstName() fallback 2: userName', () => {
        authService.user = {
            userName: 'awesome@sauce',
            profile: null
        };

        expect(authService.getUserFirstName()).toBe('awesome@sauce');
    });

    it('getUserFirstName() null', () => {
        expect(authService.getUserFirstName()).toBeUndefined();
    });

    it('getUserFullName() fullName', () => {
        authService.user = {
            userName: 'awesome@sauce',
            profile: {
                given_name: 'Awesome',
                name: 'Awesome Sauce'
            }
        };

        expect(authService.getUserFullName()).toBe('Awesome Sauce');
    });

    it('getUserFullName() fallback 1: firstName', () => {
        authService.user = {
            userName: 'awesome@sauce',
            profile: {
                given_name: 'Awesome'
            }
        };

        expect(authService.getUserFullName()).toBe('Awesome');
    });

    it('getUserFullName() fallback 2: userName', () => {
        authService.user = {
            userName: 'awesome@sauce',
            profile: null
        };

        expect(authService.getUserFullName()).toBe('awesome@sauce');
    });

    it('getUserFullName() null', () => {
        expect(authService.getUserFullName()).toBeUndefined();
    });
});
