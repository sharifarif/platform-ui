import {
    FakeMissingTranslationHandler,
    TranslateDefaultParser,
    TranslateFakeCompiler,
    TranslateFakeLoader,
    TranslateService
} from '@ngx-translate/core';
import get from 'lodash/get';
import { Observable, of } from 'rxjs';

const translations = require('../../i18n/en.json');

export default class MockTranslateService extends TranslateService {
    constructor() {
        super(
            {} as any,
            new TranslateFakeLoader(),
            new TranslateFakeCompiler(),
            new TranslateDefaultParser(),
            new FakeMissingTranslationHandler()
        );
    }

    get(key: string | string[], interpolateParams?: Object): Observable<string | any> {
        return of(this.instant(key, interpolateParams));
    }

    instant(key: string | string[], interpolateParams?: Object): string | any {
        return get(translations, key);
    }
}
