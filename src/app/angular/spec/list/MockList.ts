import { BaseList } from '../../list/BaseList';

/**
 * Mock {@link BaseList} for unit testing.
 */
export default class MockList extends BaseList<any> {}
