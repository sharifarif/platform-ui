import Search from '../../list/Search';

describe('Search', () => {
    it('getTokensLowerCase()', () => {
        expect(Search.getTokensLowerCase('AweSome Sauce')).toEqual(['awesome', 'sauce']);
    });

    it('getTokensLowerCase() whitespace', () => {
        expect(Search.getTokensLowerCase('awesome \t\r\nsauce')).toEqual([
            'awesome',
            'sauce'
        ]);
    });

    it('getTokensLowerCase() reservedCharactersInHtml', () => {
        expect(Search.getTokensLowerCase('"awesome" "sauce"')).toEqual([
            'awesome',
            'sauce'
        ]);
        expect(Search.getTokensLowerCase("'awesome' 'sauce'")).toEqual([
            'awesome',
            'sauce'
        ]);
        expect(Search.getTokensLowerCase('awesome & sauce')).toEqual([
            'awesome',
            'sauce'
        ]);
        expect(Search.getTokensLowerCase('<awesome> <sauce>')).toEqual([
            'awesome',
            'sauce'
        ]);
    });

    it('getTokensLowerCase() listSeparators', () => {
        expect(Search.getTokensLowerCase('awesome, sauce')).toEqual(['awesome', 'sauce']);
        expect(Search.getTokensLowerCase('awesome; sauce')).toEqual(['awesome', 'sauce']);
    });

    it('getTokensLowerCase() noEmptyTokens', () => {
        expect(Search.getTokensLowerCase('      ')).toEqual([]);
        expect(Search.getTokensLowerCase('      awesome      ')).toEqual(['awesome']);
        expect(Search.getTokensLowerCase('      awesome      sauce      ')).toEqual([
            'awesome',
            'sauce'
        ]);
    });

    it('getTokensLowerCase() empty', () => {
        expect(Search.getTokensLowerCase(null)).toBeUndefined();
        expect(Search.getTokensLowerCase(undefined)).toBeUndefined();
        expect(Search.getTokensLowerCase('')).toBeUndefined();
    });

    it('getTokenCount()', () => {
        expect(Search.getTokenCount(['awesome', 'sauce'], '')).toBe(0);

        expect(Search.getTokenCount(['awesome', 'sauce'], 'Awesome')).toBe(1);
        expect(Search.getTokenCount(['awesome', 'sauce'], 'Sauce')).toBe(1);

        expect(Search.getTokenCount(['awesome', 'sauce'], 'Awesome Sauce')).toBe(2);
    });

    it('hasToken() string', () => {
        expect(Search.hasToken('true', true)).toBe(true);
        expect(Search.hasToken('false', false)).toBe(true);

        expect(Search.hasToken('true', false)).toBe(false);
        expect(Search.hasToken('false', true)).toBe(false);
    });

    it('hasToken() boolean', () => {
        expect(Search.hasToken('true', true)).toBe(true);
        expect(Search.hasToken('false', false)).toBe(true);

        expect(Search.hasToken('true', false)).toBe(false);
        expect(Search.hasToken('false', true)).toBe(false);
    });

    it('hasToken() number', () => {
        expect(Search.hasToken('10', 10)).toBe(true);

        expect(Search.hasToken('10', 1)).toBe(false);
    });

    it('hasToken() array', () => {
        expect(Search.hasToken('awesome', ['Awesome Sauce'])).toBe(true);
        expect(Search.hasToken('sauce', ['Awesome Sauce'])).toBe(true);

        expect(Search.hasToken('awesome', ['Sauce'])).toBe(false);
        expect(Search.hasToken('sauce', ['Awesome'])).toBe(false);
    });

    it('hasToken() object', () => {
        expect(
            Search.hasToken('sauce', {
                awesome: 'Sauce'
            })
        ).toBe(true);
    });

    it('hasToken() object ignoreKeyValues', () => {
        expect(
            Search.hasToken('awesome', {
                awesome: 'Sauce'
            })
        ).toBe(false);
    });

    it('hasToken() null', () => {
        expect(Search.hasToken('awesome', null)).toBe(false);
        expect(Search.hasToken('awesome', undefined)).toBe(false);
    });

    it('search()', () => {
        var awesome = {
            name: 'Totally Awesome'
        };
        var sauce = {
            name: 'Totally Sauce'
        };
        var awesomeSauce = {
            name: 'Awesome Sauce'
        };

        expect(Search.search('awesome', [awesome, sauce, awesomeSauce])).toEqual([
            awesome,
            awesomeSauce
        ]);
        expect(Search.search('sauce', [awesome, sauce, awesomeSauce])).toEqual([
            sauce,
            awesomeSauce
        ]);

        expect(Search.search('awesome sauce', [awesome, sauce, awesomeSauce])).toEqual([
            awesomeSauce
        ]);
    });

    it('search() query=blank', () => {
        expect(Search.search('', ['awesome', 'sauce'])).toEqual(['awesome', 'sauce']);
        expect(Search.search('      ', ['awesome', 'sauce'])).toEqual([
            'awesome',
            'sauce'
        ]);
    });

    it('search() query=null', () => {
        expect(Search.search(null, ['awesome', 'sauce'])).toEqual(['awesome', 'sauce']);
        expect(Search.search(undefined, ['awesome', 'sauce'])).toEqual([
            'awesome',
            'sauce'
        ]);
    });

    it('search() items=null', () => {
        expect(Search.search('awesome', null)).toBeUndefined();
        expect(Search.search('awesome', undefined)).toBeUndefined();
    });

    it('search() query=null, items=null', () => {
        expect(Search.search(null, null)).toBeUndefined();
        expect(Search.search(undefined, undefined)).toBeUndefined();
    });
});
