import MockList from './MockList';

describe('BaseList', () => {
    var list: MockList;

    beforeEach(() => {
        list = new MockList();
    });

    it('hasField() always', () => {
        list.items = [
            {
                name: 'awesome',
                description: 'very awesome'
            },
            {
                name: 'sauce',
                description: 'very sauce'
            }
        ];

        expect(list.hasField('description')).toBe(true);
    });

    it('hasField() once', () => {
        list.items = [
            {
                name: 'awesome'
            },
            {
                name: 'sauce',
                description: 'very sauce'
            }
        ];

        expect(list.hasField('description')).toBe(true);
    });

    it('hasField() never', () => {
        list.items = [
            {
                name: 'awesome'
            },
            {
                name: 'sauce'
            }
        ];

        expect(list.hasField('description')).toBe(false);
    });

    it('hasField() searchResults', () => {
        var awesome = {
            name: 'awesome',
            description: 'very awesome'
        };
        var sauce = {
            name: 'sauce'
        };

        list.searchResults = [awesome];
        list.items = [awesome, sauce];

        expect(list.hasField('description')).toBe(true);

        list.searchResults = [sauce];

        expect(list.hasField('description')).toBe(false);

        list.searchResults = [awesome, sauce];

        expect(list.hasField('description')).toBe(true);
    });

    it('hasField() false', () => {
        list.items = [
            {
                name: 'awesome',
                description: false
            },
            {
                name: 'sauce',
                description: false
            }
        ];

        expect(list.hasField('description')).toBe(true);
    });

    it('hasField() 0', () => {
        list.items = [
            {
                name: 'awesome',
                description: 0
            },
            {
                name: 'sauce',
                description: 0
            }
        ];

        expect(list.hasField('description')).toBe(true);
    });

    it('hasField() emptyString', () => {
        list.items = [
            {
                name: 'awesome',
                description: ''
            },
            {
                name: 'sauce',
                description: ''
            }
        ];

        expect(list.hasField('description')).toBe(false);
    });

    it('hasField() null', () => {
        list.items = [
            {
                name: 'awesome',
                description: null
            },
            {
                name: 'sauce',
                description: null
            }
        ];

        expect(list.hasField('description')).toBe(false);
    });

    it('hasField() undefined', () => {
        list.items = [
            {
                name: 'awesome',
                description: undefined
            },
            {
                name: 'sauce',
                description: undefined
            }
        ];

        expect(list.hasField('description')).toBe(false);
    });

    it('hasField() nullItem', () => {
        list.items = [
            {
                name: 'awesome',
                description: 'totally awesome'
            },
            null
        ];

        expect(list.hasField('description')).toBe(true);
    });

    it('hasField() nullItem neverField', () => {
        list.items = [
            {
                name: 'awesome'
            },
            null
        ];

        expect(list.hasField('description')).toBe(false);
    });
});
