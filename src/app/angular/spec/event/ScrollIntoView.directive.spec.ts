import { ScrollIntoViewDirective } from '../../event/ScrollIntoView.directive';

describe('ScrollIntoViewDirective', () => {
    var scrollIntoView: ScrollIntoViewDirective,
        element: HTMLElement,
        scrollIntoViewMethod: keyof HTMLElement;

    beforeEach(() => {
        element = document.createElement('div');
        scrollIntoView = new ScrollIntoViewDirective({
            nativeElement: element
        });

        jasmine.clock().install();

        if ((element as any).scrollIntoViewIfNeeded) {
            scrollIntoViewMethod = 'scrollIntoViewIfNeeded' as any;
        } else {
            scrollIntoViewMethod = 'scrollIntoView';
        }
        spyOn(element, scrollIntoViewMethod);
    });

    afterEach(() => {
        jasmine.clock().uninstall();
    });

    it('scrollIntoView()', () => {
        scrollIntoView.scrollIntoView();

        jasmine.clock().tick(1);
        expect(element[scrollIntoViewMethod]).toHaveBeenCalledTimes(1);
    });

    it('scrollIntoView() 2x', () => {
        scrollIntoView.scrollIntoView();
        scrollIntoView.scrollIntoView();

        jasmine.clock().tick(1);
        expect(element[scrollIntoViewMethod]).toHaveBeenCalledTimes(1);
    });

    it('scrollIntoView() false', () => {
        scrollIntoView.platformScrollIntoView = false;
        scrollIntoView.scrollIntoView();

        jasmine.clock().tick(1);
        expect(element[scrollIntoViewMethod]).not.toHaveBeenCalled();
    });

    it('scrollIntoView() false 2x', () => {
        scrollIntoView.platformScrollIntoView = false;
        scrollIntoView.scrollIntoView();

        scrollIntoView.platformScrollIntoView = false;
        scrollIntoView.scrollIntoView();

        jasmine.clock().tick(1);
        expect(element[scrollIntoViewMethod]).not.toHaveBeenCalled();
    });

    it('scrollIntoView() false then true', () => {
        scrollIntoView.platformScrollIntoView = false;
        scrollIntoView.scrollIntoView();

        scrollIntoView.platformScrollIntoView = true;
        scrollIntoView.scrollIntoView();

        jasmine.clock().tick(1);
        expect(element[scrollIntoViewMethod]).toHaveBeenCalledTimes(1);
    });

    it('scrollIntoView() true then false', () => {
        scrollIntoView.platformScrollIntoView = true;
        scrollIntoView.scrollIntoView();

        scrollIntoView.platformScrollIntoView = false;
        scrollIntoView.scrollIntoView();

        jasmine.clock().tick(1);
        expect(element[scrollIntoViewMethod]).toHaveBeenCalledTimes(1);
    });
});
