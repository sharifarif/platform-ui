import { HoverItems } from '../../../event/hover/HoverItems';
import MockHoverItems from './MockHoverItems';
import MockMouseLocationService from './MockMouseLocation.service';

describe('HoverItems', () => {
    var hoverItems: MockHoverItems,
        mouseLocation: MockMouseLocationService,
        setIntervalArguments: { callback: () => any; milliseconds: number }[];

    beforeEach(() => {
        mouseLocation = new MockMouseLocationService();
        hoverItems = new MockHoverItems(mouseLocation);
        setIntervalArguments = [];

        var setInterval = (callback: () => any, milliseconds: number) => {
            setIntervalArguments.push({
                callback: callback,
                milliseconds: milliseconds
            });

            return 123;
        };

        spyOn(window, 'setInterval').and.callFake(setInterval);
        spyOn(window, 'clearInterval');
    });

    it('constructor current', done => {
        hoverItems = new MockHoverItems(
            new MockMouseLocationService(),
            'slow',
            'awesome'
        );

        hoverItems.current.subscribe(current => {
            expect(current).toBe('awesome');
            done();
        });
    });

    it('constructor current undefined', done => {
        hoverItems.current.subscribe(current => {
            expect(current).toBeUndefined();
            done();
        });
    });

    it('constructor mouseLocation', () => {
        mouseLocation.setState({
            insideViewport: false,
            x: 1,
            y: 2
        });

        expect(hoverItems.mouseLocation).toEqual({
            insideViewport: false,
            x: 1,
            y: 2
        });
    });

    it('constructor mouseLocation delayTimerIfMoved()', () => {
        spyOn(hoverItems, 'delayTimerIfMoved').and.callFake(() => {
            expect(hoverItems.mouseLocation).toEqual({
                insideViewport: false,
                x: 1,
                y: 2
            });
        });

        mouseLocation.setState({
            insideViewport: false,
            x: 1,
            y: 2
        });

        expect(hoverItems.delayTimerIfMoved).toHaveBeenCalledTimes(1);
    });

    it('setCurrent()', () => {
        var values: string[] = [];

        hoverItems.current.subscribe(current => {
            values.push(current);
        });

        hoverItems.setCurrent('awesome');

        expect(values).toEqual([undefined, 'awesome']);
        expect(hoverItems.actualCurrent).toBe('awesome');
    });

    it('setCurrent() twice', () => {
        var values: string[] = [];

        hoverItems.current.subscribe(current => {
            values.push(current);
        });

        hoverItems.setCurrent('awesome');
        hoverItems.setCurrent('sauce');

        expect(values).toEqual([undefined, 'awesome', 'sauce']);
    });

    it('setCurrent() twice noChange', () => {
        var values: string[] = [];

        hoverItems.current.subscribe(current => {
            values.push(current);
        });

        hoverItems.setCurrent('awesome');
        hoverItems.setCurrent('awesome');

        expect(values).toEqual([undefined, 'awesome']);
    });

    it('setCurrent() hadTimer', () => {
        hoverItems.actualCurrent = 'awesome';
        hoverItems.createTimer('slow');

        var values: string[] = [];

        hoverItems.current.subscribe(current => {
            values.push(current);
        });

        hoverItems.setCurrent('sauce');

        expect(values).toEqual([undefined, 'sauce']);
        expect(hoverItems.actualCurrent).toBe('sauce');
        expect(window.clearInterval).toHaveBeenCalledTimes(1);
        expect(hoverItems.timer).toBeUndefined();
    });

    it('onMouseEnter()', () => {
        var values: string[] = [];

        hoverItems.current.subscribe(current => {
            values.push(current);
        });

        hoverItems.onMouseEnter('awesome');

        expect(values).toEqual([undefined]);
        expect(hoverItems.actualCurrent).toBe('awesome');
        expect(hoverItems.timerTarget).toBe('awesome');
        expect(setIntervalArguments.length).toBe(1);
        expect(setIntervalArguments[0].milliseconds).toBe(
            HoverItems.TimeoutMilliseconds.slow
        );
    });

    it('onMouseLeave()', () => {
        var values: string[] = [];

        hoverItems.current.subscribe(current => {
            values.push(current);
        });

        hoverItems.setCurrent('awesome');

        expect(values).toEqual([undefined, 'awesome']);
        expect(hoverItems.actualCurrent).toBe('awesome');
        expect(hoverItems.timerTarget).toBeUndefined();
        expect(setIntervalArguments.length).toBe(0);

        hoverItems.onMouseLeave('awesome');

        expect(values).toEqual([undefined, 'awesome']);
        expect(hoverItems.actualCurrent).toBeUndefined();
        expect(hoverItems.timerTarget).toBeUndefined();
        expect(setIntervalArguments.length).toBe(1);
        expect(setIntervalArguments[0].milliseconds).toBe(
            HoverItems.TimeoutMilliseconds.fast
        );
    });

    it('onMouseLeave() somethingElse', () => {
        var values: string[] = [];

        hoverItems.current.subscribe(current => {
            values.push(current);
        });

        hoverItems.setCurrent('awesome');

        hoverItems.onMouseLeave('not awesome');

        expect(hoverItems.actualCurrent).toBe('awesome');
        expect(setIntervalArguments.length).toBe(0);
        expect(hoverItems.timer).toBeUndefined();
    });

    it('clearTimer()', () => {
        hoverItems.timer = 123;

        hoverItems.clearTimer();

        expect(window.clearInterval).toHaveBeenCalledTimes(1);
        expect(window.clearInterval).toHaveBeenCalledWith(123);
        expect(hoverItems.timer).toBeUndefined();
    });

    it('clearTimer() null', () => {
        hoverItems.clearTimer();

        expect(window.clearInterval).not.toHaveBeenCalled();
        expect(hoverItems.timer).toBeUndefined();
    });

    it('createTimer() slow', () => {
        hoverItems.actualCurrent = 'awesome';
        hoverItems.mouseLocation = {
            insideViewport: true,
            x: 1,
            y: 2
        };

        hoverItems.createTimer('slow');

        expect(hoverItems.timer).toBe(123);
        expect(hoverItems.timerMouseLocation).toEqual({
            insideViewport: true,
            x: 1,
            y: 2
        });
        expect(hoverItems.timerSpeed).toBe('slow');
        expect(hoverItems.timerTarget).toBe('awesome');

        expect(setIntervalArguments.length).toBe(1);
        expect(typeof setIntervalArguments[0].milliseconds).toBe('number');
        expect(setIntervalArguments[0].milliseconds).toBe(
            HoverItems.TimeoutMilliseconds.slow
        );
    });

    it('createTimer() fast', () => {
        hoverItems.createTimer('fast');

        expect(hoverItems.timerSpeed).toBe('fast');

        expect(setIntervalArguments.length).toBe(1);
        expect(typeof setIntervalArguments[0].milliseconds).toBe('number');
        expect(setIntervalArguments[0].milliseconds).toBe(
            HoverItems.TimeoutMilliseconds.fast
        );
    });

    it('createTimer() callback', () => {
        hoverItems.actualCurrent = 'awesome';
        hoverItems.mouseLocation = {
            insideViewport: true,
            x: 1,
            y: 2
        };

        hoverItems.createTimer('slow');

        var callback = setIntervalArguments[0].callback,
            values: string[] = [];

        hoverItems.current.subscribe(current => {
            values.push(current);
        });

        expect(window.clearInterval).not.toHaveBeenCalled();
        expect(values).toEqual([undefined]);

        callback();

        expect(values).toEqual([undefined, 'awesome']);
        expect(window.clearInterval).toHaveBeenCalledTimes(1);
        expect(window.clearInterval).toHaveBeenCalledWith(123);
    });

    it('createTimer() callback latest actualCurrent', () => {
        hoverItems.actualCurrent = 'awesome';
        hoverItems.mouseLocation = {
            insideViewport: true,
            x: 1,
            y: 2
        };

        hoverItems.createTimer('slow');

        var callback = setIntervalArguments[0].callback,
            values: string[] = [];

        hoverItems.current.subscribe(current => {
            values.push(current);
        });

        expect(window.clearInterval).not.toHaveBeenCalled();
        expect(values).toEqual([undefined]);

        hoverItems.actualCurrent = 'sauce';
        callback();

        expect(values).toEqual([undefined, 'sauce']);
        expect(window.clearInterval).toHaveBeenCalledTimes(1);
        expect(window.clearInterval).toHaveBeenCalledWith(123);
    });

    it('createTimer() callback outsideViewport', () => {
        hoverItems.actualCurrent = 'awesome';
        hoverItems.mouseLocation = {
            insideViewport: true,
            x: 1,
            y: 2
        };

        hoverItems.createTimer('slow');

        var callback = setIntervalArguments[0].callback,
            values: string[] = [];

        hoverItems.current.subscribe(current => {
            values.push(current);
        });

        expect(window.clearInterval).not.toHaveBeenCalled();
        expect(values).toEqual([undefined]);

        hoverItems.mouseLocation = {
            insideViewport: false,
            x: 1,
            y: 2
        };
        callback();

        expect(window.clearInterval).not.toHaveBeenCalled();
        expect(values).toEqual([undefined]);
    });

    it('delayTimer() slow', () => {
        hoverItems.createTimer('slow');

        expect(window.clearInterval).not.toHaveBeenCalled();
        expect(setIntervalArguments.length).toBe(1);

        hoverItems.delayTimer();

        expect(window.clearInterval).toHaveBeenCalledTimes(1);
        expect(window.clearInterval).toHaveBeenCalledWith(123);
        expect(setIntervalArguments.length).toBe(2);
        expect(setIntervalArguments[1].milliseconds).toBe(
            HoverItems.TimeoutMilliseconds.slow
        );
    });

    it('delayTimer() fast', () => {
        hoverItems.createTimer('fast');

        expect(window.clearInterval).not.toHaveBeenCalled();
        expect(setIntervalArguments.length).toBe(1);

        hoverItems.delayTimer();

        expect(window.clearInterval).toHaveBeenCalledTimes(1);
        expect(window.clearInterval).toHaveBeenCalledWith(123);
        expect(setIntervalArguments.length).toBe(2);
        expect(setIntervalArguments[1].milliseconds).toBe(
            HoverItems.TimeoutMilliseconds.fast
        );
    });

    it('delayTimer() noTimer', () => {
        hoverItems.delayTimer();

        expect(window.clearInterval).not.toHaveBeenCalled();
        expect(setIntervalArguments.length).toBe(0);
    });

    it('delayTimerIfMoved()', () => {
        hoverItems.mouseLocation = {
            insideViewport: true,
            x: 0,
            y: 0
        };

        hoverItems.createTimer('slow');

        spyOn(hoverItems, 'delayTimer');

        hoverItems.mouseLocation = {
            insideViewport: true,
            x: HoverItems.MouseMoveThresholdPixels,
            y: 0
        };

        hoverItems.delayTimerIfMoved();

        expect(hoverItems.delayTimer).toHaveBeenCalledTimes(1);
    });

    it('delayTimerIfMoved() notFarEnough', () => {
        hoverItems.mouseLocation = {
            insideViewport: true,
            x: 0,
            y: 0
        };

        hoverItems.createTimer('slow');

        spyOn(hoverItems, 'delayTimer');

        hoverItems.mouseLocation = {
            insideViewport: true,
            x: HoverItems.MouseMoveThresholdPixels - 1,
            y: 0
        };

        hoverItems.delayTimerIfMoved();

        expect(hoverItems.delayTimer).not.toHaveBeenCalled();
    });

    it('delayTimerIfMoved() evenFurther', () => {
        hoverItems.mouseLocation = {
            insideViewport: true,
            x: 0,
            y: HoverItems.MouseMoveThresholdPixels + 1
        };

        hoverItems.createTimer('slow');

        spyOn(hoverItems, 'delayTimer');

        hoverItems.mouseLocation = {
            insideViewport: true,
            x: 0,
            y: 0
        };

        hoverItems.delayTimerIfMoved();

        expect(hoverItems.delayTimer).toHaveBeenCalledTimes(1);
    });

    it('delayTimerIfMoved() noTimer', () => {
        hoverItems.mouseLocation = {
            insideViewport: true,
            x: 0,
            y: 0
        };

        hoverItems.createTimer('slow');
        hoverItems.clearTimer();

        spyOn(hoverItems, 'delayTimer');

        hoverItems.mouseLocation = {
            insideViewport: true,
            x: HoverItems.MouseMoveThresholdPixels,
            y: 0
        };

        hoverItems.delayTimerIfMoved();

        expect(hoverItems.delayTimer).not.toHaveBeenCalled();
    });

    it('updateTimer() noTimer actualCurrent == current', () => {
        hoverItems.updateTimer('slow');

        expect(setIntervalArguments.length).toBe(0);
        expect(window.clearInterval).not.toHaveBeenCalled();
    });

    it('updateTimer() noTimer actualCurrent != current', () => {
        hoverItems.actualCurrent = 'awesome';

        hoverItems.updateTimer('slow');

        expect(setIntervalArguments.length).toBe(1);
        expect(window.clearInterval).not.toHaveBeenCalled();
    });

    it('updateTimer() hasTimer actualCurrent == current', () => {
        hoverItems.actualCurrent = 'awesome';
        hoverItems.createTimer('slow');

        expect(setIntervalArguments.length).toBe(1);
        expect(window.clearInterval).not.toHaveBeenCalled();

        hoverItems.actualCurrent = undefined;
        hoverItems.updateTimer('slow');

        expect(hoverItems.timer).toBeUndefined();
        expect(setIntervalArguments.length).toBe(1);
        expect(window.clearInterval).toHaveBeenCalledTimes(1);
    });

    it('updateTimer() hasTimer actualCurrent == timerTarget', () => {
        hoverItems.actualCurrent = 'awesome';
        hoverItems.createTimer('slow');

        expect(setIntervalArguments.length).toBe(1);
        expect(window.clearInterval).not.toHaveBeenCalled();

        hoverItems.updateTimer('slow');

        expect(hoverItems.timerTarget).toBe('awesome');
        expect(setIntervalArguments.length).toBe(1);
        expect(window.clearInterval).not.toHaveBeenCalled();
    });

    it('updateTimer() hasTimer actualCurrent != current or timerTarget', () => {
        hoverItems.actualCurrent = 'awesome';
        hoverItems.createTimer('slow');

        expect(setIntervalArguments.length).toBe(1);
        expect(window.clearInterval).not.toHaveBeenCalled();

        hoverItems.actualCurrent = 'sauce';
        hoverItems.updateTimer('slow');

        expect(hoverItems.timerTarget).toBe('sauce');
        expect(setIntervalArguments.length).toBe(2);
        expect(window.clearInterval).toHaveBeenCalledTimes(1);
    });
});
