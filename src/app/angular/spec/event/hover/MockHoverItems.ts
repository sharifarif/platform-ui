import { BehaviorSubject } from 'rxjs';

import { HoverItems } from '../../../event/hover/HoverItems';
import { IMouseLocation } from '../../../event/hover/IMouseLocation';

/**
 * Mock {@link HoverItems} for unit testing.
 */
export default class MockHoverItems extends HoverItems<string> {
    readonly currentSubject: BehaviorSubject<string>;
    actualCurrent: string;
    mouseLocation: IMouseLocation;
    timer: NodeJS.Timer | number;
    timerMouseLocation: IMouseLocation;
    timerSpeed: 'slow' | 'fast';
    timerTarget: string;

    clearTimer() {
        return super.clearTimer();
    }

    createTimer(speed: 'slow' | 'fast') {
        return super.createTimer(speed);
    }

    delayTimer() {
        return super.delayTimer();
    }

    delayTimerIfMoved() {
        return super.delayTimerIfMoved();
    }

    updateTimer(speed: 'slow' | 'fast') {
        return super.updateTimer(speed);
    }
}
