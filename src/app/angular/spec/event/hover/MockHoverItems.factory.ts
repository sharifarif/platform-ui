import { HoverItemsFactory } from '../../../event/hover/HoverItems.factory';
import MockMouseLocationService from './MockMouseLocation.service';

/**
 * Mock {@link HoverItemsFactory} for unit testing.
 */
export default class MockHoverItemsFactory extends HoverItemsFactory {
    constructor() {
        super(new MockMouseLocationService());
    }
}
