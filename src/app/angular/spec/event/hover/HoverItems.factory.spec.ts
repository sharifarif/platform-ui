import { HoverItemsFactory } from '../../../event/hover/HoverItems.factory';
import MockMouseLocationService from './MockMouseLocation.service';

describe('HoverItemsFactory', () => {
    var hoverItemsFactory: HoverItemsFactory, mouseLocation: MockMouseLocationService;

    beforeEach(() => {
        mouseLocation = new MockMouseLocationService();
        hoverItemsFactory = new HoverItemsFactory(mouseLocation);
    });

    it('createHoverItems() default', done => {
        var hoverItems = hoverItemsFactory.createHoverItems();

        expect(hoverItems.speed).toBe('slow');

        hoverItems.current.subscribe(current => {
            expect(current).toBeUndefined();
            done();
        });
    });

    it('createHoverItems() custom', done => {
        var hoverItems = hoverItemsFactory.createHoverItems('fast', 'awesome');

        expect(hoverItems.speed).toBe('fast');

        hoverItems.current.subscribe(current => {
            expect(current).toBe('awesome');
            done();
        });
    });
});
