import { IMouseLocation } from '../../../event/hover/IMouseLocation';
import MockMouseLocationService from './MockMouseLocation.service';
import { MouseLocationService } from '../../../event/hover/MouseLocation.service';

describe('MouseLocationService', () => {
    var mouseLocation: MockMouseLocationService;

    beforeEach(() => {
        mouseLocation = new MockMouseLocationService();
    });

    afterEach(() => {
        mouseLocation.ngOnDestroy();
    });

    it('distance()', () => {
        expect(
            MouseLocationService.distance(
                {
                    x: 0,
                    y: 0
                },
                {
                    x: 3,
                    y: 4
                }
            )
        ).toBe(5);

        expect(
            MouseLocationService.distance(
                {
                    x: 3,
                    y: 4
                },
                {
                    x: 0,
                    y: 0
                }
            )
        ).toBe(5);
    });

    it('distance() null', () => {
        expect(MouseLocationService.distance(undefined, undefined)).toBeUndefined();
        expect(MouseLocationService.distance(null, null)).toBeUndefined();
        expect(MouseLocationService.distance({}, {})).toBeUndefined();

        expect(
            MouseLocationService.distance(
                {
                    x: 3,
                    y: 4
                },
                {}
            )
        ).toBeUndefined();
        expect(
            MouseLocationService.distance(
                {},
                {
                    x: 3,
                    y: 4
                }
            )
        ).toBeUndefined();
    });

    it('getImmutable()', () => {
        mouseLocation.insideViewport = true;
        mouseLocation.x = 1;
        mouseLocation.y = 1;

        const immutable = mouseLocation.getImmutable();

        mouseLocation.insideViewport = false;
        mouseLocation.x = 2;
        mouseLocation.y = 2;

        expect(immutable).toEqual({
            insideViewport: true,
            x: 1,
            y: 1
        });
    });

    it('getImmutable() initial', () => {
        const immutable = mouseLocation.getImmutable();

        mouseLocation.insideViewport = true;
        mouseLocation.x = 1;
        mouseLocation.y = 1;

        expect(immutable).toEqual({
            insideViewport: undefined,
            x: undefined,
            y: undefined
        });
    });

    it('onMouseMove()', () => {
        mouseLocation.onMouseMove({
            clientX: 1,
            clientY: 2
        } as MouseEvent);

        expect(mouseLocation.insideViewport).toBe(true);
        expect(mouseLocation.x).toBe(1);
        expect(mouseLocation.y).toBe(2);
    });

    it('onMouseOut() toElement', () => {
        mouseLocation.onMouseOut({
            relatedTarget: {}
        } as MouseEvent);

        expect(mouseLocation.insideViewport).toBe(true);
    });

    it('onMouseOut() relatedTarget', () => {
        mouseLocation.onMouseOut({
            relatedTarget: {}
        } as MouseEvent);

        expect(mouseLocation.insideViewport).toBe(true);
    });

    it('onMouseOut() no toElement or relatedTarget', () => {
        mouseLocation.onMouseOut({} as MouseEvent);

        expect(mouseLocation.insideViewport).toBe(false);
    });

    it('setState()', () => {
        mouseLocation.setState({
            insideViewport: true,
            x: 1,
            y: 1
        });

        expect(mouseLocation.insideViewport).toBe(true);
        expect(mouseLocation.x).toBe(1);
        expect(mouseLocation.y).toBe(1);
    });

    it('observable initial', () => {
        var locations: IMouseLocation[] = [];

        mouseLocation.observable.subscribe(location => {
            locations.push(location);
        });

        expect(locations).toEqual([
            {
                insideViewport: undefined,
                x: undefined,
                y: undefined
            }
        ]);
    });

    it('observable mouseMove', () => {
        var locations: IMouseLocation[] = [];

        mouseLocation.observable.subscribe(location => {
            locations.push(location);
        });

        mouseLocation.onMouseMove({
            clientX: 1,
            clientY: 2
        } as MouseEvent);

        expect(locations).toEqual([
            {
                insideViewport: undefined,
                x: undefined,
                y: undefined
            },
            {
                insideViewport: true,
                x: 1,
                y: 2
            }
        ]);
    });

    it('observable mouseOut', () => {
        var locations: IMouseLocation[] = [];

        mouseLocation.observable.subscribe(location => {
            locations.push(location);
        });

        mouseLocation.onMouseOut({} as MouseEvent);

        expect(locations).toEqual([
            {
                insideViewport: undefined,
                x: undefined,
                y: undefined
            },
            {
                insideViewport: false,
                x: undefined,
                y: undefined
            }
        ]);
    });

    it('observable mouseMove + mouseOut', () => {
        var locations: IMouseLocation[] = [];

        mouseLocation.observable.subscribe(location => {
            locations.push(location);
        });

        mouseLocation.onMouseMove({
            clientX: 1,
            clientY: 2
        } as MouseEvent);

        mouseLocation.onMouseOut({} as MouseEvent);

        expect(locations).toEqual([
            {
                insideViewport: undefined,
                x: undefined,
                y: undefined
            },
            {
                insideViewport: true,
                x: 1,
                y: 2
            },
            {
                insideViewport: false,
                x: 1,
                y: 2
            }
        ]);
    });

    it('observable noChange mouseMove', () => {
        var locations: IMouseLocation[] = [];

        mouseLocation.observable.subscribe(location => {
            locations.push(location);
        });

        mouseLocation.onMouseMove({
            clientX: 1,
            clientY: 2
        } as MouseEvent);

        mouseLocation.onMouseMove({
            clientX: 1,
            clientY: 2
        } as MouseEvent);

        expect(locations).toEqual([
            {
                insideViewport: undefined,
                x: undefined,
                y: undefined
            },
            {
                insideViewport: true,
                x: 1,
                y: 2
            }
        ]);
    });

    it('observable noChange mouseOut', () => {
        var locations: IMouseLocation[] = [];

        mouseLocation.observable.subscribe(location => {
            locations.push(location);
        });

        mouseLocation.onMouseOut({} as MouseEvent);

        mouseLocation.onMouseOut({} as MouseEvent);

        expect(locations).toEqual([
            {
                insideViewport: undefined,
                x: undefined,
                y: undefined
            },
            {
                insideViewport: false,
                x: undefined,
                y: undefined
            }
        ]);
    });
});
