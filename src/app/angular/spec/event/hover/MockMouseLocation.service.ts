import { IMouseLocation } from '../../../event/hover/IMouseLocation';
import { MouseLocationService } from '../../../event/hover/MouseLocation.service';

/**
 * Mock {@link MouseLocationService} for unit testing.
 */
export default class MockMouseLocationService extends MouseLocationService {
    constructor() {
        super(document.createElement('div') as any);
    }

    onMouseMove(event: MouseEvent) {
        return super.onMouseMove(event);
    }

    onMouseOut(event: MouseEvent) {
        return super.onMouseOut(event);
    }

    setState(state: IMouseLocation) {
        return super.setState(state);
    }
}
