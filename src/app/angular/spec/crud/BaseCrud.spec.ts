import { CrudMode } from '../../crud/CrudMode';
import MockCrud from './MockCrud';

describe('BaseCrud', () => {
    it('delete()', () => {
        var onDeleteCalled = false;

        var crud = new MockCrud();

        crud.onDelete.subscribe(() => {
            onDeleteCalled = true;
        });

        crud.delete();

        expect(crud.deleting).toBe(true);
        expect(crud.errorDeleting).toBe(false);

        crud.deleteObservable.next();

        expect(crud.errorDeleting).toBe(false);
        expect(onDeleteCalled).toBe(true);
    });

    it('delete() error', () => {
        var crud = new MockCrud();

        crud.onDelete.subscribe(() => {
            fail();
        });

        crud.delete();

        crud.deleteObservable.error('Definitely Not Awesome');

        expect(crud.deleting).toBe(false);
        expect(crud.errorDeleting).toBe(true);
    });

    it('setMode()', () => {
        var crud = new MockCrud();

        spyOn(crud, 'initEditMode');

        crud.setMode(CrudMode.Read);

        expect(crud.initEditMode).not.toHaveBeenCalled();

        crud.setMode(CrudMode.Delete);

        expect(crud.initEditMode).not.toHaveBeenCalled();

        crud.setMode(CrudMode.Edit);

        expect(crud.initEditMode).toHaveBeenCalledTimes(1);

        crud.setMode(CrudMode.Edit);

        expect(crud.initEditMode).toHaveBeenCalledTimes(1);

        crud.setMode(CrudMode.Read);
        crud.setMode(CrudMode.Edit);

        expect(crud.initEditMode).toHaveBeenCalledTimes(1);
    });
});
