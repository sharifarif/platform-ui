import getUniqueOptions from '../../crud/getUniqueOptions';

describe('getUniqueOptions()', () => {
    it('numbers', () => {
        expect(getUniqueOptions([1, 2, 3], [])).toEqual([1, 2, 3]);

        expect(getUniqueOptions([1, 2, 3], [1])).toEqual([2, 3]);
        expect(getUniqueOptions([1, 2, 3], [2])).toEqual([1, 3]);
        expect(getUniqueOptions([1, 2, 3], [3])).toEqual([1, 2]);

        expect(getUniqueOptions([1, 2, 3], [1, 2])).toEqual([3]);
        expect(getUniqueOptions([1, 2, 3], [1, 3])).toEqual([2]);
        expect(getUniqueOptions([1, 2, 3], [2, 3])).toEqual([1]);

        expect(getUniqueOptions([1, 2, 3], [1, 2, 3])).toEqual([]);
    });

    it('strings', () => {
        expect(getUniqueOptions(['awesome', 'sauce'], [])).toEqual(['awesome', 'sauce']);

        expect(getUniqueOptions(['awesome', 'sauce'], ['awesome'])).toEqual(['sauce']);
        expect(getUniqueOptions(['awesome', 'sauce'], ['sauce'])).toEqual(['awesome']);

        expect(getUniqueOptions(['awesome', 'sauce'], ['awesome', 'sauce'])).toEqual([]);
    });

    it('objects', () => {
        var awesome = {
                name: 'awesome'
            },
            sauce = {
                name: 'sauce'
            };

        expect(getUniqueOptions([awesome, sauce], [])).toEqual([awesome, sauce]);

        expect(getUniqueOptions([awesome, sauce], [awesome])).toEqual([sauce]);
        expect(getUniqueOptions([awesome, sauce], [sauce])).toEqual([awesome]);

        expect(getUniqueOptions([awesome, sauce], [awesome, sauce])).toEqual([]);
    });

    it('current numbers', () => {
        expect(getUniqueOptions([1, 2], [1], 1)).toEqual([1, 2]);
        expect(getUniqueOptions([1, 2], [2], 2)).toEqual([1, 2]);

        expect(getUniqueOptions([1, 2], [1, 2], 1)).toEqual([1]);
        expect(getUniqueOptions([1, 2], [1, 2], 2)).toEqual([2]);
    });

    it('current strings', () => {
        expect(getUniqueOptions(['awesome', 'sauce'], ['awesome'], 'awesome')).toEqual([
            'awesome',
            'sauce'
        ]);
        expect(getUniqueOptions(['awesome', 'sauce'], ['sauce'], 'sauce')).toEqual([
            'awesome',
            'sauce'
        ]);

        expect(
            getUniqueOptions(['awesome', 'sauce'], ['awesome', 'sauce'], 'awesome')
        ).toEqual(['awesome']);
        expect(
            getUniqueOptions(['awesome', 'sauce'], ['awesome', 'sauce'], 'sauce')
        ).toEqual(['sauce']);
    });

    it('current objects', () => {
        var awesome = {
                name: 'awesome'
            },
            sauce = {
                name: 'sauce'
            };

        expect(getUniqueOptions([awesome, sauce], [awesome], awesome)).toEqual([
            awesome,
            sauce
        ]);
        expect(getUniqueOptions([awesome, sauce], [sauce], sauce)).toEqual([
            awesome,
            sauce
        ]);

        expect(getUniqueOptions([awesome, sauce], [awesome, sauce], awesome)).toEqual([
            awesome
        ]);
        expect(getUniqueOptions([awesome, sauce], [awesome, sauce], sauce)).toEqual([
            sauce
        ]);
    });

    it('idKey', () => {
        var awesomeOption = {
                theOptionId: 'awesome'
            },
            sauceOption = {
                theOptionId: 'sauce'
            },
            awesomeValue = {
                theValueId: 'awesome'
            },
            sauceValue = {
                theValueId: 'sauce'
            };

        expect(
            getUniqueOptions(
                [awesomeOption, sauceOption],
                [],
                undefined,
                'theOptionId',
                'theValueId'
            )
        ).toEqual([awesomeOption, sauceOption]);

        expect(
            getUniqueOptions(
                [awesomeOption, sauceOption],
                [awesomeValue],
                undefined,
                'theOptionId',
                'theValueId'
            )
        ).toEqual([sauceOption]);
        expect(
            getUniqueOptions(
                [awesomeOption, sauceOption],
                [sauceValue],
                undefined,
                'theOptionId',
                'theValueId'
            )
        ).toEqual([awesomeOption]);

        expect(
            getUniqueOptions(
                [awesomeOption, sauceOption],
                [awesomeValue, sauceValue],
                undefined,
                'theOptionId',
                'theValueId'
            )
        ).toEqual([]);
    });

    it('idKey current', () => {
        var awesomeOption = {
                theOptionId: 'awesome'
            },
            sauceOption = {
                theOptionId: 'sauce'
            },
            awesomeValue = {
                theValueId: 'awesome'
            },
            sauceValue = {
                theValueId: 'sauce'
            };

        expect(
            getUniqueOptions(
                [awesomeOption, sauceOption],
                [awesomeValue],
                awesomeValue,
                'theOptionId',
                'theValueId'
            )
        ).toEqual([awesomeOption, sauceOption]);
        expect(
            getUniqueOptions(
                [awesomeOption, sauceOption],
                [sauceValue],
                sauceValue,
                'theOptionId',
                'theValueId'
            )
        ).toEqual([awesomeOption, sauceOption]);

        expect(
            getUniqueOptions(
                [awesomeOption, sauceOption],
                [awesomeValue, sauceValue],
                awesomeValue,
                'theOptionId',
                'theValueId'
            )
        ).toEqual([awesomeOption]);
        expect(
            getUniqueOptions(
                [awesomeOption, sauceOption],
                [awesomeValue, sauceValue],
                sauceValue,
                'theOptionId',
                'theValueId'
            )
        ).toEqual([sauceOption]);
    });

    it('null options', () => {
        expect(getUniqueOptions(null, [1])).toBeFalsy();
        expect(getUniqueOptions(null, ['awesome'])).toBeFalsy();

        expect(getUniqueOptions(undefined, [1])).toBeUndefined();
        expect(getUniqueOptions(undefined, ['awesome'])).toBeUndefined();
    });

    it('null used', () => {
        expect(getUniqueOptions([1], null)).toEqual([1]);
        expect(getUniqueOptions(['awesome'], null)).toEqual(['awesome']);

        expect(getUniqueOptions([1], undefined)).toEqual([1]);
        expect(getUniqueOptions(['awesome'], undefined)).toEqual(['awesome']);
    });

    it('null options & used', () => {
        expect(getUniqueOptions(null, null)).toBeFalsy();
        expect(getUniqueOptions(null, null)).toBeFalsy();

        expect(getUniqueOptions(undefined, undefined)).toBeUndefined();
        expect(getUniqueOptions(undefined, undefined)).toBeUndefined();
    });
});
