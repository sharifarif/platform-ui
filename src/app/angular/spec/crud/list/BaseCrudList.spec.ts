import { ElementRef } from '@angular/core';

import { CrudMode } from '../../../crud/CrudMode';
import MockCrud from '../MockCrud';
import MockCrudList from './MockCrudList';
import MockTranslateService from '../../i18n/MockTranslateService';
import { PrettySubmitGuard } from '../../../save/PrettySubmitGuard';

describe('BaseCrudList', () => {
    var crudList: MockCrudList,
        elementRef: ElementRef,
        prettySubmitGuard: PrettySubmitGuard;

    beforeEach(() => {
        elementRef = { nativeElement: document.createElement('div') };
        prettySubmitGuard = new PrettySubmitGuard(new MockTranslateService());
        crudList = new MockCrudList(elementRef, prettySubmitGuard);
    });

    it('addItem() afterInit', () => {
        crudList.init([
            {
                id: 'awesome'
            }
        ]);

        crudList.addItem();

        expect(crudList.items).toEqual([
            {},
            {
                id: 'awesome'
            }
        ]);
        expect(crudList.cruds.length).toEqual(2);
        expect(crudList.cruds[0].data).toEqual({
            item: {}
        });
        expect(crudList.cruds[0].getMode()).toBe(CrudMode.Edit);
        expect(crudList.cruds[1].data).toEqual({
            item: {
                id: 'awesome'
            }
        });
    });

    it('addItem() beforeInit', () => {
        crudList.addItem();

        crudList.init([
            {
                id: 'awesome'
            }
        ]);

        expect(crudList.items).toEqual([
            {},
            {
                id: 'awesome'
            }
        ]);
        expect(crudList.cruds.length).toEqual(2);
        expect(crudList.cruds[0].data).toEqual({
            item: {}
        });
        expect(crudList.cruds[0].getMode()).toBe(CrudMode.Edit);
        expect(crudList.cruds[1].data).toEqual({
            item: {
                id: 'awesome'
            }
        });
    });

    it('addItem() noInit', () => {
        crudList.addItem();

        expect(crudList.items).toEqual([{}]);
        expect(crudList.cruds.length).toEqual(1);
        expect(crudList.cruds[0].data).toEqual({
            item: {}
        });
        expect(crudList.cruds[0].getMode()).toBe(CrudMode.Edit);
    });

    it('clear()', () => {
        crudList.init([
            {
                id: 'awesome'
            },
            {
                id: 'sauce'
            }
        ]);

        prettySubmitGuard.canRemoveComponent = (passedElementRef: ElementRef) => {
            expect(passedElementRef).toBe(elementRef);

            return true;
        };

        expect(crudList.clear()).toBe(true);

        expect(crudList.items).toBeUndefined();
        expect(crudList.cruds).toBeUndefined();
    });

    it('clear() !canRemoveComponent', () => {
        crudList.init([
            {
                id: 'awesome'
            },
            {
                id: 'sauce'
            }
        ]);

        prettySubmitGuard.canRemoveComponent = (passedElementRef: ElementRef) => {
            expect(passedElementRef).toBe(elementRef);

            return false;
        };

        expect(crudList.clear()).toBe(false);

        expect(crudList.items).toEqual([
            {
                id: 'awesome'
            },
            {
                id: 'sauce'
            }
        ]);
        expect(crudList.cruds.length).toBe(2);
    });

    it('hasNewItem()', () => {
        crudList.init([{}]);
        crudList.addItem();

        expect(crudList.hasNewItem()).toBe(true);
    });

    it('hasNewItem() false', () => {
        crudList.init([{}]);

        expect(crudList.hasNewItem()).toBe(false);
    });

    it('hasNewItem() null', () => {
        expect(crudList.hasNewItem()).toBe(false);
    });

    it('isAnyReadVisible()', () => {
        crudList.init([
            {
                id: 'awesome'
            }
        ]);

        expect(crudList.isAnyReadVisible()).toBe(true);
    });

    it('isAnyReadVisible() new', () => {
        crudList.addItem();

        expect(crudList.isAnyReadVisible()).toBe(false);
    });

    it('isAnyReadVisible() search result', () => {
        crudList.init([
            {
                id: 'awesome'
            }
        ]);

        crudList.searchResults = [crudList.cruds[0].savedData.item];

        expect(crudList.isAnyReadVisible()).toBe(true);
    });

    it('isAnyReadVisible() search notResult', () => {
        crudList.init([
            {
                id: 'awesome'
            }
        ]);

        crudList.searchResults = [];

        expect(crudList.isAnyReadVisible()).toBe(false);
    });

    it('isAnyReadVisible() noInit', () => {
        expect(crudList.isAnyReadVisible()).toBe(false);
    });

    it('isReadVisible()', () => {
        var crud = new MockCrud();

        crud.initSaved({
            item: {
                id: 'awesome'
            }
        });

        expect(crudList.isReadVisible(crud)).toBe(true);
    });

    it('isReadVisible() new', () => {
        var crud = new MockCrud();

        crud.initNew({
            item: {
                id: 'awesome'
            }
        });

        expect(crudList.isReadVisible(crud)).toBe(false);
    });

    it('isReadVisible() edit', () => {
        var crud = new MockCrud();

        crud.initSaved({
            item: {
                id: 'awesome'
            }
        });

        crud.setMode(CrudMode.Edit);

        expect(crudList.isReadVisible(crud)).toBe(false);
    });

    it('isReadVisible() search result', () => {
        var crud = new MockCrud(),
            item = {
                id: 'awesome'
            };

        crud.initSaved({
            item: item
        });

        crudList.searchResults = [item];

        expect(crudList.isReadVisible(crud)).toBe(true);
    });

    it('isReadVisible() search notResult', () => {
        var crud = new MockCrud(),
            item = {
                id: 'awesome'
            };

        crud.initSaved({
            item: item
        });

        crudList.searchResults = [];

        expect(crudList.isReadVisible(crud)).toBe(false);
    });

    it('init()', () => {
        crudList.init([
            {
                id: 'awesome'
            },
            {
                id: 'sauce'
            }
        ]);

        expect(crudList.items).toEqual([
            {
                id: 'awesome'
            },
            {
                id: 'sauce'
            }
        ]);

        expect(crudList.cruds.length).toBe(2);

        expect(crudList.cruds[0].data).toEqual({
            item: {
                id: 'awesome'
            }
        });
        expect(crudList.cruds[0].savedData).toEqual({
            item: {
                id: 'awesome'
            }
        });
        expect(crudList.cruds[0].getMode()).toBe(CrudMode.Read);

        expect(crudList.cruds[1].data).toEqual({
            item: {
                id: 'sauce'
            }
        });
        expect(crudList.cruds[1].savedData).toEqual({
            item: {
                id: 'sauce'
            }
        });
        expect(crudList.cruds[0].getMode()).toBe(CrudMode.Read);
    });
});
