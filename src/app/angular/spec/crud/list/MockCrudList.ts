import { BaseCrudList } from '../../../crud/list/BaseCrudList';
import { ICrudData } from '../../../crud/ICrudData';
import MockCrud from '../MockCrud';

/**
 * Mock {@link BaseCrudList} for unit testing.
 */
export default class MockCrudList extends BaseCrudList<any, ICrudData<any>> {
    clear() {
        return super.clear();
    }

    init(items: any[]) {
        return super.init(items);
    }

    protected createCrud() {
        return new MockCrud();
    }
}
