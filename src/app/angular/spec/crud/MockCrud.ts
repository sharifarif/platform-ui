import { Subject } from 'rxjs';

import { BaseCrud } from '../../crud/BaseCrud';
import { ICrudData } from '../../crud/ICrudData';

/**
 * Mock {@link BaseCrud} for unit testing.
 */
export default class MockCrud extends BaseCrud<any, ICrudData<any>> {
    deleteObservable = new Subject<any>();
    saveObservable = new Subject<any>();

    constructor() {
        super(null);
    }

    initEditMode() {
        // Allow Spec to spy on this method.
    }

    protected performDelete() {
        return this.deleteObservable;
    }

    protected performSave() {
        return this.saveObservable;
    }
}
