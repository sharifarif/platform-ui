import { Subject } from 'rxjs';

import { GuidCrud } from '../../crud/GuidCrud';
import { ICrudData } from '../../crud/ICrudData';
import { IGuidModel } from '../../model/IGuidModel';

/**
 * Mock {@link GuidCrud} for unit testing.
 */
export default class MockGuidCrud extends GuidCrud<IGuidModel, ICrudData<IGuidModel>> {
    addObservable = new Subject<IGuidModel>();
    deleteObservable = new Subject<IGuidModel>();
    updateObservable = new Subject<IGuidModel>();

    constructor() {
        super(null);
    }

    protected performAdd() {
        return this.addObservable;
    }

    protected performDelete() {
        return this.deleteObservable;
    }

    protected performUpdate() {
        return this.updateObservable;
    }
}
