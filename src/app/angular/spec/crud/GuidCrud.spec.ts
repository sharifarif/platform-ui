import { IGuidModel } from '../../model/IGuidModel';
import MockGuidCrud from './MockGuidCrud';

describe('GuidCrud', () => {
    var crud: MockGuidCrud;

    beforeEach(() => {
        crud = new MockGuidCrud();
    });

    it('performSave() new', () => {
        crud.initNew({
            item: {} as IGuidModel
        });

        crud.save();

        expect(crud.isNew()).toBe(true);
        expect(crud.isSaved()).toBe(false);

        crud.addObservable.next({
            id: 'awesome',
            rowVersion: 'sauce'
        } as IGuidModel);

        expect(crud.isNew()).toBe(false);
        expect(crud.isSaved()).toBe(true);

        expect(crud.data).toEqual({
            item: {
                id: 'awesome',
                rowVersion: 'sauce'
            }
        } as any);
        expect(crud.savedData).toEqual({
            item: {
                id: 'awesome',
                rowVersion: 'sauce'
            }
        } as any);
    });

    it('performSave() update', () => {
        crud.initSaved({
            item: {
                id: 'awesome',
                rowVersion: 'sauce',
                isInactive: false
            }
        });

        crud.save();

        crud.updateObservable.next({
            id: 'awesome',
            rowVersion: 'very sauce',
            isInactive: false
        });

        expect(crud.isNew()).toBe(false);
        expect(crud.isSaved()).toBe(true);

        expect(crud.data).toEqual({
            item: {
                id: 'awesome',
                rowVersion: 'very sauce',
                isInactive: false
            }
        } as any);
        expect(crud.savedData).toEqual({
            item: {
                id: 'awesome',
                rowVersion: 'very sauce',
                isInactive: false
            }
        } as any);
    });

    it('performSave() error', () => {
        crud.initSaved({
            item: {
                id: 'awesome',
                rowVersion: 'sauce',
                isInactive: false
            }
        });

        var errorBubbled = false;

        var observable = crud.save();

        observable.subscribe(
            () => {
                fail();
            },
            () => {
                errorBubbled = true;
            }
        );

        crud.updateObservable.error('Not Awesome');

        expect(errorBubbled).toBe(true);
    });
});
