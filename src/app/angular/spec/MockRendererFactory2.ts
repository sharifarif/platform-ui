import { RendererFactory2 } from '@angular/core';

import { MockRenderer2 } from './MockRenderer2';

export const MockRendererFactory2 = {
    createRenderer: () => {
        return MockRenderer2;
    }
} as RendererFactory2;
