import { Renderer2 } from '@angular/core';

/**
 * Mock `Renderer2` for unit testing.
 */
export const MockRenderer2 = {
    addClass: (element: HTMLElement, name: string) => {
        element.classList.add(name);
    },

    appendChild: (parent: HTMLElement, newChild: HTMLElement) => {
        parent.appendChild(newChild);
    },

    createElement: (name: string) => {
        return document.createElement(name);
    },

    parentNode: (element: HTMLElement) => {
        return element.parentNode;
    },

    removeChild: (parent: HTMLElement, oldChild: HTMLElement) => {
        parent.removeChild(oldChild);
    },

    removeClass: (element: HTMLElement, name: string) => {
        element.classList.remove(name);
    },

    removeStyle: (element: HTMLElement, style: keyof CSSStyleDeclaration) => {
        element.style[style as any] = '';
    },

    setStyle: (element: HTMLElement, style: keyof CSSStyleDeclaration, value: any) => {
        element.style[style as any] = value;
    }
} as Renderer2;
