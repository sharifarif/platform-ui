import { BaseApi } from '../../api/BaseApi';
import MockApiErrorHandler from './error/MockApiErrorHandler';

/**
 * Mock {@link BaseApi} for unit testing.
 */
export default class MockApi extends BaseApi<any> {
    constructor(public baseUrl: string) {
        super(new MockApiErrorHandler(), {} as any, {} as any);
    }

    buildUrl(url: string) {
        return super.buildUrl(url);
    }

    protected getBaseUrl() {
        return this.baseUrl;
    }
}
