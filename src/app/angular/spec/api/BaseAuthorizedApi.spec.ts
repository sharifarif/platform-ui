import { Headers, RequestMethod, RequestOptions } from '@angular/http';

import MockAuthorizedApi from './MockAuthorizedApi';
import MockAuthService from '../login/adal/MockAuth.service';

describe('BaseAuthorizedApi', () => {
    var authorizedApi: MockAuthorizedApi, authService: MockAuthService;

    beforeEach(() => {
        authService = new MockAuthService();
        authorizedApi = new MockAuthorizedApi(authService);
    });

    it('buildOptionsAsync()', done => {
        authorizedApi.buildOptionsAsync().subscribe(options => {
            expect(options.headers.keys()).toEqual(['Authorization']);
            expect(options.headers.values()).toEqual([['Bearer awesome-sauce']]);

            done();
        });

        authService.tokenObservable.next('awesome-sauce');
    });

    it('buildOptionsAsync() method,body,url', done => {
        authorizedApi
            .buildOptionsAsync({
                method: RequestMethod.Put,
                body: 'the-body',
                url: 'https://www.google.com'
            } as RequestOptions)
            .subscribe(options => {
                expect(options.method).toBe(RequestMethod.Put);
                expect(options.body).toBe('the-body');
                expect(options.url).toBe('https://www.google.com');

                expect(options.headers.keys()).toEqual(['Authorization']);
                expect(options.headers.values()).toEqual([['Bearer awesome-sauce']]);

                done();
            });

        authService.tokenObservable.next('awesome-sauce');
    });

    it('buildOptionsAsync() headers', done => {
        var originalHeaders = new Headers({
            awesome: 'sauce'
        });

        authorizedApi
            .buildOptionsAsync({
                headers: originalHeaders
            } as RequestOptions)
            .subscribe(options => {
                expect(options.headers.keys()).toEqual(['awesome', 'Authorization']);
                expect(options.headers.values()).toEqual([
                    ['sauce'],
                    ['Bearer awesome-sauce']
                ]);

                // Do not mutate the passed in value.
                expect(originalHeaders.keys()).toEqual(['awesome']);
                expect(originalHeaders.values()).toEqual([['sauce']]);

                done();
            });

        authService.tokenObservable.next('awesome-sauce');
    });
});
