import { Observable, Subject } from 'rxjs';
import { RequestOptions, Response } from '@angular/http';

import { ApiCacheService } from '../../../api/cache/ApiCache.service';
import { BaseCachedApi } from '../../../api/cache/BaseCachedApi';
import MockApiAerrorHandler from '../error/MockApiErrorHandler';
import MockApiErrorHandler from '../error/MockApiErrorHandler';

export default class MockCachedApi extends BaseCachedApi<any> {
    requests: Subject<Response>[] = [];

    constructor(apiCacheService: ApiCacheService) {
        super(apiCacheService, new MockApiErrorHandler(), {} as any, undefined);
    }

    getCacheKey(url: string, options: RequestOptions) {
        return super.getCacheKey(url, options);
    }

    request(url: string, options: RequestOptions) {
        return super.request(url, options);
    }

    requestJson<T>(url: string, options: RequestOptions): Observable<T> {
        return super.requestJson(url, options);
    }

    requestCached<T>(
        url: string,
        options: RequestOptions,
        doRequest: () => Observable<T>
    ) {
        return super.requestCached(url, options, doRequest);
    }

    shouldCacheRequest(url: string, options: RequestOptions) {
        return super.shouldCacheRequest(url, options);
    }

    protected getBaseUrl() {
        return 'https://www.google.com/';
    }

    protected getClassName() {
        return 'MockCachedApi';
    }

    protected requestImpl(url: string, options: RequestOptions) {
        var request = new Subject<Response>();

        this.requests.push(request);

        return request.asObservable();
    }
}
