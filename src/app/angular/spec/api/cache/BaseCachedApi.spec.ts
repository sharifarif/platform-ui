import { RequestMethod, RequestOptions, Response, ResponseOptions } from '@angular/http';
import { Subject } from 'rxjs';
import { TransferState } from '@angular/platform-browser';

import { ApiCacheService } from '../../../api/cache/ApiCache.service';
import MockCachedApi from './MockCachedApi';

describe('BaseCachedApi', () => {
    var apiCacheService: ApiCacheService,
        cachedApi: MockCachedApi,
        transferState: TransferState;

    beforeEach(() => {
        transferState = new TransferState();
        apiCacheService = new ApiCacheService(transferState);
        cachedApi = new MockCachedApi(apiCacheService);
    });

    it('getCacheKey() params', () => {
        expect(
            cachedApi.getCacheKey(
                'header-footer',
                new RequestOptions({
                    params: {
                        awesome: 'totallyAwesome',
                        sauce: 'totallySauce'
                    }
                })
            )
        ).toBe('MockCachedApi:header-footer?awesome=totallyAwesome&sauce=totallySauce');
    });

    it('getCacheKey() search', () => {
        expect(
            cachedApi.getCacheKey(
                'header-footer',
                new RequestOptions({
                    search: {
                        awesome: 'totallyAwesome',
                        sauce: 'totallySauce'
                    }
                })
            )
        ).toBe('MockCachedApi:header-footer?awesome=totallyAwesome&sauce=totallySauce');
    });

    it('getCacheKey() null options', () => {
        expect(cachedApi.getCacheKey('header-footer', undefined)).toBe(
            'MockCachedApi:header-footer'
        );
        expect(cachedApi.getCacheKey('header-footer', null)).toBe(
            'MockCachedApi:header-footer'
        );
    });

    it('getCacheKey() empty options', () => {
        expect(cachedApi.getCacheKey('header-footer', new RequestOptions({}))).toBe(
            'MockCachedApi:header-footer'
        );
    });

    it('getCacheKey() empty params', () => {
        expect(
            cachedApi.getCacheKey(
                'header-footer',
                new RequestOptions({
                    params: {},
                    search: {}
                })
            )
        ).toBe('MockCachedApi:header-footer');
    });

    it('request() GET', () => {
        var response1 = cachedApi.request(
                'header-footer',
                new RequestOptions({
                    method: RequestMethod.Get
                })
            ),
            response2 = cachedApi.request(
                'header-footer',
                new RequestOptions({
                    method: RequestMethod.Get
                })
            ),
            values1: Response[] = [],
            values2: Response[] = [];

        response1.subscribe(value => values1.push(value));
        response2.subscribe(value => values2.push(value));

        expect(cachedApi.requests.length).toBe(1);
        expect(values1).toEqual([]);
        expect(values2).toEqual([]);

        cachedApi.requests[0].next(
            new Response(
                new ResponseOptions({
                    body: '"awesome"',
                    status: 200
                })
            )
        );

        expect(cachedApi.requests.length).toBe(1);
        expect(values1.length).toBe(1);
        expect(values2.length).toBe(1);
        expect(values1[0].json()).toBe('awesome');
        expect(values2[0].json()).toBe('awesome');
    });

    it('request() PUT', () => {
        var response1 = cachedApi.request(
                'header-footer',
                new RequestOptions({
                    method: RequestMethod.Put
                })
            ),
            response2 = cachedApi.request(
                'header-footer',
                new RequestOptions({
                    method: RequestMethod.Put
                })
            ),
            values1: Response[] = [],
            values2: Response[] = [];

        response1.subscribe(value => values1.push(value));
        response2.subscribe(value => values2.push(value));

        expect(cachedApi.requests.length).toBe(2);
        expect(values1).toEqual([]);
        expect(values2).toEqual([]);

        cachedApi.requests[0].next(
            new Response(
                new ResponseOptions({
                    body: '"awesome"',
                    status: 200
                })
            )
        );
        cachedApi.requests[1].next(
            new Response(
                new ResponseOptions({
                    body: '"sauce"',
                    status: 200
                })
            )
        );

        expect(cachedApi.requests.length).toBe(2);
        expect(values1.length).toBe(1);
        expect(values2.length).toBe(1);
        expect(values1[0].json()).toBe('awesome');
        expect(values2[0].json()).toBe('sauce');
    });

    it('request() deserializedJson', () => {
        var cacheSubject = new Subject<Response>();

        spyOn(apiCacheService, 'get').and.callFake((key: string, getter: () => any) => {
            expect(key).toBe('MockCachedApi:header-footer');
            expect(getter).toBeTruthy();

            return cacheSubject.asObservable();
        });

        var response1 = cachedApi.request(
                'header-footer',
                new RequestOptions({
                    method: RequestMethod.Get
                })
            ),
            response2 = cachedApi.request(
                'header-footer',
                new RequestOptions({
                    method: RequestMethod.Get
                })
            ),
            values1: Response[] = [],
            values2: Response[] = [];

        response1.subscribe(value => values1.push(value));
        response2.subscribe(value => values2.push(value));

        expect(cachedApi.requests.length).toBe(0);
        expect(values1).toEqual([]);
        expect(values2).toEqual([]);

        var unserializedResponse = new Response(
                new ResponseOptions({
                    body: '"awesome"',
                    status: 200
                })
            ),
            deserializedResponse = JSON.parse(JSON.stringify(unserializedResponse));

        cacheSubject.next(deserializedResponse);

        expect(cachedApi.requests.length).toBe(0);
        expect(values1.length).toBe(1);
        expect(values2.length).toBe(1);
        expect(values1[0].json()).toBe('awesome');
        expect(values2[0].json()).toBe('awesome');
    });

    it('requestJson() GET', () => {
        var response1 = cachedApi.requestJson<string>(
                'header-footer',
                new RequestOptions({
                    method: RequestMethod.Get
                })
            ),
            response2 = cachedApi.requestJson<string>(
                'header-footer',
                new RequestOptions({
                    method: RequestMethod.Get
                })
            ),
            values1: string[] = [],
            values2: string[] = [];

        response1.subscribe(value => values1.push(value));
        response2.subscribe(value => values2.push(value));

        expect(cachedApi.requests.length).toBe(1);
        expect(values1).toEqual([]);
        expect(values2).toEqual([]);

        cachedApi.requests[0].next(
            new Response(
                new ResponseOptions({
                    body: '"awesome"',
                    status: 200
                })
            )
        );

        expect(cachedApi.requests.length).toBe(1);
        expect(values1).toEqual(['awesome']);
        expect(values2).toEqual(['awesome']);
    });

    it('requestJson() PUT', () => {
        var response1 = cachedApi.requestJson<string>(
                'header-footer',
                new RequestOptions({
                    method: RequestMethod.Put
                })
            ),
            response2 = cachedApi.requestJson<string>(
                'header-footer',
                new RequestOptions({
                    method: RequestMethod.Put
                })
            ),
            values1: string[] = [],
            values2: string[] = [];

        response1.subscribe(value => values1.push(value));
        response2.subscribe(value => values2.push(value));

        expect(cachedApi.requests.length).toBe(2);
        expect(values1).toEqual([]);
        expect(values2).toEqual([]);

        cachedApi.requests[0].next(
            new Response(
                new ResponseOptions({
                    body: '"awesome"',
                    status: 200
                })
            )
        );
        cachedApi.requests[1].next(
            new Response(
                new ResponseOptions({
                    body: '"sauce"',
                    status: 200
                })
            )
        );

        expect(cachedApi.requests.length).toBe(2);
        expect(values1).toEqual(['awesome']);
        expect(values2).toEqual(['sauce']);
    });

    it('shouldCacheRequest() GET', () => {
        expect(
            cachedApi.shouldCacheRequest(
                '',
                new RequestOptions({
                    method: RequestMethod.Get
                })
            )
        ).toBe(true);
    });

    it('shouldCacheRequest() null options -> implied GET', () => {
        expect(cachedApi.shouldCacheRequest('', undefined)).toBe(true);
        expect(cachedApi.shouldCacheRequest('', null)).toBe(true);
    });

    it('shouldCacheRequest() null method -> implied GET', () => {
        expect(cachedApi.shouldCacheRequest('', new RequestOptions({}))).toBe(true);
        expect(
            cachedApi.shouldCacheRequest(
                '',
                new RequestOptions({
                    method: null
                })
            )
        ).toBe(true);
    });

    it('shouldCacheRequest() POST', () => {
        expect(
            cachedApi.shouldCacheRequest(
                '',
                new RequestOptions({
                    method: RequestMethod.Post
                })
            )
        ).toBe(false);
    });

    it('shouldCacheRequest() PUT', () => {
        expect(
            cachedApi.shouldCacheRequest(
                '',
                new RequestOptions({
                    method: RequestMethod.Put
                })
            )
        ).toBe(false);
    });

    it('shouldCacheRequest() DELETE', () => {
        expect(
            cachedApi.shouldCacheRequest(
                '',
                new RequestOptions({
                    method: RequestMethod.Delete
                })
            )
        ).toBe(false);
    });

    it('shouldCacheRequest() PATCH', () => {
        expect(
            cachedApi.shouldCacheRequest(
                '',
                new RequestOptions({
                    method: RequestMethod.Patch
                })
            )
        ).toBe(false);
    });
});
