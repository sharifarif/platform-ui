import { Observable, of, PartialObserver } from 'rxjs';
import { StateKey, TransferState } from '@angular/platform-browser';
import { ApiCacheService } from '../../../api/cache/ApiCache.service';

describe('ApiCacheService', () => {
    interface IMockApi {
        callback: () => Observable<string>;
        callbackCalls: number;
        observable: Observable<string>;
        observableSubscriptions: number;
    }

    var apiCache: ApiCacheService,
        awesomeApi: IMockApi,
        sauceApi: IMockApi,
        transferState: TransferState;

    var createMockApi = (data: string) => {
        var api: IMockApi = {
            callback: () => {
                api.callbackCalls++;

                return api.observable;
            },
            callbackCalls: 0,
            observable: Observable.create((observer: PartialObserver<string>) => {
                api.observableSubscriptions++;

                observer.next(data);
            }),
            observableSubscriptions: 0
        };

        return api;
    };

    beforeEach(() => {
        transferState = new TransferState();
        apiCache = new ApiCacheService(transferState);
        awesomeApi = createMockApi('awesome');
        sauceApi = createMockApi('sauce');
    });

    it('get()', done => {
        var observable = apiCache.get('awesomeKey', awesomeApi.callback);

        expect(awesomeApi.callbackCalls).toBe(1);
        expect(awesomeApi.observableSubscriptions).toBe(0);

        observable.subscribe(value => {
            expect(value).toBe('awesome');
            expect(awesomeApi.observableSubscriptions).toBe(1);

            done();
        });
    });

    it('get() sameKey', () => {
        var observable1 = apiCache.get('awesomeKey', awesomeApi.callback),
            observable2 = apiCache.get('awesomeKey', awesomeApi.callback),
            values: string[] = [];

        expect(awesomeApi.callbackCalls).toBe(1);
        expect(awesomeApi.observableSubscriptions).toBe(0);

        observable1.subscribe(value => {
            values.push(value);
        });
        observable2.subscribe(value => {
            values.push(value);
        });

        expect(values).toEqual(['awesome', 'awesome']);
        expect(awesomeApi.callbackCalls).toBe(1);
        expect(awesomeApi.observableSubscriptions).toBe(1);
    });

    it('get() differentKey', () => {
        var awesomeObservable = apiCache.get('awesomeKey', awesomeApi.callback),
            sauceObservable = apiCache.get('sauceKey', sauceApi.callback),
            values: string[] = [];

        expect(awesomeApi.callbackCalls).toBe(1);
        expect(awesomeApi.observableSubscriptions).toBe(0);
        expect(sauceApi.callbackCalls).toBe(1);
        expect(sauceApi.observableSubscriptions).toBe(0);

        awesomeObservable.subscribe(value => {
            values.push(value);
        });
        sauceObservable.subscribe(value => {
            values.push(value);
        });

        expect(values).toEqual(['awesome', 'sauce']);
        expect(awesomeApi.callbackCalls).toBe(1);
        expect(awesomeApi.observableSubscriptions).toBe(1);
        expect(sauceApi.callbackCalls).toBe(1);
        expect(sauceApi.observableSubscriptions).toBe(1);
    });

    it('get() TransferState.get()', () => {
        spyOn(transferState, 'get').and.callFake(
            (stateKey: StateKey<string[]>, defaultValue: string) => {
                expect(stateKey).toBe('awesomeKey');

                return ['totally awesome'];
            }
        );

        var observable = apiCache.get('awesomeKey', awesomeApi.callback),
            values: string[] = [];

        expect(transferState.get).toHaveBeenCalledTimes(1);
        expect(awesomeApi.callbackCalls).toBe(0);
        expect(awesomeApi.observableSubscriptions).toBe(0);

        observable.subscribe(value => {
            values.push(value);
        });

        expect(transferState.get).toHaveBeenCalledTimes(1);
        expect(values).toEqual(['totally awesome']);
        expect(awesomeApi.callbackCalls).toBe(0);
        expect(awesomeApi.observableSubscriptions).toBe(0);
    });

    it('get() TransferState.onSerialize()', () => {
        spyOn(transferState, 'onSerialize').and.callFake(
            (stateKey: StateKey<string[]>, callback: () => string[]) => {
                expect(stateKey).toBe('awesomeKey');
                expect(callback()).toEqual(['awesome']);
            }
        );

        var observable = apiCache.get('awesomeKey', awesomeApi.callback),
            values: string[] = [];

        expect(awesomeApi.callbackCalls).toBe(1);
        expect(awesomeApi.observableSubscriptions).toBe(0);

        observable.subscribe(value => {
            values.push(value);
        });

        expect(transferState.onSerialize).toHaveBeenCalledTimes(1);
        expect(values).toEqual(['awesome']);
        expect(awesomeApi.callbackCalls).toBe(1);
        expect(awesomeApi.observableSubscriptions).toBe(1);
    });
});
