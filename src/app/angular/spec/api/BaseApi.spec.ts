import MockApi from './MockApi';

describe('BaseApi', () => {
    it('buildUrl()', () => {
        expect(new MockApi('/awesome/').buildUrl('sauce')).toBe('/awesome/sauce');

        expect(new MockApi('https://localhost/awesome/').buildUrl('sauce')).toBe(
            'https://localhost/awesome/sauce'
        );
    });

    it('buildUrl() noBaseUrl', () => {
        expect(new MockApi('').buildUrl('sauce')).toBe('sauce');
        expect(new MockApi(null).buildUrl('sauce')).toBe('sauce');
        expect(new MockApi(undefined).buildUrl('sauce')).toBe('sauce');
    });

    it('buildUrl() noUrl', () => {
        expect(new MockApi('/awesome/').buildUrl('')).toBe('/awesome/');
        expect(new MockApi('/awesome/').buildUrl(null)).toBe('/awesome/');
        expect(new MockApi('/awesome/').buildUrl(undefined)).toBe('/awesome/');
    });
});
