import { RequestOptions, Response } from '@angular/http';

import { ApiErrorHandler } from '../../../api/error/ApiErrorHandler';
import { AlertService } from '../../../layout/alert/Alert.service';
import MockTranslateService from '../../i18n/MockTranslateService';

/**
 * Mock {@link ApiErrorHandler} for unit testing.
 */
export default class MockApiErrorHandler extends ApiErrorHandler {
    constructor() {
        super(new AlertService(), new MockTranslateService());
    }

    getAlertService(): AlertService {
        return (this as any).alertService;
    }

    getErrorAlert(error: any, requestOptions: RequestOptions) {
        return super.getErrorAlert(error, requestOptions);
    }

    getProblemCode(response: Response | any) {
        return super.getProblemCode(response);
    }

    handleError(error: any, requestOptions: RequestOptions) {
        return super.handleError(error, requestOptions);
    }

    isOffline() {
        return super.isOffline();
    }
}
