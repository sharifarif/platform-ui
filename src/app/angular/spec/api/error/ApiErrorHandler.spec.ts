import { RequestMethod, RequestOptions, Response, ResponseOptions } from '@angular/http';

import { ApiErrorHandler } from '../../../api/error/ApiErrorHandler';
import { AlertType } from '../../../layout/alert/AlertType';
import MockApiErrorHandler from './MockApiErrorHandler';

describe('ApiErrorHandler', () => {
    it('getErrorAlert() get', done => {
        new MockApiErrorHandler()
            .getErrorAlert({}, {
                method: RequestMethod.Get
            } as RequestOptions)
            .subscribe(alert => {
                expect(alert).toEqual({
                    message: 'An error occurred while loading this page.',
                    type: AlertType.warning,
                    iconClass: undefined
                });

                done();
            });
    });

    it('getErrorAlert() get string', done => {
        new MockApiErrorHandler()
            .getErrorAlert({}, {
                method: 'gEt'
            } as any)
            .subscribe(alert => {
                expect(alert).toEqual({
                    message: 'An error occurred while loading this page.',
                    type: AlertType.warning,
                    iconClass: undefined
                });

                done();
            });
    });

    it('getErrorAlert() post', done => {
        new MockApiErrorHandler()
            .getErrorAlert({}, {
                method: RequestMethod.Post
            } as RequestOptions)
            .subscribe(alert => {
                expect(alert).toEqual({
                    message: 'An error occurred while saving your data.',
                    type: AlertType.error,
                    iconClass: undefined
                });

                done();
            });
    });

    it('getErrorAlert() post string', done => {
        new MockApiErrorHandler()
            .getErrorAlert({}, {
                method: 'pOsT'
            } as any)
            .subscribe(alert => {
                expect(alert).toEqual({
                    message: 'An error occurred while saving your data.',
                    type: AlertType.error,
                    iconClass: undefined
                });

                done();
            });
    });

    it('getErrorAlert() put', done => {
        new MockApiErrorHandler()
            .getErrorAlert({}, {
                method: RequestMethod.Put
            } as RequestOptions)
            .subscribe(alert => {
                expect(alert).toEqual({
                    message: 'An error occurred while saving your data.',
                    type: AlertType.error,
                    iconClass: undefined
                });

                done();
            });
    });

    it('getErrorAlert() delete', done => {
        new MockApiErrorHandler()
            .getErrorAlert({}, {
                method: RequestMethod.Delete
            } as RequestOptions)
            .subscribe(alert => {
                expect(alert).toEqual({
                    message: 'An error occurred while saving your data.',
                    type: AlertType.error,
                    iconClass: undefined
                });

                done();
            });
    });

    it('getErrorAlert() patch', done => {
        new MockApiErrorHandler()
            .getErrorAlert({}, {
                method: RequestMethod.Patch
            } as RequestOptions)
            .subscribe(alert => {
                expect(alert).toEqual({
                    message: 'An error occurred while saving your data.',
                    type: AlertType.error,
                    iconClass: undefined
                });

                done();
            });
    });

    it('getErrorAlert() head', done => {
        new MockApiErrorHandler()
            .getErrorAlert({}, {
                method: RequestMethod.Head
            } as RequestOptions)
            .subscribe(alert => {
                expect(alert).toEqual({
                    message: 'An error occurred while loading this page.',
                    type: AlertType.warning,
                    iconClass: undefined
                });

                done();
            });
    });

    it('getErrorAlert() options', done => {
        new MockApiErrorHandler()
            .getErrorAlert({}, {
                method: RequestMethod.Options
            } as RequestOptions)
            .subscribe(alert => {
                expect(alert).toEqual({
                    message: 'An error occurred while loading this page.',
                    type: AlertType.warning,
                    iconClass: undefined
                });

                done();
            });
    });

    it('getErrorAlert() noMethod', done => {
        new MockApiErrorHandler()
            .getErrorAlert({}, {} as RequestOptions)
            .subscribe(alert => {
                expect(alert).toEqual({
                    message: 'An error occurred while loading this page.',
                    type: AlertType.warning,
                    iconClass: undefined
                });

                done();
            });
    });

    it('getErrorAlert() 0', done => {
        new MockApiErrorHandler()
            .getErrorAlert(
                {
                    status: 0
                },
                {
                    method: RequestMethod.Get
                } as RequestOptions
            )
            .subscribe(alert => {
                expect(alert).toEqual({
                    message:
                        'Your browser can’t talk with the server.  Reloading may fix this.',
                    type: AlertType.warning,
                    iconClass: 'fa fa-microphone-slash'
                });

                done();
            });
    });

    it('getErrorAlert() 400 get', done => {
        new MockApiErrorHandler()
            .getErrorAlert(
                {
                    status: 400
                },
                {
                    method: RequestMethod.Get
                } as RequestOptions
            )
            .subscribe(alert => {
                expect(alert).toEqual({
                    message: 'An error occurred while loading this page.',
                    type: AlertType.warning,
                    iconClass: undefined
                });

                done();
            });
    });

    it('getErrorAlert() 400 post', done => {
        new MockApiErrorHandler()
            .getErrorAlert(
                {
                    status: 400
                },
                {
                    method: RequestMethod.Post
                } as RequestOptions
            )
            .subscribe(alert => {
                expect(alert).toEqual({
                    message: 'Please fix your data, and save again.',
                    type: AlertType.error,
                    iconClass: undefined
                });

                done();
            });
    });

    it('getErrorAlert() adalError', done => {
        new MockApiErrorHandler()
            .getErrorAlert('Token renewal operation failed due to timeout', {
                method: RequestMethod.Get
            } as RequestOptions)
            .subscribe(alert => {
                expect(alert).toEqual({
                    message: 'Your login expired.  Log out, and log in again.',
                    type: AlertType.warning,
                    iconClass: 'fa fa-id-badge'
                });

                done();
            });
    });

    it('getErrorAlert() softTimeout get', done => {
        new MockApiErrorHandler()
            .getErrorAlert('SoftTimeout', {
                method: RequestMethod.Get
            } as RequestOptions)
            .subscribe(alert => {
                expect(alert).toEqual({
                    message: 'The server is taking a long time.',
                    type: AlertType.warning,
                    iconClass: 'fa fa-spin fa-spinner'
                });

                done();
            });
    });

    it('getErrorAlert() softTimeout post', done => {
        new MockApiErrorHandler()
            .getErrorAlert('SoftTimeout', {
                method: RequestMethod.Post
            } as RequestOptions)
            .subscribe(alert => {
                expect(alert).toEqual({
                    message: 'It’s taking a long time for the server to save your data.',
                    type: AlertType.error,
                    iconClass: 'fa fa-spin fa-spinner'
                });

                done();
            });
    });

    it('getErrorAlert() 401 get', done => {
        new MockApiErrorHandler()
            .getErrorAlert(
                {
                    status: 401
                },
                {
                    method: RequestMethod.Get
                } as RequestOptions
            )
            .subscribe(alert => {
                expect(alert).toEqual({
                    message: 'You’re not allowed to read this.',
                    type: AlertType.warning,
                    iconClass: 'fa fa-id-badge'
                });

                done();
            });
    });

    it('getErrorAlert() 401 post', done => {
        new MockApiErrorHandler()
            .getErrorAlert(
                {
                    status: 401
                },
                {
                    method: RequestMethod.Post
                } as RequestOptions
            )
            .subscribe(alert => {
                expect(alert).toEqual({
                    message: 'You’re not allowed to do this.',
                    type: AlertType.error,
                    iconClass: 'fa fa-id-badge'
                });

                done();
            });
    });

    it('getErrorAlert() 403 get', done => {
        new MockApiErrorHandler()
            .getErrorAlert(
                {
                    status: 403
                },
                {
                    method: RequestMethod.Get
                } as RequestOptions
            )
            .subscribe(alert => {
                expect(alert).toEqual({
                    message: 'You’re not allowed to read this.',
                    type: AlertType.warning,
                    iconClass: 'fa fa-id-badge'
                });

                done();
            });
    });

    it('getErrorAlert() 403 post', done => {
        new MockApiErrorHandler()
            .getErrorAlert(
                {
                    status: 403
                },
                {
                    method: RequestMethod.Post
                } as RequestOptions
            )
            .subscribe(alert => {
                expect(alert).toEqual({
                    message: 'You’re not allowed to do this.',
                    type: AlertType.error,
                    iconClass: 'fa fa-id-badge'
                });

                done();
            });
    });

    it('getErrorAlert() 404 get', done => {
        new MockApiErrorHandler()
            .getErrorAlert(
                {
                    status: 404
                },
                {
                    method: RequestMethod.Get
                } as RequestOptions
            )
            .subscribe(alert => {
                expect(alert).toEqual({
                    message: 'Whatever you’re looking for can’t be found.',
                    type: AlertType.warning,
                    iconClass: 'fa fa-search'
                });

                done();
            });
    });

    it('getErrorAlert() 404 post', done => {
        new MockApiErrorHandler()
            .getErrorAlert(
                {
                    status: 404
                },
                {
                    method: RequestMethod.Post
                } as RequestOptions
            )
            .subscribe(alert => {
                expect(alert).toEqual({
                    message: 'Whatever you’re trying to edit can’t be found.',
                    type: AlertType.error,
                    iconClass: 'fa fa-search'
                });

                done();
            });
    });

    it('getErrorAlert() 431', done => {
        new MockApiErrorHandler()
            .getErrorAlert(
                {
                    status: 431
                },
                {
                    method: RequestMethod.Get
                } as RequestOptions
            )
            .subscribe(alert => {
                expect(alert).toEqual({
                    message: 'This is too much information for the server to handle!',
                    type: AlertType.warning,
                    iconClass: 'fa fa-balance-scale'
                });

                done();
            });
    });

    it('getErrorAlert() 451 get', done => {
        new MockApiErrorHandler()
            .getErrorAlert(
                {
                    status: 451
                },
                {
                    method: RequestMethod.Get
                } as RequestOptions
            )
            .subscribe(alert => {
                expect(alert).toEqual({
                    message: 'This information cannot be read for legal reasons.',
                    type: AlertType.warning,
                    iconClass: 'fa fa-legal'
                });

                done();
            });
    });

    it('getErrorAlert() 451 post', done => {
        new MockApiErrorHandler()
            .getErrorAlert(
                {
                    status: 451
                },
                {
                    method: RequestMethod.Post
                } as RequestOptions
            )
            .subscribe(alert => {
                expect(alert).toEqual({
                    message: 'This information cannot be edited for legal reasons.',
                    type: AlertType.error,
                    iconClass: 'fa fa-legal'
                });

                done();
            });
    });

    it('getErrorAlert() 500 get', done => {
        new MockApiErrorHandler()
            .getErrorAlert(
                {
                    status: 500
                },
                {
                    method: RequestMethod.Get
                } as RequestOptions
            )
            .subscribe(alert => {
                expect(alert).toEqual({
                    message:
                        'A bug on the server prevented this page from loading.  ' +
                        'Reload the page, and try again.',
                    type: AlertType.warning,
                    iconClass: 'fa fa-bug'
                });

                done();
            });
    });

    it('getErrorAlert() 500 post', done => {
        new MockApiErrorHandler()
            .getErrorAlert(
                {
                    status: 500
                },
                {
                    method: RequestMethod.Post
                } as RequestOptions
            )
            .subscribe(alert => {
                expect(alert).toEqual({
                    message:
                        'A bug on the server prevented your data from saving.  ' +
                        'Reload the page, and try again.',
                    type: AlertType.error,
                    iconClass: 'fa fa-bug'
                });

                done();
            });
    });

    it('getErrorAlert() 501', done => {
        new MockApiErrorHandler()
            .getErrorAlert(
                {
                    status: 501
                },
                {
                    method: RequestMethod.Get
                } as RequestOptions
            )
            .subscribe(alert => {
                expect(alert).toEqual({
                    message: 'We haven’t gotten around to implementing this yet.',
                    type: AlertType.warning,
                    iconClass: 'fa fa-bicycle'
                });

                done();
            });
    });

    it('getErrorAlert() 502', done => {
        new MockApiErrorHandler()
            .getErrorAlert(
                {
                    status: 502
                },
                {
                    method: RequestMethod.Get
                } as RequestOptions
            )
            .subscribe(alert => {
                expect(alert).toEqual({
                    message:
                        'Your data didn’t make it through the servers.  ' +
                        'Usually reloading fixes this.',
                    type: AlertType.warning,
                    iconClass: 'fa fa-rocket'
                });

                done();
            });
    });

    it('getErrorAlert() 503', done => {
        new MockApiErrorHandler()
            .getErrorAlert(
                {
                    status: 503
                },
                {
                    method: RequestMethod.Get
                } as RequestOptions
            )
            .subscribe(alert => {
                expect(alert).toEqual({
                    message: 'The server is temporarily down.  Reloading may fix this.',
                    type: AlertType.warning,
                    iconClass: 'fa fa-clock-o'
                });

                done();
            });
    });

    it('getErrorAlert() 504', done => {
        new MockApiErrorHandler()
            .getErrorAlert(
                {
                    status: 504
                },
                {
                    method: RequestMethod.Get
                } as RequestOptions
            )
            .subscribe(alert => {
                expect(alert).toEqual({
                    message:
                        'The server didn’t respond fast enough.  ' +
                        'Maybe when you reload the server will respond faster.',
                    type: AlertType.warning,
                    iconClass: 'fa fa-hourglass-end'
                });

                done();
            });
    });

    it('getErrorAlert() 504 TimeoutError', done => {
        new MockApiErrorHandler()
            .getErrorAlert(
                {
                    name: 'TimeoutError'
                },
                {
                    method: RequestMethod.Get
                } as RequestOptions
            )
            .subscribe(alert => {
                expect(alert).toEqual({
                    message:
                        'The server didn’t respond fast enough.  ' +
                        'Maybe when you reload the server will respond faster.',
                    type: AlertType.warning,
                    iconClass: 'fa fa-hourglass-end'
                });

                done();
            });
    });

    it('getErrorAlert() 508 get', done => {
        new MockApiErrorHandler()
            .getErrorAlert(
                {
                    status: 508
                },
                {
                    method: RequestMethod.Get
                } as RequestOptions
            )
            .subscribe(alert => {
                expect(alert).toEqual({
                    message: 'An infinite loop prevented your data from being displayed.',
                    type: AlertType.warning,
                    iconClass: 'fa fa-circle-o-notch'
                });

                done();
            });
    });

    it('getErrorAlert() 508 post', done => {
        new MockApiErrorHandler()
            .getErrorAlert(
                {
                    status: 508
                },
                {
                    method: RequestMethod.Post
                } as RequestOptions
            )
            .subscribe(alert => {
                expect(alert).toEqual({
                    message: 'An infinite loop prevented your data from being saved.',
                    type: AlertType.error,
                    iconClass: 'fa fa-circle-o-notch'
                });

                done();
            });
    });

    it('getErrorAlert() offline', done => {
        var api = new MockApiErrorHandler();

        spyOn(api, 'isOffline').and.returnValue(true);

        api
            .getErrorAlert(
                {
                    status: 0
                },
                {
                    method: RequestMethod.Get
                } as RequestOptions
            )
            .subscribe(alert => {
                expect(alert).toEqual({
                    message: 'You are disconnected from the Internet.',
                    type: AlertType.warning,
                    iconClass: 'fa fa-wifi'
                });

                done();
            });
    });

    it('getProblemCode() "problem"', () => {
        var response = new Response({
            body: '{"problem":"DatabaseFull"}'
        } as ResponseOptions);

        expect(new MockApiErrorHandler().getProblemCode(response)).toBe('DatabaseFull');
    });

    it('getProblemCode() "error"', () => {
        var response = new Response({
            body: '{"error":"DatabaseFull"}'
        } as ResponseOptions);

        expect(new MockApiErrorHandler().getProblemCode(response)).toBe('DatabaseFull');
    });

    it('getProblemCode() emptyJson', () => {
        var response = new Response({
            body: '{}'
        } as ResponseOptions);

        expect(new MockApiErrorHandler().getProblemCode(response)).toBeUndefined();
    });

    it('getProblemCode() noJson', () => {
        expect(new MockApiErrorHandler().getProblemCode(null)).toBeUndefined();
    });

    it('handleError()', () => {
        var api = new MockApiErrorHandler(),
            alertService = api.getAlertService();

        spyOn(alertService, 'setAlert');

        var thrown = api.handleError({}, {
            method: RequestMethod.Get
        } as RequestOptions);

        expect(thrown).not.toBeUndefined();
        expect(thrown).not.toBeNull();
        expect(alertService.setAlert).toHaveBeenCalledTimes(1);
        expect(alertService.setAlert).toHaveBeenCalledWith({
            message: 'An error occurred while loading this page.',
            type: AlertType.warning,
            iconClass: undefined
        });
    });

    it('handleSoftTimeout() get', done => {
        var api = new MockApiErrorHandler(),
            alertService = api.getAlertService();

        spyOn(alertService, 'addAlert').and.callThrough();
        spyOn(alertService, 'remove').and.callThrough();

        var removeTimeoutMessage = api.handleSoftTimeout({
            method: RequestMethod.Get
        } as RequestOptions);

        expect(alertService.addAlert).toHaveBeenCalledTimes(1);
        expect(alertService.addAlert).toHaveBeenCalledWith({
            message: 'The server is taking a long time.',
            type: AlertType.warning,
            iconClass: 'fa fa-spin fa-spinner'
        });
        expect(alertService.remove).not.toHaveBeenCalled();

        removeTimeoutMessage();

        expect(alertService.addAlert).toHaveBeenCalledTimes(1);
        expect(alertService.remove).toHaveBeenCalledTimes(1);

        alertService.alerts.all.subscribe(alerts => {
            expect(alerts).toEqual([]);
            done();
        });
    });

    it('handleSoftTimeout() post', done => {
        var api = new MockApiErrorHandler(),
            alertService = api.getAlertService();

        spyOn(alertService, 'addAlert').and.callThrough();
        spyOn(alertService, 'remove').and.callThrough();

        var removeTimeoutMessage = api.handleSoftTimeout({
            method: RequestMethod.Post
        } as RequestOptions);

        expect(alertService.addAlert).toHaveBeenCalledTimes(1);
        expect(alertService.addAlert).toHaveBeenCalledWith({
            message: 'It’s taking a long time for the server to save your data.',
            type: AlertType.error,
            iconClass: 'fa fa-spin fa-spinner'
        });
        expect(alertService.remove).not.toHaveBeenCalled();

        removeTimeoutMessage();

        expect(alertService.addAlert).toHaveBeenCalledTimes(1);
        expect(alertService.remove).toHaveBeenCalledTimes(1);

        alertService.alerts.all.subscribe(alerts => {
            expect(alerts).toEqual([]);
            done();
        });
    });

    it('isWriteRequest() get implicit', () => {
        expect(ApiErrorHandler.isWriteRequest({} as RequestOptions)).toBe(false);
    });

    it('isWriteRequest() read', () => {
        expect(
            ApiErrorHandler.isWriteRequest({
                method: RequestMethod.Get
            } as RequestOptions)
        ).toBe(false);

        expect(
            ApiErrorHandler.isWriteRequest({
                method: RequestMethod.Options
            } as RequestOptions)
        ).toBe(false);

        expect(
            ApiErrorHandler.isWriteRequest({
                method: RequestMethod.Head
            } as RequestOptions)
        ).toBe(false);
    });

    it('isWriteRequest() write', () => {
        expect(
            ApiErrorHandler.isWriteRequest({
                method: RequestMethod.Post
            } as RequestOptions)
        ).toBe(true);

        expect(
            ApiErrorHandler.isWriteRequest({
                method: RequestMethod.Put
            } as RequestOptions)
        ).toBe(true);

        expect(
            ApiErrorHandler.isWriteRequest({
                method: RequestMethod.Delete
            } as RequestOptions)
        ).toBe(true);

        expect(
            ApiErrorHandler.isWriteRequest({
                method: RequestMethod.Patch
            } as RequestOptions)
        ).toBe(true);
    });
});
