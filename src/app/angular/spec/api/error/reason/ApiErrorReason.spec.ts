import { getReason } from '../../../../api/error/reason/ApiErrorReason';

describe('ApiErrorReason', () => {
    it('getReason() stringReason', () => {
        expect(getReason('Awesome', true)).toBe('Awesome');
        expect(getReason('Awesome', false)).toBe('Awesome');
    });

    it('getReason() stringReason nullWrite', () => {
        expect(getReason('Awesome', null)).toBe('Awesome');
        expect(getReason('Awesome', undefined)).toBe('Awesome');
    });

    it('getReason() objectReason', () => {
        expect(getReason({ Read: 'Reading', Write: 'Writing' }, true)).toBe('Writing');
        expect(getReason({ Read: 'Reading', Write: 'Writing' }, false)).toBe('Reading');
    });

    it('getReason() objectReason nullWrite', () => {
        expect(getReason({ Read: 'Reading', Write: 'Writing' }, null)).toBe('Reading');
        expect(getReason({ Read: 'Reading', Write: 'Writing' }, undefined)).toBe(
            'Reading'
        );
    });
});
