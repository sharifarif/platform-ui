import { RequestOptions } from '@angular/http';

import { AlertService } from '../../layout/alert/Alert.service';
import { BaseAuthorizedApi } from '../../api/BaseAuthorizedApi';
import MockApiErrorHandler from './error/MockApiErrorHandler';
import MockAuthService from '../login/adal/MockAuth.service';
import MockTranslateService from '../i18n/MockTranslateService';

/**
 * Mock {@link BaseAuthorizedApi} for unit testing.
 */
export default class MockAuthorizedApi extends BaseAuthorizedApi<any> {
    constructor(authService: MockAuthService, public baseUrl?: string) {
        super(new MockApiErrorHandler(), authService, {} as any, {} as any);
    }

    buildOptionsAsync(options?: RequestOptions) {
        return super.buildOptionsAsync(options);
    }

    protected getBaseUrl() {
        return this.baseUrl;
    }
}
