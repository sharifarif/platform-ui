import isVideo from '../../media/isVideo';

describe('isVideo()', () => {
    it('streaming object', () => {
        expect(
            isVideo({
                CDNPaths: {
                    '240w': 'tiny.jpg',
                    'H264 Broadband 720p': 'manifest.m3u8'
                }
            })
        ).toBe(true);
    });

    it('progressiveDownload object', () => {
        expect(
            isVideo({
                CDNPaths: {
                    '240w': 'tiny.jpg',
                    H264_4500kbps_AAC_und_ch2_128kbps: 'video.mp4'
                }
            })
        ).toBe(true);
    });

    it('video string', () => {
        expect(
            isVideo({
                CDNPaths: '240w:tiny.jpg\nH264_4500kbps_AAC_und_ch2_128kbps:video.mp4'
            })
        ).toBe(true);
    });

    it('image object', () => {
        expect(
            isVideo({
                CDNPaths: {
                    '240w': 'tiny.jpg'
                }
            })
        ).toBe(false);
    });

    it('image string', () => {
        expect(
            isVideo({
                CDNPaths: '240w:tiny.jpg'
            })
        ).toBe(false);
    });

    it('null', () => {
        expect(isVideo(undefined)).toBe(false);
        expect(isVideo(null)).toBe(false);
    });
});
