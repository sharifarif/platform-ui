import hasWidthHeight from '../../media/hasWidthHeight';
import { IMedia } from '../../model/media/IMedia';

describe('hasWidthHeight()', () => {
    it('hasWidthHeight()', () => {
        expect(
            hasWidthHeight({
                Width: 100,
                Height: 100
            } as IMedia)
        ).toBe(true);
    });

    it('hasWidthHeight() null', () => {
        expect(hasWidthHeight(undefined)).toBe(false);
        expect(hasWidthHeight(null)).toBe(false);
    });

    it('hasWidthHeight() null width', () => {
        expect(
            hasWidthHeight({
                Height: 100
            } as IMedia)
        ).toBe(false);
    });

    it('hasWidthHeight() null height', () => {
        expect(
            hasWidthHeight({
                Width: 100
            } as IMedia)
        ).toBe(false);
    });
});
