import { ElementRef } from '@angular/core';

import { BaseImgSizes } from '../../../media/sizes/BaseImgSizes';
import { IImgSizesWidth } from '../../../media/sizes/model/IImgSizesWidth';
import { MockImgSizes } from './MockImgSizes';

describe('BaseImgSizes', () => {
    var imgSizes: MockImgSizes;

    beforeEach(() => {
        imgSizes = new MockImgSizes();
    });

    it('getSizes()', () => {
        var originalElement = {} as ElementRef;

        imgSizes.getBreakpoints = () => {
            return [576, 768, 1005, 1255];
        };
        imgSizes.getWidth = (element: ElementRef, viewportWidthPixels: number) => {
            expect(element).toBe(originalElement);

            switch (viewportWidthPixels) {
                case 1255:
                    return {
                        pixels: 1200
                    };
                case 1005:
                    return {
                        pixels: 960
                    };
                case 768:
                    return {
                        pixels: 720
                    };
                case 576:
                    return {
                        pixels: 540
                    };
            }

            expect(viewportWidthPixels).toBeGreaterThanOrEqual(200);
            expect(viewportWidthPixels).toBeLessThanOrEqual(575);

            if (viewportWidthPixels < 576) {
                return {
                    pixels: -30,
                    viewportWidthPercentage: 100
                };
            }
        };

        expect(imgSizes.getSizes(originalElement)).toEqual([
            {
                media: {
                    minWidthPixels: 1255
                },
                width: {
                    pixels: 1200
                }
            },
            {
                media: {
                    minWidthPixels: 1005
                },
                width: {
                    pixels: 960
                }
            },
            {
                media: {
                    minWidthPixels: 768
                },
                width: {
                    pixels: 720
                }
            },
            {
                media: {
                    minWidthPixels: 576
                },
                width: {
                    pixels: 540
                }
            },
            {
                width: {
                    pixels: -30,
                    viewportWidthPercentage: 100
                }
            }
        ]);
    });

    it('getSizes() removeDuplicates', () => {
        imgSizes.getBreakpoints = () => {
            return [576, 768, 1005, 1255];
        };
        imgSizes.getWidth = (element: ElementRef, viewportWidthPixels: number) => {
            switch (viewportWidthPixels) {
                case 1255:
                    return {
                        pixels: 600
                    };
                case 1005:
                    return {
                        pixels: 600
                    };
                case 768:
                    return {
                        pixels: 600
                    };
                case 576:
                    return {
                        pixels: 300
                    };
            }

            if (viewportWidthPixels < 576) {
                return {
                    pixels: 300
                };
            }
        };

        expect(imgSizes.getSizes({} as ElementRef)).toEqual([
            {
                media: {
                    minWidthPixels: 768
                },
                width: {
                    pixels: 600
                }
            },
            {
                width: {
                    pixels: 300
                }
            }
        ]);
    });

    it('getSizesString()', () => {
        var element = {} as ElementRef;

        imgSizes.getSizes = passedElement => {
            expect(passedElement).toBe(element);

            return [
                {
                    media: {
                        minWidthPixels: 1255
                    },
                    width: {
                        pixels: 1200
                    }
                },
                {
                    media: {
                        minWidthPixels: 1005
                    },
                    width: {
                        pixels: 960
                    }
                },
                {
                    media: {
                        minWidthPixels: 768
                    },
                    width: {
                        pixels: 720
                    }
                },
                {
                    media: {
                        minWidthPixels: 576
                    },
                    width: {
                        pixels: 540
                    }
                },
                {
                    width: {
                        pixels: -30,
                        viewportWidthPercentage: 100
                    }
                }
            ];
        };

        expect(imgSizes.getSizesString(element)).toBe(
            '(min-width:1255px) 1200px, ' +
                '(min-width:1005px) 960px, ' +
                '(min-width:768px) 720px, ' +
                '(min-width:576px) 540px, ' +
                'calc(100vw-30px)'
        );
    });

    it('getSizesString() pixels', () => {
        imgSizes.getSizes = passedElement => {
            return [
                {
                    width: {
                        pixels: 300
                    }
                }
            ];
        };

        expect(imgSizes.getSizesString({} as ElementRef)).toBe('300px');
    });

    it('getSizesString() addingSubtracting', () => {
        imgSizes.getSizes = passedElement => {
            return [
                {
                    media: {
                        minWidthPixels: 576
                    },
                    width: {
                        pixels: 30,
                        viewportWidthPercentage: 50
                    }
                },
                {
                    width: {
                        pixels: -30,
                        viewportWidthPercentage: 50
                    }
                }
            ];
        };

        expect(imgSizes.getSizesString({} as ElementRef)).toBe(
            '(min-width:576px) calc(50vw+30px), calc(50vw-30px)'
        );
    });

    it('getSizesString() ignoreLastMediaCondition', () => {
        imgSizes.getSizes = passedElement => {
            return [
                {
                    media: {
                        minWidthPixels: 576
                    },
                    width: {
                        pixels: 540
                    }
                },
                {
                    media: {
                        minWidthPixels: 200
                    },
                    width: {
                        pixels: -30,
                        viewportWidthPercentage: 100
                    }
                }
            ];
        };

        expect(imgSizes.getSizesString({} as ElementRef)).toBe(
            '(min-width:576px) 540px, calc(100vw-30px)'
        );
    });

    it('getSizesString() rounding', () => {
        imgSizes.getSizes = passedElement => {
            return [
                {
                    media: {
                        minWidthPixels: 576.66666666666666667
                    },
                    width: {
                        pixels: 540.123456789
                    }
                },
                {
                    width: {
                        pixels: -30.123456789,
                        viewportWidthPercentage: 50.123456789
                    }
                }
            ];
        };

        expect(imgSizes.getSizesString({} as ElementRef)).toBe(
            '(min-width:576px) 540.12px, calc(50.1235vw-30.12px)'
        );
    });

    it('getSizesString() null', () => {
        imgSizes.getSizes = () => {
            return undefined;
        };
        expect(imgSizes.getSizesString(undefined)).toBeUndefined();

        imgSizes.getSizes = () => {
            return null;
        };
        expect(imgSizes.getSizesString(null)).toBeUndefined();
    });

    it('getSizesString() empty', () => {
        imgSizes.getSizes = () => {
            return [];
        };
        expect(imgSizes.getSizesString({} as ElementRef)).toBeUndefined();
    });

    it('getWidthPixels() pixels', () => {
        var originalElement = {} as ElementRef;

        imgSizes.getWidth = (element: ElementRef, viewportWidthPixels: number) => {
            expect(element).toBe(originalElement);
            expect(viewportWidthPixels).toBe(1255);

            return {
                pixels: 1200
            };
        };

        expect(imgSizes.getWidthPixels(originalElement, 1255)).toBe(1200);
    });

    it('getWidthPixels() viewportWidthPercentage', () => {
        var originalElement = {} as ElementRef;

        imgSizes.getWidth = (element: ElementRef, viewportWidthPixels: number) => {
            expect(element).toBe(originalElement);

            return {
                viewportWidthPercentage: 50
            };
        };

        expect(imgSizes.getWidthPixels(originalElement, 375)).toBe(187.5);
        expect(imgSizes.getWidthPixels(originalElement, 360)).toBe(180);
        expect(imgSizes.getWidthPixels(originalElement, 320)).toBe(160);
    });

    it('getWidthPixels() pixels+viewportWidthPercentage', () => {
        var originalElement = {} as ElementRef;

        imgSizes.getWidth = (element: ElementRef, viewportWidthPixels: number) => {
            expect(element).toBe(originalElement);

            return {
                pixels: -30,
                viewportWidthPercentage: 100
            };
        };

        expect(imgSizes.getWidthPixels(originalElement, 375)).toBe(345);
        expect(imgSizes.getWidthPixels(originalElement, 360)).toBe(330);
        expect(imgSizes.getWidthPixels(originalElement, 320)).toBe(290);
    });

    it('getWidthPixels() null', () => {
        imgSizes.getSizes = () => {
            return undefined;
        };
        expect(imgSizes.getWidthPixels({} as ElementRef, 1255)).toBeUndefined();

        imgSizes.getSizes = () => {
            return null;
        };
        expect(imgSizes.getWidthPixels({} as ElementRef, 1255)).toBeUndefined();
    });

    it('isPassingMediaCondition() minWidthPixels', () => {
        expect(
            BaseImgSizes.isPassingMediaCondition(
                {
                    minWidthPixels: 767
                },
                768
            )
        ).toBe(true);
        expect(
            BaseImgSizes.isPassingMediaCondition(
                {
                    minWidthPixels: 768
                },
                768
            )
        ).toBe(true);

        expect(
            BaseImgSizes.isPassingMediaCondition(
                {
                    minWidthPixels: 769
                },
                768
            )
        ).toBe(false);
    });

    it('isPassingMediaCondition() maxWidthPixels', () => {
        expect(
            BaseImgSizes.isPassingMediaCondition(
                {
                    maxWidthPixels: 769
                },
                768
            )
        ).toBe(true);
        expect(
            BaseImgSizes.isPassingMediaCondition(
                {
                    maxWidthPixels: 768
                },
                768
            )
        ).toBe(true);

        expect(
            BaseImgSizes.isPassingMediaCondition(
                {
                    maxWidthPixels: 767
                },
                768
            )
        ).toBe(false);
    });

    it('isPassingMediaCondition() minWidthPixels & maxWidthPixels', () => {
        expect(
            BaseImgSizes.isPassingMediaCondition(
                {
                    minWidthPixels: 500,
                    maxWidthPixels: 1000
                },
                500
            )
        ).toBe(true);
        expect(
            BaseImgSizes.isPassingMediaCondition(
                {
                    minWidthPixels: 500,
                    maxWidthPixels: 1000
                },
                501
            )
        ).toBe(true);
        expect(
            BaseImgSizes.isPassingMediaCondition(
                {
                    minWidthPixels: 500,
                    maxWidthPixels: 1000
                },
                999
            )
        ).toBe(true);
        expect(
            BaseImgSizes.isPassingMediaCondition(
                {
                    minWidthPixels: 500,
                    maxWidthPixels: 1000
                },
                1000
            )
        ).toBe(true);

        expect(
            BaseImgSizes.isPassingMediaCondition(
                {
                    minWidthPixels: 500,
                    maxWidthPixels: 1000
                },
                499
            )
        ).toBe(false);
        expect(
            BaseImgSizes.isPassingMediaCondition(
                {
                    minWidthPixels: 500,
                    maxWidthPixels: 1000
                },
                1001
            )
        ).toBe(false);
    });

    it('isPassingMediaCondition() null', () => {
        expect(BaseImgSizes.isPassingMediaCondition(undefined, 768)).toBe(true);
        expect(BaseImgSizes.isPassingMediaCondition(null, 768)).toBe(true);

        expect(BaseImgSizes.isPassingMediaCondition({}, 768)).toBe(true);
        expect(
            BaseImgSizes.isPassingMediaCondition(
                {
                    maxWidthPixels: undefined,
                    minWidthPixels: undefined
                },
                768
            )
        ).toBe(true);
        expect(
            BaseImgSizes.isPassingMediaCondition(
                {
                    maxWidthPixels: null,
                    minWidthPixels: null
                },
                768
            )
        ).toBe(true);
    });
});
