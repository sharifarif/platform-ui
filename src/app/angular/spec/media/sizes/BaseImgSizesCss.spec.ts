import { ElementRef } from '@angular/core';

import { IImgSizesCssProperty } from '../../../media/sizes/model/IImgSizesCssProperty';
import { MockImgSizesCss } from './MockImgSizesCss';

describe('BaseImgSizesCss', () => {
    var imgSizes: MockImgSizesCss;

    beforeEach(() => {
        imgSizes = new MockImgSizesCss();
    });

    it('getCssForElement()', () => {
        imgSizes.getCss = () => {
            return [
                {
                    class: ['modal-dialog'],
                    selector: [
                        {
                            media: {
                                minWidthPixels: 576
                            },
                            property: {
                                width: {
                                    pixels: 500
                                }
                            }
                        }
                    ]
                },
                {
                    class: ['modal-sm'],
                    selector: [
                        {
                            media: {
                                minWidthPixels: 576
                            },
                            property: {
                                width: {
                                    pixels: 300
                                }
                            }
                        }
                    ]
                },
                {
                    class: ['modal-lg'],
                    selector: [
                        {
                            media: {
                                minWidthPixels: 1005
                            },
                            property: {
                                width: {
                                    pixels: 800
                                }
                            }
                        }
                    ]
                }
            ];
        };

        var modalDialog = document.createElement('div');
        modalDialog.classList.add('modal-dialog');

        expect(imgSizes.getCssForElement(modalDialog, 575)).toEqual({});
        expect(imgSizes.getCssForElement(modalDialog, 1200)).toEqual({
            width: {
                pixels: 500
            }
        });

        var modalDialogSm = document.createElement('div');
        modalDialogSm.classList.add('modal-dialog');
        modalDialogSm.classList.add('modal-sm');

        expect(imgSizes.getCssForElement(modalDialogSm, 575)).toEqual({});
        expect(imgSizes.getCssForElement(modalDialogSm, 1200)).toEqual({
            width: {
                pixels: 300
            }
        });

        var modalDialogLg = document.createElement('div');
        modalDialogLg.classList.add('modal-dialog');
        modalDialogLg.classList.add('modal-lg');

        expect(imgSizes.getCssForElement(modalDialogLg, 575)).toEqual({});
        expect(imgSizes.getCssForElement(modalDialogLg, 1200)).toEqual({
            width: {
                pixels: 800
            }
        });
    });

    it('getCssForElement() lastWidthWins', () => {
        imgSizes.getCss = () => {
            return [
                {
                    class: ['percentPixels'],
                    selector: [
                        {
                            property: {
                                width: {
                                    percent: 50
                                }
                            }
                        },
                        {
                            property: {
                                width: {
                                    pixels: 500
                                }
                            }
                        }
                    ]
                },
                {
                    class: ['viewportWidthPercentage'],
                    selector: [
                        {
                            property: {
                                width: {
                                    viewportWidthPercentage: 25
                                }
                            }
                        }
                    ]
                }
            ];
        };

        var percentPixels = document.createElement('div');
        percentPixels.classList.add('percentPixels');

        expect(imgSizes.getCssForElement(percentPixels, 1200)).toEqual({
            width: {
                pixels: 500
            }
        });

        var allWidths = document.createElement('div');
        allWidths.classList.add('percentPixels');
        allWidths.classList.add('viewportWidthPercentage');

        expect(imgSizes.getCssForElement(allWidths, 1200)).toEqual({
            width: {
                viewportWidthPercentage: 25
            }
        });
    });

    it('getCssHierarchy()', () => {
        var container = document.createElement('div');
        container.classList.add('container');

        var row = document.createElement('row');
        row.classList.add('row');
        container.appendChild(row);

        var col6 = document.createElement('col-6');
        col6.classList.add('col-6');
        row.appendChild(col6);

        var img = document.createElement('img');
        col6.appendChild(img);

        imgSizes.getCssForElement = (
            element: HTMLElement,
            viewportWidthPixels: number
        ) => {
            expect(viewportWidthPixels).toBe(1255);

            var css: IImgSizesCssProperty;

            if (element.classList.contains('container')) {
                css = {
                    padding: {
                        left: 15,
                        right: 15
                    },
                    width: {
                        pixels: 1230
                    }
                };
            } else if (element.classList.contains('col-6')) {
                css = {
                    width: {
                        percent: 50
                    }
                };
            } else {
                css = {};
            }

            return css;
        };

        expect(imgSizes.getCssHierarchy(img, 1255)).toEqual([
            {
                padding: {
                    left: 15,
                    right: 15
                },
                width: {
                    pixels: 1230
                }
            },
            {},
            {
                width: {
                    percent: 50
                }
            },
            {}
        ]);
    });

    it('getCssHierarchy() stopAtFirstBaseWidth', () => {
        var container1 = document.createElement('div');
        container1.classList.add('container');

        var container2 = document.createElement('div');
        container2.classList.add('container');
        container1.appendChild(container2);

        var img = document.createElement('img');
        container2.appendChild(img);

        imgSizes.getCssForElement = (
            element: HTMLElement,
            viewportWidthPixels: number
        ) => {
            expect(viewportWidthPixels).toBe(1255);

            var css: IImgSizesCssProperty;

            if (element.classList.contains('container')) {
                css = {
                    padding: {
                        left: 15,
                        right: 15
                    },
                    width: {
                        pixels: 1230
                    }
                };
            } else {
                css = {};
            }

            return css;
        };

        expect(imgSizes.getCssHierarchy(img, 1255)).toEqual([
            {
                padding: {
                    left: 15,
                    right: 15
                },
                width: {
                    pixels: 1230
                }
            },
            {}
        ]);
    });

    it('getCssHierarchy() body ifNoBaseWidth', () => {
        var img = document.createElement('img');

        imgSizes.getCssForElement = (
            element: HTMLElement,
            viewportWidthPixels: number
        ) => {
            expect(viewportWidthPixels).toBe(1255);

            return {};
        };

        expect(imgSizes.getCssHierarchy(img, 1255)).toEqual([
            {
                width: {
                    viewportWidthPercentage: 100
                }
            },
            {}
        ]);
    });

    // On WebWorkers, Renderer2.parentNode() always returns a value.
    // See https://angular.io/api/core/Renderer2#parentNode
    it('getCssHierarchy() webWorkerParent', () => {
        var img = {
            tagName: 'img',
            parentNode: {}
        } as HTMLElement;

        expect(imgSizes.getCssHierarchy(img, 1255)).toEqual([
            {
                width: {
                    viewportWidthPercentage: 100
                }
            },
            {}
        ]);
    });

    it('getCssHierarchy() null', () => {
        expect(imgSizes.getCssHierarchy(undefined, 1255)).toEqual([]);
        expect(imgSizes.getCssHierarchy(null, 1255)).toEqual([]);
    });

    it('getWidth()', () => {
        var img = {} as HTMLElement;

        imgSizes.getCssHierarchy = (
            element: HTMLElement,
            viewportWidthPixels: number
        ) => {
            expect(element).toBe(img);

            return [
                {
                    padding: {
                        left: 15,
                        right: 15
                    },
                    width: {
                        pixels: 1230
                    }
                },
                {
                    margin: {
                        left: 10,
                        right: 10
                    }
                },
                {
                    width: {
                        percent: 50
                    }
                }
            ];
        };

        expect(imgSizes.getWidth({ nativeElement: img } as ElementRef, 1255)).toEqual({
            pixels: 590,
            viewportWidthPercentage: 0
        });
    });

    it('getWidth() viewportWidthPercentage', () => {
        var img = {} as HTMLElement;

        imgSizes.getCssHierarchy = (
            element: HTMLElement,
            viewportWidthPixels: number
        ) => {
            expect(element).toBe(img);

            return [
                {
                    padding: {
                        left: 15,
                        right: 15
                    },
                    width: {
                        viewportWidthPercentage: 100
                    }
                },
                {
                    margin: {
                        left: 10,
                        right: 10
                    }
                },
                {
                    width: {
                        percent: 50
                    }
                }
            ];
        };

        expect(imgSizes.getWidth({ nativeElement: img } as ElementRef, 1255)).toEqual({
            pixels: -25,
            viewportWidthPercentage: 50
        });
    });

    it('getWidth() ignoreMargin baseWidth', () => {
        var img = {} as HTMLElement;

        imgSizes.getCssHierarchy = (
            element: HTMLElement,
            viewportWidthPixels: number
        ) => {
            expect(element).toBe(img);

            return [
                {
                    margin: {
                        left: 10,
                        right: 10
                    },
                    padding: {
                        left: 15,
                        right: 15
                    },
                    width: {
                        pixels: 1230
                    }
                },
                {
                    margin: {
                        left: 10,
                        right: 10
                    }
                },
                {
                    width: {
                        percent: 50
                    }
                }
            ];
        };

        expect(imgSizes.getWidth({ nativeElement: img } as ElementRef, 1255)).toEqual({
            pixels: 590,
            viewportWidthPercentage: 0
        });
    });

    it('getWidth() ignoreMargin widthPercent', () => {
        var img = {} as HTMLElement;

        imgSizes.getCssHierarchy = (
            element: HTMLElement,
            viewportWidthPixels: number
        ) => {
            expect(element).toBe(img);

            return [
                {
                    padding: {
                        left: 15,
                        right: 15
                    },
                    width: {
                        pixels: 1230
                    }
                },
                {
                    margin: {
                        left: 10,
                        right: 10
                    }
                },
                {
                    margin: {
                        left: 10,
                        right: 10
                    },
                    width: {
                        percent: 50
                    }
                },
                {}
            ];
        };

        expect(imgSizes.getWidth({ nativeElement: img } as ElementRef, 1255)).toEqual({
            pixels: 590,
            viewportWidthPercentage: 0
        });
    });

    it('getWidth() ignoreWidthPercent baseWidth', () => {
        var img = {} as HTMLElement;

        imgSizes.getCssHierarchy = (
            element: HTMLElement,
            viewportWidthPixels: number
        ) => {
            expect(element).toBe(img);

            return [
                {
                    padding: {
                        left: 15,
                        right: 15
                    },
                    width: {
                        percent: 75,
                        pixels: 1230
                    }
                },
                {
                    margin: {
                        left: 10,
                        right: 10
                    }
                },
                {
                    width: {
                        percent: 50
                    }
                }
            ];
        };

        expect(imgSizes.getWidth({ nativeElement: img } as ElementRef, 1255)).toEqual({
            pixels: 590,
            viewportWidthPercentage: 0
        });
    });

    it('getWidth() ignorePadding image', () => {
        var img = {} as HTMLElement;

        imgSizes.getCssHierarchy = (
            element: HTMLElement,
            viewportWidthPixels: number
        ) => {
            expect(element).toBe(img);

            return [
                {
                    padding: {
                        left: 15,
                        right: 15
                    },
                    width: {
                        pixels: 1230
                    }
                },
                {
                    margin: {
                        left: 10,
                        right: 10
                    }
                },
                {
                    padding: {
                        left: 15,
                        right: 15
                    },
                    width: {
                        percent: 50
                    }
                }
            ];
        };

        expect(imgSizes.getWidth({ nativeElement: img } as ElementRef, 1255)).toEqual({
            pixels: 590,
            viewportWidthPercentage: 0
        });
    });

    it('getWidth() null', () => {
        expect(imgSizes.getWidth(undefined, 1255)).toEqual({
            pixels: undefined,
            viewportWidthPercentage: undefined
        });
        expect(imgSizes.getWidth(null, 1255)).toEqual({
            pixels: undefined,
            viewportWidthPercentage: undefined
        });
    });
});
