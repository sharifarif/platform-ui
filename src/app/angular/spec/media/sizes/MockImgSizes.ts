import { ElementRef } from '@angular/core';

import { BaseImgSizes } from '../../../media/sizes/BaseImgSizes';
import { IImgSizesWidth } from '../../../media/sizes/model/IImgSizesWidth';

/**
 * Mock {@link BaseImgSizes} for unit testing.
 */
export class MockImgSizes extends BaseImgSizes {
    getBreakpoints(): number[] {
        return [];
    }

    getSizes(element: ElementRef) {
        return super.getSizes(element);
    }

    getWidth(element: ElementRef, viewportWidthPixels: number): IImgSizesWidth {
        return undefined;
    }
}
