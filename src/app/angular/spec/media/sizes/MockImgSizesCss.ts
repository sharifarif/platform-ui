import { ElementRef } from '@angular/core';

import { BaseImgSizesCss } from '../../../media/sizes/BaseImgSizesCss';
import { IImgSizesCss } from '../../../media/sizes/model/IImgSizesCss';
import { MockRendererFactory2 } from '../../MockRendererFactory2';

/**
 * Mock {@link BaseImgSizesCss} for unit testing.
 */
export class MockImgSizesCss extends BaseImgSizesCss {
    constructor() {
        super(MockRendererFactory2);
    }

    getBreakpoints(): number[] {
        return [];
    }

    getCss(): IImgSizesCss[] {
        return [];
    }

    getCssForElement(element: HTMLElement, viewportWidthPixels: number) {
        return super.getCssForElement(element, viewportWidthPixels);
    }

    getCssHierarchy(element: HTMLElement, viewportWidthPixels: number) {
        return super.getCssHierarchy(element, viewportWidthPixels);
    }

    getWidth(element: ElementRef, viewportWidthPixels: number) {
        return super.getWidth(element, viewportWidthPixels);
    }
}
