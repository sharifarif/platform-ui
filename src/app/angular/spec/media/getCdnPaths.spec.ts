import getCdnPaths from '../../media/getCdnPaths';

describe('getCdnPaths()', () => {
    it('object', () => {
        expect(
            getCdnPaths({
                CDNPaths: {
                    '240w': 'tiny.jpg',
                    '720w': 'medium.jpg',
                    '1440w': 'big.jpg'
                }
            })
        ).toEqual({
            '240w': 'tiny.jpg',
            '720w': 'medium.jpg',
            '1440w': 'big.jpg'
        });
    });

    it('string', () => {
        expect(
            getCdnPaths({
                CDNPaths: '1440w:big.jpg\n240w:tiny.jpg\n720w:medium.jpg'
            })
        ).toEqual({
            '240w': 'tiny.jpg',
            '720w': 'medium.jpg',
            '1440w': 'big.jpg'
        });
    });

    it('string fullUrl', () => {
        expect(
            getCdnPaths({
                CDNPaths:
                    '1440w:https://image.essity.com/big.jpg\n' +
                    '240w:https://image.essity.com/tiny.jpg\n' +
                    '720w:https://image.essity.com/medium.jpg'
            })
        ).toEqual({
            '240w': 'https://image.essity.com/tiny.jpg',
            '720w': 'https://image.essity.com/medium.jpg',
            '1440w': 'https://image.essity.com/big.jpg'
        });
    });

    it('null', () => {
        expect(
            getCdnPaths({
                CDNPaths: undefined
            })
        ).toEqual({});
        expect(
            getCdnPaths({
                CDNPaths: null
            })
        ).toEqual({});

        expect(getCdnPaths(undefined)).toEqual({});
        expect(getCdnPaths(null)).toEqual({});
    });

    it('empty', () => {
        expect(
            getCdnPaths({
                CDNPaths: ''
            })
        ).toEqual({});

        expect(
            getCdnPaths({
                CDNPaths: '240w:\n720w:\nOriginal:\n'
            })
        ).toEqual({});
    });
});
