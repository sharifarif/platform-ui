import { IMedia } from '../../model/media/IMedia';
import { PictureComponent } from '../../media/Picture.component';

describe('PictureComponent', () => {
    var pictureComponent: PictureComponent;

    beforeEach(() => {
        pictureComponent = new PictureComponent();
    });

    it('getImgClass()', () => {
        pictureComponent.imgClass = 'awesome sauce';

        expect(pictureComponent.getImgClass()).toBe('awesome sauce');
    });

    it('getImgClass() responsive', () => {
        pictureComponent.image = {
            Width: 100,
            Height: 100
        } as IMedia;
        pictureComponent.responsive = true;

        expect(pictureComponent.getImgClass()).toBe('embed-responsive-item');
    });

    it('getImgClass() responsive missing widthHeight', () => {
        pictureComponent.image = {} as IMedia;
        pictureComponent.responsive = true;

        expect(pictureComponent.getImgClass()).toBe('img-fluid');
    });

    it('getImgClass() responsive + imgClass', () => {
        pictureComponent.image = {
            Width: 100,
            Height: 100
        } as IMedia;
        pictureComponent.imgClass = 'awesome sauce';
        pictureComponent.responsive = true;

        expect(pictureComponent.getImgClass()).toBe(
            'awesome sauce embed-responsive-item'
        );
    });

    it('getImgClass() null', () => {
        expect(pictureComponent.getImgClass()).toBe(undefined);
    });

    it('hasWebp()', () => {
        pictureComponent.image = {
            CDNPaths: {
                '240w': 'tiny.jpg',
                '720w': 'medium.jpg',
                '720w_webp': 'medium.webp',
                '1440w': 'big.png'
            }
        };

        expect(pictureComponent.hasWebp()).toBe(true);
    });

    it('hasWebp() cdnPathsString', () => {
        pictureComponent.image = {
            CDNPaths:
                '1440w:big.jpg\n240w:tiny.jpg\n720w_webp:medium.webp\n720w:medium.jpg'
        };

        expect(pictureComponent.hasWebp()).toBe(true);
    });

    it('hasWebp() false', () => {
        pictureComponent.image = {
            CDNPaths: {
                '240w': 'tiny.jpg',
                '720w': 'medium.jpg',
                '1440w': 'big.png'
            }
        };

        expect(pictureComponent.hasWebp()).toBe(false);
    });

    it('hasWebp() false cdnPathsString', () => {
        pictureComponent.image = {
            CDNPaths: '1440w:big.jpg\n240w:tiny.jpg\n720w:medium.jpg'
        };

        expect(pictureComponent.hasWebp()).toBe(false);
    });

    it('hasWebp() null', () => {
        expect(pictureComponent.hasWebp()).toBe(false);
    });

    it('hasWebp() null cdnPaths', () => {
        pictureComponent.image = {} as IMedia;

        expect(pictureComponent.hasWebp()).toBe(false);
    });
});
