import { ElementRef } from '@angular/core';

import { ImgSrcDirective } from '../../media/ImgSrc.directive';
import { ImgSizesBootstrap } from '../../media/sizes/ImgSizesBootstrap';
import { MockRendererFactory2 } from '../MockRendererFactory2';

describe('ImgSrcDirective', () => {
    var img: HTMLImageElement,
        imgSizes: ImgSizesBootstrap,
        imgSrcDirective: ImgSrcDirective;

    beforeEach(() => {
        img = {} as HTMLImageElement;
        imgSizes = new ImgSizesBootstrap(MockRendererFactory2);
        imgSrcDirective = new ImgSrcDirective(new ElementRef(img), imgSizes);
    });

    it('getSrc()', () => {
        expect(
            ImgSrcDirective.getSrc(
                {
                    CDNPaths: {
                        '240w': 'tiny.jpg',
                        '720w': 'medium.jpg',
                        '1440w': 'big.jpg'
                    }
                },
                239
            )
        ).toBe('tiny.jpg');
        expect(
            ImgSrcDirective.getSrc(
                {
                    CDNPaths: {
                        '240w': 'tiny.jpg',
                        '720w': 'medium.jpg',
                        '1440w': 'big.jpg'
                    }
                },
                240
            )
        ).toBe('tiny.jpg');

        expect(
            ImgSrcDirective.getSrc(
                {
                    CDNPaths: {
                        '240w': 'tiny.jpg',
                        '720w': 'medium.jpg',
                        '1440w': 'big.jpg'
                    }
                },
                241
            )
        ).toBe('medium.jpg');
        expect(
            ImgSrcDirective.getSrc(
                {
                    CDNPaths: {
                        '240w': 'tiny.jpg',
                        '720w': 'medium.jpg',
                        '1440w': 'big.jpg'
                    }
                },
                720
            )
        ).toBe('medium.jpg');

        expect(
            ImgSrcDirective.getSrc(
                {
                    CDNPaths: {
                        '240w': 'tiny.jpg',
                        '720w': 'medium.jpg',
                        '1440w': 'big.jpg'
                    }
                },
                721
            )
        ).toBe('big.jpg');
        expect(
            ImgSrcDirective.getSrc(
                {
                    CDNPaths: {
                        '240w': 'tiny.jpg',
                        '720w': 'medium.jpg',
                        '1440w': 'big.jpg'
                    }
                },
                1440
            )
        ).toBe('big.jpg');

        expect(
            ImgSrcDirective.getSrc(
                {
                    CDNPaths: {
                        '240w': 'tiny.jpg',
                        '720w': 'medium.jpg',
                        '1440w': 'big.jpg'
                    }
                },
                1441
            )
        ).toBe('big.jpg');
    });

    it('getSrc() null', () => {
        expect(ImgSrcDirective.getSrc(undefined, 720)).toBeUndefined();
        expect(ImgSrcDirective.getSrc(null, 720)).toBeUndefined();
    });

    it('getSrcSet()', () => {
        expect(
            ImgSrcDirective.getSrcSet({
                CDNPaths: {
                    '240w': 'tiny.jpg',
                    '720w': 'medium.jpg',
                    '1440w': 'big.jpg'
                }
            })
        ).toBe('tiny.jpg 240w, medium.jpg 720w, big.jpg 1440w');
    });

    it('getSrcSet() allUrls', () => {
        expect(
            ImgSrcDirective.getSrcSet({
                CDNPaths: {
                    '240w': 'https://image.essity.com/240w.jpg',
                    '480w': 'https://image.essity.com/480w.jpg',
                    '690w': 'https://image.essity.com/690w.jpg',
                    '720w': 'https://image.essity.com/720w.jpg',
                    '1080w': 'https://image.essity.com/1080w.jpg',
                    '1200w': 'https://image.essity.com/1200w.jpg',
                    '1440w': 'https://image.essity.com/1440w.jpg',
                    Original: 'https://image.essity.com/Original.jpg'
                }
            })
        ).toBe(
            'https://image.essity.com/240w.jpg 240w, ' +
                'https://image.essity.com/480w.jpg 480w, ' +
                'https://image.essity.com/690w.jpg 690w, ' +
                'https://image.essity.com/720w.jpg 720w, ' +
                'https://image.essity.com/1080w.jpg 1080w, ' +
                'https://image.essity.com/1200w.jpg 1200w, ' +
                'https://image.essity.com/1440w.jpg 1440w'
        );
    });

    it('getSrcSet() webp', () => {
        expect(
            ImgSrcDirective.getSrcSet(
                {
                    CDNPaths: {
                        '240w_webp': 'tiny.webp',
                        '720w_webp': 'medium.webp',
                        '1440w_webp': 'big.webp'
                    }
                },
                true
            )
        ).toBe('tiny.webp 240w, medium.webp 720w, big.webp 1440w');
    });

    it('getSrcSet() webp jpgPngFallback', () => {
        expect(
            ImgSrcDirective.getSrcSet(
                {
                    CDNPaths: {
                        '240w': 'tiny.jpg',
                        '720w': 'medium.jpg',
                        '720w_webp': 'medium.webp',
                        '1440w': 'big.png'
                    }
                },
                true
            )
        ).toBe('tiny.jpg 240w, medium.webp 720w, big.png 1440w');
    });

    it('getSrcSet() null', () => {
        expect(ImgSrcDirective.getSrcSet(undefined)).toBeUndefined();
        expect(ImgSrcDirective.getSrcSet(null)).toBeUndefined();
    });

    it('getSrcSet() empty', () => {
        expect(
            ImgSrcDirective.getSrcSet({
                CDNPaths: {}
            })
        ).toBeUndefined();
    });

    it('sizes', () => {
        imgSizes.getSizesString = () => {
            return '(min-width:576px) calc(50vw+30px), calc(50vw-30px)';
        };

        imgSrcDirective.ngAfterViewInit();

        expect(img.sizes).toBe('(min-width:576px) calc(50vw+30px), calc(50vw-30px)');
    });

    it('src', () => {
        imgSizes.getWidthPixels = () => {
            return 720;
        };
        imgSrcDirective.platformImgSrc = {
            CDNPaths: {
                '240w': 'tiny.jpg',
                '720w': 'medium.jpg',
                '1440w': 'big.jpg'
            }
        };

        imgSrcDirective.ngAfterViewInit();

        expect(img.src).toBe('medium.jpg');
    });

    it('srcset', () => {
        imgSrcDirective.platformImgSrc = {
            CDNPaths: {
                '240w': 'tiny.jpg',
                '720w': 'medium.jpg',
                '1440w': 'big.jpg'
            }
        };

        imgSrcDirective.ngAfterViewInit();

        expect(img.srcset).toBe('tiny.jpg 240w, medium.jpg 720w, big.jpg 1440w');
    });

    it('render()', () => {
        imgSrcDirective.platformImgSrc = {
            CDNPaths: {
                '240w': 'tiny.jpg',
                '720w': 'medium.jpg',
                '1440w': 'big.jpg'
            }
        };

        imgSrcDirective.ngAfterViewInit();

        expect(img.src).toBe('big.jpg');
        expect(img.srcset).toBe('tiny.jpg 240w, medium.jpg 720w, big.jpg 1440w');
        expect(img.sizes).toBe('calc(100vw)');
    });

    it('render() webp', () => {
        imgSrcDirective.platformImgSrc = {
            CDNPaths: {
                '240w': 'tiny.jpg',
                '240w_webp': 'tiny.webp',
                '720w': 'medium.jpg',
                '720w_webp': 'medium.webp',
                '1440w': 'big.jpg',
                '1440w_webp': 'big.webp'
            }
        };
        imgSrcDirective.platformImgWebp = true;

        imgSrcDirective.ngAfterViewInit();

        expect(img.src).toBe('big.jpg');
        expect(img.srcset).toBe('tiny.webp 240w, medium.webp 720w, big.webp 1440w');
        expect(img.sizes).toBe('calc(100vw)');
    });

    it('render() type pictureImg', () => {
        imgSrcDirective.platformImgSrc = {
            CDNPaths: {
                '240w': 'tiny.jpg',
                '720w': 'medium.jpg',
                '1440w': 'big.jpg'
            }
        };
        imgSrcDirective.platformImgType = 'pictureImg';

        imgSrcDirective.ngAfterViewInit();

        expect(img.src).toBe('big.jpg');
        expect(img.srcset).toBeUndefined();
        expect(img.sizes).toBeUndefined();
    });

    it('render() type pictureSource', () => {
        imgSrcDirective.platformImgSrc = {
            CDNPaths: {
                '240w': 'tiny.jpg',
                '720w': 'medium.jpg',
                '1440w': 'big.jpg'
            }
        };
        imgSrcDirective.platformImgType = 'pictureSource';

        imgSrcDirective.ngAfterViewInit();

        expect(img.src).toBeUndefined();
        expect(img.srcset).toBe('tiny.jpg 240w, medium.jpg 720w, big.jpg 1440w');
        expect(img.sizes).toBe('calc(100vw)');
    });

    it('render() null webp + type', () => {
        imgSrcDirective.platformImgSrc = {
            CDNPaths: {
                '240w': 'tiny.jpg',
                '240w_webp': 'tiny.webp',
                '720w': 'medium.jpg',
                '720w_webp': 'medium.webp',
                '1440w': 'big.jpg',
                '1440w_webp': 'big.webp'
            }
        };
        imgSrcDirective.platformImgWebp = undefined;
        imgSrcDirective.platformImgType = undefined;

        imgSrcDirective.ngAfterViewInit();

        expect(img.src).toBe('big.jpg');
        expect(img.srcset).toBe('tiny.jpg 240w, medium.jpg 720w, big.jpg 1440w');
        expect(img.sizes).toBe('calc(100vw)');
    });
});
