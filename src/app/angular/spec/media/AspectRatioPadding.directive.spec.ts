import { ElementRef } from '@angular/core';

import { AspectRatioPaddingDirective } from '../../media/AspectRatioPadding.directive';
import { MockRenderer2 } from '../MockRenderer2';

describe('AspectRatioPaddingDirective', () => {
    var aspectRatioPadding: AspectRatioPaddingDirective,
        elementRef: ElementRef,
        nativeElement: HTMLElement;

    beforeEach(() => {
        nativeElement = document.createElement('div');
        elementRef = {
            nativeElement: nativeElement
        };
        aspectRatioPadding = new AspectRatioPaddingDirective(elementRef, MockRenderer2);
    });

    it('calculatePaddingBottom()', () => {
        aspectRatioPadding.platformAspectRatioPadding = {
            CDNPaths: {},
            Width: 3,
            Height: 2
        };

        expect(aspectRatioPadding.calculatePaddingBottom()).toBe('66.667%');
    });

    it('calculatePaddingBottom() rounding', () => {
        aspectRatioPadding.platformAspectRatioPadding = {
            CDNPaths: {},
            Width: 4,
            Height: 3
        };

        expect(aspectRatioPadding.calculatePaddingBottom()).toBe('75%');
    });

    it('calculatePaddingBottom() fallback true', () => {
        aspectRatioPadding.platformAspectRatioFallback = true;

        expect(aspectRatioPadding.calculatePaddingBottom()).toBe('56.25%');
    });

    it('calculatePaddingBottom() fallback object', () => {
        aspectRatioPadding.platformAspectRatioFallback = {
            width: 4,
            height: 3
        };

        expect(aspectRatioPadding.calculatePaddingBottom()).toBe('75%');
    });

    it('calculatePaddingBottom() fallback nullWidthOrHeight', () => {
        aspectRatioPadding.platformAspectRatioFallback = true;

        aspectRatioPadding.platformAspectRatioPadding = {
            CDNPaths: {},
            Width: 1600
        };
        expect(aspectRatioPadding.calculatePaddingBottom()).toBe('56.25%');

        aspectRatioPadding.platformAspectRatioPadding = {
            CDNPaths: {},
            Height: 500
        };
        expect(aspectRatioPadding.calculatePaddingBottom()).toBe('56.25%');
    });

    it('calculatePaddingBottom() null', () => {
        expect(aspectRatioPadding.calculatePaddingBottom()).toBeUndefined();

        aspectRatioPadding.platformAspectRatioPadding = null;
        expect(aspectRatioPadding.calculatePaddingBottom()).toBeUndefined();

        aspectRatioPadding.platformAspectRatioPadding = {
            CDNPaths: {}
        };
        expect(aspectRatioPadding.calculatePaddingBottom()).toBeUndefined();
    });

    it('calculatePaddingBottom() nullWidthOrHeight', () => {
        aspectRatioPadding.platformAspectRatioPadding = {
            CDNPaths: {},
            Width: 1600
        };
        expect(aspectRatioPadding.calculatePaddingBottom()).toBeUndefined();

        aspectRatioPadding.platformAspectRatioPadding = {
            CDNPaths: {},
            Height: 500
        };
        expect(aspectRatioPadding.calculatePaddingBottom()).toBeUndefined();
    });

    it('render()', () => {
        aspectRatioPadding.platformAspectRatioFallback = {
            width: 4,
            height: 3
        };

        aspectRatioPadding.ngAfterViewInit();

        expect(nativeElement.style.paddingBottom).toBe('75%');
        expect(nativeElement.outerHTML).toBe('<div style="padding-bottom: 75%;"></div>');

        aspectRatioPadding.platformAspectRatioFallback = null;

        aspectRatioPadding.ngOnChanges();

        expect(nativeElement.style.paddingBottom).toBe('');
        expect(nativeElement.outerHTML).toBe('<div style=""></div>');
    });
});
