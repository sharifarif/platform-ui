/**
 * Mock data for unit testing.
 *
 * Like {@link IMockData}, but without {@link IMockData.ignoredData}.
 */
export interface IMockRequest {
    data1?: string;
    data2?: string;
}
