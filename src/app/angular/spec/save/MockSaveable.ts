import { Observable, Subscriber } from 'rxjs';

import { BaseSaveable } from '../../save/BaseSaveable';
import { IMockData } from './IMockData';
import { IMockRequest } from './IMockRequest';

/**
 * Mock {@link BaseSaveable} for unit testing.
 */
export default class MockSaveable extends BaseSaveable<IMockData> {
    subscriber: Subscriber<any>;

    constructor(data?: IMockData, savedData?: IMockData) {
        super();

        this.data = data;
        this.savedData = savedData;
    }

    getSaveableData(data: IMockData): IMockRequest {
        return {
            data1: data.data1,
            data2: data.data2
        };
    }

    performSave(): Observable<any> {
        return new Observable(subscriber => {
            this.subscriber = subscriber;
        });
    }
}
