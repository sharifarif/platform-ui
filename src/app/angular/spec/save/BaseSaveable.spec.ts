import { IMockData } from './IMockData';
import MockSaveable from './MockSaveable';

describe('BaseSaveable', () => {
    it('initNew()', () => {
        var saveable = new MockSaveable();

        saveable.initNew({
            data1: 'awesome',
            ignoredData: 'sauce'
        });

        expect(saveable.data).toEqual({
            data1: 'awesome',
            ignoredData: 'sauce'
        });
        expect(saveable.isNew()).toBe(true);
        expect(saveable.isSaved()).toBe(false);
    });

    it('initSaved()', () => {
        var saveable = new MockSaveable();

        saveable.initSaved({
            data1: 'awesome',
            ignoredData: 'sauce'
        });

        expect(saveable.data).toEqual({
            data1: 'awesome',
            ignoredData: 'sauce'
        });
        expect(saveable.isNew()).toBe(false);
        expect(saveable.isSaved()).toBe(true);
    });

    it('initSaved() then edit', () => {
        var saveable = new MockSaveable();
        var savedData = {
            data1: 'awesome',
            ignoredData: 'sauce'
        };

        saveable.initSaved(savedData);

        saveable.data.data1 = 'notAwesome';

        expect(saveable.isSaved()).toBe(false);
        expect(savedData).toEqual({
            data1: 'awesome',
            ignoredData: 'sauce'
        });
    });

    it('initSaved() then initNew()', () => {
        var saveable = new MockSaveable();

        saveable.initSaved({
            data1: 'awesome',
            ignoredData: 'sauce'
        });

        saveable.initNew({
            data1: 'awesome',
            ignoredData: 'sauce'
        });

        expect(saveable.isSaved()).toBe(false);
    });

    it('isInitialData()', () => {
        var saveable = new MockSaveable();

        saveable.initNew({
            data1: 'awesome'
        });

        expect(saveable.isInitialData()).toBe(true);
    });

    it('isInitialData() changed', () => {
        var saveable = new MockSaveable();

        saveable.initNew({
            data1: 'awesome'
        });

        saveable.data.data1 = 'sauce';

        expect(saveable.isInitialData()).toBe(false);

        saveable.data.data1 = 'awesome';

        expect(saveable.isInitialData()).toBe(true);
    });

    it('isInitialData() changed ignored', () => {
        var saveable = new MockSaveable();

        saveable.initNew({
            data1: 'awesome'
        });

        saveable.data.ignoredData = 'not awesome';

        expect(saveable.isInitialData()).toBe(true);
    });

    it('isInitialData() save', () => {
        var saveable = new MockSaveable();

        saveable.initNew({
            data1: 'awesome'
        });

        saveable.save();

        expect(saveable.isInitialData()).toBe(true);

        saveable.subscriber.next();

        expect(saveable.isInitialData()).toBe(false);
    });

    it('isInitialData() saved', () => {
        var saveable = new MockSaveable();

        saveable.initSaved({
            data1: 'awesome'
        });

        expect(saveable.isInitialData()).toBe(false);
    });

    it('isInitialData() null', () => {
        var saveable = new MockSaveable();

        expect(saveable.isInitialData()).toBe(undefined);
    });

    it('isNew()', () => {
        expect(
            new MockSaveable({
                data1: 'awesome'
            }).isNew()
        ).toBe(true);

        expect(
            new MockSaveable(
                {
                    data1: 'awesome'
                },
                {
                    data1: 'sauce'
                }
            ).isNew()
        ).toBe(false);
    });

    it('isSaved()', () => {
        expect(
            new MockSaveable(
                {
                    data1: 'awesome'
                },
                {
                    data1: 'awesome'
                }
            ).isSaved()
        ).toBe(true);

        expect(
            new MockSaveable(
                {
                    data1: 'awesome',
                    data2: 'sauce'
                },
                {
                    data1: 'awesome',
                    data2: 'sauce'
                }
            ).isSaved()
        ).toBe(true);
    });

    it('isSaved() notEqual', () => {
        expect(
            new MockSaveable(
                {
                    data1: 'awesome'
                },
                {
                    data1: 'notAwesome'
                }
            ).isSaved()
        ).toBe(false);

        expect(
            new MockSaveable(
                {
                    data1: 'awesome',
                    data2: 'sauce'
                },
                {
                    data1: 'awesome',
                    data2: 'notSauce'
                }
            ).isSaved()
        ).toBe(false);
    });

    it('isSaved() unsavedData', () => {
        expect(
            new MockSaveable(
                {
                    data1: 'awesome',
                    ignoredData: 'notAwesome'
                },
                {
                    data1: 'awesome'
                }
            ).isSaved()
        ).toBe(true);

        expect(
            new MockSaveable(
                {
                    data1: 'awesome'
                },
                {
                    data1: 'awesome',
                    ignoredData: 'notAwesome'
                }
            ).isSaved()
        ).toBe(true);

        expect(
            new MockSaveable(
                {
                    data1: 'awesome',
                    data2: 'sauce',
                    ignoredData: 'notAwesome'
                },
                {
                    data1: 'awesome',
                    data2: 'sauce',
                    ignoredData: 'notSauce'
                }
            ).isSaved()
        ).toBe(true);
    });

    it('isSaved() null data', () => {
        expect(new MockSaveable(null, null).isSaved()).toBeUndefined();
        expect(new MockSaveable(undefined, undefined).isSaved()).toBeUndefined();
    });

    it('isSaved() null savedData', () => {
        expect(
            new MockSaveable(
                {
                    data1: 'awesome'
                },
                null
            ).isSaved()
        ).toBe(false);
        expect(
            new MockSaveable(
                {
                    data1: 'awesome'
                },
                undefined
            ).isSaved()
        ).toBe(false);
    });

    it('save()', () => {
        var onSaveData: IMockData;

        var saveable = new MockSaveable(
            {
                data1: 'awesome'
            },
            {
                data1: 'sauce'
            }
        );

        saveable.onSave.subscribe((savedData: IMockData) => {
            onSaveData = savedData;
        });

        saveable.save();

        expect(saveable.saving).toBe(true);
        expect(saveable.isSaved()).toBe(false);
        expect(saveable.errorSaving).toBe(false);

        saveable.subscriber.next();

        expect(saveable.saving).toBe(false);
        expect(saveable.isSaved()).toBe(true);
        expect(saveable.errorSaving).toBe(false);
        expect(onSaveData).toBe(saveable.savedData);
    });

    it('save() editWhileSaving', () => {
        var saveable = new MockSaveable(
            {
                data1: 'awesome'
            },
            {
                data1: 'sauce'
            }
        );

        saveable.save();

        saveable.data.data1 = 'notAwesome';

        saveable.subscriber.next();

        expect(saveable.saving).toBe(false);
        expect(saveable.isSaved()).toBe(false);
    });

    it('save() error', () => {
        var saveable = new MockSaveable(
            {
                data1: 'awesome'
            },
            {
                data1: 'sauce'
            }
        );

        saveable.onSave.subscribe(() => {
            fail();
        });

        saveable.save();

        saveable.subscriber.error();

        expect(saveable.saving).toBe(false);
        expect(saveable.isSaved()).toBe(false);
        expect(saveable.errorSaving).toBe(true);
    });
});
