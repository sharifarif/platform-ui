import MockTranslateService from '../i18n/MockTranslateService';
import { PrettySubmitDirective } from '../../save/PrettySubmit.directive';
import { PrettySubmitGuard } from '../../save/PrettySubmitGuard';
import { PrettySubmitState } from '../../save/PrettySubmitState';

describe('PrettySubmit', () => {
    var prettySubmit: PrettySubmitDirective,
        prettySubmitGuard: PrettySubmitGuard,
        button: HTMLButtonElement;

    beforeEach(() => {
        var translate = new MockTranslateService();

        button = document.createElement('button');
        prettySubmitGuard = new PrettySubmitGuard(translate);
        prettySubmit = new PrettySubmitDirective(
            {
                nativeElement: button
            },
            prettySubmitGuard,
            translate
        );

        // Allow unit tests to work when offline.
        prettySubmit.isOffline = () => false;
    });

    it('canDeactivate() unsaved', () => {
        prettySubmit.promptUnsaved = true;
        prettySubmit.saved = false;

        expect(prettySubmit.canDeactivate()).toBe(false);
    });

    it('canDeactivate() saved', () => {
        prettySubmit.promptUnsaved = true;
        prettySubmit.saved = true;

        expect(prettySubmit.canDeactivate()).toBe(true);
    });

    it('canDeactivate() !promptUnsaved', () => {
        prettySubmit.promptUnsaved = false;
        prettySubmit.saved = false;

        expect(prettySubmit.canDeactivate()).toBe(true);
    });

    it('canDeactivate() initialData', () => {
        prettySubmit.initialData = true;
        prettySubmit.promptUnsaved = true;
        prettySubmit.saved = false;

        expect(prettySubmit.canDeactivate()).toBe(true);
    });

    it('canDeactivate() unrecoverableError', () => {
        prettySubmit.promptUnsaved = true;
        prettySubmit.saved = false;
        prettySubmit.unrecoverableError = true;

        expect(prettySubmit.canDeactivate()).toBe(true);
    });

    it('getState()', () => {
        expect(prettySubmit.getState()).toBe(PrettySubmitState.Unsaved);
    });

    it('getState() formInvalid', () => {
        prettySubmit.formInvalid = true;

        expect(prettySubmit.getState()).toBe(PrettySubmitState.Unsaved);
    });

    it('getState() formInvalid saved', () => {
        prettySubmit.saved = true;
        prettySubmit.formInvalid = true;

        expect(prettySubmit.getState()).toBe(PrettySubmitState.Unsaved);
    });

    it('getState() offline', () => {
        spyOn(prettySubmit, 'isOffline').and.returnValue(true);

        expect(prettySubmit.getState()).toBe(PrettySubmitState.Offline);
    });

    it('getState() offline saved,saving', () => {
        spyOn(prettySubmit, 'isOffline').and.returnValue(true);
        prettySubmit.saved = true;
        prettySubmit.saving = true;

        expect(prettySubmit.getState()).toBe(PrettySubmitState.Offline);
    });

    it('getState() saved', () => {
        prettySubmit.saved = true;

        expect(prettySubmit.getState()).toBe(PrettySubmitState.Saved);
    });

    it('getState() saving', () => {
        prettySubmit.saving = true;

        expect(prettySubmit.getState()).toBe(PrettySubmitState.Saving);
    });

    it('getState() saving saved', () => {
        prettySubmit.saving = true;
        prettySubmit.saved = true;

        expect(prettySubmit.getState()).toBe(PrettySubmitState.Saving);
    });

    it('getState() unrecoverableError', () => {
        prettySubmit.unrecoverableError = true;

        expect(prettySubmit.getState()).toBe(PrettySubmitState.UnrecoverableError);
    });

    it('getState() unrecoverableError saved,saving', () => {
        prettySubmit.unrecoverableError = true;
        prettySubmit.saved = true;
        prettySubmit.saving = true;

        expect(prettySubmit.getState()).toBe(PrettySubmitState.UnrecoverableError);
    });

    it('getText()', () => {
        expect(prettySubmit.getText(PrettySubmitState.Offline)).toBe('Offline');
        expect(prettySubmit.getText(PrettySubmitState.Saved)).toBe('Saved');
        expect(prettySubmit.getText(PrettySubmitState.Saving)).toBe('Saving');
        expect(prettySubmit.getText(PrettySubmitState.UnrecoverableError)).toBe(
            'Error Saving'
        );
        expect(prettySubmit.getText(PrettySubmitState.Unsaved)).toBe('Save');
    });

    it('getText() custom', () => {
        prettySubmit.textOffline = 'Totally Offline';
        prettySubmit.textSaved = 'Totally Saved';
        prettySubmit.textSaving = 'Totally Saving';
        prettySubmit.textUnrecoverableError = 'Totally an Error';
        prettySubmit.textSave = 'Save for Sure';

        expect(prettySubmit.getText(PrettySubmitState.Offline)).toBe('Totally Offline');
        expect(prettySubmit.getText(PrettySubmitState.Saved)).toBe('Totally Saved');
        expect(prettySubmit.getText(PrettySubmitState.Saving)).toBe('Totally Saving');
        expect(prettySubmit.getText(PrettySubmitState.UnrecoverableError)).toBe(
            'Totally an Error'
        );
        expect(prettySubmit.getText(PrettySubmitState.Unsaved)).toBe('Save for Sure');
    });

    it('isDisabled() Offline', () => {
        expect(prettySubmit.isDisabled(PrettySubmitState.Offline)).toBe(true);
    });

    it('isDisabled() Saved', () => {
        expect(prettySubmit.isDisabled(PrettySubmitState.Saved)).toBe(true);
    });

    it('isDisabled() Saving', () => {
        expect(prettySubmit.isDisabled(PrettySubmitState.Saving)).toBe(true);
    });

    it('isDisabled() UnrecoverableError', () => {
        expect(prettySubmit.isDisabled(PrettySubmitState.UnrecoverableError)).toBe(true);
    });

    it('isDisabled() Unsaved', () => {
        expect(prettySubmit.isDisabled(PrettySubmitState.Unsaved)).toBe(false);
    });

    it('isDisabled() prettySubmitDisabled', () => {
        prettySubmit.prettySubmitDisabled = true;

        expect(prettySubmit.isDisabled(PrettySubmitState.Unsaved)).toBe(true);
    });

    it('ngOnInit() ngOnDestroy()', () => {
        expect(prettySubmitGuard.submits).toEqual([]);

        prettySubmit.ngOnInit();

        expect(prettySubmitGuard.submits).toEqual([prettySubmit]);

        prettySubmit.ngOnDestroy();

        expect(prettySubmitGuard.submits).toEqual([]);
    });

    it('onBeforeUnload() unsaved', () => {
        var event = {} as BeforeUnloadEvent;

        prettySubmit.promptUnsaved = true;
        prettySubmit.saved = false;

        prettySubmit.onBeforeUnload(event);

        expect(event.returnValue).toBe('You have unsaved changes.');
    });

    it('onBeforeUnload() saved', () => {
        var event = {} as BeforeUnloadEvent;

        prettySubmit.promptUnsaved = true;
        prettySubmit.saved = true;

        prettySubmit.onBeforeUnload(event);

        expect(event.returnValue).toBeUndefined();
    });

    it('onBeforeUnload() !promptUnsaved', () => {
        var event = {} as BeforeUnloadEvent;

        prettySubmit.promptUnsaved = false;
        prettySubmit.saved = false;

        prettySubmit.onBeforeUnload(event);

        expect(event.returnValue).toBeUndefined();
    });

    it('onBeforeUnload() unrecoverableError', () => {
        var event = {} as BeforeUnloadEvent;

        prettySubmit.promptUnsaved = true;
        prettySubmit.saved = false;
        prettySubmit.unrecoverableError = true;

        prettySubmit.onBeforeUnload(event);

        expect(event.returnValue).toBeUndefined();
    });

    it('render()', () => {
        spyOn(prettySubmit, 'renderDisabled');
        spyOn(prettySubmit, 'renderClasses');
        spyOn(prettySubmit, 'renderContent');

        prettySubmit.render();

        expect(prettySubmit.renderDisabled).toHaveBeenCalledTimes(1);
        expect(prettySubmit.renderDisabled).toHaveBeenCalledWith(false);
        expect(prettySubmit.renderClasses).toHaveBeenCalledTimes(1);
        expect(prettySubmit.renderClasses).toHaveBeenCalledWith(
            PrettySubmitState.Unsaved
        );
        expect(prettySubmit.renderContent).toHaveBeenCalledTimes(1);
        expect(prettySubmit.renderContent).toHaveBeenCalledWith(
            PrettySubmitState.Unsaved
        );

        prettySubmit.saving = true;

        prettySubmit.render();

        expect(prettySubmit.renderDisabled).toHaveBeenCalledTimes(2);
        expect(prettySubmit.renderDisabled).toHaveBeenCalledWith(true);
        expect(prettySubmit.renderClasses).toHaveBeenCalledTimes(2);
        expect(prettySubmit.renderClasses).toHaveBeenCalledWith(PrettySubmitState.Saving);
        expect(prettySubmit.renderContent).toHaveBeenCalledTimes(2);
        expect(prettySubmit.renderContent).toHaveBeenCalledWith(PrettySubmitState.Saving);
    });

    it('renderClasses()', () => {
        prettySubmit.renderClasses(PrettySubmitState.Unsaved);

        expect(button.className).toBe('btn-primary');

        prettySubmit.renderClasses(PrettySubmitState.Offline);

        expect(button.className).toBe('btn-warning');

        prettySubmit.renderClasses(PrettySubmitState.Saved);

        expect(button.className).toBe('btn-success');

        prettySubmit.renderClasses(PrettySubmitState.Saving);

        expect(button.className).toBe('btn-primary');

        prettySubmit.renderClasses(PrettySubmitState.UnrecoverableError);

        expect(button.className).toBe('btn-danger');

        prettySubmit.renderClasses(PrettySubmitState.Unsaved);

        expect(button.className).toBe('btn-primary');
    });

    it('renderClasses() custom multiple', () => {
        prettySubmit.classOffline = 'btn-offline offline';
        prettySubmit.classSave = 'btn-save save';
        prettySubmit.classSaved = 'btn-saved saved';
        prettySubmit.classUnrecoverableError = 'btn-error error';

        prettySubmit.renderClasses(PrettySubmitState.Unsaved);

        expect(button.className).toBe('btn-save save');

        prettySubmit.renderClasses(PrettySubmitState.Offline);

        expect(button.className).toBe('btn-offline offline');

        prettySubmit.renderClasses(PrettySubmitState.Saved);

        expect(button.className).toBe('btn-saved saved');

        prettySubmit.renderClasses(PrettySubmitState.Saving);

        expect(button.className).toBe('btn-save save');

        prettySubmit.renderClasses(PrettySubmitState.UnrecoverableError);

        expect(button.className).toBe('btn-error error');

        prettySubmit.renderClasses(PrettySubmitState.Unsaved);

        expect(button.className).toBe('btn-save save');
    });

    it('renderContent()', () => {
        prettySubmit.renderContent(PrettySubmitState.Unsaved);

        expect(button.innerHTML).toBe('Save');

        prettySubmit.renderContent(PrettySubmitState.Offline);

        expect(button.innerHTML).toBe('<i class="fa fa-warning mr-btn"></i>Offline');

        prettySubmit.renderContent(PrettySubmitState.Saved);

        expect(button.innerHTML).toBe('Saved<i class="fa fa-check ml-btn"></i>');

        prettySubmit.renderContent(PrettySubmitState.Saving);

        expect(button.innerHTML).toBe(
            'Saving<i class="fa fa-spin fa-spinner ml-btn"></i>'
        );

        prettySubmit.renderContent(PrettySubmitState.UnrecoverableError);

        expect(button.innerHTML).toBe('<i class="fa fa-warning mr-btn"></i>Error Saving');

        prettySubmit.renderContent(PrettySubmitState.Unsaved);

        expect(button.innerHTML).toBe('Save');
    });

    it('renderContent() custom escapeHtml', () => {
        prettySubmit.textOffline = 'Offline & Offline';
        prettySubmit.textSaved = 'Saved & Saved';
        prettySubmit.textSaving = 'Saving & Saving';
        prettySubmit.textUnrecoverableError = 'Error & Error';
        prettySubmit.textSave = 'Save & Save';

        prettySubmit.renderContent(PrettySubmitState.Unsaved);

        expect(button.innerHTML).toBe('Save &amp; Save');

        prettySubmit.renderContent(PrettySubmitState.Offline);

        expect(button.innerHTML).toBe(
            '<i class="fa fa-warning mr-btn"></i>Offline &amp; Offline'
        );

        prettySubmit.renderContent(PrettySubmitState.Saved);

        expect(button.innerHTML).toBe(
            'Saved &amp; Saved<i class="fa fa-check ml-btn"></i>'
        );

        prettySubmit.renderContent(PrettySubmitState.Saving);

        expect(button.innerHTML).toBe(
            'Saving &amp; Saving<i class="fa fa-spin fa-spinner ml-btn"></i>'
        );

        prettySubmit.renderContent(PrettySubmitState.UnrecoverableError);

        expect(button.innerHTML).toBe(
            '<i class="fa fa-warning mr-btn"></i>Error &amp; Error'
        );

        prettySubmit.renderContent(PrettySubmitState.Unsaved);

        expect(button.innerHTML).toBe('Save &amp; Save');
    });

    it('renderContent() custom icon', () => {
        prettySubmit.classIconSaved = 'icon-saved';
        prettySubmit.classIconSaving = 'icon-saving';
        prettySubmit.classIconWarning = 'icon-warning';

        prettySubmit.renderContent(PrettySubmitState.Unsaved);

        expect(button.innerHTML).toBe('Save');

        prettySubmit.renderContent(PrettySubmitState.Offline);

        expect(button.innerHTML).toBe('<i class="icon-warning mr-btn"></i>Offline');

        prettySubmit.renderContent(PrettySubmitState.Saved);

        expect(button.innerHTML).toBe('Saved<i class="icon-saved ml-btn"></i>');

        prettySubmit.renderContent(PrettySubmitState.Saving);

        expect(button.innerHTML).toBe('Saving<i class="icon-saving ml-btn"></i>');

        prettySubmit.renderContent(PrettySubmitState.UnrecoverableError);

        expect(button.innerHTML).toBe('<i class="icon-warning mr-btn"></i>Error Saving');

        prettySubmit.renderContent(PrettySubmitState.Unsaved);

        expect(button.innerHTML).toBe('Save');
    });

    it('renderDisabled()', () => {
        expect(button.disabled).toBe(false);

        prettySubmit.renderDisabled(true);

        expect(button.disabled).toBe(true);

        prettySubmit.renderDisabled(false);

        expect(button.disabled).toBe(false);
    });
});
