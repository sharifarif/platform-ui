/**
 * Mock data for unit testing.
 */
export interface IMockData {
    data1?: string;
    data2?: string;
    ignoredData?: string;
}
