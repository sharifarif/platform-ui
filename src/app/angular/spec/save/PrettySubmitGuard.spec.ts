import { ElementRef } from '@angular/core';

import MockTranslateService from '../i18n/MockTranslateService';
import { PrettySubmitDirective } from '../../save/PrettySubmit.directive';
import { PrettySubmitGuard } from '../../save/PrettySubmitGuard';

describe('PrettySubmitGuard', () => {
    var translate: MockTranslateService,
        prettySubmitGuard: PrettySubmitGuard,
        prettySubmit: PrettySubmitDirective,
        button: HTMLButtonElement,
        buttonRef: ElementRef;

    beforeEach(() => {
        translate = new MockTranslateService();

        button = document.createElement('button');
        buttonRef = {
            nativeElement: button
        };
        prettySubmitGuard = new PrettySubmitGuard(translate);
        prettySubmit = new PrettySubmitDirective(buttonRef, prettySubmitGuard, translate);
    });

    it('add() remove()', () => {
        expect(prettySubmitGuard.submits).toEqual([]);

        prettySubmitGuard.add(prettySubmit);

        expect(prettySubmitGuard.submits).toEqual([prettySubmit]);

        prettySubmitGuard.remove(prettySubmit);

        expect(prettySubmitGuard.submits).toEqual([]);
    });

    it('canDeactivate() confirm no', () => {
        prettySubmitGuard.add(prettySubmit);

        spyOn(prettySubmit, 'canDeactivate').and.returnValue(false);
        spyOn(window, 'confirm').and.returnValue(false);

        expect(prettySubmitGuard.canDeactivate()).toBe(false);
        expect(window.confirm).toHaveBeenCalledTimes(1);
        expect(window.confirm).toHaveBeenCalledWith('Discard your changes?');
    });

    it('canDeactivate() confirm yes', () => {
        prettySubmitGuard.add(prettySubmit);

        spyOn(prettySubmit, 'canDeactivate').and.returnValue(false);
        spyOn(window, 'confirm').and.returnValue(true);

        expect(prettySubmitGuard.canDeactivate()).toBe(true);
        expect(window.confirm).toHaveBeenCalledTimes(1);
        expect(window.confirm).toHaveBeenCalledWith('Discard your changes?');
    });

    it('canDeactivate() multipleChangedButtons', () => {
        var prettySubmit2 = new PrettySubmitDirective(
            {
                nativeElement: button
            },
            prettySubmitGuard,
            translate
        );

        prettySubmitGuard.add(prettySubmit);
        prettySubmitGuard.add(prettySubmit2);

        spyOn(prettySubmit, 'canDeactivate').and.returnValue(false);
        spyOn(prettySubmit2, 'canDeactivate').and.returnValue(false);
        spyOn(window, 'confirm').and.returnValue(false);

        expect(prettySubmitGuard.canDeactivate()).toBe(false);
        expect(window.confirm).toHaveBeenCalledTimes(1);
        expect(window.confirm).toHaveBeenCalledWith('Discard your changes?');
    });

    it('canDeactivate() noChanges', () => {
        prettySubmitGuard.add(prettySubmit);

        spyOn(prettySubmit, 'canDeactivate').and.returnValue(true);
        spyOn(window, 'confirm');

        expect(prettySubmitGuard.canDeactivate()).toBe(true);
        expect(window.confirm).not.toHaveBeenCalled();
    });

    it('canDeactivate() noButtons', () => {
        spyOn(window, 'confirm');

        expect(prettySubmitGuard.canDeactivate()).toBe(true);
        expect(window.confirm).not.toHaveBeenCalled();
    });

    it('canRemoveComponent() buttonRef', () => {
        prettySubmitGuard.add(prettySubmit);

        spyOn(prettySubmit, 'canDeactivate').and.returnValue(false);
        spyOn(window, 'confirm').and.returnValue(false);

        expect(prettySubmitGuard.canRemoveComponent(buttonRef)).toBe(false);
        expect(window.confirm).toHaveBeenCalledTimes(1);
        expect(window.confirm).toHaveBeenCalledWith('Discard your changes?');
    });

    it('canRemoveComponent() ancestorRef', () => {
        var ancestor = document.createElement('div'),
            parent = document.createElement('div');

        ancestor.appendChild(parent);
        parent.appendChild(button);

        prettySubmitGuard.add(prettySubmit);

        spyOn(prettySubmit, 'canDeactivate').and.returnValue(false);
        spyOn(window, 'confirm').and.returnValue(false);

        expect(
            prettySubmitGuard.canRemoveComponent({
                nativeElement: ancestor
            })
        ).toBe(false);
        expect(window.confirm).toHaveBeenCalledTimes(1);
        expect(window.confirm).toHaveBeenCalledWith('Discard your changes?');
    });

    it('canRemoveComponent() otherRef', () => {
        var otherElement = document.createElement('div');

        prettySubmitGuard.add(prettySubmit);

        spyOn(prettySubmit, 'canDeactivate').and.returnValue(false);
        spyOn(window, 'confirm').and.returnValue(false);

        expect(
            prettySubmitGuard.canRemoveComponent({
                nativeElement: otherElement
            })
        ).toBe(true);
        expect(window.confirm).not.toHaveBeenCalled();
    });
});
