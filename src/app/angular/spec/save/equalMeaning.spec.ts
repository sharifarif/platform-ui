import equalMeaning from '../../save/equalMeaning';

describe('equalMeaning', () => {
    it('null == undefined == NaN', () => {
        expect(equalMeaning(null, null)).toBe(true);
        expect(equalMeaning(null, undefined)).toBe(true);
        expect(equalMeaning(null, NaN)).toBe(true);
        expect(equalMeaning(undefined, null)).toBe(true);
        expect(equalMeaning(undefined, undefined)).toBe(true);
        expect(equalMeaning(undefined, NaN)).toBe(true);
        expect(equalMeaning(NaN, null)).toBe(true);
        expect(equalMeaning(NaN, undefined)).toBe(true);
        expect(equalMeaning(NaN, NaN)).toBe(true);
    });

    // In most cases, false and null have the same meaning.
    it('null == false', () => {
        expect(equalMeaning(false, null)).toBe(true);
        expect(equalMeaning(false, undefined)).toBe(true);
        expect(equalMeaning(false, NaN)).toBe(true);

        expect(equalMeaning(null, false)).toBe(true);
        expect(equalMeaning(undefined, false)).toBe(true);
        expect(equalMeaning(NaN, false)).toBe(true);

        // You can override the behavior when the meaning is different.
        expect(equalMeaning(false, null, false)).toBe(false);
        expect(equalMeaning(false, undefined, false)).toBe(false);
        expect(equalMeaning(false, NaN, false)).toBe(false);

        expect(equalMeaning(null, false, false)).toBe(false);
        expect(equalMeaning(undefined, false, false)).toBe(false);
        expect(equalMeaning(NaN, false, false)).toBe(false);
    });

    // In most cases, '' and null have the same meaning.
    it('null == ""', () => {
        expect(equalMeaning('', null)).toBe(true);
        expect(equalMeaning('', undefined)).toBe(true);
        expect(equalMeaning('', NaN)).toBe(true);

        expect(equalMeaning(null, '')).toBe(true);
        expect(equalMeaning(undefined, '')).toBe(true);
        expect(equalMeaning(NaN, '')).toBe(true);

        // You can override the behavior when the meaning is different.
        expect(equalMeaning('', null, undefined, false)).toBe(false);
        expect(equalMeaning('', undefined, undefined, false)).toBe(false);
        expect(equalMeaning('', NaN, undefined, false)).toBe(false);

        expect(equalMeaning(null, '', undefined, false)).toBe(false);
        expect(equalMeaning(undefined, '', undefined, false)).toBe(false);
        expect(equalMeaning(NaN, '', undefined, false)).toBe(false);
    });

    // With number inputs, 0 and null have a different meaning.
    it('0 !== null', () => {
        expect(equalMeaning(0, null)).toBe(false);
        expect(equalMeaning(0, undefined)).toBe(false);
        expect(equalMeaning(0, NaN)).toBe(false);

        expect(equalMeaning(null, 0)).toBe(false);
        expect(equalMeaning(undefined, 0)).toBe(false);
        expect(equalMeaning(NaN, 0)).toBe(false);
    });

    it('0 !== ""', () => {
        expect(equalMeaning(0, '')).toBe(false);
        expect(equalMeaning('', 0)).toBe(false);
    });

    // number and (number + '') have the same meaning.
    it('# == "#"', () => {
        expect(equalMeaning(0, '0')).toBe(true);
        expect(equalMeaning(0.5, '0.5')).toBe(true);
        expect(equalMeaning(1, '1')).toBe(true);
        expect(equalMeaning(1.25, '1.25')).toBe(true);
        expect(equalMeaning(-1.25, '-1.25')).toBe(true);

        expect(equalMeaning('0', 0)).toBe(true);
        expect(equalMeaning('0.5', 0.5)).toBe(true);
        expect(equalMeaning('1', 1)).toBe(true);
        expect(equalMeaning('1.25', 1.25)).toBe(true);
        expect(equalMeaning('-1.25', -1.25)).toBe(true);
    });

    it('array', () => {
        expect(equalMeaning([], [])).toBe(true);
        expect(equalMeaning([1], [1])).toBe(true);
        expect(equalMeaning(['Awesome'], ['Awesome'])).toBe(true);
        expect(equalMeaning(['Awesome', 'Sauce'], ['Awesome', 'Sauce'])).toBe(true);

        expect(equalMeaning([], [1])).toBe(false);
        expect(equalMeaning([1], [])).toBe(false);
        expect(equalMeaning(['Awesome'], ['Sauce'])).toBe(false);
        expect(equalMeaning(['Awesome', 'Awesome'], ['Awesome', 'Sauce'])).toBe(false);
    });

    it('array nulls', () => {
        expect(equalMeaning([null, undefined, NaN], [NaN, null, undefined])).toBe(true);
        expect(equalMeaning([null, 1], [1, null])).toBe(false);

        expect(equalMeaning([null, false], [false, null])).toBe(true);
        expect(equalMeaning([null, false], [false, null], false)).toBe(false);

        expect(equalMeaning([null, ''], ['', null])).toBe(true);
        expect(equalMeaning([null, ''], ['', null], undefined, false)).toBe(false);
    });

    it('object', () => {
        expect(equalMeaning({}, {})).toBe(true);
        expect(equalMeaning({ Awesome: 'Sauce' }, { Awesome: 'Sauce' })).toBe(true);
        expect(equalMeaning({ one: 1, two: 2 }, { one: 1, two: 2 })).toBe(true);

        expect(equalMeaning({}, { Awesome: 'Sauce' })).toBe(false);
        expect(equalMeaning({ Awesome: 'Sauce' }, {})).toBe(false);

        expect(equalMeaning({ Awesome: 'Sauce' }, { Awesome: 'No Sauce' })).toBe(false);
        expect(equalMeaning({ Awesome: 'Sauce' }, { 'No Awesome': 'Sauce' })).toBe(false);

        expect(equalMeaning({ one: 1, two: 2 }, { one: 1, two: 1 })).toBe(false);
        expect(equalMeaning({ one: 1, two: 2 }, { one: 2, two: 2 })).toBe(false);
    });

    it('object nulls', () => {
        expect(equalMeaning({ a: null, b: undefined, c: NaN }, {})).toBe(true);
        expect(equalMeaning({}, { a: null, b: undefined, c: NaN })).toBe(true);

        expect(equalMeaning({ itsFalse: false }, {})).toBe(true);
        expect(equalMeaning({}, { itsFalse: false })).toBe(true);
        expect(equalMeaning({ itsFalse: false }, {}, false)).toBe(false);
        expect(equalMeaning({}, { itsFalse: false }, false)).toBe(false);

        expect(equalMeaning({ itsBlank: '' }, {})).toBe(true);
        expect(equalMeaning({}, { itsBlank: '' })).toBe(true);
        expect(equalMeaning({ itsBlank: '' }, {}, undefined, false)).toBe(false);
        expect(equalMeaning({}, { itsBlank: '' }, undefined, false)).toBe(false);
    });
});
