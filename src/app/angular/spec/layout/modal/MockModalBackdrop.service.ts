import { MockRendererFactory2 } from '../../MockRendererFactory2';
import { ModalBackdropService } from '../../../layout/modal/ModalBackdrop.service';

/**
 * Mock {@link ModalBackdropService} for unit testing.
 */
export default class MockModalBackdropService extends ModalBackdropService {
    constructor(document: Document) {
        super(document, MockRendererFactory2);
    }

    addBackdrop() {
        return super.addBackdrop();
    }

    removeBackdrop() {
        return super.removeBackdrop();
    }
}
