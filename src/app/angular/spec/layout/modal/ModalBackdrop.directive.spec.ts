import MockModalBackdropService from './MockModalBackdrop.service';
import { ModalBackdropDirective } from '../../../layout/modal/ModalBackdrop.directive';

describe('ModalBackdropDirective', () => {
    var allModals: Object[],
        modalBackdrop: ModalBackdropDirective,
        modalBackdropService: MockModalBackdropService;

    beforeEach(() => {
        modalBackdropService = new MockModalBackdropService(document);
        modalBackdrop = new ModalBackdropDirective(modalBackdropService);

        modalBackdropService.modals.subscribe(modals => {
            allModals = modals;
        });
    });

    afterEach(() => {
        modalBackdrop.ngOnDestroy();
    });

    it('ngOnChanges()', () => {
        modalBackdrop.ngOnChanges();

        expect(allModals.length).toBe(1);
        expect(allModals[0]).toBe(modalBackdrop);
    });

    it('ngOnChanges() true', () => {
        modalBackdrop.platformModalBackdrop = true;
        modalBackdrop.ngOnChanges();

        expect(allModals.length).toBe(1);
        expect(allModals[0]).toBe(modalBackdrop);
    });

    it('ngOnChanges() false', () => {
        modalBackdrop.platformModalBackdrop = false;
        modalBackdrop.ngOnChanges();

        expect(allModals.length).toBe(0);
    });

    it('ngOnChanges() false was true', () => {
        modalBackdrop.platformModalBackdrop = true;
        modalBackdrop.ngOnChanges();

        modalBackdrop.platformModalBackdrop = false;
        modalBackdrop.ngOnChanges();

        expect(allModals.length).toBe(0);
    });

    it('ngOnDestroy()', () => {
        modalBackdrop.ngOnChanges();

        modalBackdrop.ngOnDestroy();

        expect(allModals.length).toBe(0);
    });
});
