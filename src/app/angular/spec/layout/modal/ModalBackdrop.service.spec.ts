import pull from 'lodash/pull';

import MockModalBackdropService from './MockModalBackdrop.service';
import { MockRenderer2 } from '../../MockRenderer2';

describe('ModalBackdropService', () => {
    var bodyClasses: string[],
        bodyElements: HTMLElement[],
        document: HTMLDocument,
        modalBackdrop: MockModalBackdropService;

    beforeEach(() => {
        bodyClasses = [];
        bodyElements = [];
        document = {
            documentElement: {},
            body: {
                appendChild: (element: HTMLElement) => {
                    bodyElements.push(element);
                },
                classList: {
                    add: (cssClass: string) => {
                        bodyClasses.push(cssClass);
                    },
                    contains: (cssClass: string) => {
                        return bodyClasses.includes(cssClass);
                    },
                    remove: (cssClass: string) => {
                        pull(bodyClasses, cssClass);
                    }
                },
                removeChild: (element: HTMLElement) => {
                    pull(bodyElements, element);
                }
            }
        } as HTMLDocument;
        modalBackdrop = new MockModalBackdropService(document);
    });

    afterEach(() => {
        modalBackdrop.ngOnDestroy();
    });

    it('addModal()', done => {
        var modal = {};

        spyOn(modalBackdrop, 'addBackdrop');
        spyOn(modalBackdrop, 'removeBackdrop');

        modalBackdrop.addModal(modal);

        expect(modalBackdrop.addBackdrop).toHaveBeenCalledTimes(1);
        expect(modalBackdrop.removeBackdrop).not.toHaveBeenCalled();

        modalBackdrop.modals.subscribe(modals => {
            expect(modals.length).toBe(1);
            expect(modals[0]).toBe(modal);
            done();
        });
    });

    it('addModal() 2x', done => {
        var modal1 = {},
            modal2 = {};

        spyOn(modalBackdrop, 'addBackdrop');
        spyOn(modalBackdrop, 'removeBackdrop');

        modalBackdrop.addModal(modal1);
        modalBackdrop.addModal(modal2);

        expect(modalBackdrop.addBackdrop).toHaveBeenCalledTimes(2);
        expect(modalBackdrop.removeBackdrop).not.toHaveBeenCalled();

        modalBackdrop.modals.subscribe(modals => {
            expect(modals.length).toBe(2);
            expect(modals[0]).toBe(modal1);
            expect(modals[1]).toBe(modal2);
            done();
        });
    });

    it('removeModal()', done => {
        var modal = {};

        spyOn(modalBackdrop, 'addBackdrop');
        spyOn(modalBackdrop, 'removeBackdrop');

        modalBackdrop.addModal(modal);
        modalBackdrop.removeModal(modal);

        expect(modalBackdrop.addBackdrop).toHaveBeenCalledTimes(1);
        expect(modalBackdrop.removeBackdrop).toHaveBeenCalledTimes(1);

        modalBackdrop.modals.subscribe(modals => {
            expect(modals.length).toBe(0);
            done();
        });
    });

    it('removeModal() removeOnly1', done => {
        var modal = {};

        spyOn(modalBackdrop, 'addBackdrop');
        spyOn(modalBackdrop, 'removeBackdrop');

        modalBackdrop.addModal(modal);
        modalBackdrop.addModal(modal);
        modalBackdrop.removeModal(modal);

        expect(modalBackdrop.addBackdrop).toHaveBeenCalledTimes(3);
        expect(modalBackdrop.removeBackdrop).not.toHaveBeenCalled();

        modalBackdrop.modals.subscribe(modals => {
            expect(modals.length).toBe(1);
            expect(modals[0]).toBe(modal);
            done();
        });
    });

    it('addBackdrop()', () => {
        modalBackdrop.addBackdrop();

        expect(document.body.classList.contains('modal-open')).toBe(true);
        expect(bodyElements.length).toBe(1);
    });

    it('removeBackdrop()', () => {
        modalBackdrop.addBackdrop();
        modalBackdrop.removeBackdrop();

        expect(document.body.classList.contains('modal-open')).toBe(false);
        expect(bodyElements.length).toBe(0);
    });

    it('removeBackdrop() restoreScroll', () => {
        (document.documentElement as any).clientWidth = 100;
        document.documentElement.scrollTop = 1;
        document.body.scrollTop = 2;

        modalBackdrop.addBackdrop();

        document.documentElement.scrollTop = 3;
        document.body.scrollTop = 4;

        modalBackdrop.removeBackdrop();

        expect(document.documentElement.scrollTop).toBe(1);
        expect(document.body.scrollTop).toBe(2);
        expect(document.body.classList.contains('modal-open')).toBe(false);
        expect(bodyElements.length).toBe(0);
    });

    it('removeBackdrop() restoreScroll unlessResize', () => {
        (document.documentElement as any).clientWidth = 100;
        document.documentElement.scrollTop = 1;
        document.body.scrollTop = 2;

        modalBackdrop.addBackdrop();

        (document.documentElement as any).clientWidth = 105;
        document.documentElement.scrollTop = 3;
        document.body.scrollTop = 4;

        modalBackdrop.removeBackdrop();

        expect(document.documentElement.scrollTop).toBe(3);
        expect(document.body.scrollTop).toBe(4);
        expect(document.body.classList.contains('modal-open')).toBe(false);
        expect(bodyElements.length).toBe(0);
    });
});
