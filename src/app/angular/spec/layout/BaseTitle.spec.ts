import { Title } from '@angular/platform-browser';

import { BaseTitle } from '../../layout/BaseTitle';

describe('BaseTitle', () => {
    var title: Title;

    beforeEach(() => {
        title = new Title({});
    });

    it('string', () => {
        spyOn(title, 'setTitle');

        new BaseTitle(title, 'Site').setTitle('Awesome');

        expect(title.setTitle).toHaveBeenCalledWith('Awesome - Site');
    });

    it('string customSeparator', () => {
        spyOn(title, 'setTitle');

        new BaseTitle(title, 'Site', ' | ').setTitle('Awesome');

        expect(title.setTitle).toHaveBeenCalledWith('Awesome | Site');
    });

    it('string noSiteName', () => {
        spyOn(title, 'setTitle');

        new BaseTitle(title).setTitle('Awesome');

        expect(title.setTitle).toHaveBeenCalledWith('Awesome');
    });

    it('array 1 item', () => {
        spyOn(title, 'setTitle');

        new BaseTitle(title, 'Site').setTitle(['Awesome']);

        expect(title.setTitle).toHaveBeenCalledWith('Awesome - Site');
    });

    it('array 2 items', () => {
        spyOn(title, 'setTitle');

        new BaseTitle(title, 'Site').setTitle(['Awesome', 'Sauce']);

        expect(title.setTitle).toHaveBeenCalledWith('Awesome - Sauce - Site');
    });

    it('array 3 items', () => {
        spyOn(title, 'setTitle');

        new BaseTitle(title, 'Site').setTitle(['Awesome', 'Sauce', 'Awesome Sauce']);

        expect(title.setTitle).toHaveBeenCalledWith(
            'Awesome - Sauce - Awesome Sauce - Site'
        );
    });

    it('array 3 items 1 emptyItem', () => {
        spyOn(title, 'setTitle');

        new BaseTitle(title, 'Site').setTitle(['Awesome', '', 'Sauce']);

        expect(title.setTitle).toHaveBeenCalledWith('Awesome - Sauce - Site');
    });

    it('array 3 items customSeparator', () => {
        spyOn(title, 'setTitle');

        new BaseTitle(title, 'Site', ' | ').setTitle([
            'Awesome',
            'Sauce',
            'Awesome Sauce'
        ]);

        expect(title.setTitle).toHaveBeenCalledWith(
            'Awesome | Sauce | Awesome Sauce | Site'
        );
    });

    it('array 3 items noSiteName', () => {
        spyOn(title, 'setTitle');

        new BaseTitle(title).setTitle(['Awesome', 'Sauce', 'Awesome Sauce']);

        expect(title.setTitle).toHaveBeenCalledWith('Awesome - Sauce - Awesome Sauce');
    });

    it('array empty', () => {
        spyOn(title, 'setTitle');

        new BaseTitle(title, 'Site').setTitle([]);

        expect(title.setTitle).toHaveBeenCalledWith('Site');
    });

    it('array empty noSiteName', () => {
        spyOn(title, 'setTitle');

        new BaseTitle(title).setTitle([]);

        expect(title.setTitle).toHaveBeenCalledWith('');
    });

    it('empty', () => {
        spyOn(title, 'setTitle');

        new BaseTitle(title, 'Site').setTitle('');

        expect(title.setTitle).toHaveBeenCalledWith('Site');
    });

    it('empty emptySiteName', () => {
        spyOn(title, 'setTitle');

        new BaseTitle(title, '').setTitle('');

        expect(title.setTitle).toHaveBeenCalledWith('');
    });

    it('null', () => {
        spyOn(title, 'setTitle');

        new BaseTitle(title, 'Site').setTitle(null);

        expect(title.setTitle).toHaveBeenCalledWith('Site');
    });

    it('null nullSiteName', () => {
        spyOn(title, 'setTitle');

        new BaseTitle(title, null).setTitle(null);

        expect(title.setTitle).toHaveBeenCalledWith('');
    });

    it('undefined', () => {
        spyOn(title, 'setTitle');

        new BaseTitle(title, 'Site').setTitle();

        expect(title.setTitle).toHaveBeenCalledWith('Site');
    });

    it('undefined undefinedSiteName', () => {
        spyOn(title, 'setTitle');

        new BaseTitle(title, undefined).setTitle();

        expect(title.setTitle).toHaveBeenCalledWith('');
    });
});
