import { AlertsComponent } from '../../../layout/alert/Alerts.component';
import { AlertType } from '../../../layout/alert/AlertType';
import { IAlert } from '../../../layout/alert/IAlert';

describe('Alerts', () => {
    var alerts: AlertsComponent;

    beforeEach(() => {
        alerts = new AlertsComponent(undefined, undefined);
    });

    it('canClose() default', () => {
        expect(
            alerts.canClose({
                message: 'Awesome',
                type: AlertType.error
            })
        ).toBe(true);
    });

    it('canClose() true', () => {
        expect(
            alerts.canClose({
                message: 'Awesome',
                type: AlertType.error,
                close: true
            })
        ).toBe(true);
    });

    it('canClose() false', () => {
        expect(
            alerts.canClose({
                message: 'Awesome',
                type: AlertType.error,
                close: false
            })
        ).toBe(false);
    });

    it('getClass() success', () => {
        expect(
            alerts.getClass({
                message: 'Awesome',
                type: AlertType.success
            })
        ).toBe('alert alert-success alert-dismissible mb-0 rounded-0');
    });

    it('getClass() warning', () => {
        expect(
            alerts.getClass({
                message: 'Awesome',
                type: AlertType.warning
            })
        ).toBe('alert alert-warning alert-dismissible mb-0 rounded-0');
    });

    it('getClass() error', () => {
        expect(
            alerts.getClass({
                message: 'Awesome',
                type: AlertType.error
            })
        ).toBe('alert alert-danger alert-dismissible mb-0 rounded-0');
    });

    it('getClass() info', () => {
        expect(
            alerts.getClass({
                message: 'Awesome',
                type: AlertType.info
            })
        ).toBe('alert alert-info alert-dismissible mb-0 rounded-0');
    });

    it('getClass() noClose success', () => {
        expect(
            alerts.getClass({
                message: 'Awesome',
                type: AlertType.success,
                close: false
            })
        ).toBe('alert alert-success mb-0 rounded-0');
    });

    it('getClass() noClose warning', () => {
        expect(
            alerts.getClass({
                message: 'Awesome',
                type: AlertType.warning,
                close: false
            })
        ).toBe('alert alert-warning mb-0 rounded-0');
    });

    it('getClass() noClose error', () => {
        expect(
            alerts.getClass({
                message: 'Awesome',
                type: AlertType.error,
                close: false
            })
        ).toBe('alert alert-danger mb-0 rounded-0');
    });

    it('getClass() noClose info', () => {
        expect(
            alerts.getClass({
                message: 'Awesome',
                type: AlertType.info,
                close: false
            })
        ).toBe('alert alert-info mb-0 rounded-0');
    });

    it('getIconClass() success', () => {
        expect(
            alerts.getIconClass({
                message: 'Awesome',
                type: AlertType.success
            })
        ).toBeUndefined();
    });

    it('getIconClass() warning', () => {
        expect(
            alerts.getIconClass({
                message: 'Awesome',
                type: AlertType.warning
            })
        ).toBe('fa fa-warning');
    });

    it('getIconClass() error', () => {
        expect(
            alerts.getIconClass({
                message: 'Awesome',
                type: AlertType.error
            })
        ).toBe('fa fa-warning');
    });

    it('getIconClass() info', () => {
        expect(
            alerts.getIconClass({
                message: 'Awesome',
                type: AlertType.info
            })
        ).toBe('fa fa-info-circle');
    });

    it('getIconClass() custom alertIcon', () => {
        expect(
            alerts.getIconClass({
                message: 'Awesome',
                type: AlertType.success,
                iconClass: 'fa fa-bug'
            })
        ).toBe('fa fa-bug');
    });

    it('getIconClass() custom directiveIcons success', () => {
        alerts.iconClassSuccess = 'Icon Icon-success';

        expect(
            alerts.getIconClass({
                message: 'Awesome',
                type: AlertType.success
            })
        ).toBe('Icon Icon-success');
    });

    it('getIconClass() custom directiveIcons warning', () => {
        alerts.iconClassWarning = 'Icon Icon-warning';

        expect(
            alerts.getIconClass({
                message: 'Awesome',
                type: AlertType.warning
            })
        ).toBe('Icon Icon-warning');
    });

    it('getIconClass() custom directiveIcons error', () => {
        alerts.iconClassError = 'Icon Icon-error';

        expect(
            alerts.getIconClass({
                message: 'Awesome',
                type: AlertType.error
            })
        ).toBe('Icon Icon-error');
    });

    it('getIconClass() custom directiveIcons info', () => {
        alerts.iconClassInfo = 'Icon Icon-info';

        expect(
            alerts.getIconClass({
                message: 'Awesome',
                type: AlertType.info
            })
        ).toBe('Icon Icon-info');
    });
});
