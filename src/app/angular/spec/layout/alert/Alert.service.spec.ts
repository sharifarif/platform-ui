import { BehaviorSubject } from 'rxjs';

import { AlertService } from '../../../layout/alert/Alert.service';
import { AlertType } from '../../../layout/alert/AlertType';
import { IAlert } from '../../../layout/alert/IAlert';

describe('AlertService', () => {
    var alertService: AlertService, alertSubject: BehaviorSubject<IAlert[]>;

    beforeEach(() => {
        alertService = new AlertService();
        alertSubject = (alertService as any).alertsSubject;
    });

    it('addAlert()', () => {
        expect(alertSubject.getValue()).toEqual([]);

        alertService.addAlert({
            message: 'Awesome',
            type: AlertType.error
        });

        expect(alertSubject.getValue()).toEqual([
            {
                message: 'Awesome',
                type: AlertType.error
            }
        ]);
    });

    it('addAlert() multiple', () => {
        expect(alertSubject.getValue()).toEqual([]);

        alertService.addAlert({
            message: 'Awesome',
            type: AlertType.error
        });

        expect(alertSubject.getValue()).toEqual([
            {
                message: 'Awesome',
                type: AlertType.error
            }
        ]);

        alertService.addAlert({
            message: 'Sauce',
            type: AlertType.warning
        });

        expect(alertSubject.getValue()).toEqual([
            {
                message: 'Awesome',
                type: AlertType.error
            },
            {
                message: 'Sauce',
                type: AlertType.warning
            }
        ]);
    });

    it('alerts all', done => {
        alertService.addAlert({
            message: 'Awesome',
            type: AlertType.warning
        });
        alertService.addAlert({
            message: 'Sauce',
            type: AlertType.warning
        });
        alertService.addAlert({
            message: 'Awesome',
            type: AlertType.warning
        });

        alertService.alerts.all.subscribe(alerts => {
            expect(alerts).toEqual([
                {
                    message: 'Awesome',
                    type: AlertType.warning
                },
                {
                    message: 'Sauce',
                    type: AlertType.warning
                },
                {
                    message: 'Awesome',
                    type: AlertType.warning
                }
            ]);

            done();
        });
    });

    it('alerts unique', done => {
        alertService.addAlert({
            message: 'Awesome',
            type: AlertType.warning
        });
        alertService.addAlert({
            message: 'Sauce',
            type: AlertType.warning
        });
        alertService.addAlert({
            message: 'Awesome',
            type: AlertType.warning
        });

        alertService.alerts.unique.subscribe(alerts => {
            expect(alerts).toEqual([
                {
                    message: 'Awesome',
                    type: AlertType.warning
                },
                {
                    message: 'Sauce',
                    type: AlertType.warning
                }
            ]);

            done();
        });
    });

    it('setAlert()', () => {
        expect(alertSubject.getValue()).toEqual([]);

        alertService.setAlert({
            message: 'Awesome',
            type: AlertType.error
        });

        expect(alertSubject.getValue()).toEqual([
            {
                message: 'Awesome',
                type: AlertType.error
            }
        ]);
    });

    it('setAlert() multiple', () => {
        expect(alertSubject.getValue()).toEqual([]);

        alertService.setAlert({
            message: 'Awesome',
            type: AlertType.error
        });

        expect(alertSubject.getValue()).toEqual([
            {
                message: 'Awesome',
                type: AlertType.error
            }
        ]);

        alertService.setAlert({
            message: 'Sauce',
            type: AlertType.warning
        });

        expect(alertSubject.getValue()).toEqual([
            {
                message: 'Sauce',
                type: AlertType.warning
            }
        ]);
    });

    it('remove()', () => {
        var awesome = {
            message: 'Awesome',
            type: AlertType.error
        };
        var sauce = {
            message: 'Sauce',
            type: AlertType.warning
        };

        alertService.addAlert(awesome);
        alertService.addAlert(sauce);
        alertService.addAlert(awesome);

        alertService.remove(awesome);

        expect(alertSubject.getValue()).toEqual([
            {
                message: 'Sauce',
                type: AlertType.warning
            }
        ]);
    });

    it('removeAll()', () => {
        var awesome = {
            message: 'Awesome',
            type: AlertType.error
        };
        var sauce = {
            message: 'Sauce',
            type: AlertType.warning
        };

        alertService.addAlert(awesome);
        alertService.addAlert(sauce);
        alertService.addAlert(awesome);

        alertService.removeAll();

        expect(alertSubject.getValue()).toEqual([]);
    });
});
