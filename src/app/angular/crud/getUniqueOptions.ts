/**
 * Get the `options` that are not already `used`.
 *
 * @param options - All of the `options` that can be `used`.
 * @param used - `options` that are already `used`.
 * @param allowUsed - Allow an option, even if it’s already `used`.
 * @param optionKey - Field in each of the `options` to compare with.
 * @param usedKey - Field in each of the `used` to compare with.
 */
export default function getUniqueOptions<O, U>(
    options: O[],
    used: U[],
    allowUsed?: U,
    optionKey?: keyof O,
    usedKey?: keyof U
) {
    if (options && used) {
        let usedIds: any[], allowUsedId: any;

        if (usedKey) {
            usedIds = used.map(value => {
                return (value as any)[usedKey];
            });
            if (allowUsed) {
                allowUsedId = (allowUsed as any)[usedKey];
            }
        } else {
            usedIds = used;
            allowUsedId = allowUsed;
        }

        options = options.filter(option => {
            var optionId: any;

            if (optionKey) {
                optionId = (option as any)[optionKey];
            } else {
                optionId = option;
            }

            return optionId === allowUsedId || !usedIds.includes(optionId);
        });
    }

    return options;
}
