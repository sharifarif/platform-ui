/**
 * Interface that holds an {@link item},
 * plus any extra information about the {@link item}.
 *
 * @param I - Type of {@link item}.
 */
export interface ICrudData<I> {
    /**
     * Item the user is Creating, Reading, Updating, or Deleting.
     */
    item: I;
}
