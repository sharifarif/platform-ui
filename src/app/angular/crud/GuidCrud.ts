import { Observable } from 'rxjs';
import { share } from 'rxjs/operators';

import { BaseCrud } from './BaseCrud';
import { ICrudData } from './ICrudData';
import { IGuidModel } from '../model/IGuidModel';

/**
 * Create, Read, Update, and Delete (CRUD) an item of type {@link IGuidModel}.
 *
 * @param I - Type of item.
 * @param D - Interface that holds the item, plus any extra information about the item.
 */
export class GuidCrud<I extends IGuidModel, D extends ICrudData<I>> extends BaseCrud<
    I,
    D
> {
    /**
     * Save the data to the server.
     *
     * Update {@link IGuidModel.id} and {@link IGuidModel.rowVersion} as needed.
     *
     * @param data - The data to save.
     * @param savedData - What {@link BaseSaveable.savedData} will become
     *                    after the save completes.
     */
    protected performSave(data: D, savedData: D) {
        var observable: Observable<I>;

        var item = data.item,
            savedItem = savedData.item,
            add = !item.id;

        if (add) {
            observable = this.performAdd(data);
        } else {
            observable = this.performUpdate(data);
        }

        observable = observable.pipe(share());

        observable.subscribe(
            saved => {
                if (add) {
                    item.id = saved.id;
                    savedItem.id = saved.id;
                }

                item.rowVersion = saved.rowVersion;
                savedItem.rowVersion = saved.rowVersion;
            },
            () => {
                // Do nothing.
            }
        );

        return observable;
    }
}
