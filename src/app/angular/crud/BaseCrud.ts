import { BaseSaveable } from '../save/BaseSaveable';
import { EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';
import { share } from 'rxjs/operators';

import { CrudMode } from './CrudMode';
import { ICrudApi } from '../api/ICrudApi';
import { ICrudData } from './ICrudData';

/**
 * Create, Read, Update, and Delete (CRUD) an item.
 *
 * @param I - Type of item.
 * @param D - Interface that holds the item, plus any extra information about the item.
 */
export abstract class BaseCrud<I, D extends ICrudData<I>> extends BaseSaveable<D> {
    /**
     * `true` if the item is being deleted on the server.
     */
    deleting = false;

    /**
     * `true` if there was an error deleting the item.
     */
    errorDeleting = false;

    /**
     * Event fired when the item is deleted successfully.
     */
    onDelete = new EventEmitter<void>();
    private editLoaded = false;
    private mode = CrudMode.Read;

    /**
     * @param api - API that can Create, Update, and Delete the item.
     */
    constructor(protected api: ICrudApi<I, any>) {
        super();
    }

    /**
     * Delete the item.
     */
    delete() {
        this.deleting = true;

        var observable = this.performDelete(this.data).pipe(share());

        observable.subscribe(
            () => {
                this.onDelete.emit();
                this.errorDeleting = false;
            },
            () => {
                this.deleting = false;
                this.errorDeleting = true;
            }
        );

        return observable;
    }

    /**
     * Determine whether the user is Reading, Editing, or Deleting the data.
     */
    getMode() {
        return this.mode;
    }

    /**
     * Specify whether the user is Reading, Editing, or Deleting the data.
     *
     * @param mode - Whether the user is Reading, Editing, or Deleting the data.
     */
    setMode(mode: CrudMode) {
        this.mode = mode;

        if (mode === CrudMode.Edit && !this.editLoaded) {
            this.editLoaded = true;
            this.initEditMode();
        }
    }

    /**
     * Determine what `data` should be sent to the server
     * when an item is Created or Edited.
     *
     * If extra information should be sent, override this function in your subclass.
     *
     * @param data - Interface that holds the item, plus any extra information.
     */
    protected getEditRequest(data: D) {
        return data.item;
    }

    /**
     * Load extra information needed in {@link CrudMode.Edit}.
     *
     * If extra information is needed in {@link CrudMode.Edit},
     * override this function in your subclass.
     */
    protected initEditMode() {
        // A subclass may implement this method.
    }

    /**
     * Tell the server to Create an item.
     *
     * @param data - Interface that holds the item, plus any extra information.
     */
    protected performAdd(data: D) {
        return this.api.add(this.getEditRequest(data));
    }

    /**
     * Tell the server to Delete an item.
     *
     * @param data - Interface that holds the item, plus any extra information.
     */
    protected performDelete(data: D) {
        return this.api.inactivate(data.item);
    }

    /**
     * Tell the server to Edit an item.
     *
     * @param data - Interface that holds the item, plus any extra information.
     */
    protected performUpdate(data: D) {
        return this.api.update(this.getEditRequest(data));
    }
}
