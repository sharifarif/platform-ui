/**
 * Is the user Reading, Editing, or Deleting data?
 *
 * This is a simplified version of CRUD (Create, Read, Update, Delete)
 * where only 3 cases need to be handled.
 */
export type CrudMode = 'Read' | 'Edit' | 'Delete';

export const CrudMode = {
    /**
     * The user is Reading data.
     */
    Read: 'Read' as CrudMode,

    /**
     * The user is Editing (Creating or Updating) data.
     */
    Edit: 'Edit' as CrudMode,

    /**
     * The user is Deleting data.
     */
    Delete: 'Delete' as CrudMode
};
