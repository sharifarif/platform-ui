import { ElementRef } from '@angular/core';

import { BaseList } from '../../list/BaseList';
import { BaseCrud } from '../BaseCrud';
import { CrudMode } from '../CrudMode';
import { ICrudData } from '../ICrudData';
import { PrettySubmitGuard } from '../../save/PrettySubmitGuard';

/**
 * Base class for list components that allow items to be edited in place.
 *
 * @param I - Type of items in the list.
 * @param D - Interface that holds an item, plus any extra information about the item.
 */
export abstract class BaseCrudList<I, D extends ICrudData<I>> extends BaseList<I> {
    /**
     * Create, Read, Update, and Delete (CRUD) each item separately.
     */
    cruds: BaseCrud<I, D>[];

    constructor(
        protected elementRef: ElementRef,
        private prettySubmitGuard: PrettySubmitGuard
    ) {
        super();
    }

    /**
     * Add an item.
     */
    addItem() {
        var item = this.createItem(),
            crud = this.createCrud();

        crud.initNew({
            item: item
        } as D);
        crud.setMode(CrudMode.Edit);
        this.subscribeToCrud(crud);

        (this.items || (this.items = [])).unshift(item);
        (this.cruds || (this.cruds = [])).unshift(crud);
    }

    /**
     * Remove all items from the list.
     *
     * The user will be prompted if there are unsaved changes.
     *
     * @returns `false` if the user wants to keep the unsaved changes.
     */
    clear() {
        var clear = this.prettySubmitGuard.canRemoveComponent(this.elementRef);

        if (clear) {
            this.items = undefined;
            this.cruds = undefined;
        }

        return clear;
    }

    /**
     * Determine whether the list has an item that has never been saved.
     */
    hasNewItem() {
        var hasNewItem = false;

        var cruds = this.cruds;
        if (cruds) {
            cruds.forEach(crud => {
                if (crud.isNew()) {
                    hasNewItem = true;
                }
            });
        }

        return hasNewItem;
    }

    /**
     * Determine whether any visible {@link cruds} are in {@link CrudMode.Read}.
     */
    isAnyReadVisible() {
        var cruds = this.cruds;
        if (cruds) {
            for (let index = 0, length = cruds.length; index < length; index++) {
                if (this.isReadVisible(cruds[index])) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Determine whether a {@link BaseCrud} is visible and in {@link CrudMode.Read}.
     *
     * @param crud - {@link BaseCrud} to check.
     */
    isReadVisible(crud: BaseCrud<I, D>) {
        var savedData = crud.savedData;
        if (!savedData) {
            return false;
        }

        if (crud.getMode() !== CrudMode.Read) {
            return false;
        }

        var searchResults = this.searchResults;

        return !searchResults || searchResults.includes(crud.savedData.item);
    }

    /**
     * Create an empty {@link BaseCrud}.
     *
     * Implement this function in your subclass.
     */
    protected abstract createCrud(): BaseCrud<I, D>;

    /**
     * Underlying implementation for {@link addItem}.
     *
     * If a new item should start with anything other than `{}`,
     * override this function in your subclass.
     */
    protected createItem() {
        return {} as I;
    }

    /**
     * Call this in `ngOnInit()` once the list of items is loaded.
     *
     * @param items - Loaded items.
     */
    protected init(items: I[]) {
        var cruds = items.map(item => {
            var crud = this.createCrud();

            crud.initSaved({
                item: item
            } as D);
            this.subscribeToCrud(crud);

            return crud;
        });

        if (this.items) {
            items = this.items.concat(items);
        }
        if (this.cruds) {
            cruds = this.cruds.concat(cruds);
        }

        this.items = items;
        this.cruds = cruds;
    }

    // FIXME: Unit test.
    private subscribeToCrud(crud: BaseCrud<I, D>) {
        crud.onSave.subscribe((savedData: D) => {
            var cruds = this.cruds,
                items = this.items,
                index = cruds.indexOf(crud);

            if (index >= 0) {
                items[index] = savedData.item;
            }
        });
        crud.onDelete.subscribe(() => {
            var cruds = this.cruds,
                items = this.items,
                index = cruds.indexOf(crud);

            if (index >= 0) {
                cruds.splice(index, 1);
                items.splice(index, 1);
            }
        });
    }
}
