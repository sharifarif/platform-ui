import * as express from 'express';
import { ngExpressEngine } from '@nguniversal/express-engine';
import { NgModuleFactory, Type } from '@angular/core';

import { BaseServer } from './BaseServer';
import { IServerEnvironment } from '../environment/server/IServerEnvironment';
import { IServerEnvironments } from '../environment/server/IServerEnvironments';

/**
 * Server-Side Rendering
 *
 * @see https://scadev.atlassian.net/wiki/spaces/TP/pages/123437385/Server-Side+Rendering
 */
export class Server<E extends IServerEnvironment> extends BaseServer<E> {
    /**
     * @param environments - Environment variables for each site.
     * @param index - Index HTML file, relative to {@link distDirectory}.
     * @param module - Server-Side Rendering class with `@NgModule` decorator.
     */
    constructor(
        environments: IServerEnvironments<E>,
        protected index: string,
        module: Type<{}> | NgModuleFactory<{}>
    ) {
        super(environments, express(), module, ngExpressEngine);
    }

    /**
     * Serve static files.
     */
    protected serveStatic() {
        const distDirectory = this.distDirectory,
            expressInstance = this.expressValue,
            index = this.index;

        expressInstance.use(
            '/',
            express.static(distDirectory, {
                index: index
            })
        );
    }
}
