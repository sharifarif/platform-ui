import * as compression from 'compression';
import { enableProdMode, NgModuleFactory, Type } from '@angular/core';
import * as express from 'express';
import { NgSetupOptions, RenderOptions } from '@nguniversal/express-engine';
import merge from 'lodash/merge';
import * as path from 'path';

import { ExpressEngine } from './express/ExpressEngine';
import { ExpressEngineCallback } from './express/ExpressEngineCallback';
import { IServerEnvironment } from '../environment/server/IServerEnvironment';
import { IServerEnvironments } from '../environment/server/IServerEnvironments';
import { ServerRequest } from './ServerRequest';

/**
 * Base class for Server-Side Rendering.
 *
 * @see https://scadev.atlassian.net/wiki/spaces/TP/pages/123437385/Server-Side+Rendering
 */
export abstract class BaseServer<E extends IServerEnvironment> {
    /**
     * @param environments - Environment variables for each site.
     * @param expressValue - Result of calling `express()`.
     * @param module - Server-Side Rendering class with `@NgModule` decorator.
     * @param ngExpressEngine - Reference to `ngExpressEngine`.
     */
    constructor(
        public environments: IServerEnvironments<E>,
        public expressValue: express.Express,
        protected module: Type<{}> | NgModuleFactory<{}>,
        protected ngExpressEngine: (setupOptions: NgSetupOptions) => ExpressEngine
    ) {
        environments.sites = (environments.sites || []).map(site => {
            return merge({}, environments.default, site);
        });
    }

    /**
     * Absolute path of the `/dist/` directory.
     */
    get distDirectory() {
        return path.resolve(process.argv[1], '../../..');
    }

    /**
     * Port number the {@link Server} runs on.
     */
    get port() {
        return Number(process.env.PORT) || 8000;
    }

    /**
     * Run the server.
     */
    run() {
        const expressLocal = this.expressValue,
            port = this.port;

        expressLocal.use(compression());

        this.serveStatic();
        this.serveAngular();

        expressLocal.listen(port, () => {
            // tslint:disable-next-line no-console
            console.log(`Listening to http://localhost:${port}`);
        });
    }

    /**
     * Render Angular on the {@link BaseServer} with Angular Universal.
     */
    protected serveAngular() {
        const distDirectory = this.distDirectory,
            environments = this.environments,
            expressLocal = this.expressValue,
            module = this.module,
            ngExpressEngine = this.ngExpressEngine;

        enableProdMode();

        const engine: ExpressEngine = (
            filePath: string,
            options: RenderOptions,
            callback: ExpressEngineCallback
        ) => {
            new ServerRequest(environments, module, ngExpressEngine, options).render(
                filePath,
                callback
            );
        };

        expressLocal.engine('html', engine);
        expressLocal.set('view engine', 'html');
        expressLocal.set('views', distDirectory);

        expressLocal.get('/**', (request, response) => {
            response.render('index', {
                req: request,
                res: response
            });
        });
    }

    /**
     * Serve static files.
     */
    protected abstract serveStatic(): void;
}
