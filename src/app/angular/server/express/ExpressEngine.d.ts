import { RenderOptions } from '@nguniversal/express-engine';

import { ExpressEngineCallback } from './ExpressEngineCallback';

/**
 * Function that renders an HTML page.
 */
export type ExpressEngine = (
    filePath: string,
    options: RenderOptions,
    callback: ExpressEngineCallback
) => void;
