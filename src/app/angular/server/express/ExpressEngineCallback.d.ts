/**
 * Function called once an HTML page is rendered.
 */
export type ExpressEngineCallback = (error?: Error, html?: string) => void;
