/**
 * Type of domain used in this {@link ServerRequest}.
 */
export enum ServerRequestDomainType {
    /**
     * Each domain is served by a dedicated domain.
     *
     * For example, `tork.co.uk` may be served by `https://prdlv.tork.co.uk/`.
     */
    DedicatedDomain = 1,

    /**
     * Every domain is served by the same domain.
     *
     * For example, `tork.co.uk` may be served by
     * `https://server.azurewebsites.net/tork.co.uk/`.
     */
    SharedDomain,

    /**
     * Every domain is served by the same domain,
     * and a developer is accessing the server directly.
     *
     * For example, `tork.co.uk` may be served by `http://localhost:8000/tork.co.uk/`.
     */
    SharedDomainDevelopment
}
