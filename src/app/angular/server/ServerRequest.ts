import { APP_BASE_HREF } from '@angular/common';
import forEach from 'lodash/forEach';
import { NgModuleFactory, Type } from '@angular/core';
import { NgSetupOptions, RenderOptions } from '@nguniversal/express-engine';
import sortBy from 'lodash/sortBy';

import { dotNotation } from './dotNotation';
import { ENVIRONMENT } from '../environment/ENVIRONMENT';
import { ExpressEngine } from './express/ExpressEngine';
import { ExpressEngineCallback } from './express/ExpressEngineCallback';
import { IServerEnvironment } from '../environment/server/IServerEnvironment';
import { IServerEnvironments } from '../environment/server/IServerEnvironments';
import { ServerRequestDomainType } from './ServerRequestDomainType';

/**
 * Render an HTML page with Angular Universal.
 *
 * @see https://scadev.atlassian.net/wiki/spaces/TP/pages/123437385/Server-Side+Rendering
 */
export class ServerRequest<E extends IServerEnvironment> {
    private _environment: E;

    /**
     * @param environments - Environment variables for each site.
     * @param module - Server-Side Rendering class with `@NgModule` decorator.
     * @param ngExpressEngine - Reference to `ngExpressEngine`.
     * @param options - Request options.
     */
    constructor(
        private environments: IServerEnvironments<E>,
        private module: Type<{}> | NgModuleFactory<{}>,
        private ngExpressEngine: (setupOptions: NgSetupOptions) => ExpressEngine,
        private options: RenderOptions
    ) {}

    /**
     * Normalized URL every path on the site starts with.
     *
     * @see https://scadev.atlassian.net/wiki/spaces/TP/pages/249036881/Configuration
     */
    get basePath() {
        var site = this.environment.site,
            basePath = site.basePath;

        if (this.domainType === ServerRequestDomainType.SharedDomainDevelopment) {
            basePath = '/' + site.domain + basePath;
        }
        if (!basePath.endsWith('/')) {
            basePath += '/';
        }

        return basePath;
    }

    /**
     * Type of domain used for this {@link ServerRequest}.
     */
    get domainType() {
        var environment = this.environment,
            options = this.options,
            optionsRequest = options && options.req,
            request = this.normalizeRequest,
            domain = environment.site.domain,
            host = request.host,
            xForwardedFor = optionsRequest && optionsRequest.header('x-forwarded-for');

        if (host === domain || host.endsWith('.' + domain)) {
            return ServerRequestDomainType.DedicatedDomain;
        }

        if (host.indexOf('localhost') < 0 || xForwardedFor) {
            return ServerRequestDomainType.SharedDomain;
        }

        return ServerRequestDomainType.SharedDomainDevelopment;
    }

    /**
     * Environment variables for the current site.
     */
    get environment() {
        var environment = this._environment;

        if (!environment) {
            let request = this.normalizeRequest,
                sites = this.normalizeSites,
                host = request.host,
                originalUrl = request.originalUrl;

            let site = sites.find(localSite => {
                let basePath = localSite.basePath,
                    domain = localSite.domain;

                return (
                    (host === domain || host.endsWith('.' + domain)) &&
                    (!basePath || (originalUrl && originalUrl.startsWith(basePath)))
                );
            });

            if (!site) {
                site = sites.find(localSite => {
                    let basePath = localSite.basePath,
                        domain = localSite.domain;

                    return originalUrl && originalUrl.startsWith('/' + domain + basePath);
                });
            }

            if (site) {
                environment = site.environment;
                this._environment = environment;
            }
        }

        return environment;
    }

    /**
     * Normalize the request, so logic can easily be built around it.
     */
    get normalizeRequest() {
        let options = this.options,
            request = options && options.req,
            host = request && request.header('host'),
            originalUrl = request && request.originalUrl;

        host = host ? host.replace(/:.+/, '').toLowerCase() : '';
        originalUrl = originalUrl ? originalUrl.toLowerCase() : '/';

        return {
            host: host,
            originalUrl: originalUrl
        };
    }

    /**
     * Normalize the list of sites, so logic can easily be built around them.
     */
    get normalizeSites() {
        interface INormalizedSite {
            basePath: string;
            domain: string;
            environment: E;
        }

        var sites: INormalizedSite[] = this.environments.sites.map(site => {
            var basePath = site.site.basePath,
                domain = site.site.domain;

            if (basePath) {
                basePath = basePath.toLowerCase().replace(/\/+$/, '');
            }
            if (domain) {
                domain = domain.toLowerCase();
            }

            return {
                basePath: basePath || '',
                domain: domain || '',
                environment: site
            };
        });

        sites = sortBy(sites, [
            (site: INormalizedSite) => {
                return -site.domain.length;
            },
            (site: INormalizedSite) => {
                return -site.basePath.length;
            }
        ]);

        return sites;
    }

    /**
     * Render an HTML page with Angular Universal.
     *
     * @param filePath - Path to the an HTML template.
     * @param callback - Function called once an HTML page is rendered.
     */
    render(filePath: string, callback: ExpressEngineCallback) {
        let environment = this.environment,
            options = this.options;

        if (this.domainType === ServerRequestDomainType.SharedDomain) {
            let domainLength = environment.site.domain.length,
                request = options.req,
                originalUrl = request.originalUrl,
                url = request.url;

            if (originalUrl) {
                request.originalUrl = originalUrl.substring(domainLength + 1);
            }
            if (url) {
                request.url = url.substring(domainLength + 1);
            }
        }

        let ngExpressEngine = this.ngExpressEngine({
            bootstrap: this.module,
            providers: [
                {
                    provide: APP_BASE_HREF,
                    useValue: this.basePath
                },
                {
                    provide: ENVIRONMENT,
                    useValue: environment
                }
            ]
        });

        ngExpressEngine(filePath, options, (error?: Error, html?: string) => {
            html = this.replaceEnvironmentVariables(html);

            callback(error, html);
        });
    }

    /**
     * Replace environment variables in the HTML.
     *
     * @param html - HTML to replace the variables in.
     */
    replaceEnvironmentVariables(html: string) {
        if (html) {
            let variables = dotNotation(this.environment);

            html = html.split('#{site.basePath}').join(this.basePath);

            forEach(variables, (value, name) => {
                html = html.split('#{' + name + '}').join(value);
            });
        }

        return html;
    }
}
