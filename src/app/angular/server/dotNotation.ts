import forEach from 'lodash/forEach';
import isObjectLike from 'lodash/isObjectLike';

/**
 * Flatten an object into an object with keys in dot notation.
 *
 * @param object - Object to flatten.
 * @param prefix - Prefix for dot notation.
 */
export function dotNotation(object: any, prefix = '') {
    if (!isObjectLike(object)) {
        return object;
    }

    prefix = prefix ? prefix + '.' : '';

    var result: { [resultKey: string]: any } = {};

    forEach(object, (value, key) => {
        var resultKey = prefix + key;

        if (isObjectLike(value)) {
            Object.assign(result, dotNotation(value, resultKey));
        } else {
            result[resultKey] = value;
        }
    });

    return result;
}
