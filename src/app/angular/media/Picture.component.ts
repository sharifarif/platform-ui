import { Component, Input } from '@angular/core';

import getCdnPaths from './getCdnPaths';
import hasWidthHeight from './hasWidthHeight';
import { IMedia } from '../model/media/IMedia';

/**
 * HTML5 `<picture>` element.
 *
 * **Responsive Image:**
 *
 * ```html
 * <platform-picture [image]="image" [responsive]="true">
 * </platform-picture>
 * ```
 *
 * @see https://scadev.atlassian.net/wiki/spaces/TP/pages/106382518/Images#Images-ResponsiveImages
 */
@Component({
    selector: 'platform-picture',
    templateUrl: 'Picture.component.html'
})
export class PictureComponent {
    /**
     * Image being displayed.
     */
    @Input() image: IMedia;

    /**
     * `true` to automatically fill the container width.
     */
    @Input() responsive?: boolean;

    /**
     * CSS classes for the `<img>` element.
     */
    @Input() imgClass?: string;

    /**
     * Get the CSS classes for the `<img>` element.
     */
    getImgClass() {
        var imgClass = this.imgClass;

        if (this.responsive) {
            imgClass = imgClass ? imgClass + ' ' : '';

            if (hasWidthHeight(this.image)) {
                imgClass += 'embed-responsive-item';
            } else {
                imgClass += 'img-fluid';
            }
        }

        return imgClass;
    }

    /**
     * Determine whether a WebP image exists for the {@link image}.
     */
    hasWebp() {
        var hasWebp = false,
            cdnPaths = getCdnPaths(this.image);

        if (cdnPaths) {
            Object.keys(cdnPaths).forEach(key => {
                if (key.endsWith('_webp')) {
                    hasWebp = true;
                }
            });
        }

        return hasWebp;
    }

    /**
     * Determine whether the width and height are known for the {@link image}.
     */
    hasWidthHeight() {
        return hasWidthHeight(this.image);
    }
}
