import { IMedia } from '../model/media/IMedia';

/**
 * Determine whether the width and height are known for an {@link IMedia}.
 */
export default function hasWidthHeight(media: IMedia) {
    return !!(media && media.Width && media.Height);
}
