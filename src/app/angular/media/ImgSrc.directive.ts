import { AfterViewInit, Directive, ElementRef, Input, OnChanges } from '@angular/core';
import max from 'lodash/max';
import min from 'lodash/min';

import { BaseImgSizes } from './sizes/BaseImgSizes';
import getCdnPaths from './getCdnPaths';
import { IMedia } from '../model/media/IMedia';
import { ImgSizesBootstrap } from './sizes/ImgSizesBootstrap';
import { Rendition } from '../model/media/Rendition';

/**
 * Automatically set the `src`, `srcset`, and `sizes` attribute of an `<img>` element.
 *
 * **Example:**
 *
 * ```html
 * <img [platformImgSrc]="image">
 * ```
 *
 * @see [Responsive Images](https://scadev.atlassian.net/wiki/spaces/TP/pages/106382518/Images#Images-ResponsiveImages)
 */
@Directive({
    selector: '[platformImgSrc]',
    providers: [
        {
            provide: BaseImgSizes,
            useClass: ImgSizesBootstrap
        }
    ]
})
export class ImgSrcDirective implements AfterViewInit, OnChanges {
    /**
     * Viewport width to assume when setting a fallback `src` attribute on an `<img>`.
     */
    static readonly FallbackViewportWidthPixels = 1200;

    /**
     * Image being displayed.
     */
    @Input() platformImgSrc: IMedia;

    /**
     * Image element type:
     * * A standalone `img` has a `src`, `srcset`, and `sizes` attribute.
     * * A `pictureImg` has a `src` attribute.
     * * A `pictureSource` has a `srcset` and `sizes` attribute.
     *
     * @see https://scadev.atlassian.net/wiki/spaces/TP/pages/106382518/Images#Images-ResponsiveImages
     */
    @Input() platformImgType: 'img' | 'pictureImg' | 'pictureSource' = 'img';

    /**
     * `true` to use WebP images, when available.
     */
    @Input() platformImgWebp = false;

    /**
     * Get a source for an `image` that can fill `imageWidthPixels`.
     *
     * Images are only upscaled when a larger image does not exist.
     *
     * @param image - Image being displayed.
     * @param imageWidthPixels - Desired image width in pixels.
     */
    static getSrc(image: IMedia, imageWidthPixels: number): string {
        var getWidth = (rendition: Rendition) => {
            var match = /(\d+)w/.exec(rendition);

            return match ? Number(match[1]) : undefined;
        };

        var cdnPaths = getCdnPaths(image),
            renditionWidths = Object.keys(cdnPaths)
                .map((rendition: Rendition) => {
                    return getWidth(rendition);
                })
                .filter(renditionWidth => {
                    return renditionWidth > 0;
                }),
            validRenditionWidths = renditionWidths.filter(renditionWidth => {
                return renditionWidth >= imageWidthPixels;
            }),
            minRenditionWidth = validRenditionWidths.length
                ? min(validRenditionWidths)
                : max(renditionWidths);

        return (cdnPaths as any)[minRenditionWidth + 'w'];
    }

    /**
     * Calculate the `srcset` attribute for a given `image`.
     *
     * @param image - Image being displayed.
     * @param webp - `true` to use WebP images, when available.
     */
    static getSrcSet(image: IMedia, webp = false) {
        var srcSet: string[] = [],
            cdnPaths = getCdnPaths(image);

        [
            Rendition['240w'],
            Rendition['480w'],
            Rendition['690w'],
            Rendition['720w'],
            Rendition['1080w'],
            Rendition['1200w'],
            Rendition['1440w']
        ].forEach((rendition: Rendition) => {
            var path: string;

            if (webp) {
                path = (cdnPaths as any)[rendition + '_webp'];
            }
            if (!path) {
                path = (cdnPaths as any)[rendition];
            }
            if (path) {
                srcSet.push(path + ' ' + rendition);
            }
        });

        return srcSet.join(', ') || undefined;
    }

    constructor(private elementRef: ElementRef, private imgSizes: BaseImgSizes) {}

    ngAfterViewInit() {
        this.render();
    }

    ngOnChanges() {
        this.render();
    }

    /**
     * Update the `src`, `srcset`, and `sizes` attributes on the `<img>` element.
     */
    protected render() {
        var elementRef = this.elementRef,
            element = elementRef.nativeElement as HTMLImageElement,
            image = this.platformImgSrc,
            imgSizes = this.imgSizes,
            type = this.platformImgType;

        if (type !== 'pictureImg') {
            element.sizes = imgSizes.getSizesString(elementRef);
            element.srcset = ImgSrcDirective.getSrcSet(image, this.platformImgWebp);
        }

        if (type !== 'pictureSource') {
            let fallbackWidthPixels = imgSizes.getWidthPixels(
                elementRef,
                ImgSrcDirective.FallbackViewportWidthPixels
            );

            element.src = ImgSrcDirective.getSrc(image, fallbackWidthPixels);
        }
    }
}
