import { IMedia } from '../model/media/IMedia';
import { Rendition } from '../model/media/Rendition';

/**
 * Normalize {@link IMedia.CDNPaths}.
 *
 * @param media - Image, video, or document to get the CDN Urls of.
 */
export default function getCdnPaths(
    media: IMedia
): { [rendition in Rendition]?: string } {
    var cdnPaths = media && media.CDNPaths;

    if (typeof cdnPaths === 'string') {
        let cdnPathsObject: { [rendition in Rendition]?: string } = {};

        cdnPaths.split('\n').forEach(renditionSource => {
            let colonIndex = renditionSource.indexOf(':'),
                rendition = renditionSource.substring(0, colonIndex) as Rendition,
                source = renditionSource.substring(colonIndex + 1);

            if (rendition && source) {
                (cdnPathsObject as any)[rendition] = source;
            }
        });

        cdnPaths = cdnPathsObject;
    }

    return cdnPaths || {};
}
