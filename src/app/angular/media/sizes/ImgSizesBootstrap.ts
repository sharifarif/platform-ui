import { Injectable } from '@angular/core';

import { BaseImgSizesCss } from './BaseImgSizesCss';
import { IImgSizesCss } from './model/IImgSizesCss';

/**
 * Approximate the `sizes` attribute of an `<img>` element using Bootstrap’s CSS.
 *
 * @see [Responsive Images](https://scadev.atlassian.net/wiki/spaces/TP/pages/106382518/Images#Images-ResponsiveImages)
 */
@Injectable()
export class ImgSizesBootstrap extends BaseImgSizesCss {
    private css: IImgSizesCss[];

    /**
     * Get variables used by Bootstrap.
     *
     * If you use different values for these variables,
     * override this function in your subclass.
     */
    protected getBootstrapVariables() {
        return {
            '$container-max-widths': {
                sm: 570,
                md: 750,
                lg: 990,
                xl: 1230
            } as { [breakpoint: string]: number },
            '$grid-breakpoints': {
                xs: 0,
                sm: 576,
                md: 768,
                lg: 1005,
                xl: 1255
            } as { [breakpoint: string]: number },
            '$grid-columns': 12,
            '$grid-gutter-width': 30,
            '$modal-lg': 800,
            '$modal-md': 500,
            '$modal-sm': 300
        };
    }

    /**
     * Get the `min-width`s in pixels that can be used in the `sizes` attribute.
     */
    protected getBreakpoints() {
        var breakpoints = this.getBootstrapVariables()['$grid-breakpoints'];

        return Object.values(breakpoints).filter(breakpoint => breakpoint > 0);
    }

    // tslint:disable:max-func-body-length

    /**
     * Get the CSS selectors that can affect the width of an image.
     */
    protected getCss() {
        var css = this.css;

        if (!css) {
            this.css = css = [];

            let leftRight = (value: number) => {
                    return {
                        left: value,
                        right: value
                    };
                },
                variables = this.getBootstrapVariables(),
                containerWidths = variables['$container-max-widths'],
                breakpoints = Object.keys(containerWidths),
                breakpointMinWidth = variables['$grid-breakpoints'],
                columns = variables['$grid-columns'],
                gutterWidth = variables['$grid-gutter-width'];

            css.push({
                class: ['container', 'container-fluid'],
                selector: [
                    {
                        property: {
                            padding: leftRight(gutterWidth / 2)
                        }
                    }
                ]
            });

            css.push({
                class: ['container'],
                selector: breakpoints.map(breakpoint => {
                    return {
                        media: {
                            minWidthPixels: breakpointMinWidth[breakpoint]
                        },
                        property: {
                            width: {
                                pixels: containerWidths[breakpoint]
                            }
                        }
                    };
                })
            });

            let addColumnWidth = (prefix: string, minWidthPixels?: number) => {
                for (var column = 1; column <= columns; column++) {
                    css.push({
                        class: [prefix + column],
                        selector: [
                            {
                                media: {
                                    minWidthPixels: minWidthPixels
                                },
                                property: {
                                    width: {
                                        percent: column * 100 / columns
                                    }
                                }
                            }
                        ]
                    });
                }
            };

            addColumnWidth('col-');
            breakpoints.forEach(breakpoint => {
                addColumnWidth('col-' + breakpoint + '-', breakpointMinWidth[breakpoint]);
            });

            css.push(
                {
                    class: ['modal-dialog'],
                    selector: [
                        {
                            media: {
                                minWidthPixels: breakpointMinWidth.sm
                            },
                            property: {
                                width: {
                                    pixels: variables['$modal-md']
                                }
                            }
                        }
                    ]
                },
                {
                    class: ['modal-sm'],
                    selector: [
                        {
                            media: {
                                minWidthPixels: breakpointMinWidth.sm
                            },
                            property: {
                                width: {
                                    pixels: variables['$modal-sm']
                                }
                            }
                        }
                    ]
                },
                {
                    class: ['modal-lg'],
                    selector: [
                        {
                            media: {
                                minWidthPixels: breakpointMinWidth.lg
                            },
                            property: {
                                width: {
                                    pixels: variables['$modal-lg']
                                }
                            }
                        }
                    ]
                }
            );
        }

        return css;
    }

    // tslint:enable
}
