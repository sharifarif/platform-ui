import { ElementRef, Injectable } from '@angular/core';
import isEqual from 'lodash/isEqual';
import sortBy from 'lodash/sortBy';

import { IImgSizesItem } from './model/IImgSizesItem';
import { IImgSizesMediaMinMax } from './model/IImgSizesMediaMinMax';
import { IImgSizesWidth } from './model/IImgSizesWidth';

/**
 * Calculate the `sizes` attribute of an `<img>` element.
 *
 * @see [Responsive Images](https://scadev.atlassian.net/wiki/spaces/TP/pages/106382518/Images#Images-ResponsiveImages)
 */
@Injectable()
export abstract class BaseImgSizes {
    /**
     * Determine whether a `media` condition will pass at the given `viewportWidthPixels`.
     *
     * @param media - Media condition.
     * @param viewportWidthPixels - Viewport width in pixels.
     */
    static isPassingMediaCondition(
        media: IImgSizesMediaMinMax,
        viewportWidthPixels: number
    ) {
        var max = media && media.maxWidthPixels,
            min = media && media.minWidthPixels;

        return (
            (typeof max !== 'number' || max >= viewportWidthPixels) &&
            (typeof min !== 'number' || min <= viewportWidthPixels)
        );
    }

    /**
     * Calculate the `sizes` attribute of a given `<img>` `element`.
     *
     * @param element - `<img>` element to calculate the `sizes` of.
     */
    getSizesString(element: ElementRef) {
        let sizesString: string,
            sizes = this.getSizes(element);
        if (sizes) {
            sizesString = sizes
                .map((size, index) => {
                    var sizeString = '',
                        media = size.media,
                        width = size.width,
                        pixels = Math.round(width.pixels * 100) / 100,
                        viewportWidthPercentage =
                            Math.round(width.viewportWidthPercentage * 10000) / 10000;
                    if (media && index < sizes.length - 1) {
                        let minWidthPixels = media.minWidthPixels;
                        if (minWidthPixels) {
                            sizeString +=
                                '(min-width:' + Math.floor(minWidthPixels) + 'px) ';
                        }
                    }
                    if (viewportWidthPercentage) {
                        sizeString += 'calc(' + viewportWidthPercentage + 'vw';
                    }
                    if (viewportWidthPercentage && pixels > 0) {
                        sizeString += '+';
                    }
                    if (pixels) {
                        sizeString += pixels + 'px';
                    }
                    if (viewportWidthPercentage) {
                        sizeString += ')';
                    }

                    return sizeString;
                })
                .join(', ');
        }

        return sizesString || undefined;
    }

    /**
     * Calculate the width in pixels of a given `<img>` `element`
     * at the given `viewportWidthPixels`.
     *
     * @param element - `<img>` element to calculate the width of.
     * @param viewportWidthPixels - Viewport width in pixels.
     */
    getWidthPixels(element: ElementRef, viewportWidthPixels: number) {
        var widthPixels: number;

        var width = this.getWidth(element, viewportWidthPixels);
        if (width) {
            let pixels = width.pixels || 0,
                viewportWidthPercentage = width.viewportWidthPercentage || 0;

            widthPixels =
                pixels + (viewportWidthPercentage / 100) * (viewportWidthPixels || 0);
        }

        return widthPixels;
    }

    /**
     * Get the `min-width`s in pixels that can be used in the `sizes` attribute.
     */
    protected abstract getBreakpoints(): number[];

    /**
     * Calculate the items in the `sizes` attribute of a given `<img>` `element`.
     *
     * Items are ordered from most specific to least specific media conditions,
     * since browsers will use the first item with a passing media condition.
     *
     * @param element - `<img>` element to calculate the `sizes` of.
     */
    protected getSizes(element: ElementRef) {
        var sizes: IImgSizesItem[] = [],
            breakpoints = sortBy(this.getBreakpoints()).reverse(),
            minBreakpoint = breakpoints[breakpoints.length - 1];

        var addBreakpoint = (minWidthPixels?: number) => {
            var width = this.getWidth(element, minWidthPixels || minBreakpoint / 2),
                size: IImgSizesItem = {
                    width: width
                };

            if (minWidthPixels) {
                size.media = {
                    minWidthPixels: minWidthPixels
                };
            }

            var sizesLength = sizes.length;
            if (sizesLength && isEqual(sizes[sizesLength - 1].width, width)) {
                sizes.splice(sizesLength - 1, 1);
            }

            sizes.push(size);
        };

        breakpoints.forEach(addBreakpoint);
        addBreakpoint();

        return sizes;
    }

    /**
     * Calculate the width of a given `<img>` `element`
     * at the given `viewportWidthPixels`.
     *
     * @param element - `<img>` element to calculate the width of.
     * @param viewportWidthPixels - Viewport width in pixels.
     */
    protected abstract getWidth(
        element: ElementRef,
        viewportWidthPixels: number
    ): IImgSizesWidth;
}
