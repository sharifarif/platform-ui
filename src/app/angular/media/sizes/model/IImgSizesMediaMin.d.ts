/**
 * Media condition with a `min-width` in pixels.
 */
export interface IImgSizesMediaMin {
    /**
     * Pixels used in `(min-width: ...px)`.
     */
    minWidthPixels?: number;
}
