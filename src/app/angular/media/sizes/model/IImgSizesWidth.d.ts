/**
 * Width of an image.
 *
 * If both `pixels` and `viewportWidthPercentage` are provided,
 * the two are added together.
 */
export interface IImgSizesWidth {
    /**
     * Pixels used in `...px`.
     */
    pixels?: number;

    /**
     * Percentage of the viewport width used in `calc(...vw)`.
     */
    viewportWidthPercentage?: number;
}
