import { IImgSizesMediaMin } from './IImgSizesMediaMin';

/**
 * Media condition with a `min-width` and `max-width` in pixels.
 */
export interface IImgSizesMediaMinMax extends IImgSizesMediaMin {
    /**
     * Pixels used in `(max-width: ...px)`.
     */
    maxWidthPixels?: number;
}
