import { IImgSizesMediaMin } from './IImgSizesMediaMin';
import { IImgSizesWidth } from './IImgSizesWidth';

/**
 * An item in an `<img> sizes` attribute.
 *
 * @see https://developer.mozilla.org/en-US/docs/Web/HTML/Element/img#attr-sizes
 */
export interface IImgSizesItem {
    /**
     * Media condition.
     *
     * Must be omitted for the last item.
     */
    media?: IImgSizesMediaMin;

    /**
     * Width of the image.
     *
     * If both `pixels` and `viewportWidthPercentage` are provided,
     * the two are added together.
     */
    width: IImgSizesWidth;
}
