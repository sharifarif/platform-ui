import { IImgSizesCssProperty } from './IImgSizesCssProperty';
import { IImgSizesMediaMinMax } from './IImgSizesMediaMinMax';

/**
 * CSS that can affect the width of an image.
 */
export interface IImgSizesCss {
    /**
     * CSS class without the dot.
     */
    class: string[];

    /**
     * CSS selector.
     */
    selector: {
        /**
         * Media condition.
         */
        media?: IImgSizesMediaMinMax;

        /**
         * CSS property.
         */
        property: IImgSizesCssProperty;
    }[];
}
