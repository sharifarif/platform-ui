/**
 * CSS property that can affect the width of an image.
 */
export interface IImgSizesCssProperty {
    /**
     * CSS `margin` property.
     */
    margin?: {
        /**
         * Left margin in pixels.
         */
        left?: number;

        /**
         * Right margin in pixels.
         */
        right?: number;
    };

    /**
     * CSS `padding` property.
     */
    padding?: {
        /**
         * Left padding in pixels.
         */
        left?: number;

        /**
         * Right padding in pixels.
         */
        right?: number;
    };

    /**
     * CSS `width` or `max-width` property.
     */
    width?: {
        /**
         * `width` or `max-width` as a percentage of its parent’s width.
         *
         * For example, `width: ...%;` or `max-width: ...%;`.
         */
        percent?: number;

        /**
         * `width` or `max-width` in pixels.
         *
         * For example, `width: ...px;` or `max-width: ...px;`.
         */
        pixels?: number;

        /**
         * `width` or `max-width` as a percentage of the viewport width.
         *
         * For example, `width: calc(...vw);` or `max-width: calc(...vw);`.
         */
        viewportWidthPercentage?: number;
    };
}
