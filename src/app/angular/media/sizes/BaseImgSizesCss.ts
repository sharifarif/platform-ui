import { ElementRef, Injectable, RendererFactory2 } from '@angular/core';
import intersection from 'lodash/intersection';
import merge from 'lodash/merge';

import { BaseImgSizes } from './BaseImgSizes';
import { IImgSizesCss } from './model/IImgSizesCss';
import { IImgSizesCssProperty } from './model/IImgSizesCssProperty';
import { IImgSizesItem } from './model/IImgSizesItem';
import { IImgSizesWidth } from './model/IImgSizesWidth';

/**
 * Approximate the `sizes` attribute of an `<img>` element using the page’s CSS.
 *
 * @see [Responsive Images](https://scadev.atlassian.net/wiki/spaces/TP/pages/106382518/Images#Images-ResponsiveImages)
 */
@Injectable()
export abstract class BaseImgSizesCss extends BaseImgSizes {
    constructor(protected rendererFactory: RendererFactory2) {
        super();
    }

    protected getBodyCss(viewportWidthPixels: number): IImgSizesCssProperty {
        return {
            width: {
                viewportWidthPercentage: 100
            }
        };
    }

    /**
     * Get the CSS selectors that can affect the width of an image.
     */
    protected abstract getCss(): IImgSizesCss[];

    /**
     * Get the CSS for a given `element`.
     *
     * @param element - Element to get the CSS for.
     * @param viewportWidthPixels - Viewport width in pixels.
     */
    protected getCssForElement(element: HTMLElement, viewportWidthPixels: number) {
        var css: IImgSizesCssProperty = {},
            classList = element.classList;

        this.getCss().forEach(cssClass => {
            if (intersection(classList, cssClass.class).length > 0) {
                cssClass.selector.forEach(selector => {
                    if (
                        BaseImgSizes.isPassingMediaCondition(
                            selector.media,
                            viewportWidthPixels
                        )
                    ) {
                        let property = selector.property,
                            width = property.width;

                        // The last declaration of any width should win.
                        if (
                            width &&
                            (width.percent ||
                                width.pixels ||
                                width.viewportWidthPercentage)
                        ) {
                            css.width = undefined;
                        }

                        merge(css, property);
                    }
                });
            }
        });

        return css;
    }

    /**
     * Get the CSS for each element:
     * * From the closest parent of the given `element` with a
     *   `width.pixels` or `width.viewportWidthPercentage`.
     * * To the given `element`.
     *
     * @param element - `<img>` element to get the CSS hierarchy for.
     * @param viewportWidthPixels - Viewport width in pixels.
     */
    protected getCssHierarchy(element: HTMLElement, viewportWidthPixels: number) {
        var cssHierarchy: IImgSizesCssProperty[] = [],
            baseWidth = false,
            renderer = this.rendererFactory.createRenderer(element, null);

        var addNode = (node: HTMLElement) => {
            if (node && node.tagName) {
                let css = this.getCssForElement(node, viewportWidthPixels);

                cssHierarchy.unshift(css);

                let width = css.width;
                if (width && (width.pixels || width.viewportWidthPercentage)) {
                    baseWidth = true;
                } else {
                    addNode(renderer.parentNode(node));
                }
            }
        };

        addNode(element);

        if (!baseWidth && element) {
            cssHierarchy.unshift(this.getBodyCss(viewportWidthPixels));
        }

        return cssHierarchy;
    }

    /**
     * Calculate the width of an `<img>` `element` at the given `viewportWidthPixels`.
     *
     * @param element - `<img>` element to calculate the width of.
     * @param viewportWidthPixels - Viewport width in pixels.
     */
    protected getWidth(element: ElementRef, viewportWidthPixels: number): IImgSizesWidth {
        var pixels: number, viewportWidthPercentage: number;

        if (element) {
            let cssHierarchy = this.getCssHierarchy(
                    element.nativeElement,
                    viewportWidthPixels
                ),
                baseWidthCss = cssHierarchy[0],
                baseWidth = baseWidthCss.width,
                imageCss = cssHierarchy[cssHierarchy.length - 1];

            pixels = (baseWidth && baseWidth.pixels) || 0;
            viewportWidthPercentage =
                (baseWidth && baseWidth.viewportWidthPercentage) || 0;

            cssHierarchy.forEach(css => {
                var margin = css.margin,
                    padding = css.padding,
                    width = css.width,
                    widthPercent = width && width.percent;

                if (margin && css !== baseWidthCss && !widthPercent) {
                    pixels -= (margin.left || 0) + (margin.right || 0);
                }
                if (widthPercent && css !== baseWidthCss) {
                    pixels *= widthPercent / 100;
                    viewportWidthPercentage *= widthPercent / 100;
                }
                if (padding && css !== imageCss) {
                    pixels -= (padding.left || 0) + (padding.right || 0);
                }
            });
        }

        return {
            pixels: pixels,
            viewportWidthPercentage: viewportWidthPercentage
        };
    }
}
