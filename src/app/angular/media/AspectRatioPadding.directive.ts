import {
    AfterViewInit,
    Directive,
    ElementRef,
    Input,
    OnChanges,
    Renderer2
} from '@angular/core';

import { IMedia } from '../model/media/IMedia';

/**
 * Automatically set the height on a media element before it loads.
 *
 * **Example:**
 *
 * ```html
 * <div [platformAspectRatioPadding]="image" class="embed-responsive">
 *     <img [platformImgSrc]="image" class="embed-responsive-item">
 * <div>
 * ```
 *
 * @see [Set Height Before Load](https://scadev.atlassian.net/wiki/spaces/TP/pages/106382518/Images#Images-SetHeightBeforeLoad)
 */
@Directive({
    selector: '[platformAspectRatioPadding]'
})
export class AspectRatioPaddingDirective implements AfterViewInit, OnChanges {
    /**
     * Aspect ratio to use when the width or height is missing
     * and {@link platformAspectRatioFallback} is `true`.
     */
    static readonly FallbackAspectRatio = { width: 16, height: 9 };

    /**
     * Aspect ratio to use when the width or height is missing.
     *
     * If `true`, {@link FallbackAspectRatio} is used.
     */
    @Input() platformAspectRatioFallback?: boolean | { width: number; height: number };

    /**
     * Media being displayed.
     */
    @Input() platformAspectRatioPadding: IMedia;

    constructor(private elementRef: ElementRef, private renderer2: Renderer2) {}

    /**
     * Calculate the value of the `padding-bottom` style.
     */
    calculatePaddingBottom() {
        var paddingBottom: string, width: number, height: number;

        var media = this.platformAspectRatioPadding;
        if (media) {
            width = media.Width;
            height = media.Height;
        }

        if (!width || !height) {
            let fallback = this.platformAspectRatioFallback;
            if (fallback === true) {
                fallback = AspectRatioPaddingDirective.FallbackAspectRatio;
            }
            if (fallback) {
                width = fallback.width;
                height = fallback.height;
            }
        }

        if (width && height) {
            paddingBottom = Math.round(height * 100000 / width) / 1000 + '%';
        }

        return paddingBottom;
    }

    ngAfterViewInit() {
        this.render();
    }

    ngOnChanges() {
        this.render();
    }

    /**
     * Update the `padding-bottom` style on the media element.
     */
    protected render() {
        var nativeElement = this.elementRef.nativeElement,
            paddingBottom = this.calculatePaddingBottom(),
            renderer2 = this.renderer2;

        if (paddingBottom) {
            renderer2.setStyle(nativeElement, 'padding-bottom', paddingBottom);
        } else {
            renderer2.removeStyle(nativeElement, 'padding-bottom');
        }
    }
}
