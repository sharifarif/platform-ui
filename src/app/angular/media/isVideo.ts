import getCdnPaths from './getCdnPaths';
import { IMedia } from '../model/media/IMedia';

/**
 * Determine whether an {@link IMedia} has a video URL.
 *
 * @param media - Media to check.
 */
export default function isVideo(media: IMedia) {
    var cdnPaths = getCdnPaths(media);

    return !!(
        cdnPaths['H264 Broadband 720p'] || cdnPaths.H264_4500kbps_AAC_und_ch2_128kbps
    );
}
