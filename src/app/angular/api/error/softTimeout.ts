import { concat, tap } from 'rxjs/operators';
import { empty, Observable } from 'rxjs';

/**
 *
 *
 * @param observable
 * @param timeout
 * @param onTimeout
 * @param onNextErrorComplete
 */
export default function softTimeout<T>(
    observable: Observable<T>,
    timeout: number,
    onTimeout: () => any,
    onNextErrorComplete?: () => any
) {
    var timeoutId: any;

    function removeTimeout() {
        if (timeoutId) {
            clearTimeout(timeoutId);
            timeoutId = undefined;
        }
    }

    function nextErrorComplete() {
        removeTimeout();

        if (onNextErrorComplete) {
            onNextErrorComplete();
        }
    }

    return empty().pipe(
        tap(null, null, () => {
            removeTimeout();

            timeoutId = setTimeout(() => {
                if (onTimeout) {
                    onTimeout();
                }
            }, timeout);
        }),
        concat(observable),
        tap(nextErrorComplete, nextErrorComplete, nextErrorComplete)
    ) as Observable<T>;
}
