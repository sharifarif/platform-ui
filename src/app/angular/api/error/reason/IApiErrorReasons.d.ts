import { ApiErrorReason } from './ApiErrorReason';

/**
 * Reasons for common API errors that the user will understand.
 */
export interface IApiErrorReasons {
    /**
     * Reason shown when a more specific reason does not exist.
     */
    Generic?: ApiErrorReason;

    /**
     * Reason shown when the user’s login has expired.
     */
    LoginExpired?: ApiErrorReason;

    /**
     * Reason shown when the user is disconnected from the Internet.
     */
    Offline?: ApiErrorReason;

    /**
     * Reasons shown for common `problem`s described by the back end.
     *
     * **Example:**
     *
     * Response Body with a 400+
     * [HTTP Response Code](https://en.wikipedia.org/wiki/List_of_HTTP_status_codes):
     * ```json
     * {
     *     "problem": "DatabaseFull",
     *     ...
     * }
     * ```
     *
     * {@link IApiErrorReasons}:
     * ```json
     * {
     *     "ProblemCode": {
     *         "DatabaseFull": "The database is full.  Delete something, and try again."
     *     },
     *     ...
     * }
     * ```
     */
    ProblemCode: {
        /**
         * Reason shown for a `problem` described by the back end.
         */
        [problem: string]: ApiErrorReason;
    };

    /**
     * Reasons shown for common HTTP Response Codes.
     *
     * @see https://en.wikipedia.org/wiki/List_of_HTTP_status_codes
     */
    ResponseCode: {
        /**
         * Reason shown for an HTTP Response Code.
         *
         * @see https://en.wikipedia.org/wiki/List_of_HTTP_status_codes
         */
        [code: number]: ApiErrorReason;
    };

    /**
     * Reason shown when an API is taking more than 4 seconds to respond.
     *
     * The reason will disappear when the API finally responds.
     */
    SoftTimeout?: ApiErrorReason;
}
