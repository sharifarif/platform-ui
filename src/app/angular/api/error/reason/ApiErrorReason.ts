/**
 * The reason for an API error.
 *
 * This will be shown to the user, so it should be incredibly easy to understand.
 */
export type ApiErrorReason = string | { Read?: string; Write?: string };

/**
 * Get a reason for an API error that the user will understand.
 *
 * @param reason - The reason for an API error.
 * @param write - `true` if the error occurs while the user is changing data.
 */
export function getReason(reason: ApiErrorReason, write: boolean) {
    if (typeof reason === 'object') {
        reason = reason[write ? 'Write' : 'Read'];
    }

    return reason;
}
