import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Observable, throwError as _throw } from 'rxjs';
import property from 'lodash/property';
import { RequestMethod, RequestOptions, Response } from '@angular/http';
import { TranslateService } from '@ngx-translate/core';

import { AlertService } from '../../layout/alert/Alert.service';
import { AlertType } from '../../layout/alert/AlertType';
import { getReason } from './reason/ApiErrorReason';
import { IAlert } from '../../layout/alert/IAlert';
import { IApiErrorReasons } from './reason/IApiErrorReasons';
import { HttpResponseCode } from './HttpResponseCode';

/**
 * Tell the user when an error occurs.
 *
 * To change the behavior of {@link ApiErrorHandler}:
 * * Create a class that `extends` {@link ApiErrorHandler}.
 * * Provide that class in a subclass of {@link BaseApi}.
 */
@Injectable()
export class ApiErrorHandler {
    /**
     * Determine whether a request is intended to change data.
     *
     * @param requestOptions - Request to check.
     */
    static isWriteRequest(requestOptions: RequestOptions) {
        var method = new RequestOptions(requestOptions).method || RequestMethod.Get;

        return (
            method !== RequestMethod.Get &&
            method !== RequestMethod.Options &&
            method !== RequestMethod.Head
        );
    }

    constructor(
        private alertService: AlertService,
        private translate: TranslateService
    ) {}

    /**
     * Tell the user that an error occurred.
     *
     * @param error - The error that occurred.
     * @param requestOptions - Options used when making the request.
     */
    handleError(error: any, requestOptions: RequestOptions) {
        this.getErrorAlert(error, requestOptions).subscribe(alert => {
            this.alertService.setAlert(alert);
        });

        return _throw(error);
    }

    /**
     * Tell the user the API is taking a long time.
     *
     * @param requestOptions - Options used when making the request.
     * @returns `Function` that removes the message.
     */
    handleSoftTimeout(requestOptions: RequestOptions) {
        var alert: IAlert;

        var alertSubscription = this.getErrorAlert(
            'SoftTimeout',
            requestOptions
        ).subscribe(timeoutAlert => {
            alert = timeoutAlert;
            this.alertService.addAlert(timeoutAlert);
        });

        return () => {
            if (alertSubscription) {
                alertSubscription.unsubscribe();
                alertSubscription = undefined;
            }
            if (alert) {
                this.alertService.remove(alert);
                alert = undefined;
            }
        };
    }

    /**
     * Get an error {@link IAlert} that tells the user the reason for an API error.
     *
     * @param error - The error that occurred.
     * @param requestOptions - Options used when making the request.
     */
    protected getErrorAlert(error: any, requestOptions: RequestOptions) {
        return this.translate.get('ApiError').pipe(
            map((messages: IApiErrorReasons) => {
                var icon: string,
                    icons = this.getErrorIcons(),
                    message: string,
                    write = ApiErrorHandler.isWriteRequest(requestOptions);

                function setReason(...reason: (string | number)[]) {
                    var getter = property(reason);

                    message = getReason(getter(messages), write);
                    icon = getReason(getter(icons), write);
                }

                if (this.isOffline()) {
                    setReason('Offline');
                } else if (error === 'SoftTimeout') {
                    setReason('SoftTimeout');
                } else if (error === 'Token renewal operation failed due to timeout') {
                    setReason('LoginExpired');
                } else if (error && error.name === 'TimeoutError') {
                    setReason('ResponseCode', HttpResponseCode.GatewayTimeout);
                } else {
                    setReason('ProblemCode', this.getProblemCode(error));
                }
                if (!message) {
                    setReason('ResponseCode', error && error.status);
                }
                if (!message) {
                    setReason('Generic');
                }

                return {
                    message: message,
                    type: write ? AlertType.error : AlertType.warning,
                    iconClass: icon
                } as IAlert;
            })
        );
    }

    /**
     * Get icons that describe common API errors.
     *
     * If you use a different icon set, override this function in your subclass.
     */
    protected getErrorIcons(): IApiErrorReasons {
        return {
            LoginExpired: 'fa fa-id-badge',
            Offline: 'fa fa-wifi',
            ProblemCode: {},
            ResponseCode: {
                0: 'fa fa-microphone-slash',
                401: 'fa fa-id-badge',
                403: 'fa fa-id-badge',
                404: 'fa fa-search',
                431: 'fa fa-balance-scale',
                451: 'fa fa-legal',
                500: 'fa fa-bug',
                501: 'fa fa-bicycle',
                502: 'fa fa-rocket',
                503: 'fa fa-clock-o',
                504: 'fa fa-hourglass-end',
                508: 'fa fa-circle-o-notch'
            },
            SoftTimeout: 'fa fa-spin fa-spinner'
        };
    }

    /**
     * Get a `problem` or `error` code from a `Response`.
     *
     * **Example:**
     *
     * In the following `Response` body, the `problem` code is `'DatabaseFull'`:
     * ```json
     * {
     *     "problem": "DatabaseFull"
     * }
     * ```
     *
     * @param response - `Response` to check for a `problem` or `error` code.
     */
    protected getProblemCode(response: Response | any) {
        var problemCode: string;

        if (response instanceof Response) {
            try {
                let json = response.json();
                if (json) {
                    problemCode = json.problem || json.error;
                }
            } catch (e) {
                // Do nothing.
            }
        }

        return problemCode;
    }

    /**
     * Determine whether the user is disconnected from the Internet.
     */
    protected isOffline() {
        return typeof window !== 'undefined' && window.navigator.onLine === false;
    }
}
