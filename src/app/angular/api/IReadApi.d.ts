import { Observable } from 'rxjs/Observable';

/**
 * The contract that should be implemented by every API that reads data.
 *
 * The server should not return any data the current user does not have permission to see.
 *
 * @param T - Type of the item or items being read.
 * @param ID - Type of the item’s unique ID.
 */
export interface IReadApi<T, ID> {
    /**
     * Get all of the items the user has access to.
     */
    getAll(): Observable<T[]>;

    /**
     * Get an item the user has access to.
     *
     * @param id - ID of the item.
     */
    getById(id: ID): Observable<T>;

    /**
     * Get all of the inactive items the user has access to.
     *
     * This function is not be implemented by every {@link IReadApi}.
     */
    getInactive?(): Observable<T[]>;
}
