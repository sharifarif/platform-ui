import { Headers, Http, RequestOptions } from '@angular/http';
import { Inject, Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

import { ApiErrorHandler } from './error/ApiErrorHandler';
import { AuthService } from '../login/adal/Auth.service';
import { BaseApi } from './BaseApi';
import { ENVIRONMENT } from '../environment/ENVIRONMENT';
import { IApiEnvironment } from './IApiEnvironment';

/**
 * Base class for an API that requires the user to be logged in to Azure Active Directory.
 */
@Injectable()
export abstract class BaseAuthorizedApi<E extends IApiEnvironment> extends BaseApi<E> {
    constructor(
        apiErrorHandler: ApiErrorHandler,
        private authService: AuthService,
        @Inject(ENVIRONMENT) environment: E,
        http: Http
    ) {
        super(apiErrorHandler, environment, http);
    }

    /**
     * Update the `RequestOptions` before a request is made.
     *
     * @param options - Original `RequestOptions`.
     */
    protected buildOptionsAsync(options: RequestOptions) {
        var tokenObservable = this.authService.getToken();

        return tokenObservable.pipe(
            map(token => {
                if (token) {
                    let headers = new Headers(options ? options.headers : undefined);

                    headers.append('Authorization', 'Bearer ' + token);

                    options = new RequestOptions().merge(options).merge({
                        headers: headers
                    });
                }

                return options;
            })
        );
    }
}
