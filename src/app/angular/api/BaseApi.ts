import { catchError, map, mergeMap } from 'rxjs/operators';
import {
    Http,
    RequestMethod,
    RequestOptions,
    RequestOptionsArgs,
    Response
} from '@angular/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiErrorHandler } from './error/ApiErrorHandler';
import { ENVIRONMENT } from '../environment/ENVIRONMENT';
import { IApiEnvironment } from './IApiEnvironment';
import softTimeout from './error/softTimeout';

/**
 * Base class for an API.
 *
 * To add an API, create a class that:
 * * `extends` {@link BaseApi}.
 * * `implements` either {@link ICrudApi} or {@link IReadApi}.
 *
 * If a suitable base class extending {@link BaseApi} already exists, extend that instead.
 */
@Injectable() // FIXME: Move from @angular/http to @angular/common/http.
export abstract class BaseApi<E extends IApiEnvironment> {
    /**
     * Milliseconds until a message is shown saying the API is taking a long time.
     */
    static readonly SoftTimeout = 4000;

    constructor(
        private apiErrorHandler: ApiErrorHandler,
        @Inject(ENVIRONMENT) protected environment: E,
        private http: Http
    ) {}

    /**
     * Get the base URL of the API.
     *
     * Implement this function in your subclass.
     */
    protected abstract getBaseUrl(): string;

    /**
     * Perform a request with the `get` HTTP method.
     *
     * @param url - URL to `get`, relative to `getBaseUrl()`.
     * @param options - Request options.
     */
    protected get(url: string, options?: RequestOptionsArgs): Observable<Response> {
        return this.request(
            url,
            new RequestOptions({
                method: RequestMethod.Get
            }).merge(options)
        );
    }

    /**
     * Perform a request with the `get` HTTP method, and deserialize the JSON returned.
     *
     * @param url - URL to `get`, relative to `getBaseUrl()`.
     * @param options - Request options.
     */
    protected getJson<T>(url: string, options?: RequestOptionsArgs): Observable<T> {
        return this.requestJson(
            url,
            new RequestOptions({
                method: RequestMethod.Get
            }).merge(options)
        );
    }

    /**
     * Perform a request with the `post` HTTP method.
     *
     * @param url - URL to `post` to, relative to `getBaseUrl()`.
     * @param body - Data to send in the request.
     * @param options - Request options.
     */
    protected post(
        url: string,
        body: any,
        options?: RequestOptionsArgs
    ): Observable<Response> {
        return this.request(
            url,
            new RequestOptions({
                method: RequestMethod.Post,
                body: body
            }).merge(options)
        );
    }

    /**
     * Perform a request with the `post` HTTP method, and deserialize the JSON returned.
     *
     * @param url - URL to `post` to, relative to `getBaseUrl()`.
     * @param body - Data to send in the request.
     * @param options - Request options.
     */
    protected postJson<T>(
        url: string,
        body: any,
        options?: RequestOptionsArgs
    ): Observable<T> {
        return this.requestJson(
            url,
            new RequestOptions({
                method: RequestMethod.Post,
                body: body
            }).merge(options)
        );
    }

    /**
     * Perform a request with the `put` HTTP method.
     *
     * @param url - URL to `put` to, relative to `getBaseUrl()`.
     * @param body - Data to send in the request.
     * @param options - Request options.
     */
    protected put(
        url: string,
        body: any,
        options?: RequestOptionsArgs
    ): Observable<Response> {
        return this.request(
            url,
            new RequestOptions({
                method: RequestMethod.Put,
                body: body
            }).merge(options)
        );
    }

    /**
     * Perform a request with the `put` HTTP method, and deserialize the JSON returned.
     *
     * @param url - URL to `put` to, relative to `getBaseUrl()`.
     * @param body - Data to send in the request.
     * @param options - Request options.
     */
    protected putJson<T>(
        url: string,
        body: any,
        options?: RequestOptionsArgs
    ): Observable<T> {
        return this.requestJson(
            url,
            new RequestOptions({
                method: RequestMethod.Put,
                body: body
            }).merge(options)
        );
    }

    /**
     * Perform a request with the `delete` HTTP method.
     *
     * @param url - URL for the `delete` request, relative to `getBaseUrl()`.
     * @param options - Request options.
     */
    protected delete(url: string, options?: RequestOptionsArgs): Observable<Response> {
        return this.request(
            url,
            new RequestOptions({
                method: RequestMethod.Delete
            }).merge(options)
        );
    }

    /**
     * Perform a request with the `delete` HTTP method, and deserialize the JSON returned.
     *
     * @param url - URL for the `delete` request, relative to `getBaseUrl()`.
     * @param options - Request options.
     */
    protected deleteJson<T>(url: string, options?: RequestOptionsArgs): Observable<T> {
        return this.requestJson(
            url,
            new RequestOptions({
                method: RequestMethod.Delete
            }).merge(options)
        );
    }

    /**
     * Perform a request with the `patch` HTTP method.
     *
     * @param url - URL to `patch` to, relative to `getBaseUrl()`.
     * @param body - Data to send in the request.
     * @param options - Request options.
     */
    protected patch(
        url: string,
        body: any,
        options?: RequestOptionsArgs
    ): Observable<Response> {
        return this.request(
            url,
            new RequestOptions({
                method: RequestMethod.Patch,
                body: body
            }).merge(options)
        );
    }

    /**
     * Perform a request with the `patch` HTTP method, and deserialize the JSON returned.
     *
     * @param url - URL to `patch` to, relative to `getBaseUrl()`.
     * @param body - Data to send in the request.
     * @param options - Request options.
     */
    protected patchJson<T>(
        url: string,
        body: any,
        options?: RequestOptionsArgs
    ): Observable<T> {
        return this.requestJson(
            url,
            new RequestOptions({
                method: RequestMethod.Patch,
                body: body
            }).merge(options)
        );
    }

    /**
     * Perform a request with the `head` HTTP method.
     *
     * @param url - URL for the `head` request, relative to `getBaseUrl()`.
     * @param options - Request options.
     */
    protected head(url: string, options?: RequestOptionsArgs): Observable<Response> {
        return this.request(
            url,
            new RequestOptions({
                method: RequestMethod.Head
            }).merge(options)
        );
    }

    /**
     * Perform a request with the `options` HTTP method.
     *
     * @param url - URL for the `options` request, relative to `getBaseUrl()`.
     * @param options - Request options.
     */
    protected options(url: string, options?: RequestOptionsArgs): Observable<Response> {
        return this.request(
            url,
            new RequestOptions({
                method: RequestMethod.Options
            }).merge(options)
        );
    }

    /**
     * Perform a request with the `options` HTTP method,
     * and deserialize the JSON returned.
     *
     * @param url - URL for the `options` request, relative to `getBaseUrl()`.
     * @param options - Request options.
     */
    protected optionsJson<T>(url: string, options?: RequestOptionsArgs): Observable<T> {
        return this.requestJson(
            url,
            new RequestOptions({
                method: RequestMethod.Options
            }).merge(options)
        );
    }

    /**
     * Update the `RequestOptions` before a request is made.
     *
     * Implement this in a subclass if you need to supply special headers
     * (e.g. for authentication).
     *
     * @param options - Original `RequestOptions`.
     */
    protected buildOptionsAsync(
        options: RequestOptions
    ): Observable<RequestOptions> | void {
        // A subclass may implement this method.
    }

    /**
     * Get a `url` relative to `getBaseUrl()`.
     *
     * @param url - URL relative to `getBaseUrl()`.
     */
    protected buildUrl(url: string) {
        if (!url) {
            url = '';
        }

        var baseUrl = this.getBaseUrl();
        if (baseUrl) {
            url = baseUrl + url;
        }

        return url;
    }

    /**
     * Perform a request.
     *
     * @param url - URL for the request, relative to `getBaseUrl()`.
     * @param options - Request options.
     */
    protected request(url: string, options: RequestOptions): Observable<Response> {
        return this.requestImpl(url, options).pipe(
            catchError(error => this.apiErrorHandler.handleError(error, options))
        );
    }

    /**
     * Perform a request, and deserialize the JSON returned.
     *
     * @param url - URL for the request, relative to `getBaseUrl()`.
     * @param options - Request options.
     */
    protected requestJson<T>(url: string, options: RequestOptions): Observable<T> {
        return this.requestImpl(url, options).pipe(
            map(response => {
                var data = response.json();

                if (data && data.result) {
                    data = data.result;
                }

                return data;
            }),
            catchError(error => this.apiErrorHandler.handleError(error, options))
        );
    }

    /**
     * Underlying implementation for {@link request} and {@link requestJson}.
     *
     * This should not be used directly, as it may change in the future.
     *
     * @param url - URL for the request, relative to `getBaseUrl()`.
     * @param options - Request options.
     */
    protected requestImpl(url: string, options: RequestOptions): Observable<Response> {
        var request: Observable<Response>, removeTimeoutMessage: () => void;

        var optionsAsync = this.buildOptionsAsync(options);
        if (optionsAsync) {
            request = optionsAsync.pipe(
                mergeMap(returnedOptions => {
                    return this.http.request(this.buildUrl(url), returnedOptions);
                })
            );
        } else {
            request = this.http.request(this.buildUrl(url), options);
        }

        return softTimeout(
            request,
            BaseApi.SoftTimeout,
            () => {
                removeTimeoutMessage = this.apiErrorHandler.handleSoftTimeout(options);
            },
            () => {
                if (removeTimeoutMessage) {
                    removeTimeoutMessage();
                }
            }
        );
    }
}
