import { map, shareReplay } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { makeStateKey, TransferState } from '@angular/platform-browser';
import { Observable, of } from 'rxjs';

/**
 * Cache API Requests.
 */
@Injectable()
export class ApiCacheService {
    private observables: { [key: string]: Observable<any> } = {};

    constructor(private transferState: TransferState) {}

    /**
     * Get a value from the cache.
     *
     * If the value is not cached, `getFirstTime()` will be used to load the data.
     *
     * @param key - Unique key for an API request.
     *              This should include any data sent to the server.
     * @param getFirstTime - Function called the first time the data is needed.
     */
    get<T>(key: string, getFirstTime: () => Observable<T>) {
        var observables = this.observables,
            observable: Observable<T> = observables[key];

        if (!observable) {
            let stateKey = makeStateKey(key),
                transferState = this.transferState,
                values: T[] = transferState.get(stateKey, undefined) as any;

            if (values) {
                observable = of(values[0]);
            } else {
                observable = getFirstTime().pipe(
                    map(value => {
                        transferState.onSerialize(stateKey, () => [value]);

                        return value;
                    }),
                    shareReplay()
                );
            }

            observables[key] = observable;
        }

        return observable;
    }
}
