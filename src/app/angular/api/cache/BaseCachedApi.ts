import {
    Http,
    RequestMethod,
    RequestOptions,
    Response,
    ResponseOptions
} from '@angular/http';
import { Inject, Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { ApiCacheService } from './ApiCache.service';
import { ApiErrorHandler } from '../error/ApiErrorHandler';
import { BaseApi } from '../BaseApi';
import { ENVIRONMENT } from '../../environment/ENVIRONMENT';
import { IApiEnvironment } from '../IApiEnvironment';

/**
 * Base class for an API that caches GET requests:
 * * In the user’s browser for the lifespan of a Single-Page Application.
 * * In the HTML as serialized JSON when using Server-Side Rendering.
 */
@Injectable()
export abstract class BaseCachedApi<E extends IApiEnvironment> extends BaseApi<E> {
    constructor(
        private apiCacheService: ApiCacheService,
        apiErrorHandler: ApiErrorHandler,
        @Inject(ENVIRONMENT) environment: E,
        http: Http
    ) {
        super(apiErrorHandler, environment, http);
    }

    /**
     * Get the name of the current TypeScript/JavaScript class.
     *
     * Implement this function in your subclass by providing the name of your subclass.
     */
    protected abstract getClassName(): string;

    /**
     * Get a unique key for an API request.
     *
     * This includes the GET parameters sent to the server.
     *
     * The API’s class name is used instead of the base URL,
     * because users and server-side rendering may use different endpoints.
     *
     * @param url - URL for the request, relative to `getBaseUrl()`.
     * @param options - Request options.
     */
    protected getCacheKey(url: string, options: RequestOptions): string {
        var cacheKey = this.getClassName() + ':' + url,
            params = options && (options.params || options.search),
            paramsString = params && params.toString();

        if (paramsString) {
            cacheKey += '?' + paramsString;
        }

        return cacheKey;
    }

    /**
     * Perform a request.
     *
     * @param url - URL for the request, relative to `getBaseUrl()`.
     * @param options - Request options.
     */
    protected request(url: string, options: RequestOptions): Observable<Response> {
        return this.requestCached(url, options, () => {
            return super.request(url, options);
        }).pipe(
            map(response => {
                var deserializedResponse = new Response(new ResponseOptions(response));

                Object.assign(deserializedResponse, response);

                return deserializedResponse;
            })
        );
    }

    /**
     * Perform a request, and deserialize the JSON returned.
     *
     * @param url - URL for the request, relative to `getBaseUrl()`.
     * @param options - Request options.
     */
    protected requestJson<T>(url: string, options: RequestOptions): Observable<T> {
        return this.requestCached(url, options, () => {
            return super.requestJson(url, options);
        });
    }

    /**
     * Perform a request.
     *
     * If {@link shouldCacheRequest} returns `true`, the response will be cached.
     *
     * If the response is already cached, the cached copy is returned.
     *
     * @param url
     * @param options
     * @param doRequest
     */
    protected requestCached<T>(
        url: string,
        options: RequestOptions,
        doRequest: () => Observable<T>
    ) {
        var observable: Observable<T>;

        if (this.shouldCacheRequest(url, options)) {
            let cacheKey = this.getCacheKey(url, options);

            observable = this.apiCacheService.get(cacheKey, () => {
                return doRequest();
            });
        } else {
            observable = doRequest();
        }

        return observable;
    }

    /**
     * Determine whether a request should be cached.
     *
     * By default, requests should be cached if they are a GET request.
     *
     * Implement this in a subclass if you need to restrict or expand caching.
     *
     * @param url - URL for the request, relative to `getBaseUrl()`.
     * @param options - Request options.
     */
    protected shouldCacheRequest(url: string, options: RequestOptions): boolean {
        var method = (options && options.method) || RequestMethod.Get;

        return method === RequestMethod.Get;
    }
}
