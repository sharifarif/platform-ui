import { Observable } from 'rxjs/Observable';

import { IReadApi } from './IReadApi';

/**
 * The contract that should be implemented by every API that
 * Creates, Reads, Updates, and Deletes (CRUD) data.
 *
 * The server should **not**:
 * * Return any data the current user does not have permission to see.
 * * Allow the current user to edit any data they do not have permission to edit.
 *
 * @param T - Type of the item or items being read.
 * @param ID - Type of the item’s unique ID.
 */
export interface ICrudApi<T, ID> extends IReadApi<T, ID> {
    /**
     * Add an item.
     *
     * @param item - Item to add.
     */
    add(item: T): Observable<T>;

    /**
     * Update an item.
     *
     * @param item - Item to update.
     */
    update(item: T): Observable<T>;

    /**
     * Activate an item that was previously inactivated.
     *
     * This function is not be implemented by every {@link ICrudApi}.
     *
     * @param item - Item to activate.
     */
    activate?(item: T): Observable<any>;

    /**
     * Inactivate an item.
     *
     * Inactive items should not be displayed or used in logic.
     *
     * @param item - Item to inactivate.
     */
    inactivate(item: T): Observable<any>;
}
