/**
 * To use {@link BaseApi}, add the base URL of the API to `environment.api`,
 * as shown below:
 *
 * ```typescript
 * environment = {
 *     api: {
 *         myApi: 'https://...'
 *     }
 * };
 * ```
 */
export interface IApiEnvironment {
    /**
     * APIs used by the application.
     * * Each key is a unique name for an API.
     * * Each value is the base URL of an API.
     */
    api: {
        [apiName: string]:
            | string
            | {
                  [apiName: string]:
                      | string
                      | {
                            [apiName: string]:
                                | string
                                | {
                                      [apiName: string]: string;
                                  };
                        };
              };
    };
}
