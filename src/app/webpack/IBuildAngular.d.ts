/**
 * Options for building Angular.
 */
export interface IBuildAngular {
    /**
     * Path to the file that bootstraps Angular
     * (e.g. by calling `platformBrowserDynamic().bootstrapModule()`),
     * relative to {@link IBuildDirectorySrc.script}.
     */
    applicationMain: string;

    /**
     * Application module.
     */
    applicationModule: {
        /**
         * Path to the application module,
         * relative to {@link IBuildDirectorySrc.script}.
         */
        path: string;

        /**
         * Class name of the application module.
         */
        className: string;
    };

    /**
     * Files to exclude from Ahead-of-Time compilation.
     *
     * Defaults to files in {@link IBuildDirectorySrc.test}.
     */
    exclude?: string | string[];

    /**
     * `true` if building the server portion of
     * [Angular Universal](https://universal.angular.io/).
     */
    server?: boolean;

    /**
     * Path to `tsconfig.json`, relative to {@link IBuildDirectory.project}.
     *
     * Defaults to `"tsconfig.json"`,
     * or `"tsconfig.server.json"` if {@link server} is `true`.
     */
    tsConfigPath?: string;
}
