/**
 * Entry points for scripts and stylesheets.
 *
 * @see https://webpack.js.org/concepts/entry-points/#scenarios
 */
export interface IBuildEntry {
    /**
     * Entry points for scripts.
     *
     * Each key represents the output file in {@link IBuildDirectoryDist.script}.
     *
     * Each value represents the input file or files in {@link IBuildDirectorySrc.script}.
     */
    script?: {
        [distScriptSubpath: string]: string | string[];
    };

    /**
     * Entry points for stylesheets.
     *
     * Each key represents the output file in {@link IBuildDirectoryDist.style}.
     *
     * Each value represents the input file or files in {@link IBuildDirectorySrc.style}.
     */
    style?: {
        [distStyleSubpath: string]: string | string[];
    };

    /**
     * Entry points for tests.
     *
     * Each key represents the output file in {@link IBuildDirectoryDist.test}.
     *
     * Each value represents the input file or files in {@link IBuildDirectorySrc.test}.
     */
    test?: {
        [distStyleSubpath: string]: string | string[];
    };
}
