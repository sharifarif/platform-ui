"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const path = require("path");
/**
 * Resolve one or more child paths relative to a parent path.
 */
function resolvePath(parent, child) {
    var resolved;
    if (child) {
        if (!parent) {
            resolved = child;
        }
        else if (typeof child === 'string') {
            resolved = path.resolve(parent, child);
        }
        else if (Array.isArray(child)) {
            resolved = child.map(childItem => {
                return resolvePath(parent, childItem);
            });
        }
        else {
            resolved = {};
            Object.entries(child).forEach(([key, childItem]) => {
                resolved[key] = resolvePath(parent, childItem);
            });
        }
    }
    return resolved;
}
exports.default = resolvePath;
