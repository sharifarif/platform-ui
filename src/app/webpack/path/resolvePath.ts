import * as path from 'path';

/**
 * Resolve one or more child paths relative to a parent path.
 */
export default function resolvePath<
    T extends string | string[] | { [key: string]: string | string[] }
>(parent: string, child: T): T {
    var resolved: any;

    if (child) {
        if (!parent) {
            resolved = child;
        } else if (typeof child === 'string') {
            resolved = path.resolve(parent, child);
        } else if (Array.isArray(child)) {
            resolved = child.map(childItem => {
                return resolvePath(parent, childItem);
            });
        } else {
            resolved = {};

            Object.entries(child).forEach(([key, childItem]) => {
                resolved[key] = resolvePath(parent, childItem as string | string[]);
            });
        }
    }

    return resolved as T;
}
