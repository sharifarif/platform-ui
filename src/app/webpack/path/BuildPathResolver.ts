import { IBuildOptions } from '../IBuildOptions';
import resolvePath from './resolvePath';

/**
 * Resolve paths in {@link IBuildOptions}.
 */
export default class BuildPathResolver {
    constructor(private options: IBuildOptions) {}

    /**
     * Path to the file that bootstraps Angular
     * (e.g. by calling `platformBrowserDynamic().bootstrapModule()`).
     */
    get angularApplicationMain() {
        return resolvePath(this.srcScript, this.options.angular.applicationMain);
    }

    /**
     * Path to the application module.
     */
    get angularApplicationModulePath() {
        return resolvePath(this.srcScript, this.options.angular.applicationModule.path);
    }

    /**
     * Files to exclude from Ahead-of-Time compilation.
     *
     * Defaults to files in {@link IBuildDirectorySrc.test}.
     */
    get angularExclude() {
        return this.options.angular.exclude || resolvePath(this.srcTest, '**/*');
    }

    /**
     * Path to `tsconfig.json`.
     *
     * Defaults to `"tsconfig.json"`,
     * or `"tsconfig.server.json"` if {@link server} is `true`.
     */
    get angularTsConfig() {
        var angular = this.options.angular,
            tsConfigPath =
                angular.tsConfigPath ||
                (angular.server ? 'tsconfig.server.json' : 'tsconfig.json');

        return resolvePath(this.project, tsConfigPath);
    }

    /**
     * Files or folders to copy, without any processing (e.g. minification).
     *
     * * Each key represents the output file or folder.
     * * Each value represents the input file or folder.
     */
    get copy() {
        return resolvePath(this.src, this.options.copy);
    }

    /**
     * Base directory for output files.
     */
    get distAsset() {
        return resolvePath(this.project, this.options.directory.dist.asset);
    }

    /**
     * Directory for output fonts.
     */
    get distFont() {
        return resolvePath(this.distAsset, this.options.directory.dist.font);
    }

    /**
     * Directory for output images.
     */
    get distImage() {
        return resolvePath(this.distAsset, this.options.directory.dist.image);
    }

    /**
     * Directory for output scripts.
     */
    get distScript() {
        return resolvePath(this.distAsset, this.options.directory.dist.script);
    }

    /**
     * Directory for output stylesheets.
     */
    get distStyle() {
        return resolvePath(this.distAsset, this.options.directory.dist.style);
    }

    /**
     * Directory for output tests.
     */
    get distTest() {
        return resolvePath(this.distAsset, this.options.directory.dist.test);
    }

    /**
     * Entry points for scripts.
     *
     * Each key represents the output file in {@link IBuildDirectoryDist.script}.
     *
     * Each value represents the input file or files.
     */
    get entryScript() {
        return resolvePath(this.srcScript, this.options.entry.script);
    }

    /**
     * Entry points for stylesheets.
     *
     * Each key represents the output file in {@link IBuildDirectoryDist.style}.
     *
     * Each value represents the input file or files.
     */
    get entryStyle() {
        return resolvePath(this.srcStyle, this.options.entry.style);
    }

    /**
     * Entry points for tests.
     *
     * Each key represents the output file in {@link IBuildDirectoryDist.test}.
     *
     * Each value represents the input file or files.
     */
    get entryTest() {
        return resolvePath(this.srcTest, this.options.entry.test);
    }

    /**
     * Base directory for the project.
     */
    get project() {
        return this.options.directory.project;
    }

    /**
     * Base directory for input files.
     */
    get src() {
        return resolvePath(this.project, this.options.directory.src.src);
    }

    /**
     * Directory or directories containing input fonts.
     *
     * If an object is provided:
     * * Each key represents the output folder.
     * * Each value represents the input folder.
     */
    get srcFont() {
        return resolvePath(this.src, this.options.directory.src.font);
    }

    /**
     * Directory or directories containing input images.
     *
     * If an object is provided:
     * * Each key represents the output folder.
     * * Each value represents the input folder.
     */
    get srcImage() {
        return resolvePath(this.src, this.options.directory.src.image);
    }

    /**
     * Directory containing input scripts.
     */
    get srcScript() {
        return resolvePath(this.src, this.options.directory.src.script);
    }

    /**
     * Directory containing input stylesheets.
     */
    get srcStyle() {
        return resolvePath(this.src, this.options.directory.src.style);
    }

    /**
     * Directory containing input tests.
     */
    get srcTest() {
        return resolvePath(this.src, this.options.directory.src.test);
    }
}
