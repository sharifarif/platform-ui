import { IBuildAngular } from './IBuildAngular';
import { IBuildDirectory } from './IBuildDirectory';
import { IBuildEntry } from './IBuildEntry';

/**
 * Options for building your project.
 */
export interface IBuildOptions {
    /**
     * Directories where files are located.
     */
    directory: IBuildDirectory;

    /**
     * Entry points for scripts and stylesheets.
     *
     * @see https://webpack.js.org/concepts/entry-points/#scenarios
     */
    entry: IBuildEntry;

    /**
     * `true` if files should be minified.
     */
    production: boolean;

    /**
     * Options for building Angular.
     */
    angular?: IBuildAngular;

    /**
     * `true` if babel transpiler plugin should be used.
     */
    babel: boolean;


    /**
     * Files or folders to copy, without any processing (e.g. minification).
     *
     * * Each key represents the output file or folder, relative to
     *   {@link IBuildDirectoryDist.asset}.
     * * Each value represents the input file or folder, relative to
     *   {@link IBuildDirectorySrc.src}.
     */
    copy?: {
        [distAssetSubpath: string]: string;
    };
}
