/**
 * Directories where input files are located.
 */
export interface IBuildDirectorySrc {
    /**
     * Base directory for input files, relative to {@link IBuildDirectory.project}.
     */
    src: string;

    /**
     * Directory or directories containing input fonts, relative to {@link src}.
     *
     * If an object is provided:
     * * Each key represents the output folder, relative to
     *   {@link IBuildDirectoryDist.font}.
     * * Each value represents the input folder, relative to {@link src}.
     */
    font?:
        | string
        | {
              [distFontSubdirectory: string]: string;
          };

    /**
     * Directory or directories containing input images, relative to {@link src}.
     *
     * If an object is provided:
     * * Each key represents the output folder, relative to
     *   {@link IBuildDirectoryDist.image}.
     * * Each value represents the input folder, relative to {@link src}.
     */
    image?:
        | string
        | {
              [distImageSubdirectory: string]: string;
          };

    /**
     * Directory containing input scripts, relative to {@link src}.
     */
    script?: string;

    /**
     * Directory containing input stylesheets, relative to {@link src}.
     */
    style?: string;

    /**
     * Directory containing input tests, relative to {@link src}.
     */
    test?: string;
}
