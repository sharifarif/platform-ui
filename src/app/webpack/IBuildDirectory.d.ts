import { IBuildDirectoryDist } from './IBuildDirectoryDist';
import { IBuildDirectorySrc } from './IBuildDirectorySrc';

/**
 * Directories where files are located.
 */
export interface IBuildDirectory {
    /**
     * Base directory for the project.
     */
    project: string;

    /**
     * Directories where input files are located.
     */
    src: IBuildDirectorySrc;

    /**
     * Directories where output files will be located.
     */
    dist: IBuildDirectoryDist;
}
