import { CheckerPlugin } from 'awesome-typescript-loader';
import * as webpack from 'webpack';

import WebpackFactory from './WebpackFactory';

/**
 * Factory used by {@link BuildWebpack.getConfigTest}.
 *
 * Avoid calling this class directly.
 */
export default class WebpackFactoryTest extends WebpackFactory {
    protected getEntry() {
        return this.getPath().entryTest;
    }

    protected getOutputDirectory() {
        return this.getPath().distTest;
    }

    protected getPlugins() {
        var options = this.options,
            production = options.production;

        var plugins: webpack.Plugin[] = [
            this.getPluginBanner(),
            this.getPluginNotifier(),
            new CheckerPlugin(),
            ...this.plugins
        ];

        if (production) {
            plugins.push(this.getPluginUglifyJs());
        }

        return plugins;
    }

    protected getRules() {
        var options = this.options,
            production = options.production;

        var rules: webpack.Rule[] = [
            {
                test: /\.tsx?$/,
                loader: 'awesome-typescript-loader'
            }
        ];

        if (production) {
            rules.unshift(this.getLoaderTsLint());
        }

        return rules;
    }
}
