import { AngularCompilerPlugin } from '@ngtools/webpack';
import { CheckerPlugin } from 'awesome-typescript-loader';
import * as webpack from 'webpack';

import WebpackFactory from './WebpackFactory';

/**
 * Factory used by {@link BuildWebpack.getConfigScript}.
 *
 * Avoid calling this class directly.
 */
export default class WebpackFactoryScript extends WebpackFactory {
    protected getEntry() {
        return this.getPath().entryScript;
    }

    protected getOutputDirectory() {
        return this.getPath().distScript;
    }

    protected getPlugins() {
        const options = this.options,
            angular = options.angular,
            path = this.getPath(),
            production = options.production;

        var plugins: webpack.Plugin[] = [
            this.getPluginClean([path.distScript]),
            this.getPluginNotifier(),
            ...this.plugins
        ];

        if (angular) {
            plugins.push(this.getPluginAngular());
        } else {
            plugins.push(new CheckerPlugin());
        }
        if (production) {
            plugins.push(this.getPluginUglifyJs());
            plugins.push(this.getContextReplacementPlugin());
            let ModuleConcatenation = webpack.optimize.ModuleConcatenationPlugin;
            if (ModuleConcatenation) {
                plugins.push(new ModuleConcatenation());
            }
        }

        return plugins;
    }

    protected getRules() {
        var options = this.options,
            angular = options.angular,
            production = options.production,
            babel = options.babel;

        var rules: webpack.Rule[] = [
            {
                test: angular ? /(?:\.ngfactory\.js|\.ngstyle\.js|\.ts)$/ : /\.tsx?$/,
                loader: angular ? '@ngtools/webpack' : 'awesome-typescript-loader'
            },
            {
                test: /vendor\/.+\.(jsx|js)$/,
                loader: 'imports?jQuery=jquery,$=jquery,this=>window'
            },
            this.getLoaderHtml()
        ];

        if (production) {
            rules.unshift(this.getLoaderTsLint());
        }

        if (babel) {
            rules.push(this.getLoaderBabel());
        }

        return rules;
    }

    protected getTarget() {
        var angular = this.options.angular,
            angularServer = angular && angular.server;

        return angularServer ? 'node' : 'web';
    }

    private getLoaderBabel() {
        return {
            test: /\.m?(jsx|js)$/,
            exclude: /(node_modules|bower_components)\/(?!(dom7|swiper)\/).*/,
            use: {
                loader: 'babel-loader',
                options: {
                    presets: ['@babel/preset-env']
                }
            }
        };

    }

    private getLoaderHtml() {
        var loader: webpack.LoaderRule,
            options = this.options,
            angular = options.angular,
            angularServer = angular && angular.server,
            production = options.production;

        if (production || angularServer) {
            loader = {
                test: /\.html$/,
                loader: 'html-loader',
                options: {
                    minimize: true,
                    removeAttributeQuotes: false,
                    caseSensitive: true,
                    customAttrSurround: [
                        [/#/, /(?:)/],
                        [/\*/, /(?:)/],
                        [/\[?\(?/, /(?:)/]
                    ],
                    customAttrAssign: [/\)?\]?=/]
                }
            };
        } else {
            loader = {
                test: /\.html$/,
                loader: 'raw-loader'
            };
        }

        return loader;
    }

    private getPluginAngular() {
        var options = this.options,
            angular = options.angular,
            path = this.getPath(),
            production = options.production;

        return new AngularCompilerPlugin({
            entryModule:
                path.angularApplicationModulePath +
                '#' +
                angular.applicationModule.className,
            mainPath: path.angularApplicationMain,
            tsConfigPath: path.angularTsConfig,
            skipCodeGeneration: !production
        });
    }
}
