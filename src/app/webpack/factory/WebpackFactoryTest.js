'use strict';
Object.defineProperty(exports, '__esModule', { value: true });
const awesome_typescript_loader_1 = require('awesome-typescript-loader');
const WebpackFactory_1 = require('./WebpackFactory');
/**
 * Factory used by {@link BuildWebpack.getConfigTest}.
 *
 * Avoid calling this class directly.
 */
class WebpackFactoryTest extends WebpackFactory_1.default {
    getEntry() {
        return this.getPath().entryTest;
    }
    getOutputDirectory() {
        return this.getPath().distTest;
    }
    getPublicPath() {
        return this.getPath().distPublicPathTest;
    }
    getPlugins() {
        var options = this.options,
            production = options.production;
        var plugins = [
            this.getPluginBanner(),
            this.getPluginNotifier(),
            new awesome_typescript_loader_1.CheckerPlugin(),
            ...this.plugins
        ];
        if (production) {
            plugins.push(this.getPluginUglifyJs());
        }
        return plugins;
    }
    getRules() {
        var options = this.options,
            production = options.production;
        var rules = [
            {
                test: /\.tsx?$/,
                loader: 'awesome-typescript-loader'
            }
        ];
        if (production) {
            rules.unshift(this.getLoaderTsLint());
        }
        return rules;
    }
    getOptimization() {
        return this.options.optimization ? this.options.optimization.test : {};
    }
}
exports.default = WebpackFactoryTest;
