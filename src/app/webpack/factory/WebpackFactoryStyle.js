'use strict';
Object.defineProperty(exports, '__esModule', { value: true });
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const fs = require('fs');
const glob = require('glob');
const path = require('path');
const resolvePath_1 = require('../path/resolvePath');
const WebpackFactory_1 = require('./WebpackFactory');
/**
 * Factory used by {@link BuildWebpack.getConfigStyle}.
 *
 * Avoid calling this class directly.
 */
class WebpackFactoryStyle extends WebpackFactory_1.default {
    constructor() {
        super(...arguments);
        this.extractTextPlugin = new ExtractTextPlugin({
            filename: '[name].css'
        });
    }
    getDevtool() {
        return this.options.production ? 'cheap-source-map' : 'cheap-eval-source-map';
    }
    getEntry() {
        return this.getPath().entryStyle;
    }
    getExtensions() {
        return ['.scss', '.css'];
    }
    getOutputDirectory() {
        return this.getPath().distStyle;
    }
    getOutputFilename() {
        return '[name].css';
    }
    getOutputFilesExpected() {
        var filesExpected = super.getOutputFilesExpected();
        filesExpected = filesExpected.concat(this.getCleanCopyPaths().filesExpected);
        return filesExpected;
    }
    getPublicPath() {
        return this.getPath().distPublicPathCss;
    }
    getPlugins() {
        var cleanCopyPaths = this.getCleanCopyPaths(),
            options = this.options,
            production = options.production;
        var plugins = [
            this.getPluginClean(cleanCopyPaths.clean),
            this.getPluginBanner(),
            this.getPluginNotifier(),
            this.extractTextPlugin,
            ...this.plugins
        ];
        if (production) {
            plugins.unshift(this.getPluginStyleLint());
        }
        if (cleanCopyPaths.copy.length) {
            plugins.push(this.getPluginCopy(cleanCopyPaths.copy));
        }
        return plugins;
    }
    getRules() {
        var options = this.options,
            production = options.production;
        var rules = [
            {
                test: /\.s?css$/,
                use: this.getLoaderExtractText()
            }
        ];
        return rules;
    }
    pathOptions() {
        var options = this.options,
            cleanPaths = [this.getPath().distStyle],
            copyPaths = [],
            filesExpected = [];
        return {
            options: options,
            cleanPaths: cleanPaths,
            copyPaths: copyPaths,
            filesExpected: filesExpected
        };
    }
    getCleanCopyPaths() {
        const options = this.pathOptions();
        function copy(from, to) {
            if (from) {
                if (typeof from === 'string') {
                    options.copyPaths.push({
                        from: from,
                        to: to
                    });
                    if (fs.lstatSync(from).isDirectory()) {
                        options.cleanPaths.push(to);
                        glob.sync(from + '/**/*').forEach(file => {
                            file = path.relative(from, file);
                            options.filesExpected.push(resolvePath_1.default(to, file));
                        });
                    } else {
                        options.filesExpected.push(to);
                    }
                } else {
                    Object.entries(from).forEach(([toSubdirectory, fromItem]) => {
                        copy(fromItem, resolvePath_1.default(to, toSubdirectory));
                    });
                }
            }
        }
        copy(this.getPath().copy, this.getPath().distAsset);
        copy(this.getPath().srcFont, this.getPath().distFont);
        copy(this.getPath().srcImage, this.getPath().distImage);
        return {
            clean: options.cleanPaths,
            copy: options.copyPaths,
            filesExpected: options.filesExpected
        };
    }
    /**
     * Extract CSS into a file.
     *
     * See {@link https://github.com/webpack-contrib/extract-text-webpack-plugin}
     */
    getLoaderExtractText() {
        return this.extractTextPlugin.extract({
            use: [
                {
                    loader: 'css-loader',
                    options: {
                        sourceMap: true,
                        url: false
                    }
                },
                {
                    loader: 'postcss-loader',
                    options: {
                        sourceMap: true,
                        plugins: [require('autoprefixer')()],
                        ctx: {
                            autoprefixer: {
                                browsers: ['last 4 version', 'ie 9', 'ie 10']
                            }
                        }
                    }
                },
                {
                    loader: 'sass-loader',
                    options: {
                        sourceMap: true
                    }
                }
            ]
        });
    }
    getPluginCopy(patterns) {
        const CopyWebpackPlugin = require('copy-webpack-plugin');
        return new CopyWebpackPlugin(patterns);
    }
    getPluginStyleLint() {
        const StyleLintPlugin = require('stylelint-webpack-plugin');
        return new StyleLintPlugin({
            context: this.getPath().srcStyle
        });
    }
    getOptimization() {
        return this.options.optimization ? this.options.optimization.style : {};
    }
}
exports.default = WebpackFactoryStyle;
