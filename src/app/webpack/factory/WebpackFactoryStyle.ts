import * as ExtractTextPlugin from 'extract-text-webpack-plugin';
import * as fs from 'fs';
import * as glob from 'glob';
import * as path from 'path';
import * as webpack from 'webpack';

import resolvePath from '../path/resolvePath';
import WebpackFactory from './WebpackFactory';

/**
 * Factory used by {@link BuildWebpack.getConfigStyle}.
 *
 * Avoid calling this class directly.
 */
export default class WebpackFactoryStyle extends WebpackFactory {
    private extractTextPlugin = new ExtractTextPlugin({
        filename: '[name].css'
    });

    protected getDevtool() {
        return this.options.production ? 'cheap-source-map' : 'cheap-eval-source-map';
    }

    protected getEntry() {
        return this.getPath().entryStyle;
    }

    protected getExtensions() {
        return ['.scss', '.css'];
    }

    protected getOutputDirectory() {
        return this.getPath().distStyle;
    }

    protected getOutputFilename() {
        return '[name].css';
    }

    protected getOutputFilesExpected(): string[] {
        var filesExpected = super.getOutputFilesExpected();

        filesExpected = filesExpected.concat(this.getCleanCopyPaths().filesExpected);

        return filesExpected;
    }

    protected getPlugins() {
        var cleanCopyPaths = this.getCleanCopyPaths(),
            options = this.options,
            production = options.production;

        var plugins: webpack.Plugin[] = [
            this.getPluginClean(cleanCopyPaths.clean),
            this.getPluginBanner(),
            this.getPluginNotifier(),
            this.extractTextPlugin,
            ...this.plugins
        ];

        if (production) {
            plugins.unshift(this.getPluginStyleLint());
        }
        if (cleanCopyPaths.copy.length) {
            plugins.push(this.getPluginCopy(cleanCopyPaths.copy));
        }

        return plugins;
    }

    protected getRules() {
        var options = this.options,
            production = options.production;

        var rules: webpack.Rule[] = [
            {
                test: /\.s?css$/,
                use: this.getLoaderExtractText()
            }
        ];

        return rules;
    }

    private pathOptions() {
        var options = this.options,
        cleanPaths = [this.getPath().distStyle],
        copyPaths: { from: string; to: string }[] = [],
        filesExpected: string[] = [];

        return {
            options: options,
            cleanPaths: cleanPaths,
            copyPaths: copyPaths,
            filesExpected: filesExpected,
        }
    }

    private getCleanCopyPaths() {
        const options = this.pathOptions();

        function copy(from: string | { [toSubdirectory: string]: string }, to: string) {
            if (from) {
                if (typeof from === 'string') {
                    options.copyPaths.push({
                        from: from,
                        to: to
                    });

                    if (fs.lstatSync(from).isDirectory()) {
                        options.cleanPaths.push(to);
                        glob.sync(from + '/**/*').forEach(file => {
                            file = path.relative(from, file);
                            options.filesExpected.push(resolvePath(to, file));
                        });
                    } else {
                        options.filesExpected.push(to);
                    }
                } else {
                    Object.entries(from).forEach(([toSubdirectory, fromItem]) => {
                        copy(fromItem, resolvePath(to, toSubdirectory));
                    });
                }
            }
        }

        copy(this.getPath().copy, this.getPath().distAsset);
        copy(this.getPath().srcFont, this.getPath().distFont);
        copy(this.getPath().srcImage, this.getPath().distImage);

        return {
            clean: options.cleanPaths,
            copy: options.copyPaths,
            filesExpected: options.filesExpected
        };
    }

    /**
     * Extract CSS into a file.
     *
     * See {@link https://github.com/webpack-contrib/extract-text-webpack-plugin}
     */
    private getLoaderExtractText() {
        return this.extractTextPlugin.extract({
            use: [
                {
                    loader: 'css-loader',
                    options: {
                        sourceMap: true,
                        url: false
                    }
                },
                {
                    loader: 'postcss-loader',
                    options: {
                        sourceMap: true,
                        plugins: [require('autoprefixer')()],
                        ctx: {
                            autoprefixer: {
                                browsers: ['last 4 version', 'ie 9', 'ie 10']
                            }
                        }
                    }
                },
                {
                    loader: 'sass-loader',
                    options: {
                        sourceMap: true
                    }
                }
            ]
        });
    }

    private getPluginCopy(patterns: { from: string; to: string }[]) {
        const CopyWebpackPlugin: any = require('copy-webpack-plugin');

        return new CopyWebpackPlugin(patterns);
    }

    private getPluginStyleLint() {
        const StyleLintPlugin: any = require('stylelint-webpack-plugin');

        return new StyleLintPlugin({
            context: this.getPath().srcStyle
        });
    }
}
