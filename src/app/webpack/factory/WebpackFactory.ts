import * as path from 'path';
import * as webpack from 'webpack';

import BuildPathResolver from '../path/BuildPathResolver';
import { IBuildOptions } from '../IBuildOptions';
import resolvePath from '../path/resolvePath';

/**
 * Factory used by {@link BuildWebpack}.
 *
 * Avoid calling this class directly.
 */
export default abstract class WebpackFactory {
    /**
     * Options for building your project.
     */
    options: IBuildOptions;
    plugins: webpack.Plugin[];

    constructor(
        options: IBuildOptions,
        plugins?: webpack.Plugin[]
    ) {
        this.options = options;
        this.plugins = plugins ? plugins : [];
    }

    /**
     * Get a Webpack [Configuration](https://webpack.js.org/configuration/) object.
     */
    getConfig(): webpack.Configuration {
        return {
            devtool: this.getDevtool(),
            entry: this.getEntry(),
            module: {
                rules: this.getRules()
            },
            output: {
                path: this.getOutputDirectory(),
                filename: this.getOutputFilename()
            },
            plugins: this.getPlugins(),
            resolve: {
                extensions: this.getExtensions()
            },
            target: this.getTarget(),
            watchOptions: {
                ignored: /node_modules/
            }
        };
    }

    /**
     * This option controls if and how source maps are generated.
     *
     * See {@link https://webpack.js.org/configuration/devtool/}
     */
    protected getDevtool(): webpack.Options.Devtool {
        return this.options.production ? '#source-map' : 'eval';
    }

    /**
     * The point or points to enter the application.
     *
     * See {@link https://webpack.js.org/configuration/entry-context/#entry}
     */
    protected abstract getEntry(): string | string[] | webpack.Entry;

    /**
     * Automatically resolve certain extensions.
     *
     * See {@link https://webpack.js.org/configuration/resolve/#resolve-extensions}
     */
    protected getExtensions(): string[] {
        return ['.ts', '.tsx', '.js', '.jsx'];
    }

    /**
     * Load TSLint.
     *
     * See {@link https://github.com/wbuchwalter/tslint-loader}
     */
    protected getLoaderTsLint(): webpack.LoaderRule {
        return {
            test: /\.[jt]sx?$/i,
            enforce: 'pre',
            exclude: /(build|node_modules)[\/\\]/,
            loader: 'tslint-loader',
            options: {
                emitErrors: !!this.options.production
            }
        };
    }

    /**
     * The output directory as an absolute path.
     *
     * See {@link https://webpack.js.org/configuration/output/#output-path}
     */
    protected abstract getOutputDirectory(): string;

    /**
     * This option determines the name of each output bundle.
     *
     * See {@link https://webpack.js.org/configuration/output/#output-filename}
     */
    protected getOutputFilename(): string {
        return '[name].js';
    }

    /**
     * Get a list of output files that should be generated.
     */
    protected getOutputFilesExpected(): string[] {
        let filesExpected: string[] = [],
            filenamePattern = this.getOutputFilename();

        Object.keys(this.getEntry()).forEach(filename => {
            filename = filenamePattern.replace(/\[name\]/g, filename);

            filesExpected.push(filename);
            filesExpected.push(filename + '.map');
        });

        return resolvePath(this.getOutputDirectory(), filesExpected);
    }

    /**
     * Resolve paths in {@link IBuildOptions}.
     */
    protected getPath() {
        return new BuildPathResolver(this.options);
    }

    /**
     * Adds a banner to the top of each generated chunk.
     *
     * See {@link https://webpack.js.org/plugins/banner-plugin/}
     */
    protected getPluginBanner() {
        return new webpack.BannerPlugin({
            banner: 'This is a generated file.\nDo not modify this file directly.'
        });
    }

    /**
     * Remove your build folders before building.
     *
     * See {@link https://github.com/johnagan/clean-webpack-plugin}
     *
     * @param paths - Absolute paths to remove.
     */
    protected getPluginClean(paths: string[]) {
        const CleanWebpackPlugin: any = require('clean-webpack-plugin');

        let exclude: string[] = [];

        if (!this.options.production) {
            this.getOutputFilesExpected().forEach(file => {
                paths.forEach(directory => {
                    exclude.push(path.relative(directory, file));
                });
            });
        }

        return new CleanWebpackPlugin(paths, {
            exclude: exclude,
            root: this.getPath().project
        });
    }

    /**
     * Display build status system notifications to the user.
     *
     * See {@link https://github.com/Turbo87/webpack-notifier}
     */
    protected getPluginNotifier() {
        const WebpackNotifierPlugin: any = require('webpack-notifier');

        return new WebpackNotifierPlugin({
            excludeWarnings: true,
            skipFirstNotification: true
        });
    }

    /**
     * The plugins option is used to customize the Webpack build process.
     *
     * See {@link https://webpack.js.org/configuration/plugins/}
     */
    protected abstract getPlugins(): webpack.Plugin[];

    /**
     * Compress JavaScript.
     *
     * See {@link https://github.com/webpack-contrib/uglifyjs-webpack-plugin}
     */
    protected getPluginUglifyJs() {
        const UglifyJSPlugin: any = require('uglifyjs-webpack-plugin');

        return new UglifyJSPlugin({
            comments: /^!/,
            compress: {
                warnings: false
            },
            mangle: true,
            sourceMap: true
        });
    }
    /*
     *  Plugin to remove angular related compilation warnings.
     */
    protected getContextReplacementPlugin() {
        return new webpack.ContextReplacementPlugin(
            // The (\\|\/) piece accounts for path separators in *nix and Windows
            /angular(\\|\/)core(\\|\/)@angular/,
            path.join(__dirname, './src'), // location of your src
            {}
        );
    }

    /**
     * Rules apply loaders to a module, or modify the parser.
     *
     * See {@link https://webpack.js.org/configuration/module/#module-rules}
     */
    protected abstract getRules(): webpack.Rule[];

    /**
     * Instructs Webpack to target a specific environment.
     *
     * See {@link https://webpack.js.org/configuration/target/}
     */
    protected getTarget():
        | 'web'
        | 'webworker'
        | 'node'
        | 'async-node'
        | 'node-webkit'
        | 'atom'
        | 'electron'
        | 'electron-renderer'
        | 'electron-main'
        | ((compiler?: any) => void) {
        return 'web';
    }
}
