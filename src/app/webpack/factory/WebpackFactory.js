'use strict';
Object.defineProperty(exports, '__esModule', { value: true });
const path = require('path');
const webpack = require('webpack');
const BuildPathResolver_1 = require('../path/BuildPathResolver');
const resolvePath_1 = require('../path/resolvePath');
/**
 * Factory used by {@link BuildWebpack}.
 *
 * Avoid calling this class directly.
 */
class WebpackFactory {
    constructor(options, plugins) {
        this.options = options;
        this.plugins = plugins ? plugins : [];
    }
    /**
     * Get a Webpack [Configuration](https://webpack.js.org/configuration/) object.
     */
    getConfig() {
        return {
            mode: this.getMode(),
            devtool: this.getDevtool(),
            entry: this.getEntry(),
            module: {
                rules: this.getRules()
            },
            optimization: this.getOptimization(),
            output: {
                path: this.getOutputDirectory(),
                filename: this.getOutputFilename(),
                chunkFilename: this.getChunkFileName(),
                publicPath: this.getPublicPath()
            },
            plugins: this.getPlugins(),
            resolve: {
                extensions: this.getExtensions()
            },
            target: this.getTarget(),
            watchOptions: {
                ignored: /node_modules/
            }
        };
    }
    /**
     * This option controls if and how source maps are generated.
     *
     * See {@link https://webpack.js.org/configuration/devtool/}
     */

    getDevtool() {
        return this.options.production ? '#source-map' : 'eval';
    }

    /**
     * Chosen mode tells webpack to use its built-in optimizations accordingly
     *
     * See {@link https://webpack.js.org/configuration}
     */

    getMode() {
        return this.options.production ? 'production' : 'development';
    }

    /**
     * Automatically resolve certain extensions.
     *
     * See {@link https://webpack.js.org/configuration/resolve/#resolve-extensions}
     */
    getExtensions() {
        return ['.ts', '.tsx', '.js', '.jsx'];
    }
    /**
     * Load TSLint.
     *
     * See {@link https://github.com/wbuchwalter/tslint-loader}
     */
    getLoaderTsLint() {
        return {
            test: /\.[jt]sx?$/i,
            enforce: 'pre',
            exclude: /(build|node_modules)[\/\\]/,
            loader: 'tslint-loader',
            options: {
                emitErrors: !!this.options.production
            }
        };
    }
    /**
     * This option determines the name of each output bundle.
     *
     * See {@link https://webpack.js.org/configuration/output/#output-filename}
     */
    getOutputFilename() {
        return '[name].js';
    }

    /**
     * This option determines the name of chunk file.
     *
     * See {@link https://webpack.js.org/configuration/output/#outputchunkfilename}
     */
    getChunkFileName() {
        return '[id].js' + '?v=[hash:5]';
    }
    /**
     * Get a list of output files that should be generated.
     */
    getOutputFilesExpected() {
        let filesExpected = [],
            filenamePattern = this.getOutputFilename();
        Object.keys(this.getEntry()).forEach(filename => {
            filename = filenamePattern.replace(/\[name\]/g, filename);
            filesExpected.push(filename);
            filesExpected.push(filename + '.map');
        });
        return resolvePath_1.default(this.getOutputDirectory(), filesExpected);
    }
    /**
     * Resolve paths in {@link IBuildOptions}.
     */
    getPath() {
        return new BuildPathResolver_1.default(this.options);
    }
    /**
     * Adds a banner to the top of each generated chunk.
     *
     * See {@link https://webpack.js.org/plugins/banner-plugin/}
     */
    getPluginBanner() {
        return new webpack.BannerPlugin({
            banner: 'This is a generated file.\nDo not modify this file directly.'
        });
    }
    /**
     * Remove your build folders before building.
     *
     * See {@link https://github.com/johnagan/clean-webpack-plugin}
     *
     * @param paths - Absolute paths to remove.
     */
    getPluginClean(paths) {
        const { CleanWebpackPlugin } = require('clean-webpack-plugin');
        let exclude = [];

        if (!this.options.production) {
            this.getOutputFilesExpected().forEach(file => {
                paths.forEach(directory => {
                    exclude.push('!' + path.relative(directory, file));
                });
            });
        }

        return new CleanWebpackPlugin({
            dry: false,
            verbose: true,
            cleanStaleWebpackAssets: false,
            dangerouslyAllowCleanPatternsOutsideProject: true,
            cleanOnceBeforeBuildPatterns: this.options.production ? paths : exclude,
            root: this.getPath().project
        });
    }
    /**
     * Display build status system notifications to the user.
     *
     * See {@link https://github.com/Turbo87/webpack-notifier}
     */
    getPluginNotifier() {
        const WebpackNotifierPlugin = require('webpack-notifier');
        return new WebpackNotifierPlugin({
            excludeWarnings: true,
            skipFirstNotification: true
        });
    }
    /**
     * Compress JavaScript.
     *
     * See {@link https://github.com/webpack-contrib/uglifyjs-webpack-plugin}
     */
    getPluginUglifyJs() {
        const TerserPlugin = require('terser-webpack-plugin');

        return new TerserPlugin({
            cache: true,
            parallel: true,
            sourceMap: true
        });
    }
    /*
     *  Plugin to remove angular related compilation warnings.
     */
    getContextReplacementPlugin() {
        return new webpack.ContextReplacementPlugin(
            // The (\\|\/) piece accounts for path separators in *nix and Windows
            /angular(\\|\/)core(\\|\/)@angular/,
            path.join(__dirname, './src'), // location of your src
            {}
        );
    }
    /**
     * Instructs Webpack to target a specific environment.
     *
     * See {@link https://webpack.js.org/configuration/target/}
     */
    getTarget() {
        return 'web';
    }
}
exports.default = WebpackFactory;
