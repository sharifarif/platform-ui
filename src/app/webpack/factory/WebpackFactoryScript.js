'use strict';
Object.defineProperty(exports, '__esModule', { value: true });
const webpack_1 = require('@ngtools/webpack');
const awesome_typescript_loader_1 = require('awesome-typescript-loader');
const webpack = require('webpack');
const WebpackFactory_1 = require('./WebpackFactory');
/**
 * Factory used by {@link BuildWebpack.getConfigScript}.
 *
 * Avoid calling this class directly.
 */
class WebpackFactoryScript extends WebpackFactory_1.default {
    getEntry() {
        return this.getPath().entryScript;
    }
    getOutputDirectory() {
        return this.getPath().distScript;
    }
    getPublicPath() {
        return this.options.directory.dist.publicPathJs;
    }
    getPlugins() {
        const options = this.options,
            angular = options.angular,
            path = this.getPath(),
            production = options.production;
        var plugins = [
            this.getPluginClean([path.distScript]),
            this.getPluginNotifier(),
            ...this.plugins
        ];
        if (angular) {
            plugins.push(this.getPluginAngular());
        } else {
            plugins.push(new awesome_typescript_loader_1.CheckerPlugin());
        }
        if (production) {
            plugins.push(this.getPluginUglifyJs());
            plugins.push(this.getContextReplacementPlugin());
            let ModuleConcatenation = webpack.optimize.ModuleConcatenationPlugin;
            if (ModuleConcatenation) {
                plugins.push(new ModuleConcatenation());
            }
        }
        return plugins;
    }
    getRules() {
        var options = this.options,
            angular = options.angular,
            production = options.production,
            babel = options.babel;
        var rules = [
            {
                test: angular ? /(?:\.ngfactory\.js|\.ngstyle\.js|\.ts)$/ : /\.tsx?$/,
                loader: angular ? '@ngtools/webpack' : 'awesome-typescript-loader'
            },
            {
                test: /vendor\/.+\.(jsx|js)$/,
                loader: 'imports?jQuery=jquery,$=jquery,this=>window'
            },
            this.getLoaderHtml(),
            {
                test: /\.s?css$/,
                use: this.getLoaderExtractText()
            }
        ];
        if (production) {
            rules.unshift(this.getLoaderTsLint());
        }
        if (babel) {
            rules.push(this.getLoaderBabel());
        }

        return rules;
    }
    getTarget() {
        var angular = this.options.angular,
            angularServer = angular && angular.server;
        return angularServer ? 'node' : 'web';
    }
    getLoaderBabel() {
        return {
            test: /\.m?(jsx|js)$/,
            exclude: /(node_modules|bower_components)\/(?!(dom7|swiper)\/).*/,
            use: {
                loader: 'babel-loader',
                options: {
                    presets: ['@babel/preset-env']
                }
            }
        };
    }
    getLoaderExtractText() {
        return [
            'to-string-loader',
            {
                loader: 'css-loader',
                options: {
                    sourceMap: false,
                    url: false
                }
            },
            {
                loader: 'postcss-loader',
                options: {
                    sourceMap: false,
                    plugins: [require('autoprefixer')()],
                    ctx: {
                        autoprefixer: {
                            browsers: ['last 4 version', 'ie 9', 'ie 10']
                        }
                    }
                }
            },
            {
                loader: 'sass-loader',
                options: {
                    sourceMap: true
                }
            }
        ];
    }
    getLoaderHtml() {
        var loader,
            options = this.options,
            angular = options.angular,
            angularServer = angular && angular.server,
            production = options.production;
        if (production || angularServer) {
            loader = {
                test: /\.html$/,
                loader: 'html-loader',
                options: {
                    minimize: true,
                    removeAttributeQuotes: false,
                    caseSensitive: true,
                    customAttrSurround: [
                        [/#/, /(?:)/],
                        [/\*/, /(?:)/],
                        [/\[?\(?/, /(?:)/]
                    ],
                    customAttrAssign: [/\)?\]?=/]
                }
            };
        } else {
            loader = {
                test: /\.html$/,
                loader: 'raw-loader'
            };
        }
        return loader;
    }
    getPluginAngular() {
        var options = this.options,
            angular = options.angular,
            path = this.getPath(),
            production = options.production;
        return new webpack_1.AngularCompilerPlugin({
            entryModule:
                path.angularApplicationModulePath +
                '#' +
                angular.applicationModule.className,
            mainPath: path.angularApplicationMain,
            tsConfigPath: path.angularTsConfig,
            skipCodeGeneration: !production
        });
    }
    getOptimization() {
        return this.options.optimization ? this.options.optimization.script : {};
    }
}
exports.default = WebpackFactoryScript;
