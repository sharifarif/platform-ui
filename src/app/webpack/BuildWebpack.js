"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const WebpackFactoryScript_1 = require("./factory/WebpackFactoryScript");
const WebpackFactoryStyle_1 = require("./factory/WebpackFactoryStyle");
const WebpackFactoryTest_1 = require("./factory/WebpackFactoryTest");
/**
 * Build your project with Webpack.
 *
 * Add the following to `webpack.config.js`:
 *
 * ```javascript
 * const BuildWebpack = require('platform-ui/src/app/webpack/BuildWebpack').default;
 *
 * const directory = {
 *     project: __dirname,
 *     src: {
 *         ...
 *     },
 *     dist: {
 *         ...
 *     }
 * };
 *
 * const entry = {
 *     script: {
 *         ...
 *     },
 *     style: {
 *         ...
 *     },
 *     test: {
 *         ...
 *     }
 * };
 *
 * module.exports = (webpackEnv, webpackOptions) => {
 *     return new BuildWebpack({
 *         directory: directory,
 *         entry: entry,
 *         production: webpackOptions.p
 *     }).getConfig();
 * };
 * ```
 */
class BuildWebpack {
    constructor(options, plugins) {
        this.options = options;
        this.plugins = plugins ? plugins : [];
    }
    /**
     * Get an array of Webpack
     * [Configuration](https://webpack.js.org/configuration/) objects.
     */
    getConfig() {
        var config = [], options = this.options, entry = options.entry;
        if (entry.style && Object.keys(entry.style).length) {
            config.push(this.getConfigStyle());
        }
        else {
            let invalidOptions = [], font = options.directory.src.font, image = options.directory.src.image;
            if (font && (typeof font === 'string' || Object.keys(font).length)) {
                invalidOptions.push('IBuildOptions.directory.src.font');
            }
            if (image && (typeof image === 'string' || Object.keys(image).length)) {
                invalidOptions.push('IBuildOptions.directory.src.image');
            }
            if (options.copy && Object.keys(options.copy).length) {
                invalidOptions.push('IBuildOptions.copy');
            }
            if (invalidOptions.length) {
                throw new Error(invalidOptions.join(', ') +
                    ' requires one or more IBuildOptions.entry.style.');
            }
        }
        if (entry.script && Object.keys(entry.script).length) {
            config.push(this.getConfigScript());
        }
        if (entry.test && Object.keys(entry.test).length) {
            config.push(this.getConfigTest());
        }
        return config;
    }
    /**
     * Get a Webpack [Configuration](https://webpack.js.org/configuration/) object
     * for the TypeScript and JavaScript code.
     */
    getConfigScript() {
        return new WebpackFactoryScript_1.default(this.options, this.plugins).getConfig();
    }
    /**
     * Get a Webpack [Configuration](https://webpack.js.org/configuration/) object
     * for the Sass and CSS code.
     */
    getConfigStyle() {
        return new WebpackFactoryStyle_1.default(this.options, this.plugins).getConfig();
    }
    /**
     * Get a Webpack [Configuration](https://webpack.js.org/configuration/) object
     * for the Jasmine tests.
     */
    getConfigTest() {
        return new WebpackFactoryTest_1.default(this.options, this.plugins).getConfig();
    }
}
exports.default = BuildWebpack;
