/**
 * Directories where output files will be located.
 */
export interface IBuildDirectoryDist {
    /**
     * Base directory for output files, relative to {@link IBuildDirectory.project}.
     */
    asset: string;

    /**
     * Directory for output fonts, relative to {@link asset}.
     */
    font?: string;

    /**
     * Directory for output images, relative to {@link asset}.
     */
    image?: string;

    /**
     * Directory for output scripts, relative to {@link asset}.
     */
    script?: string;

    /**
     * Directory for output stylesheets, relative to {@link asset}.
     */
    style?: string;

    /**
     * Directory for output tests, relative to {@link asset}.
     */
    test?: string;
}
