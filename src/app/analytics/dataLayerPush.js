"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Pass data into Google Tag Manager.
 *
 * @param data - Data to pass into Google Tag Manager.
 * @see https://developers.google.com/tag-manager/devguide
 */
function dataLayerPush(data) {
    var dataLayer = window.dataLayer;
    if (!dataLayer) {
        dataLayer = [];
        window.dataLayer = dataLayer;
    }
    dataLayer.push(data);
}
exports.default = dataLayerPush;
