import { IDataLayerData } from './IDataLayerData';

/**
 * A `window` containing `window.dataLayer`.
 *
 * @see https://developers.google.com/tag-manager/devguide
 */
export interface IDataLayerWindow extends Window {
    /**
     * Data passed into Google Tag Manager.
     */
    dataLayer?: IDataLayerData[];
}
