/**
 * Data passed into Google Tag Manager.
 *
 * @see https://developers.google.com/tag-manager/devguide
 */
export interface IDataLayerData {
    [key: string]: string | number;
}
