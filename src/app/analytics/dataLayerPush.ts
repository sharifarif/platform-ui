import { IDataLayerData } from './IDataLayerData';
import { IDataLayerWindow } from './IDataLayerWindow';

/**
 * Pass data into Google Tag Manager.
 *
 * @param data - Data to pass into Google Tag Manager.
 * @see https://developers.google.com/tag-manager/devguide
 */
export default function dataLayerPush(data: IDataLayerData) {
    var dataLayer = (window as IDataLayerWindow).dataLayer;
    if (!dataLayer) {
        dataLayer = [];
        (window as IDataLayerWindow).dataLayer = dataLayer;
    }

    dataLayer.push(data);
}
