'use strict';

(function() {
    var KeyCode = {
        Enter: 13,
        Space: 32
    };

    /**
     * Get the `data-layer-*` attributes on an `element`.
     *
     * @param {HTMLElement} element - Element to get the `data-layer-*` attributes from.
     * @returns {Object}
     */
    function getDataLayerAttributes(element) {
        var dataLayerAttributes = {},
            dataset = element.dataset,
            attributes = element.attributes,
            dataLayerKey;

        // Use HTMLElement.dataset when supported, since jQuery may not update attributes.
        if (dataset) {
            var dataPrefix = 'layer';

            for (var key in dataset) {
                if (key.length > dataPrefix.length &&
                    key.substring(0, dataPrefix.length) === dataPrefix) {

                    dataLayerKey = key.substring(dataPrefix.length);

                    // Convert dataLayerKey from camelCase to kebab-case.
                    dataLayerKey = dataLayerKey.substring(0, 1)
                        + dataLayerKey.substring(1).replace(/([A-Z])/g, '-$1');
                    dataLayerKey = dataLayerKey.toLowerCase();

                    dataLayerAttributes[dataLayerKey] = dataset[key];
                }
            }

        // IE 6-10 has Element.attributes, but not HTMLElement.dataset.
        } else if (attributes) {
            for (var index = 0, length = attributes.length; index < length; index++) {
                var attribute = attributes[index],
                    attributePrefix = 'data-layer-',
                    name = attribute.name.toLowerCase();

                if (name.length > attributePrefix.length &&
                    name.substring(0, attributePrefix.length) === attributePrefix) {

                    // dataLayerKey is already in kebab-case.
                    dataLayerKey = name.substring(attributePrefix.length);

                    dataLayerAttributes[dataLayerKey] = attribute.value;
                }
            }
        }

        return dataLayerAttributes;
    }

    /**
     * Return `true` if an `element` has the `data-layer` attribute.
     *
     * @param {HTMLElement} element - Element to check.
     * @returns {boolean}
     */
    function hasDataLayerAttribute(element) {
        var hasDataLayer = false,
            dataset = element.dataset,
            attributes = element.attributes;

        // Use HTMLElement.dataset when supported, since jQuery may not update attributes.
        if (dataset) {
            if (dataset.hasOwnProperty('layer')) {
                hasDataLayer = true;
            }

        // IE 6-10 has Element.attributes, but not HTMLElement.dataset.
        } else if (attributes) {
            for (var index = 0, length = attributes.length; index < length; index++) {
                var attribute = attributes[index],
                    name = attribute.name.toLowerCase();

                if (name === 'data-layer') {
                    hasDataLayer = true;
                }
            }
        }

        return hasDataLayer;
    }

    /**
     * Callback fired when the user presses down on a key somewhere in the page.
     *
     * @param {KeyboardEvent} event - Event fired.
     */
    function onKeyDown(event) {

        // IE 8 and lower use KeyboardEvent.which.
        // IE 9 and later use KeyboardEvent.keyCode.
        var eventKeyCode = event.keyCode || event.which;

        // Modern browsers use KeyboardEvent.code.
        if (event.code === 'Enter' || eventKeyCode === KeyCode.Enter) {
            onMouseDownOrEnter(event);
        } else {

            // IE 8 and lower use Event.srcElement instead of Event.target.
            var element = event.target || event.srcElement,
                tagName = element.tagName.toUpperCase();

            // On buttons, checkboxes, and radio buttons, a space acts the same as enter.
            if ((tagName === 'INPUT' || tagName === 'BUTTON') &&
                (event.code === 'Space' || eventKeyCode === KeyCode.Space)) {

                onMouseDownOrEnter(event);
            }
        }
    }

    /**
     * Callback fired when the user presses down on the mouse or the enter key
     * somewhere in the page.
     *
     * @param {KeyboardEvent|MouseEvent} event - Event fired.
     */
    function onMouseDownOrEnter(event) {

        // IE 8 and lower use Event.srcElement instead of Event.target.
        var element = event.target || event.srcElement;

        for (; element; element = element.parentElement) {
            var tagName = element.tagName.toUpperCase();

            if (hasDataLayerAttribute(element) && tagName !== 'FORM') {
                pushAttributesToDataLayer(element);
            }
        }
    }

    /**
     * Callback fired when the user submits a `<form>` somewhere in the page.
     *
     * @param {Event} event - Event fired.
     */
    function onSubmit(event) {

        // IE 8 and lower use Event.srcElement instead of Event.target.
        var element = event.target || event.srcElement;

        if (hasDataLayerAttribute(element)) {
            pushAttributesToDataLayer(element);
        }
    }

    /**
     * Push the `data-layer-*` attributes on an `element` to the Data Layer.
     *
     * @param {HTMLElement} element -
     *     Element with `data-layer-*` attributes to push to the Data Layer.
     */
    function pushAttributesToDataLayer(element) {
        var dataLayer = window.dataLayer;
        if (!dataLayer) {
            dataLayer = [];
            window.dataLayer = dataLayer;
        }

        var attributes = getDataLayerAttributes(element);

        dataLayer.push(attributes);
    }

    if (document.addEventListener) {
        document.addEventListener('mousedown', onMouseDownOrEnter);
        document.addEventListener('keydown', onKeyDown);
        document.addEventListener('submit', onSubmit);

    // IE 6-8 use EventTarget.attachEvent().
    } else if (document.attachEvent) {
        document.attachEvent('onmousedown', onMouseDownOrEnter);
        document.attachEvent('onkeydown', onKeyDown);

        // In IE 8 and lower, submit events do not bubble up to the DOM.
        var forms = document.getElementsByTagName('form');

        for (var formIndex = 0; formIndex < forms.length; formIndex++) {
            forms[formIndex].attachEvent('onsubmit', onSubmit);
        }
    }
})();