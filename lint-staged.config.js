module.exports = {
    '*.{sass,scss}': ['prettier --print-width 90 --tab-width 4 --write', 'git add'],
    '*.ts': [
        'prettier --print-width 90 --tab-width 4 --write --single-quote',
        'tslint',
        'git add'
    ],
    '*.js': ['prettier --print-width 90 --tab-width 4 --write --single-quote', 'git add']
};
