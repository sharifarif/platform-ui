const BuildWebpack = require('./src/app/webpack/BuildWebpack').default;

const directory = {
    project: __dirname,
    src: {
        src: 'src',

        test: 'app/spec'
    },
    dist: {
        asset: 'dist/assets',

        test: '../../test'
    }
};

const entry = {
    test: {
        Specs: ['../angular/spec/Specs', 'Specs']
    }
};

module.exports = (webpackEnv, webpackOptions) => {
    return new BuildWebpack({
        directory: directory,
        entry: entry,
        production: webpackOptions.p
    }).getConfig();
};
